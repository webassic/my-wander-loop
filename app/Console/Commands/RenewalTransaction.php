<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Razorpay\Api\Api;
 use Razorpay\Api\Errors\BadRequestError;
use App\Repositories\RazorpayCredentialsRepository;
use App\Models\BanduserPaymentTransactions;
use App\Models\BanduserSubscriptionTransactions;
use DB;
use App\Models\RazorpayCredentials;

class RenewalTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'renewalTransactions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch All Transactions from Razorpay and based on payment id check entry exists or not , if not then create new entry...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        RazorpayCredentialsRepository $razorpayCredentialsRepository,
        BanduserPaymentTransactions $banduserPaymentTransactionsModel,
        BanduserSubscriptionTransactions $banduserSubscriptionTransactionsModel,
        RazorpayCredentials $razorpayCredentials
    ) {
        parent::__construct();
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
        $this->banduserPaymentTransactionsModel = $banduserPaymentTransactionsModel;
        $this->banduserSubscriptionTransactionsModel = $banduserSubscriptionTransactionsModel;
        $this->razorpayCredentials = $razorpayCredentials;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // Get credentials
            $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
            $api = new Api($credentials['key_id'], $credentials['key_secret']);

            $count = 100;
            $skip = 0;
            for ($skip = 0; $skip <= 1000; $skip++) {
                // This is done - for pagination and count 
                if ($skip == 0) {
                    $options = ['count' => $count,'skip' => $skip];
                } else {
                    $options = ['count' => $count,'skip' => $skip*$count];
                }

                // fetch all transactions list
                $payment  = $api->payment->all($options);
                if (count($payment->items) > 0) {
                    foreach ($payment->items as $razorPaykey => $razorPayVal) {
                        DB::beginTransaction();
                        echo "Payment Id >> ".$razorPayVal->id;
                        try {
                            // check record is already exists in db based on paymet id
                            $bandUserPaymentTransactions = $this->checkBandUserPaymentTransactionDetailsPresentInDbOrNot($razorPayVal);
                            echo 'Band User Payment Transaction Details From DB based on Payment Id >> '.json_encode($bandUserPaymentTransactions).PHP_EOL;
                    
                            if ($bandUserPaymentTransactions == null) {
                                // Invoice Details
                                echo 'Fetch Invoice Details From Razorpay '.PHP_EOL;
                                $invoiceDetails = $this->fetchRazorpayInvoiceDetails($razorPayVal->invoice_id, $api);

                                // generate invoice number 
                                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                                $invoiceNumber = '';
                                if (isset($credentials['last_invoice_number']) && !empty($credentials['last_invoice_number'])) {
                                    $prefix = substr($credentials['last_invoice_number'], 0, 4);
                                    $lastInvoiceNo = substr($credentials['last_invoice_number'], 4);
                                    $addInvoiceNumber = $lastInvoiceNo + 1;
                                    $invoiceNumber = $prefix . $addInvoiceNumber;
                                }
                                // from invoice details check invoice number , subscription id and payment id present or not , if one missing then throw exception
                                if (
                                    $invoiceDetails->payment_id == null ||
                                    $invoiceDetails->subscription_id == null) {
                                    echo "Invoice Details Missing..."." Payment Id >> ".$invoiceDetails->payment_id." Subscription Id >> ".$invoiceDetails->subscription_id.PHP_EOL;
                                } else {
                                    echo "Invoice Details Present...."." Payment Id >> ".$invoiceDetails->payment_id." Subscription Id >> ".$invoiceDetails->subscription_id.PHP_EOL;
                                    // get subscription id
                                    echo 'Fetch Subscription Details From Razorpay '.PHP_EOL;
                                    $subScriptionId = isset($invoiceDetails['subscription_id']) && !empty($invoiceDetails['subscription_id']) ? $invoiceDetails['subscription_id'] : null;
                                    echo 'Subscription Id >>  '.$subScriptionId.PHP_EOL;
    
                                    $subscriptionDetails  = $api->subscription->fetch($subScriptionId);
                                    
                                    // create band user payment transaction details
                                    // check for subscription id , payment present or not
                                    $this->checkPaymentTransDetailsPresentOrNotIfNotCreateTransaction($invoiceDetails,$subscriptionDetails,$invoiceNumber);

                                    // fetch subscription details
                                    // based on subscription id - get start date and end date and update on subscription table
                                    $this->fetchSubscriptionDetailsFromRazorpayAndUpdateStartAndEndDates($subscriptionDetails);

                                    echo '****************************************************************************************************'.PHP_EOL;
                                }
                            } else {
                                echo 'Band User Transaction Details are already present for this payment id >>  '.$razorPayVal->id.PHP_EOL;
                              
                                // check start date and end date is empty if empty then only update
                                if ($bandUserPaymentTransactions->start_date == null || 
                                    $bandUserPaymentTransactions->end_date == null ) {
                                    // update band subscription_start_date and subscription_end_date in band user payment transactions table
                                    // Invoice Details
                                    echo 'Fetch Invoice Details From Razorpay In Else Block '.PHP_EOL;
                                    $existingRecordsInvoiceDetails = $this->fetchRazorpayInvoiceDetails($razorPayVal->invoice_id, $api);
                                    echo 'Invoice Details In Else Block >> '.json_encode($existingRecordsInvoiceDetails).PHP_EOL;
    
                                    // start date
                                    $subscriptionStartDateTimestamp = strtotime(gmdate('r', $existingRecordsInvoiceDetails->billing_start));
                                    $convertedSubscriptionStartdate = date('Y-m-d H:i:s', $subscriptionStartDateTimestamp);
                                
                                    // end date
                                    $subscriptionEndDateTimestamp = strtotime(gmdate('r', $existingRecordsInvoiceDetails->billing_end));
                                    $convertedSubscriptionEnddate = date('Y-m-d H:i:s', $subscriptionEndDateTimestamp);
                                    
                                    if (!empty($convertedSubscriptionStartdate) || !empty($convertedSubscriptionEnddate)) {
                                        // update subscription dates in banduser payment transactions table
                                        
                                        \App\Models\BanduserPaymentTransactions::whereIn('id', [$bandUserPaymentTransactions->id])
                                                                                ->where(array('start_date' => null,'end_date' => null))
                                                                                ->update(array(
                                                                                    'start_date' => $convertedSubscriptionStartdate,
                                                                                    'end_date' => $convertedSubscriptionEnddate
                                                                                ));
                                    }
                                } else{
                                    echo 'Invoice Details Start Date , End date is null so not updated';
                                }
                            }
                            DB::commit();
                        } catch (\Exception $ex) {
                            DB::rollback();
                            \Log::error('Exception >> ' . $ex->getLine());
                            \Log::error('Exception >> ' . $ex->getMessage());
                        }
                    }
                } else if(count($payment->items) == 0){
                    echo 'Payment Item Details Not Found , Done Successfully'.PHP_EOL;
                    return true;
                }
            }
            echo 'Done';exit;
            
        } catch (\Exception $ex) {
            DB::rollback();
            \Log::error('Exception In Main Catch Block >> '.$ex->getTraceAsString());
            throw new \Exception($ex->getMessage(), 500);
        }
    }

    private function checkBandUserPaymentTransactionDetailsPresentInDbOrNot($razorPayVal)
    {
        try {
            $bandUserPaymentTransactions = null;
            if (isset($razorPayVal->id) && !empty($razorPayVal->id)) {
                $bandUserPaymentTransactions = $this->banduserPaymentTransactionsModel
                        ->where(array('payment_id' => $razorPayVal->id))
                        ->first();
            } else {
                echo 'Payment ID is empty'.PHP_EOL;
            }
            
            return $bandUserPaymentTransactions;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 500);
        }
    }

    public function fetchSubscriptionDetailsFromRazorpayAndUpdateStartAndEndDates($subscriptionDetails)
    {
        try {
            $bandUserSubscriptionTransDetails = [];
            if(isset($subscriptionDetails) && !empty($subscriptionDetails)){
                echo 'Subscription Details From Razorpay >>  '.json_encode($subscriptionDetails).PHP_EOL;

                // based on this subscription id
                // fetch existing banduser_subscription_transactions details
                $bandUserSubscriptionTransDetails = $this->banduserSubscriptionTransactionsModel
                                                    ->where(array(
                                                        'subscription_id' => $subscriptionDetails->id
                                                    ))
                                                    ->whereNotIn('status', ['cancel'])
                                                    ->get()
                                                    ->toArray();
    
            }

            echo 'Band user Subscription Details >>  '.json_encode($bandUserSubscriptionTransDetails).PHP_EOL;

            // update band user subscription transaction details
            if (isset($bandUserSubscriptionTransDetails) && !empty($bandUserSubscriptionTransDetails)) {
                // start date
                $startDateTimestamp = strtotime(gmdate('r', $subscriptionDetails->current_start));
                $convertedStartdate = date('Y-m-d H:i:s', $startDateTimestamp);
            
                // end date
                $endDateTimestamp = strtotime(gmdate('r', $subscriptionDetails->current_end));
                $convertedEnddate = date('Y-m-d H:i:s', $endDateTimestamp);

                foreach ($bandUserSubscriptionTransDetails as $k => $v) {
                    // update dates for specific subscription id
                    $updateBanduserSubData = array(
                        'start_date' => $convertedStartdate,
                        'end_date' => $convertedEnddate
                    );
                    \App\Models\BanduserSubscriptionTransactions::
                        where(array('id' => $v['id'],
                                    'subscription_id' => $subscriptionDetails->id,
                                    'banduser_id' => $v['banduser_id']
                                ))
                        ->update($updateBanduserSubData);

                    echo 'Band User Subscription Transaction Record updated successfully....'.PHP_EOL;
                }
            } else {
                echo 'Band User Subscription Transaction Details noy found based on subscription id....'.PHP_EOL;
                DB::rollback();
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 500);
        }
    }

    private function checkPaymentTransDetailsPresentOrNotIfNotCreateTransaction($invoiceDetails,$subscriptionDetails,$invoiceNumber)
    {
        try {
            $existingBandUserPaymentTransDetailsCount = 0;
            if(isset($subscriptionDetails) && !empty($subscriptionDetails)){
                // create band user payment transaction details
                // check for subscription id , payment present or not
                $existingBandUserPaymentTransDetailsCount = $this->banduserPaymentTransactionsModel
                        ->where(array('subscription_id' => $subscriptionDetails->id,
                        'payment_id' => $invoiceDetails->payment_id))
                        ->get()
                        ->count();
            }

            echo 'Existing Band User Payment Trans Details count >> '.$existingBandUserPaymentTransDetailsCount.PHP_EOL;
            if ($existingBandUserPaymentTransDetailsCount == 0) {// create new payment transaction details for above subscription id
                // prepare details for new payment transaction details
                // payment date
                $paymentDateTimestamp = strtotime(gmdate('r', $invoiceDetails->paid_at));
                $convertedPaymentDateTimestamp = date('Y-m-d H:i:s', $paymentDateTimestamp);
                                    
                // start date
                $startDateTimestamp = strtotime(gmdate('r', $subscriptionDetails->current_start));
                $convertedStartdate = date('Y-m-d H:i:s', $startDateTimestamp);
            
                // end date
                $endDateTimestamp = strtotime(gmdate('r', $subscriptionDetails->current_end));
                $convertedEnddate = date('Y-m-d H:i:s', $endDateTimestamp);
                
                $bandUserPaymentTransactionDetails = array(
                                        'subscription_id' => $subscriptionDetails->id,
                                        'invoice_number' => $invoiceNumber,
                                        'payment_id' => $invoiceDetails->payment_id,
                                        'payment_date' => $convertedPaymentDateTimestamp,
                                        'status' => 'captured',
                                        'signature' => null, // TODO Item
                                        'created_at' => $convertedPaymentDateTimestamp,
                                        'updated_at' => date('Y-m-d H:i:s'),
                                        'is_renewal_payment' => 'Yes',
                                        'start_date' => $convertedStartdate,
                                        'end_date' => $convertedEnddate 
                                    );
                
                echo 'Band User Payment Record Created Array >> '.json_encode($bandUserPaymentTransactionDetails).PHP_EOL;
                // based on payment_id,subscription_id,payment_date  - check record is present or not 
                $existingCount = 0;
                $existingCount = \App\Models\BanduserPaymentTransactions::select('*')->where(array(
                    'payment_id' => $invoiceDetails->payment_id,
                    'subscription_id' => $subscriptionDetails->id,
                    'payment_date' => date('Y-m-d',strtotime($convertedPaymentDateTimestamp))
                ))->count();

                if($existingCount == 0){
                    // create new entry into banduser payment transactions
                    \App\Models\BanduserPaymentTransactions::create($bandUserPaymentTransactionDetails);
                    echo 'Band User Payment Record Created Successfully....'.PHP_EOL;

                    // update - last invoice number -
                    $response = $this->razorpayCredentials
                        ->first()->toArray();

                    if (\Config::get('app.razorpay_mode_flag') == 0) {
                         \App\Models\RazorpayCredentials::where('id', '=', $response['id'])
                                ->update(array('test_last_invoice_number' => $invoiceNumber));
                    } else {
                         \App\Models\RazorpayCredentials::where('id', '=', $response['id'])
                                ->update(array('live_last_invoice_number' => $invoiceNumber));
                    }
                } 
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 500);
        }
    }

    private function fetchRazorpayInvoiceDetails($invoiceId, $api)
    {
        try {
            $invoiceDetails = [];
            if (isset($invoiceId) && !empty($invoiceId)) {
                echo 'Invoice Id >> '.$invoiceId.PHP_EOL;
                // invoice details
                $invoiceDetails  = $api->invoice->fetch($invoiceId);
            } else {
                echo 'Invoice Id Not Present in Razorpay >> '.$invoiceId.PHP_EOL;
            }
            return $invoiceDetails;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage(), 500);
        }
    }
}
