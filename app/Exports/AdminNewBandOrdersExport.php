<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AdminNewBandOrdersExport implements FromArray,WithHeadings
{
    protected $bandusers;
    protected $heading;

    public function __construct(array $bandusers, array $heading) {
        $this->bandusers = $bandusers;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->bandusers;
    }

    public function headings(): array {
        return $this->heading;
    }
}
