<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AdminUserExport implements FromArray,WithHeadings
{
   protected $adminusers;
    protected $heading;

    public function __construct(array $adminusers, array $heading) {
        $this->adminusers = $adminusers;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->adminusers;
    }

    public function headings(): array {
        return $this->heading;
    }
}
