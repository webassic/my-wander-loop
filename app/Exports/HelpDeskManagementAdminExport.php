<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HelpDeskManagementAdminExport implements FromArray,WithHeadings
{
    protected $helpdesk;
    protected $heading;

    public function __construct(array $helpdesk, array $heading) {
        $this->helpdesk = $helpdesk;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->helpdesk;
    }

    public function headings(): array {
        return $this->heading;
    }
}
