<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InactiveCustomerExport implements FromArray, WithHeadings
{
    protected $customers;
    protected $heading;

    public function __construct(array $customers, array $heading) {
        $this->customers = $customers;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->customers;
    }

    public function headings(): array {
        return $this->heading;
    }
}
