<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadsExport implements FromArray, WithHeadings{
    protected $leads;
    protected $heading;

    public function __construct(array $leads, array $heading) {
        $this->leads = $leads;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->leads;
    }

    public function headings(): array {
        return $this->heading;
    }
}
