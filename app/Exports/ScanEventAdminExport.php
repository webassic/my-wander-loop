<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ScanEventAdminExport implements FromArray,WithHeadings
{
    protected $scanevent;
    protected $heading;

    public function __construct(array $scanevent, array $heading) {
        $this->scanevent = $scanevent;
        $this->heading = $heading;
    }

    public function array(): array {
        return $this->scanevent;
    }

    public function headings(): array {
        return $this->heading;
    }
}
