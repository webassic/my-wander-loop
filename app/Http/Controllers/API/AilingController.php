<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use App\Repositories\AilingRepository;
use DB;

class AilingController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $ailingsRepository;

    public function __construct(AilingRepository $ailingsRepository) {
        $this->ailingsRepository = $ailingsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            return view('ailings.index');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $content = $request->getContent();
            if (is_string($request->getContent())) {
                $content = json_decode($request->getContent(), true);
            }
            $validator = Validator::make($content, [
                        'name' => 'required|unique:ailings',
                        'status' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $return = $this->ailingsRepository->storeAiling($content);
            DB::commit();
            return $this->sendResponse($return, 'Ailing has been created successfully.');
        } catch (\Expectional $ex) {
            DB::rollback();
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $return = $this->ailingsRepository->showAiling($id);
            if (is_null($return)) {
                return $this->sendError('Ailing not found.');
            }
            return $this->sendResponse($return, 'Common illness retrieved successfully.');
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $content = $request->getContent();
            if (is_string($request->getContent())) {
                $content = json_decode($request->getContent(), true);
            }
            $validator = Validator::make($content, [
                        'name' => 'required',
                        'status' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->ailingsRepository->updateAiling($content, $id);
            DB::commit();
            if (is_null($return)) {
                return $this->sendError('Common illness not update.');
            }
            return $this->sendResponse($return, 'Common illness updated successfully.');
        } catch (\Expectional $ex) {
            DB::rollback();
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $return = $this->ailingsRepository->destroyAiling($id);

            return $this->sendResponse([], 'Common illness deleted successfully.');
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][1]['search']['value']) ? $_GET['columns'][1]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $response = $this->ailingsRepository->getAllAilingDetails($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
