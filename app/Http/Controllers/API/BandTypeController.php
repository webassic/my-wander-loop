<?php

namespace App\Http\Controllers\API;

use App\Repositories\BandTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class BandTypeController extends RestController {

    protected $bandTypeRepository;

    /**
     * 
     * @param BandTypeRepository $bandTypeRepository
     */
    public function __construct(BandTypeRepository $bandTypeRepository) {
        $this->bandTypeRepository = $bandTypeRepository;
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function index() {
        try {
            return view('bandtypes.index');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function store(Request $request) {
        try {
           $postData = $request->all();
            $validator = Validator::make($postData, [
                        'band_name' => 'required|unique:band_types',
                        'color' => 'required',
                        'description' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $postData['created_by'] = Auth::user()->id;

            $return = $this->bandTypeRepository->storeBandType($postData);
            return $this->sendResponse($return, 'Band Type has been created successfully.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $return = $this->bandTypeRepository->showBandType($id);
            if (is_null($return)) {
                return $this->sendError('Band Type not found.');
            }
            return $this->sendResponse($return, 'Band Type has been fetched successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $postData = $request->all();
             $validator = Validator::make($postData, [
                        'band_name' => [
                            'required',
                            Rule::unique('band_types')->ignore($id),
                        ],
                        'color' => 'required',
                        'description' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $postData['modified_by']=Auth::user()->id;
            $return = $this->bandTypeRepository->updateBandType($postData, $id);
            if (is_null($return)) {
                return $this->sendError('Band Type not update.');
            }
            return $this->sendResponse($return, 'Band Type details has been updated successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try{
            $return = $this->bandTypeRepository->destroyBandType($id);
            return $this->sendResponse([], 'Band Type has been deleted successfully.');
        }catch(\Exception $ex){
           return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][1]['search']['value']) ? $_GET['columns'][1]['search']['value'] : '';
             if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $response = $this->bandTypeRepository->getAllBandTypes($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
