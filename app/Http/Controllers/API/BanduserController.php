<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Event;
use App\Events\BandUserApprovalMailEvent;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Banduser;
use App\Repositories\BanduserRepository;
use App\Repositories\AilingRepository;
use Illuminate\Validation\Rule;
use App\Repositories\GuardiansRepository;
use DB;
use ZipArchive;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Mail;
use App\Repositories\RazorpayCredentialsRepository;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\BadRequestError;
use App\Exports\ActiveBanduserExport;
use App\Exports\InactiveBanduserExport;
use App\Exports\BanduserExport;
use App\Exports\UsersBanduserExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Events\SendOtpEvent;
use App\Models\RazorpayErrorsLog;

class BanduserController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $banduser;
    private $banduserRepository;
    private $ailingRepository;
    private $razorpayCredentialsRepository;

    public function __construct(Banduser $banduser, BanduserRepository $banduserRepository, AilingRepository $ailingRepository, GuardiansRepository $guardiansRepository, RazorpayCredentialsRepository $razorpayCredentialsRepository) {
        $this->banduser = $banduser;
        $this->banduserRepository = $banduserRepository;
        $this->ailingRepository = $ailingRepository;
        $this->guardiansRepository = $guardiansRepository;
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $guardianId = 0;
            $getGuardianId = $this->guardiansRepository->showGuardian(Auth::user()->id);
            try {
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);
                $planDetails = $api->plan->fetch($credentials['plan_id']);
                
                $packageList = [];
                $packageList[$credentials['basic_plan_id']] = ucfirst(env('RAZORPAY_BASIC_PACKAGE'));
                $packageList[$credentials['plan_id']] = ucfirst(env('RAZORPAY_STANDARD_PACKAGE'));
                
                $planAmount = $planDetails['item']->amount;
                // Check guardian info is present or not.
                if (isset($getGuardianId) && !empty($getGuardianId)) {
                    if ($getGuardianId['guardian_type'] == "Individual" && $getGuardianId['title'] != "" && $getGuardianId['firstname'] != "" && $getGuardianId['lastname'] != "" && $getGuardianId['dob'] != "" &&
                            $getGuardianId['address'] != "" && $getGuardianId['locality'] != "" && $getGuardianId['city'] != "" && $getGuardianId['state'] != "" && $getGuardianId['pin'] != "") {
                        $guardianId = $getGuardianId['id'];
                    } elseif ($getGuardianId['guardian_type'] == "Institution" && $getGuardianId['institution_name'] != "" && $getGuardianId['mobile'] != "" && $getGuardianId['address'] != "" && $getGuardianId['locality'] != "" &&
                            $getGuardianId['city'] != "" && $getGuardianId['state'] != "" && $getGuardianId['pin'] != "") {
                        $guardianId = $getGuardianId['id'];
                    }
                }
                $error['rzerror'] = '';
                return view('bandusers.index', compact('guardianId', 'credentials', 'planAmount', 'error','packageList'));
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'BanduserController/index';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $error['rzerror'] = 'Opps! Plan is not a valid.';
                return view('bandusers.index', compact('guardianId', 'credentials', 'error'));
            }
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $postData = $request->all();
            $postData['created_by'] = Auth::user()->id;
            $messages = [
                'dob.required' => 'This field is required',
            ];
            if (isset($postData) && !empty($postData)) {
                if (!empty($postData['date']) && !empty($postData['month']) && !empty($postData['year'])) {
                    if ($postData['month'] == 'Feb' && $postData['date'] > 29) {
                        $messages = [
                            'dob.required' => 'Date cannot greater than 29',
                        ];
                    } else {
                        $date = $postData['date'] . '-' . $postData['month'] . '-' . $postData['year'];
                        $postData['dob'] = !empty($date) ? date('Y-m-d', strtotime($date)) : '';
                        unset($postData['date']);
                        unset($postData['month']);
                        unset($postData['year']);
                    }
                } else {
                    $postData['dob'] = '';
                }
            }
            if($postData['package_name'] == 'standard'){
                
            $validator = Validator::make($postData, [
                        'created_by' => 'required',
                        'title' => 'required|in:' . implode(',', $this->banduser->titleArray),
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required|integer',
                        'firstname' => 'required|max:255',
                        'lastname' => 'required|max:255',
                        'email' => 'required|email',
                        'blood_group' => 'required|in:' . implode(',', $this->banduser->bloodGroupArray),
                        'relationship_with_applicant' => 'required',
//                        'aadhaar_card_number' => 'required|digits:4',
                        'share_code' => 'required|digits:4',
                            ], $messages);              
            }else{
            $validator = Validator::make($postData, [
                        'created_by' => 'required',
                        'title' => 'required|in:' . implode(',', $this->banduser->titleArray),
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required|integer',
                        'firstname' => 'required|max:255',
                        'lastname' => 'required|max:255',
                        'email' => 'required|email',
                        'blood_group' => 'required|in:' . implode(',', $this->banduser->bloodGroupArray),
                        'relationship_with_applicant' => 'required',
                            ], $messages);
            }
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->banduserRepository->storeBanduser($postData);
            DB::commit();
            return $this->sendResponse($return, 'Band User has been created successfully.');
        } catch (\Expectional $exc) {
            DB::rollback();
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $response = $this->banduserRepository->showBanduser($id);
            if (is_null($response)) {
                return $this->sendError('Banduser not found.');
            }
            //return view for admin
            return view('bandusers.view')->with('data', $response);
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            $paramsData = $request->all();

            unset($paramsData['_token']);
            DB::beginTransaction();
            if (($paramsData['status'] != "active") && ($paramsData['status'] != "pending")) {
                $messages = [
                    'dob.required' => 'This field is required',
                ];
                if (isset($paramsData) && !empty($paramsData)) {
                    if (!empty($paramsData['date']) && !empty($paramsData['month']) && !empty($paramsData['year'])) {
                        if ($paramsData['month'] == 'Feb' && $paramsData['date'] > 29) {
                            $messages = [
                                'dob.required' => 'Date cannot greater than 29',
                            ];
                        } else {
                            $date = $paramsData['date'] . '-' . $paramsData['month'] . '-' . $paramsData['year'];
                            $paramsData['dob'] = !empty($date) ? date('Y-m-d', strtotime($date)) : '';
                            unset($paramsData['date']);
                            unset($paramsData['month']);
                            unset($paramsData['year']);
                        }
                    } else {
                        $paramsData['dob'] = '';
                    }
                }
                $validator = Validator::make($paramsData, $messages);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors());
                }
            }
            $return = $this->banduserRepository->updateBanduser($paramsData);
            DB::commit();

            if (is_null($return)) {
                return $this->sendError('Banduser not update.');
            }
            return $this->sendResponse($return, 'Band User has been updated successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $return = $this->banduserRepository->destroyBanduser($id);
            return $this->sendResponse([], 'Banduser has been deleted successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function datatableListings(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->bandusersListing($postData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return json_encode($datatable);
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function bandUsersListingsBasedOnGuardiansId(Request $request, $guardiansId) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->bandUsersListingsBasedOnGuardiansId($postData, $guardiansId);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return json_encode($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function add($bandUserCnt) {
        try {
            try {
                $bandUserCnt = base64_decode($bandUserCnt);
                $packageName = base64_decode($_GET['p']);                 
                if (is_numeric($bandUserCnt)) {
                    // Check guardian info is present or not.
                    $getGuardianId = $this->guardiansRepository->showGuardian(Auth::user()->id);
                    if (isset($getGuardianId) && !empty($getGuardianId)) {
                        if ($getGuardianId['guardian_type'] == "Individual" && $getGuardianId['title'] != "" && $getGuardianId['firstname'] != "" && $getGuardianId['lastname'] != "" && $getGuardianId['dob'] != "" &&
                                $getGuardianId['address'] != "" && $getGuardianId['locality'] != "" && $getGuardianId['city'] != "" && $getGuardianId['state'] != "" && $getGuardianId['pin'] != "") {
                            $guardianId = $getGuardianId['id'];
                        } elseif ($getGuardianId['guardian_type'] == "Institution" && $getGuardianId['institution_name'] != "" && $getGuardianId['mobile'] != "" && $getGuardianId['address'] != "" && $getGuardianId['locality'] != "" &&
                                $getGuardianId['city'] != "" && $getGuardianId['state'] != "" && $getGuardianId['pin'] != "") {
                            $guardianId = $getGuardianId['id'];
                        } else {
                            return redirect('bandusers');
                        }
                    }
                    $titles = $this->banduser->titleArray;
                    $relationShipWithGuardian = $this->banduser->relationShipWithGuardian;
                    $bloodGroupArray = $this->banduser->bloodGroupArray;
                    $getAllAilingsList = [];
                    $getAilingsList = $this->ailingRepository->getAilingsList();
                    $datearray = $this->getDateArrayForBandUser();
                    if (isset($getAilingsList) && !empty($getAilingsList)) {
                        foreach ($getAilingsList as $value) {
                            $getAllAilingsList[$value['id']] = $value['name'];
                        }
                    }
                    $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                    $api = new Api($credentials['key_id'], $credentials['key_secret']);
                    $planDetails = $api->plan->fetch($credentials['plan_id']);
                    $planAmount = $planDetails['item']->amount;
                    $error['rzerror'] = '';
                    return view('bandusers.add', compact('titles', 'relationShipWithGuardian', 'bloodGroupArray', 'bandUserCnt', 'getAllAilingsList', 'datearray', 'credentials', 'planAmount', 'error','packageName'));
                } else {
                    return redirect('bandusers');
                }
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'BanduserController/add';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $error['rzerror'] = 'Opps! Plan is not a valid.';
                return redirect('bandusers');
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function bandUsersListingsForApproval(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->bandUsersListingsForApproval($postData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;

            return json_encode($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     */
    public function bandUsersApproval() {
        try {
            return view('bandusers.bandusers_approval');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approveBandUserByAdmin(Request $request, $id) {
        try {
            $paramsData = $request->all();
            $validator = Validator::make($paramsData, [
                        'status' => 'in:active,inactive,pending'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }            
            $return = $this->banduserRepository->approveBandUserByAdmin($paramsData, $id);
            if (is_null($return)) {
                return $this->sendError('Band user not update.');
            }
            $mailCheck = $this->banduserRepository->getDetailsForMail($id);
            if (!empty($mailCheck)) {
                event(new BandUserApprovalMailEvent($mailCheck));
//                $maildata = $mailCheck[0];
//                $maildata['emailIds'] = $maildata['email'];
//                $maildata['subject'] = 'Band User Approved Notification';
//                Mail::send('emails.banduser_approved_notification_emails', ['maildata' => $maildata], function($message) use ($maildata) {
//                    $message->subject($maildata['subject']);
//                    $message->to($maildata['emailIds']);
//                    $message->bcc('webassic3@gmail.com');
//                });
            }
            return $this->sendResponse($return, 'Band user approved successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactiveBandUserByAdmin(Request $request, $id) {
        try {
            $paramsData = $request->all();
            $validator = Validator::make($paramsData, [
                        'status' => 'in:active,inactive,pending'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->banduserRepository->inactiveBandUserByAdmin($paramsData, $id);
            if (is_null($return)) {
                return $this->sendError('Band user not update.');
            }

            return $this->sendResponse($return, 'Band user inactive successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Use for scan event module
     * @param type $uniqueId
     * @return type
     */
    public function banduserExist($uniqueId) {
        try {

            $return = $this->banduserRepository->getCountBanduserByUniqueId($uniqueId);
            if (is_null($return) || $return == 0) {
                return $this->sendError('Invalid Unique Id');
            }
            return $this->sendResponse($return, 'Unique Id is valid');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Use for scan event module
     * @param type $uniqueId
     * @return type
     */
    public function banduserDetails($uniqueId) {
        try {

            $return = $this->banduserRepository->getBanduserByUniqueId($uniqueId);
            if (is_null($return)) {
                return $this->sendError('Invalid Unique Id');
            }
            return $this->sendResponse($return, 'Unique Id is valid');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * confirmBandUsersDatatableListings
     * @param Request $request
     * @return type
     */
    public function confirmBandUsersDatatableListings(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->confirmBandusersListing($postData);
            try {
                // Get credentials 
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();                               
                $api = new Api($credentials['key_id'], $credentials['key_secret']);
                if($postData['package_name'] == env('RAZORPAY_BASIC_PACKAGE')){
                    $planDetails = $api->plan->fetch($credentials['basic_plan_id']);
                }else{
                    $planDetails = $api->plan->fetch($credentials['plan_id']);
                }
                
                if ($planDetails) {
                    $currency = $this->convertCurrency($planDetails['item']->currency);
                    $subscriptionAmt = (substr($planDetails['item']->amount, 0, -2));

                    if (isset($response['records']) && !empty($response['records'])) {
                        foreach ($response['records'] as &$confirmBandUserDetail) {
                            $confirmBandUserDetail['name'] = !empty($confirmBandUserDetail['firstname']) ? $confirmBandUserDetail['firstname'] . " " . $confirmBandUserDetail['lastname'] : '-';
                            $confirmBandUserDetail['email'] = !empty($confirmBandUserDetail['email']) ? $confirmBandUserDetail['email'] : '-';
                            $confirmBandUserDetail['mobile'] = !empty($confirmBandUserDetail['mobile']) ? $confirmBandUserDetail['mobile'] : '-';
                            $confirmBandUserDetail['subscriptionAmt'] = $currency . " " . $subscriptionAmt;
                        }
                    }
                    $confirmBandUserDetailData = [];
                    $confirmBandUserDetailData['data'] = isset($response['records']) ? $response['records'] : [];
                    $confirmBandUserDetailData['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
                    $confirmBandUserDetailData['totalAmount'] = isset($subscriptionAmt) ? ($subscriptionAmt * $response['total_count']) : 0;
                    $confirmBandUserDetailData['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
                }
                return json_encode($confirmBandUserDetailData);
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'BanduserController/index';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $error['rzerror'] = $errorLog['error_message'];
                return json_encode($error);
            }
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * edit
     * @return type
     * @throws \Exception
     */
    public function edit($id) {
        try {
            $titles = $this->banduser->titleArray;
            $relationShipWithGuardian = $this->banduser->relationShipWithGuardian;
            $bloodGroupArray = $this->banduser->bloodGroupArray;
            $getAllAilingsList = [];
            $getAilingsList = $this->ailingRepository->getAilingsList();
            $datearray = $this->getDateArrayForBandUser();
            if (isset($getAilingsList) && !empty($getAilingsList)) {
                foreach ($getAilingsList as $value) {
                    $getAllAilingsList[$value['id']] = $value['name'];
                }
            }
            $bandUserDetails = $this->banduserRepository->showBanduser($id);
            return view('bandusers.edit', compact('titles', 'relationShipWithGuardian', 'bloodGroupArray', 'getAllAilingsList', 'bandUserDetails', 'datearray'));
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * bandUserGetOtp
     * @param type $mobile
     * @return type
     */
    public function bandUserGetOtp($mobile, $id) {
        try {
            $data = array();
            if (!empty($mobile)) {
                if (isset($id) && !empty($id) && $id != 'null') {
                    $checkMobileNumber = $this->banduserRepository->checkMobileNumber($mobile, $id);
                    if ($checkMobileNumber < 1) {
                        $otp = $this->generateOtp();
                        $data['mobile'] = $mobile;
                        $data['otp'] = $otp;
                        $response = $this->sendResponse($otp, 'OTP sent successfully.');
                    } else {
                        $response = $this->sendResponse('no_otp', '');
                    }
                } else {
                    $otp = $this->generateOtp();
                    $data['mobile'] = $mobile;
                    $data['otp'] = $otp;
                    $response = $this->sendResponse($otp, 'OTP sent successfully.');
                }
                if (!empty($data['mobile']) && !empty($data['otp'])) {
                    event(new SendOtpEvent($data));
                }
            }

            return $response;
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    public function serveImage($name, $banduser_id, $file) {
        try {
            if ($name == 'qr_codes') {
                if (Auth::user()->user_type == 'Administration') {
                    $image = \Illuminate\Support\Facades\File::get('banduser_details/banduser_' . $banduser_id . '/' . $name . '/' . $file);
                    return response()->make($image, 200, ['content-type' => 'image/jpg']);
                } else {
                    return redirect('/authorization-error');
                }
            } else {
                $image = \Illuminate\Support\Facades\File::get('banduser_details/banduser_' . $banduser_id . '/' . $name . '/' . $file);
                return response()->make($image, 200, ['content-type' => 'image/jpg']);
            }
        } catch (FileNotFoundException $exc) {
            abort(403);
        }
    }

    /**
     * deleteAadhaarCardNumber
     * @param type $id
     * @return type
     */
    public function deleteAadhaarCardNumber($id) {
        try {
            $return = $this->banduserRepository->deleteAadhaarCardNumber($id);
            if (is_null($return)) {
                return $this->sendError('Banduser not update.');
            }
            return $this->sendResponse($return, 'Aadhaar card number deleted successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * activeBandUsers
     * @return type
     */
    public function activeBandUsers() {
        try {
            return view('bandusers.active_bandusers');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * activeBandUsers
     * @return type
     */
    public function inactiveBandUsers() {
        try {
            return view('bandusers.inactive_bandusers');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * activeBandUsersListings
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function activeBandUsersListings(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->activeBandUsersListings($postData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return json_encode($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * inactiveBandUsersListings
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function inactiveBandUsersListings(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->inactiveBandUsersListings($postData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return json_encode($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * downloadBandUserDetails
     * @param type $id
     * @return type
     */
    public function downloadBandUserDetails($id) {
        try {
            $bandUserDetails = $this->banduserRepository->getBandUsersDetails($id);
            if (!empty($bandUserDetails)) {
                view()->share(compact('bandUserDetails'));
                $pdf = \PDF::loadView('bandusers.download_banduser');
                if ($bandUserDetails['title'] == 'Other' || $bandUserDetails['title'] == 'default') {
                    $fileName = $bandUserDetails['firstname'] . " " . $bandUserDetails['lastname'] . "_" . $bandUserDetails['uniqueid'] . ".pdf";
                } else {

                    $fileName = $bandUserDetails['title'] . " " . $bandUserDetails['firstname'] . " " . $bandUserDetails['lastname'] . "_" . $bandUserDetails['uniqueid'] . ".pdf";
                }
                return $pdf->download($fileName);
            }
        } catch (Exception $exc) {
            return response(json_encode(['error' => $exc->getMessage()]))
                            ->setStatusCode(500, $exc->getMessage());
        }
    }

    /**
     * convertCurrency
     * @param type $mycurrency
     * @return type
     */
    public function convertCurrency($mycurrency) {
        $currencies = array(
            '₹' => 'INR',
            '$' => 'USD',
            'AU$' => 'AUD',
            '€' => 'EUR',
            '£' => 'GBP'
        );
        $key = array_search(strtoupper($mycurrency), $currencies);
        if ($key) {
            return $key;
        }
    }

    public function activeBandUsersExport() {
        try {
            $response = $this->banduserRepository->activeBandUsersExport();
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Unquie Id'] = $value['uniqueid'];
                    $banduserDataCollect['Firstname'] = $value['firstname'];
                    $banduserDataCollect['Lastname'] = $value['lastname'];
                    $banduserDataCollect['Guardian Name'] = $value['ufirstname'] . ' ' . $value['ulastname'];
                    $banduserDataCollect['Mobile'] = $value['mobile'];
                    $banduserDataCollect['Email'] = $value['email'];
                    $banduserDataCollect['Package Type'] = isset($value['package_name']) && !empty($value['package_name']) ? $value['package_name'] : NULL;
                    $banduserDataCollect['Payment Date'] = isset($value['payment_date']) && !empty($value['payment_date']) ? $value['payment_date'] : NULL;
                    $banduserDataCollect['Created On'] = empty($value['created_at']) ? '' : date('d/m/Y', strtotime($value['created_at']));
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Unquie Id'] = '';
                $banduserDataCollect['Firstname'] = '';
                $banduserDataCollect['Lastname'] = '';
                $banduserDataCollect['Guardian Name'] = '';
                $banduserDataCollect['Mobile'] = '';
                $banduserDataCollect['Email'] = '';
                $banduserDataCollect['Package Type'] = '';
                $banduserDataCollect['Payment Date'] = '';
                $banduserDataCollect['Created On'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }


            $headingArray = [
                'Unquie Id',
                'Firstname',
                'Lastname',
                'Guardian Name',
                'Mobile',
                'Email',
                'Package Type',
                'Payment Date',
                'Created On'
            ];

            $export = new ActiveBanduserExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'activebandusers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    public function inactiveBandUsersExport() {
        try {
            $response = $this->banduserRepository->inactiveBandUsersExport();
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Unquie Id'] = $value['uniqueid'];
                    $banduserDataCollect['Firstname'] = $value['firstname'];
                    $banduserDataCollect['Lastname'] = $value['lastname'];
                    $banduserDataCollect['Guardian Name'] = $value['ufirstname'] . ' ' . $value['ulastname'];
                    $banduserDataCollect['Mobile'] = $value['mobile'];
                    $banduserDataCollect['Email'] = $value['email'];
                    $banduserDataCollect['Created On'] = empty($value['created_at']) ? '' : date('d/m/Y', strtotime($value['created_at']));
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Unquie Id'] = '';
                $banduserDataCollect['Firstname'] = '';
                $banduserDataCollect['Lastname'] = '';
                $banduserDataCollect['Guardian Name'] = '';
                $banduserDataCollect['Mobile'] = '';
                $banduserDataCollect['Email'] = '';
                $banduserDataCollect['Created On'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }

            $headingArray = [
                'Unquie Id',
                'Firstname',
                'Lastname',
                'Guardian Name',
                'Mobile',
                'Email',
                'Created On'
            ];

            $export = new InactiveBanduserExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'inactivebandusers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    public function bandusersExport($id) {
        try {
            $response = $this->banduserRepository->bandusersExport($id);
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Unquie Id'] = $value['uniqueid'];
                    $banduserDataCollect['Firstname'] = $value['firstname'];
                    $banduserDataCollect['Lastname'] = $value['lastname'];
                    $banduserDataCollect['Guardian Name'] = $value['ufirstname'] . ' ' . $value['ulastname'];
                    $banduserDataCollect['Mobile'] = $value['mobile'];
                    $banduserDataCollect['Email'] = $value['email'];
                    $banduserDataCollect['Status'] = $value['status'];
                    $banduserDataCollect['Created On'] = empty($value['created_at']) ? '' : date('d/m/Y', strtotime($value['created_at']));
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Unquie Id'] = '';
                $banduserDataCollect['Firstname'] = '';
                $banduserDataCollect['Lastname'] = '';
                $banduserDataCollect['Guardian Name'] = '';
                $banduserDataCollect['Mobile'] = '';
                $banduserDataCollect['Email'] = '';
                $banduserDataCollect['Status'] = '';
                $banduserDataCollect['Created On'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }

            $headingArray = [
                'Unquie Id',
                'Firstname',
                'Lastname',
                'Guardian Name',
                'Mobile',
                'Email',
                'Status',
                'Created On'
            ];

            $export = new BanduserExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'bandusers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    public function exportUserBanduser() {
        try {
            $response = $this->banduserRepository->exportUserBanduser();
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Unquie Id'] = $value['uniqueid'];
                    $banduserDataCollect['Firstname'] = $value['firstname'];
                    $banduserDataCollect['Lastname'] = $value['lastname'];
                    $banduserDataCollect['Mobile'] = $value['mobile'];
                    $banduserDataCollect['Email'] = $value['email'];
                    $banduserDataCollect['Status'] = $value['status'];
                    $banduserDataCollect['Created On'] = empty($value['created_at']) ? '' : date('d/m/Y', strtotime($value['created_at']));
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Unquie Id'] = '';
                $banduserDataCollect['Firstname'] = '';
                $banduserDataCollect['Lastname'] = '';
                $banduserDataCollect['Mobile'] = '';
                $banduserDataCollect['Email'] = '';
                $banduserDataCollect['Status'] = '';
                $banduserDataCollect['Created On'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }

            $headingArray = [
                'Unquie Id',
                'Firstname',
                'Lastname',
                'Mobile',
                'Email',
                'Status',
                'Created On'
            ];

            $export = new UsersBanduserExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'bandusers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * create_subscription
     * @param type 
     * @return type
     */
    public function createSubscription(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->createSubscription($postData);
            return json_encode($response);
        } catch (\Exception $ex) {
            $response['error'] = $ex->getMessage();
            return json_encode($response);
        }
    }

    /**
     * saveTransactionDetails
     * @param Request $request
     * @return type
     */
    public function saveTransactionDetails(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->saveTransactionDetails($postData);
            return json_encode($response);
        } catch (\Exception $ex) {
            $response['error'] = $ex->getMessage();
            return json_encode($response);
        }
    }

    /**
     * Display a listing of the resource.
     * bandUserTransactions
     * @return \Illuminate\Http\Response
     */
    public function bandUserTransactions() {
        try {
            return view('banduser_transactions.index');
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * verifyAadhharDetailsByAdmin
     * @param type $id
     * @return type
     */
    public function verifyAadhharDetailsByAdmin($id) {
        try {
            $details = $this->banduserRepository->getAadhharCardDetails($id);
            if (isset($details) && !empty($details)) {

                $destinationPath = public_path("banduser_details/banduser_" . $details['id'] . "/" . $details['aadhaar_card_file_name']);

                // Call to Aadhaar API
                $headers = array(
                    'Content-type: multipart/form-data'
                );

                $url = "https://ola.innodeed.com/v1/api/aadhar/data";

                $mime = mime_content_type($destinationPath);

                $info = pathinfo($destinationPath);
                $name = $info['basename'];
                $zip_file = new \CURLFile($destinationPath, $mime, $name);
                $post_data = array(
                    "file" => $zip_file,
                    "secureCode" => $details['share_code']
                );
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_VERBOSE, true);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                $response = curl_exec($curl);
                $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                $aadhaarVerifyAPIArray = json_decode($response, true);
                if ($status == 200) {
                    $aadhaarVerifyAPIArray['status'] = 'success';
                } else {
                    $aadhaarVerifyAPIArray['status'] = 'error';
                }
                if ($aadhaarVerifyAPIArray) {
                    $result = $this->banduserRepository->saveAadhaarInformations($id, $aadhaarVerifyAPIArray, $response);
                    return json_encode($result);
                }
            }
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * downloadBandUserAddressDetails
     * @param type $id
     * @return type
     */
    public function downloadBandUserAddressDetails($id) {
        try {
            $bandUserDetails = $this->banduserRepository->getBandUsersDetails($id);
            if (!empty($bandUserDetails)) {
                view()->share(compact('bandUserDetails'));
                $pdf = \PDF::loadView('bandusers.download_banduser_address');
                if ($bandUserDetails['title'] == 'Other' || $bandUserDetails['title'] == 'default') {
                    $fileName = $bandUserDetails['firstname'] . " " . $bandUserDetails['lastname'] . "_" . $bandUserDetails['uniqueid'] . ".pdf";
                } else {

                    $fileName = $bandUserDetails['title'] . " " . $bandUserDetails['firstname'] . " " . $bandUserDetails['lastname'] . "_" . $bandUserDetails['uniqueid'] . ".pdf";
                }
                return $pdf->download($fileName);
            }
        } catch (Exception $exc) {
            return response(json_encode(['error' => $exc->getMessage()]))
                            ->setStatusCode(500, $exc->getMessage());
        }
    }

    /**
     * getPackageDetails
     * @param type 
     * @return type
     */
    public function getPackageDetails(Request $request) {
        try {
            $postData = $request->all();           
            $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);
                $planDetails = $api->plan->fetch($postData['planID']);
                 $planDetail = [];
                 $planDetail['name'] = $planDetails->item->name;
                 $planDetail['description'] = $planDetails->item->description;                  
                 $planDetail['period'] = $planDetails->period;
                 $currency = $this->convertCurrency($planDetails['item']->currency);
                 $subscriptionAmt = (substr($planDetails['item']->amount, 0, -2));
                 $planDetail['subscriptionAmt'] = $currency . " " . $subscriptionAmt . ' Per Band.';
                 echo json_encode($planDetail);exit;
        } catch (\Exception $ex) {
            $response['error'] = $ex->getMessage();
            return json_encode($response);
        }
    }
    
     /**
     * getPackageTypeDetailCount
     * @param type 
     * @return type
     */
    public function getPackageTypeDetailCount(Request $request) {
        try {
            $bandUserDetails = $this->banduserRepository->getPackageTypeDetailCountOfBandUsers();            
            echo json_encode($bandUserDetails);exit;
        } catch (\Exception $ex) {
            $response['error'] = $ex->getMessage();
            return json_encode($response);
        }
    }
}
