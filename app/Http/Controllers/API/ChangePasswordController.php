<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;
use Validator;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request) {
        try {
            DB::beginTransaction();
            $content = $request->getContent();
            if (is_string($request->getContent())) {
                $content = json_decode($request->getContent(), true);
            }
            $validator = Validator::make($content, [
                        'current_password' => 'required',
                        'new_password' => 'required',
                        'new_confirm_password' => 'required|same:new_password'
            ]);
            if ($validator->fails()) {
                if ($request->ajax()) {
                    if ($validator->messages()->getMessages()) {
                        $errors = $validator->messages()->getMessages();
                        foreach ($errors as $error) {
                            $errorsArr = implode(", ", array_map(function ($error) {
                                        return $error;
                                    }, $error));
                        }
                    }
                    $returnarray['status'] = 1;
                    $returnarray['message'] = $error;
                    $returnarray['error'] = $error;
                    echo json_encode($returnarray);
                    exit;
                } else {
                    return response($validator->messages())
                                    ->setStatusCode(201, $validator->messages());
                }
            }

            $checkExistingPasswordResponse = Hash::check($content['current_password'], auth()->user()->password);
            if (empty($checkExistingPasswordResponse)) {
                if ($request->ajax()) {
                    $returnarray['status'] = 1;
                    $returnarray['message'] = 'Current password is not matched with existing password';
                    $returnarray['error'] = 'Current password is not matched with existing password';
                    echo json_encode($returnarray);
                    exit;
                } else {
                    return response('Current password is not matched with existing password')
                                    ->setStatusCode(201, 'Current password is not matched with existing password');
                }
            }
            $responseData = User::find(auth()->user()->id)
                    ->update(['password' => Hash::make($content['new_password']),
                'modified_by' => auth()->user()->id]);

            Auth::guard('web')->logout();
            if ($request->ajax()) {
                if (isset($responseData) && !empty($responseData)) {
                    $returnarray['status'] = 0;
                    $returnarray['message'] = 'Password Updated successfully.Please login again..';
                } else {
                    $returnarray['status'] = 1;
                    $returnarray['message'] = 'Something Went wrong...';
                }
            } else {
                DB::commit();
                return redirect('/login')->withInput()
                                ->withErrors(['Password Updated successfully.Please login again.']);
            }
            DB::commit();
            echo json_encode($returnarray);
            exit;
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

}
