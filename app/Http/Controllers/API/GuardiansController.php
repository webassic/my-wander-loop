<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Guardian;
use App\Repositories\GuardiansRepository;

//use Illuminate\Support\Facades\Session;

class GuardiansController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $guardian;
    private $guardiansRepository;

    /**
     * 
     * @param Guardian $guardian
     * @param GuardiansRepository $guardiansRepository
     */
    public function __construct(Guardian $guardian, GuardiansRepository $guardiansRepository) {
        $this->guardian = $guardian;
        $this->guardiansRepository = $guardiansRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $data = [];
            $exist = $this->guardiansRepository->showGuardian(Auth::user()->id);
            if (!empty($exist) && $exist['is_exist'] == 1) {
                $data = $exist;
//                    dd(Session::get('flash'));
                return view('guardians.index', compact('data'));
            }
            return redirect('/guardians/add');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {

            $postData = $request->all();
            $postData['user_id'] = Auth::user()->id;
            $validator = Validator::make($postData, [
                        'user_id' => 'required|unique:guardians',
                        'title' => 'required|in:' . implode(',', $this->guardian->titleArray),
                        'dob' => 'required|date',
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required',
//                        'identity_type' => 'in:' . implode(',', $this->guardian->identityTypeArray)
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $return = $this->guardiansRepository->storeGuardian($postData);
            return $this->sendResponse($return, 'Guardians details created successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $return = $this->guardiansRepository->showGuardian($id);
            if (is_null($return)) {
                return $this->sendError('Guardians not found.');
            }
            return $this->sendResponse($return, 'Guardians details fetched successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $postData = $request->all();
          
            $userId = Auth::user()->id;
            unset($postData['_token']);
             $messages = [
                        'dob.required'  => 'This field is required',
    
                        ]; 
            if (isset($postData) && !empty($postData)) {
                if(!empty($postData['date']) && !empty($postData['month']) && !empty($postData['year']) ){
                   if($postData['month'] == 'Feb' && $postData['date'] > 29){
                       $messages = [
                        'dob.required'  => 'Date cannot greater than 29',
    
                        ]; 
                   }else{
                       $date= $postData['date'].'-'.$postData['month'].'-'.$postData['year'];
                       
                        $postData['dob'] = !empty($date) ? date('Y-m-d', strtotime($date)) : '';
                        unset($postData['date']);
                        unset($postData['month']);
                        unset($postData['year']);
                   }
                }else{
                   
                    $postData['dob'] ='';
                }
            }

            $validator = Validator::make($postData, [
                        'title' => 'in:' . implode(',', $this->guardian->titleArray),
                        'dob' => 'required|date',
                        'firstname' => 'required',
                        'lastname' => 'required',
                        'mobile' => 'required|numeric|unique:users,mobile,'.$userId.',id,deleted_at,NULL',
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required'
            ],$messages);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->guardiansRepository->updateGuardian($postData, $id);
            if (is_null($return)) {
                return $this->sendError('Guardian not update.');
            }
            return $this->sendResponse($return, 'Guardian details saved successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {

            $return = $this->guardiansRepository->destroyGuardian($id);

            return $this->sendResponse([], 'Guardian details deleted successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    public function institution() {
        try {
            return view('guardians.institution');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function add() {
        try {
            $datearray = $this->getDateArray();
            $titles = $this->guardian->titleArray;
            $identities = $this->guardian->identityTypeArray;
            $exists = $this->guardiansRepository->recordExistGuardianType(Auth::user()->id);
            if(!empty($exists) && $exists[0]['is_exist']==1){
                return view('guardians.edit', compact('titles', 'identities', 'exists','datearray'));
            }
            return view('guardians.add', compact('titles', 'identities', 'exists','datearray'));
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function storeGuardianType(Request $request, $user_id) {
        try {
            $postData = $request->all();
            $postData['user_id'] = $user_id;
            $return = $this->guardiansRepository->storeGuardianType($postData);
            return $this->sendResponse($return, 'Guardian created successfully.');
        } catch (\Exception $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    public function storeInstitution(Request $request, $id) {
        try {
            $postData = $request->all();
            unset($postData['_token']);
            $userId = Auth::user()->id;
            $validator = Validator::make($postData, [
                        'mobile' => 'required|numeric|unique:users,mobile,' . $userId.',id,deleted_at,NULL',
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required',
//                        'landline' => 'max:15'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->guardiansRepository->updateGuardian($postData, $id);
            if (is_null($return)) {
                return $this->sendError('Guardian not update.');
            }
            return $this->sendResponse($return, 'Guardian details saved successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
    /**
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateIndividualData(Request $request, $id) {
        try {
            $postData = $request->all();
            $userId = Auth::user()->id;
            unset($postData['_token']);
            $validator = Validator::make($postData, [
                        'mobile' => 'required|numeric|unique:users,mobile,' . $userId.',id,deleted_at,NULL',
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required',
//                        'landline' => 'digits_between:6,15'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
//            dd($postData);
            $return = $this->guardiansRepository->updateGuardian($postData, $id);
            if (is_null($return)) {
                return $this->sendError('Guardian not update.');
            }
            return $this->sendResponse($return, 'Guardian details saved successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
    /**
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function updateInstitutionData(Request $request, $id) {
        try {
            $postData = $request->all();
             $userId = Auth::user()->id;
            unset($postData['_token']);
            $validator = Validator::make($postData, [
                        'mobile' => 'required|numeric|unique:users,mobile,' . $userId.',id,deleted_at,NULL',
                        'address' => 'required',
                        'locality' => 'required',
                        'city' => 'required',
                        'state' => 'required',
                        'pin' => 'required',
//                        'landline' => 'digits_between:6,15'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
//            dd($postData);
            $return = $this->guardiansRepository->updateGuardian($postData, $id);
            if (is_null($return)) {
                return $this->sendError('Guardian not update.');
            }
            return $this->sendResponse($return, 'Guardian details saved successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
    /**
     * 
     * @param type $month
     * @param type $year
     * @return type
     */
    public function getdays($month,$year){
        try {
            $dateArray=[];
            $month = date('m', strtotime($month));
            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for($i=1;$i<=$days;$i++){
                 $dateArray[$i] = ($i<10) ? '0'.$i : $i;
             }

            if (empty($dateArray)) {
                return $this->sendError('Something went wrong');
            }
            return $this->sendResponse($dateArray, 'Days fetch successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
}
