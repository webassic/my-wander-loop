<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Institution;
use App\Repositories\InstitutionRepository;

class InstitutionController extends RestController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $institution;
    private $institutionRepository;
    public function __construct(Institution $institution,InstitutionRepository $institutionRepository)
    {
        $this->institution =$institution;
        $this->institutionRepository =$institutionRepository;
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{ 
            if(auth()->user()->type=='Individual'){
                return redirect('/guardians');
                 
            }else{
               
                $exists = $this->institutionRepository->recordExist(Auth::user()->id);
               return view('guardians.institution',compact('exists')); 
            }
           return view('guardians.institution'); 
        } catch (\Expectional $exc){
             \Log::error($ex->getTraceAsString()); 
            return  $this->sendError('Expectional Error.', $ex->getMessage(),500); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            
            $postData=$request->all();
            $postData['user_id'] = Auth::user()->id;
//            dd($postData);
            $validator = Validator::make($postData, [
                'user_id'=>'required|unique:institutions',
                'name' => 'required',
                'address' => 'required',
                'locality' => 'required',
                'city' => 'required', 
                'state' => 'required', 
                'pin'=> 'required|integer'
            ]);
            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }
            
            $return = $this->institutionRepository->storeInstitution($postData);
            return $this->sendResponse($return, 'Institutions has been created successfully.');
        }catch(\Expectional $exc){
             \Log::error($ex->getTraceAsString()); 
            return  $this->sendError('Expectional Error.', $ex->getMessage(),500); 
           
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $return = $this->institutionRepository->showInstitution($id);
            if (is_null($return)) {
                return $this->sendError('Institutions not found.');
            }
            return $this->sendResponse($return, 'Institutions has been retrieved successfully.');
        }catch(\Expectional $exc){
            \Log::error($ex->getTraceAsString()); 
            return  $this->sendError('Expectional Error.', $ex->getMessage(),500); 
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $postData = $request->all();
            $validator = Validator::make($postData, [
                    'pin'=> 'integer',
                    
            ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $return = $this->institutionRepository->updateInstitution($postData,$id);
        if (is_null($return)) {
            return $this->sendError('Institutions not update.');
        }
        return $this->sendResponse($return, 'Institutions has been updated successfully.');
        
        }catch(\Expectional $exc){
            \Log::error($ex->getTraceAsString()); 
            return  $this->sendError('Expectional Error.', $ex->getMessage(),500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            
        $return = $this->institutionRepository->destroyInstitution($id);
        
        return $this->sendResponse([], 'Institutions has been deleted successfully.');
        
        }catch(\Expectional $exc){
            \Log::error($ex->getTraceAsString()); 
            return  $this->sendError('Expectional Error.', $ex->getMessage(),500); 
        }
    }
    
}
