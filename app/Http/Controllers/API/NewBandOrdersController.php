<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use App\Repositories\NewBandOrdersRepository;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\AdminNewBandOrdersExport;
use App\Exports\AdminDeliveredOrdersExport;
use App\Exports\NewBandOrdersExport;
use App\Exports\DeliveredOrdersExport;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;

class NewBandOrdersController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $newBandOrdersRepository;

    public function __construct(NewBandOrdersRepository $newBandOrdersRepository) {
        $this->newBandOrdersRepository = $newBandOrdersRepository;
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function storeNewBandOrder(Request $request) {
        try {
            $postData = $request->all();
            unset($postData['_token']);
            if (isset($postData['bands_number']) && !empty($postData['bands_number'])) {
                $postData['number_of_bands'] = $postData['bands_number'];
                unset($postData['bands_number']);
            }
            $postData['created_by'] = Auth::user()->id;
            $postData['order_date'] = date('Y-m-d H:i:s');

            $validator = Validator::make($postData, [
                        'number_of_bands' => 'required',
                        'total_amount' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->newBandOrdersRepository->storeNewBandOrder($postData);
            if (!empty($return)) {
                if (isset($return['rzerror'])) {
                    return $this->sendError('Razorpay Error', $return);
                }
                return $this->sendResponse($return, 'New band purchased successfully.');
            }
            return $this->sendError($return, 'Something went wrong');
        } catch (\Expectional $ex) {

            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function userIndex() {
        try {
            return view('new_band_orders.user_index');
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function userDeliveredIndex() {
        try {
            return view('new_band_orders.user_delivered_index');
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function adminIndex() {
        try {
            return view('new_band_orders.admin_index');
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function adminDeliveredIndex() {
        try {
            return view('new_band_orders.admin_delivered_index');
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function userNewBandOrdersListings(Request $request) {
        try {
            $paramsData = $request->all();

            $response = $this->newBandOrdersRepository->userNewBandOrdersListings($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function adminNewBandOrdersListings(Request $request) {
        try {
            $paramsData = $request->all();

            $response = $this->newBandOrdersRepository->adminNewBandOrdersListings($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function adminDeliveredListings(Request $request) {
        try {
            $paramsData = $request->all();

            $response = $this->newBandOrdersRepository->adminDeliveredListings($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function userDeliveredListings(Request $request) {
        try {
            $paramsData = $request->all();

            $response = $this->newBandOrdersRepository->userDeliveredListings($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function updateStatus(Request $request) {
        try {
            $paramsData = $request->all();
            $response = [];
            if (!empty($paramsData) && $paramsData['banduser_id']) {
                $id = $paramsData['banduser_id'];
                unset($paramsData['banduser_id']);
                $response = $this->newBandOrdersRepository->updateStatus($paramsData, $id);
            }

            if (!empty($response)) {

                return $this->sendResponse($response, 'New band purchased successfully.');
            }
            return $this->sendError($response, 'Something went wrong');
            return response()->json($datatable);
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    public function updateTransactionDetails(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->newBandOrdersRepository->updateTransactionDetails($postData);
            return $this->sendResponse($response, 'Order placed successfully.');
        } catch (\Exception $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    public function exportAdminBandOrders() {
        try {
            $response = $this->newBandOrdersRepository->exportAdminBandOrders();
            $dataCollect = [];
            $dataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $dataCollect['Band User Name'] = $value['firstname'] . ' ' . $value['lastname'];
                    $dataCollect['Guardian Name'] = $value['ufirstname'] . ' ' . $value['ulastname'];
                    $dataCollect['Band Lost'] = $value['band_lost'];
                    $dataCollect['Number Of Bands'] = $value['number_of_bands'];
                    $dataCollect['Price Per Band'] = $value['price_per_band'];
                    $dataCollect['Total Amount'] = $value['total_amount'];
                    $dataCollect['Payment ID'] = $value['rzpayment_id'];
                    $dataCollect['Order Status'] = $value['total_amount'];
                    $dataCollect['Order Date'] = $value['order_status'];
                    $dataArray[] = $dataCollect;
                }
            } else {
                $dataCollect['Band User Name'] = '';
                $dataCollect['Guardian Name'] = '';
                $dataCollect['Band Lost'] = '';
                $dataCollect['Number Of Bands'] = '';
                $dataCollect['Price Per Band'] = '';
                $dataCollect['Total Amount'] = '';
                $dataCollect['Payment ID'] = '';
                $dataCollect['Order Status'] = '';
                $dataCollect['Order Date'] = '';
                $dataArray[] = $dataCollect;
            }

            $headingArray = [
                'Band User Name',
                'Guardian Name',
                'Band Lost',
                'Number Of Bands',
                'Price Per Band',
                'Total Amount',
                'Payment ID',
                'Order Status',
                'Order Date'
            ];

            $export = new AdminNewBandOrdersExport([$dataArray],
                    [$headingArray]);
            return Excel::download($export, 'admin_band_orders.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     */
    public function exportAdminDeliveredOrders() {
        try {
            $response = $this->newBandOrdersRepository->exportAdminDeliveredOrders();
            $dataCollect = [];
            $dataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $dataCollect['Band User Name'] = $value['firstname'] . ' ' . $value['lastname'];
                    $dataCollect['Guardian Name'] = $value['ufirstname'] . ' ' . $value['ulastname'];
                    $dataCollect['Band Lost'] = $value['band_lost'];
                    $dataCollect['Number Of Bands'] = $value['number_of_bands'];
                    $dataCollect['Price Per Band'] = $value['price_per_band'];
                    $dataCollect['Total Amount'] = $value['total_amount'];
                    $dataCollect['Payment ID'] = $value['rzpayment_id'];
                    $dataCollect['Order Status'] = $value['total_amount'];
                    $dataCollect['Order Date'] = $value['order_status'];
                    $dataArray[] = $dataCollect;
                }
            } else {
                $dataCollect['Band User Name'] = '';
                $dataCollect['Guardian Name'] = '';
                $dataCollect['Band Lost'] = '';
                $dataCollect['Number Of Bands'] = '';
                $dataCollect['Price Per Band'] = '';
                $dataCollect['Total Amount'] = '';
                $dataCollect['Payment ID'] = '';
                $dataCollect['Order Status'] = '';
                $dataCollect['Order Date'] = '';
                $dataArray[] = $dataCollect;
            }

            $headingArray = [
                'Band User Name',
                'Guardian Name',
                'Band Lost',
                'Number Of Bands',
                'Price Per Band',
                'Total Amount',
                'Payment ID',
                'Order Status',
                'Order Date'
            ];

            $export = new AdminDeliveredOrdersExport([$dataArray],
                    [$headingArray]);
            return Excel::download($export, 'admin_delivered_orders.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     */
    public function exportUserBandOrders() {
        try {
            $response = $this->newBandOrdersRepository->exportUserBandOrders();
            $dataCollect = [];
            $dataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $dataCollect['Band User Name'] = $value['firstname'] . ' ' . $value['lastname'];
                    $dataCollect['Band Lost'] = $value['band_lost'];
                    $dataCollect['Number Of Bands'] = $value['number_of_bands'];
                    $dataCollect['Price Per Band'] = $value['price_per_band'];
                    $dataCollect['Total Amount'] = $value['total_amount'];
                    $dataCollect['Payment ID'] = $value['rzpayment_id'];
                    $dataCollect['Order Status'] = $value['total_amount'];
                    $dataCollect['Order Date'] = $value['order_status'];
                    $dataArray[] = $dataCollect;
                }
            } else {
                $dataCollect['Band User Name'] = '';
                $dataCollect['Band Lost'] = '';
                $dataCollect['Number Of Bands'] = '';
                $dataCollect['Price Per Band'] = '';
                $dataCollect['Total Amount'] = '';
                $dataCollect['Payment ID'] = '';
                $dataCollect['Order Status'] = '';
                $dataCollect['Order Date'] = '';
                $dataArray[] = $dataCollect;
            }

            $headingArray = [
                'Band User Name',
                'Band Lost',
                'Number Of Bands',
                'Price Per Band',
                'Total Amount',
                'Payment ID',
                'Order Status',
                'Order Date'
            ];

            $export = new NewBandOrdersExport([$dataArray],
                    [$headingArray]);
            return Excel::download($export, 'new_band_orders.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     */
    public function exportUserDeliveredOrders() {
        try {
            $response = $this->newBandOrdersRepository->exportUserDeliveredOrders();
            $dataCollect = [];
            $dataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $dataCollect['Band User Name'] = $value['firstname'] . ' ' . $value['lastname'];
                    $dataCollect['Band Lost'] = $value['band_lost'];
                    $dataCollect['Number Of Bands'] = $value['number_of_bands'];
                    $dataCollect['Price Per Band'] = $value['price_per_band'];
                    $dataCollect['Total Amount'] = $value['total_amount'];
                    $dataCollect['Payment ID'] = $value['rzpayment_id'];
                    $dataCollect['Order Status'] = $value['total_amount'];
                    $dataCollect['Order Date'] = $value['order_status'];
                    $dataArray[] = $dataCollect;
                }
            } else {
                $dataCollect['Band User Name'] = '';
                $dataCollect['Band Lost'] = '';
                $dataCollect['Number Of Bands'] = '';
                $dataCollect['Price Per Band'] = '';
                $dataCollect['Total Amount'] = '';
                $dataCollect['Payment ID'] = '';
                $dataCollect['Order Status'] = '';
                $dataCollect['Order Date'] = '';
                $dataArray[] = $dataCollect;
            }

            $headingArray = [
                'Band User Name',
                'Band Lost',
                'Number Of Bands',
                'Price Per Band',
                'Total Amount',
                'Payment ID',
                'Order Status',
                'Order Date'
            ];

            $export = new DeliveredOrdersExport([$dataArray],
                    [$headingArray]);
            return Excel::download($export, 'delivered_orders.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * downloadInvoice
     * @return type
     * @throws \Exception
     */
    public function downloadInvoice($id) {
        try {
            $invoice = $this->newBandOrdersRepository->downloadInvoice($id);
            view()->share(compact('invoice'));
            $pdf = \PDF::loadView('new_band_orders.invoice');
            $fileName = "Invoice-" . $invoice['invoiceNumber'] . ".pdf";
            return $pdf->download($fileName);
        } catch (\Exception $ex) {
            return response(json_encode(['error' => $ex->getMessage()]))
                            ->setStatusCode($ex->getCode(), $ex->getMessage());
        }
    }

}
