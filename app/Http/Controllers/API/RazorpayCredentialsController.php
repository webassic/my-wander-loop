<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use App\Repositories\RazorpayCredentialsRepository;
use DB;

class RazorpayCredentialsController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $razorpayCredentialsRepository;

    public function __construct(RazorpayCredentialsRepository $razorpayCredentialsRepository) {
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            return view('razorpay_credentials.index');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $response = $this->razorpayCredentialsRepository->showRazorpayCredentials($id);

            if (is_null($response)) {
                return $this->sendError('Razorpay Credentials not found.');
            }
            return view('razorpay_credentials.show')->with('data', $response);
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function edit($id) {
        try {
            $response = $this->razorpayCredentialsRepository->showRazorpayCredentials($id);
            if (is_null($response)) {
                return $this->sendError('Razorpay Credentials not found.');
            }
            return view('razorpay_credentials.edit')->with('data', $response);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $content = $request->all();
            unset($content['_token']);
            $validator = Validator::make($content, [
                        'test_key_id' => 'required',
                        'test_key_secret' => 'required',
                        'test_basic_plan_id' => 'required',
                        'test_plan_id' => 'required',
                        'test_banduser_cost' => 'required',
                        'live_key_id' => 'required',
                        'live_key_secret' => 'required',
                        'live_basic_plan_id' => 'required',
                        'live_plan_id' => 'required',
                        'live_banduser_cost' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }          

            $return = $this->razorpayCredentialsRepository->updateRazorpayCredentials($content, $id);
            DB::commit();
            if (is_null($return)) {
                return $this->sendError('Razorpay Credentials not update.');
            }
            return $this->sendResponse($return, 'Razorpay Credentials updated successfully.');
        } catch (\Expectional $ex) {
            DB::rollback();
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

}
