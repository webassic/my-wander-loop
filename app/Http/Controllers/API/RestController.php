<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class RestController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 422)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
    
    public function generateOtp(){
        $uniqueId = mt_rand(100000,999999);
        return $uniqueId;
    }
   
    public function getDateArray(){
        $dateArray = [];
        //date
        for($i=1;$i<=31;$i++){
            $dateArray['date'][$i] = ($i<10) ? '0'.$i : $i;
        }
        $dateArray['date'] = array_combine($dateArray['date'],$dateArray['date']);
        //Month 
        $dateArray['month'] = array_reduce(range(1,12),function($rslt,$m){ $rslt[$m] = date('M',mktime(0,0,0,$m,10)); return $rslt; });
        $dateArray['month'] = array_combine($dateArray['month'],$dateArray['month']);
        //Years 
        $time = strtotime("-18 year", time());        
        $start_year = date("Y", $time);
        $range = array_reverse(range($start_year, 1930));
        $dateArray['year'] = array_combine($range, $range);
        return  $dateArray;
    }
    
    public function getDateArrayForBandUser(){
        $dateArray = [];
        //date
        for($i=1;$i<=31;$i++){
            $dateArray['date'][$i] = ($i<10) ? '0'.$i : $i;
        }
        $dateArray['date'] = array_combine($dateArray['date'],$dateArray['date']);
        //Month 
        $dateArray['month'] = array_reduce(range(1,12),function($rslt,$m){ $rslt[$m] = date('M',mktime(0,0,0,$m,10)); return $rslt; });
        $dateArray['month'] = array_combine($dateArray['month'],$dateArray['month']);
        //Years 
        $time = strtotime("-1 year", time());        
        $start_year = date("Y", $time);
        $range = array_reverse(range($start_year, 1930));
        $dateArray['year'] = array_combine($range, $range);
        return  $dateArray;
    }
}