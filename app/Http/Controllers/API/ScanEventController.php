<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\Repositories\ScanEventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\Banduser;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ScanEventAdminExport;
use App\Events\ScanEventOtpEvent;
use App\Events\ScanEventSmsEvent;
use App\Repositories\BanduserRepository;

class ScanEventController extends RestController {

    protected $scanEventRepository;
    private $banduserRepository;

    /**
     * 
     * @param BandTypeRepository $bandTypeRepository
     */
    public function __construct(ScanEventRepository $scanEventRepository, BanduserRepository $banduserRepository) {
        $this->scanEventRepository = $scanEventRepository;
        $this->banduserRepository = $banduserRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $this->scanEventRepository->loggedEvent();
            return view('scanevents.User.listing');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex() {
        try {
            $this->scanEventRepository->loggedEvent();
            return view('scanevents.admin.index');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
//            dd('here');
            $paramsData = $request->all();
//             dd($paramsData);
            $response = $this->scanEventRepository->userScanEventsList($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
//            dd($datatable);
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function adminList(Request $request) {
        try {
            $paramsData = $request->all();
            $response = $this->scanEventRepository->adminScanEventsList($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
//            dd($datatable);
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $postData = $request->all();
            $data = [];
            if (!empty($postData) && isset($postData['ip'])) {
                $data['ip_address'] = $postData['ip'];
            }
            if (is_array($postData)) {
                $data['json_data_by_ip'] = json_encode($postData);
            } else {
                $data['json_data_by_ip'] = $postData;
            }
            $data['scanned_on'] = date("Y-m-d H:i:s");
            $uniqueIDExistOrNot = $this->banduserRepository->getCountOfActiveBanduserByUniqueId($postData['uniqueID']);

            if ($uniqueIDExistOrNot > 0) {
                $return = $this->scanEventRepository->storeScanEvent($data);

                return $this->sendResponse($return, 'Scan Event has been created successfully.');
            } else {
                return $this->sendResponse($uniqueIDExistOrNot, "");
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $return = $this->scanEventRepository->showScanEvents($id);
            if (is_null($return)) {
                return $this->sendError('Scan Events not found.');
            }
            return $this->sendResponse($return, 'Scan Events fetched successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $postData = $request->all();
            if (isset($postData['banduser_uniqueid'])) {
                $getBanduserDetails = Banduser::where('bandusers.uniqueid', '=', $postData['banduser_uniqueid'])
                        ->select('bandusers.title', 'bandusers.firstname', 'bandusers.lastname', 'guardians.user_id')
                        ->join('guardians', 'guardians.id', 'bandusers.guardians_id')
                        ->get()
                        ->toArray();
                if (!empty($getBanduserDetails)) {

                    $postData['user_id'] = $getBanduserDetails[0]['user_id'];
                    $postData['banduser_name'] = $getBanduserDetails[0]['title'] . ' ' . $getBanduserDetails[0]['firstname'] . ' ' . $getBanduserDetails[0]['lastname'];
                }
            }
            if (isset($postData['latitude']) && !empty($postData['latitude']) && isset($postData['logitude']) && !empty($postData['logitude'])) {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $postData['latitude'] . ',' . $postData['logitude'] . '&key=AIzaSyDSYbGPyLqLSR_55pUJhZH50rSbSBdTk-8';

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($ch);
                curl_close($ch);

                $locationDetails = json_decode($response, true);
                if (isset($locationDetails['status']) && $locationDetails['status'] == 'OK') {
                    if (isset($locationDetails['results'][0]['formatted_address'])) {
                        $postData['location'] = $locationDetails['results'][0]['formatted_address'];
                    } else {
                        $postData['location'] = 'Not Capture';
                    }
                } else {
                    $postData['location'] = 'Not Capture';
                }
            }
            $return = $this->scanEventRepository->updateScanEvent($postData, $id);

            if (is_null($return)) {
                return $this->sendError('Scan Events not update.');
            }
            if (isset($postData['banduser_info_displayed'])) {
                $mailCheck = $this->scanEventRepository->getDetailsForMail($id);

                if (!empty($mailCheck)) {
                    $sms = [];
                    $maildata = $mailCheck[0];
                    $sms['uniqueid'] = $maildata['banduser_uniqueid'];
                    $sms['mobile'] = $maildata['banduser_mobile'];
                    $sms['scanner_mobile'] = $maildata['scanner_mobile'];
                    event(new ScanEventSmsEvent($sms));
                    $maildata['emailIds'] = $maildata['email'];
                    if (!empty($maildata['banduser_email'])) {
                        $maildata['emailIds'] = $maildata['emailIds'] . ',' . $maildata['banduser_email'];
                    }
                    if (!empty($maildata['banduser_alternate_email'])) {
                        $maildata['emailIds'] = $maildata['emailIds'] . ',' . $maildata['banduser_alternate_email'];
                    }
                    $maildata['subject'] = 'Scan Event Alert For - (' . $maildata['banduser_uniqueid'] . ' and ' . $maildata['banduser_name'] . ')';
                    Mail::send('emails.scanevent_notification_emails', ['maildata' => $maildata], function($message) use ($maildata) {
                        $message->subject($maildata['subject']);
                        $message->to(explode(',', $maildata['emailIds']));
                        $message->bcc('webassic3@gmail.com');
                    });
                }
            }
            return $this->sendResponse($return, 'Scan Events updated successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function scanEventGetOtp($mobile) {
        try {
//            dd($mobile);
            $data = array();
            if (!empty($mobile)) {

                $otp = $this->generateOtp();
                $data['mobile'] = $mobile;
                $data['otp'] = $otp;
                event(new ScanEventOtpEvent($data));
            }

            return $this->sendResponse($otp, 'OTP sent successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Display the eventlogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeEventLog() {
        try {
            $return = $this->scanEventRepository->getEventLogs();
            return $return;
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function exportAdminScanEvent() {
        try {
            $return = $this->scanEventRepository->exportAdminScanEvent();
            $dataCollect = [];
            $dataArray = [];
            if (!empty($return)) {
                foreach ($return as $value) {
                    $dataCollect['Guardian Name'] = $value['firstname'] . ' ' . $value['lastname'];
                    $dataCollect['Guardian Mobile'] = $value['mobile'];
                    $dataCollect['Guardian Email'] = $value['email'];
                    $dataCollect['Band user unique id'] = $value['banduser_uniqueid'];
                    $dataCollect['Band user name'] = $value['banduser_name'];
                    $dataCollect['Scanner Mobile'] = $value['scanner_mobile'];
                    $dataCollect['Scanned On'] = empty($value['scanned_on']) ? '' : date('d/m/Y H:i:s', strtotime($value['scanned_on']));
                    $dataCollect['Scanner Mobile Verified'] = $value['mobile_verify'];
                    $dataArray[] = $dataCollect;
                }
            } else {
                $dataCollect['Guardian Name'] = '';
                $dataCollect['Guardian Mobile'] = '';
                $dataCollect['Guardian Email'] = '';
                $dataCollect['Band user unique id'] = '';
                $dataCollect['Band user name'] = '';
                $dataCollect['Scanner Mobile'] = '';
                $dataCollect['Scanned On'] = '';
                $dataCollect['Scanner Mobile Verified'] = '';
                $dataArray[] = $dataCollect;
            }


            $headingArray = [
                'Guardian Name',
                'Guardian Mobile',
                'Guardian Email',
                'Band user unique id',
                'Band user name',
                'Scanner Mobile',
                'Scanned On',
                'Scanner Mobile Verified',
            ];
            $export = new ScanEventAdminExport([$dataArray],
                    [$headingArray]);
            return Excel::download($export, 'admin_scanevents.xlsx');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

}
