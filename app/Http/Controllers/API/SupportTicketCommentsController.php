<?php

namespace App\Http\Controllers\API;

use App\Repositories\SupportTicketsCommentsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Mail;

class SupportTicketCommentsController extends RestController {

    protected $supportTicketsCommentsRepository;

    /**
     * 
     * @param $supportTicketsCommentsRepository 
     */
    public function __construct(SupportTicketsCommentsRepository $supportTicketsCommentsRepository) {
        $this->supportTicketsCommentsRepository = $supportTicketsCommentsRepository;
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $postData = $request->all();
            $validator = Validator::make($postData, [
                        'message' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $return = $this->supportTicketsCommentsRepository->storeSupportTicketCommentDetails($postData);

            DB::commit();
            if (auth()->user()->user_type == "Administration") { // Send email to 
                $getUserDetails = $this->supportTicketsCommentsRepository->getUserDetails($postData['support_ticket_id']);

                if (!empty($getUserDetails)) {
                    $maildata = $getUserDetails[0];
                    $maildata['subject'] = 'Support Ticket Alert For - ' . $maildata['unique_ticket_id'];
                    $maildata['support_ticket_id'] = $postData['support_ticket_id'];
                    $maildata['adminMessage'] = $postData['message'];

                    Mail::send('emails.support_ticket_comment_notification_emails', ['maildata' => $maildata], function($message) use ($maildata) {
                        $message->subject($maildata['subject']);
                        $message->to($maildata['email']);
                        $message->bcc('webassic1@gmail.com');
                    });
                }
            }
            return $this->sendResponse($return, 'Support Tickets Comment Created Successfully.');
        } catch (\Exception $ex) {
            DB::rollback();
        }
    }

    /**
     * getSupportTicketCommentsList(saperate view)
     * @return type
     */
    public function getSupportTicketCommentsList($id) {
        $result = $this->supportTicketsCommentsRepository->getSupportTicketCommentsList($id);
        return view('support_tickets.user.support_ticket_comments_list')->with(array('supportTicketCommentsListArray' => $result));
    }

}

?>