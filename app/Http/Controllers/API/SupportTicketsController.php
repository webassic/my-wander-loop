<?php

namespace App\Http\Controllers\API;

use App\Repositories\SupportTicketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HelpDeskManagementAdminExport;

class SupportTicketsController extends RestController {

    protected $supportTicketRepository;

    /**
     * 
     * @param $supportTicketRepository 
     */
    public function __construct(SupportTicketRepository $supportTicketRepository) {
        $this->supportTicketRepository = $supportTicketRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            return view('support_tickets.user.listing');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex() {
        try {
            return view('support_tickets.admin.index');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function adminList(Request $request) {
        try {
            $paramsData = $request->all();
            $response = $this->supportTicketRepository->adminSuportTicketsList($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminshow($id) {
        try {
            $return = $this->supportTicketRepository->adminShowSupportTickets($id);
            if (is_null($return)) {
                return $this->sendError('Support Tickets not found.');
            }
            return $this->sendResponse($return, 'Support Tickets fetched successfully.');
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminViewPage($id) {
        try {
            $supportid = $id;
            return view('support_tickets.admin.admin_support_ticket_view', compact('supportid'));
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     */
    public function add() {
        try {
            $usersList = $this->supportTicketRepository->getAllBandUserList();
            $data = [];
            if (isset($usersList) && !empty($usersList)) {
                foreach ($usersList as $value) {
                    $data['usersList'][$value['id']] = $value['user_name'];
                }
            }
            return view('support_tickets.user.add')->with(array('data' => $data));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
            $paramsData = $request->all();
            $response = $this->supportTicketRepository->userSupportTicketsList($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $postData = $request->getContent();
            if (is_string($request->getContent())) {
                $postData = json_decode($request->getContent(), true);
            }
            unset($postData['_token']);
            $validator = Validator::make($postData, [
                        'subject' => 'required',
                        'related_to' => 'required',
                        'message' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->supportTicketRepository->storeSupportTicketDetails($postData);
            DB::commit();
            return $this->sendResponse($return, 'Support Tickets Created Successfully.');
        } catch (\Exception $ex) {
            DB::rollback();
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function show($id) {
        try {
            $response = $this->supportTicketRepository->showSupportTicketDetails($id);
            if (is_null($response)) {
                return $this->sendError('Support Ticket Details not found.');
            }
            return view('support_tickets.user.view')->with(array('data' => $response));
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * updateStatus
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id, Request $request) {
        try {
            $postData = $request->all();
            \Log::info('$postData >> ' . print_r($postData, true));
            $return = $this->supportTicketRepository->updateResolvedStatus($postData);
            if (is_null($return)) {
                return $this->sendError('Support Tickets not found.');
            }
            if ($postData['supportTicketStatus'] == "resolved") {
                return $this->sendResponse($return, 'Support Ticket Resolved Successfully.');
            } else {
                return $this->sendResponse($return, 'Support Ticket Closed Successfully.');
            }
        } catch (\Expectional $exc) {
            \Log::error($exc->getTraceAsString());
            return $this->sendError('Expectional Error.', $exc->getMessage(), 500);
        }
    }
    /**
     * 
     * @return type
     */
    public function getEventLog() {
        try {
            $return = $this->supportTicketRepository->getEventLog();
            return $return;
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }
    /**
     * 
     * @param type $param
     * @return type
     */
    public function exportAdminHelpDesk() {
         try {
            $return = $this->supportTicketRepository->exportAdminHelpDesk();
            $dataCollect = [];
            $dataArray = [];
            if(!empty($return)){
                foreach($return as $value){
                $dataCollect['Ticket Unique Id'] = $value['unique_ticket_id'];
                $dataCollect['Created By'] = $value['firstname'].' '.$value['lastname'];
                $dataCollect['Subject'] = $value['subject'];
                $dataCollect['Related To'] = ucwords($value['related_to']);
                $dataCollect['Status'] = $value['status'];
                $dataCollect['Created On'] = empty($value['created_at'])?'' : date('d/m/Y',strtotime($value['created_at']));
                $dataArray[]= $dataCollect;
                }
            }else{
                $dataCollect['Ticket Unique Id'] = '';
                $dataCollect['Created By'] ='';
                $dataCollect['Subject'] = '';
                $dataCollect['Related To'] = '';
                $dataCollect['Status'] = '';
                $dataCollect['Created On'] = '';
                $dataArray[]= $dataCollect;
            }
            
            
            $headingArray=[
                'Ticket Unique Id',
                'Created By',
                'Subject',
                'Related To',
                'Status',
                'Created On'
            ];
            $export = new HelpDeskManagementAdminExport([$dataArray],
                        [$headingArray]);
           return Excel::download($export, 'admin_helpdesk.xlsx');
            
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

}
