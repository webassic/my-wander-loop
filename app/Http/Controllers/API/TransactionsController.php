<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Banduser;
use App\Repositories\BanduserRepository;
use Illuminate\Validation\Rule;
use DB;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Mail;
use App\Repositories\RazorpayCredentialsRepository;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\BadRequestError;
use App\Exports\BanduserTransactionExport;
use Maatwebsite\Excel\Facades\Excel;

class TransactionsController extends RestController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $banduser;
    private $banduserRepository;
    private $razorpayCredentialsRepository;

    public function __construct(Banduser $banduser, BanduserRepository $banduserRepository, RazorpayCredentialsRepository $razorpayCredentialsRepository) {
        $this->banduser = $banduser;
        $this->banduserRepository = $banduserRepository;
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
    }

    /**
     * Display a listing of the resource.
     * bandUserTransactions
     * @return \Illuminate\Http\Response
     */
    public function bandUserTransactions() {
        try {
            return view('banduser_transactions.index');
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * bandUserTransactionListing
     * @param Request $request
     * @return type
     */
    public function bandUserTransactionListing(Request $request) {
        try {
            $postData = $request->all();
            $response = $this->banduserRepository->bandusersTransactionListing($postData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return json_encode($datatable);
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * cancelSubscription
     * @return type
     * @throws \Exception
     */
    public function cancelSubscription($id) {
        try {
            $bandUserDetails = $this->banduserRepository->cancelSubscription($id);
            if ($bandUserDetails > 0) {
                return redirect('transactions/index')->with('message', 'Cancel subscription successfully.');
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * downloadInvoice
     * @return type
     * @throws \Exception
     */
    public function downloadInvoice($id,$paymentId) {
        try {
            $invoice = $this->banduserRepository->downloadInvoice($id,$paymentId);
            view()->share(compact('invoice'));
            $pdf = \PDF::loadView('banduser_transactions.invoice');
            $fileName = "Invoice-" . $invoice['invoiceNumber'] . ".pdf";
            return $pdf->download($fileName);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * exportBanduserTransactions
     * @return type
     */
    public function exportBanduserTransactions() {
        try {
            $response = $this->banduserRepository->exportBanduserTransactions();
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Name'] = $value['firstname'] . " " . $value['lastname'];
                    $banduserDataCollect['Payment Date'] = empty($value['payment_date']) ? '' : date('d/m/Y', strtotime($value['payment_date']));
                    $banduserDataCollect['Payment ID'] = $value['payment_id'];
                    $banduserDataCollect['Subscription Start Date'] = empty($value['start_date']) ? '' : date('d/m/Y', strtotime($value['start_date']));
                    $banduserDataCollect['Subscription End Date'] = empty($value['end_date']) ? '' : date('d/m/Y', strtotime($value['end_date']));
                    $banduserDataCollect['Invoice Number'] = $value['invoice_number'];
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Name'] = '';
                $banduserDataCollect['Payment Date'] = '';
                $banduserDataCollect['Payment ID'] = '';
                $banduserDataCollect['Subscription Start Date'] = '';
                $banduserDataCollect['Subscription End Date'] = '';
                $banduserDataCollect['Invoice Number'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }

            $headingArray = [
                'Name',
                'Payment Date',
                'Payment ID',
                'Subscription Start Date',
                'Subscription End Date',
                'Invoice Number'
            ];

            $export = new BanduserTransactionExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'banduser_transactions.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Display a listing of the resource.
     * adminBandUserTransactions
     * @return \Illuminate\Http\Response
     */
    public function adminBandUserTransactions() {
        try {
            return view('banduser_transactions.admin_index');
        } catch (\Expectional $ex) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * exportAdminBanduserTransactions
     * @return type
     */
    public function exportAdminBanduserTransactions() {
        try {
            $response = $this->banduserRepository->exportBanduserTransactions();
            $banduserDataCollect = [];
            $banduserDataArray = [];
            if (!empty($response)) {
                foreach ($response as $value) {
                    $banduserDataCollect['Guardian Name'] = $value['ufirstname'] . " " . $value['ulastname'];
                    $banduserDataCollect['Band User Name'] = $value['firstname'] . " " . $value['lastname'];
                    $banduserDataCollect['Payment Date'] = empty($value['payment_date']) ? '' : date('d/m/Y', strtotime($value['payment_date']));
                    $banduserDataCollect['Payment ID'] = $value['payment_id'];
                    $banduserDataCollect['Subscription Start Date'] = empty($value['start_date']) ? '' : date('d/m/Y', strtotime($value['start_date']));
                    $banduserDataCollect['Subscription End Date'] = empty($value['end_date']) ? '' : date('d/m/Y', strtotime($value['end_date']));
                    $banduserDataCollect['Invoice Number'] = $value['invoice_number'];
                    $banduserDataArray[] = $banduserDataCollect;
                }
            } else {
                $banduserDataCollect['Guardian Name'] = '';
                $banduserDataCollect['Name'] = '';
                $banduserDataCollect['Payment Date'] = '';
                $banduserDataCollect['Payment ID'] = '';
                $banduserDataCollect['Subscription Start Date'] = '';
                $banduserDataCollect['Subscription End Date'] = '';
                $banduserDataCollect['Invoice Number'] = '';
                $banduserDataArray[] = $banduserDataCollect;
            }

            $headingArray = [
                'Guardian Name',
                'Name',
                'Payment Date',
                'Payment ID',
                'Subscription Start Date',
                'Subscription End Date',
                'Invoice Number'
            ];

            $export = new BanduserTransactionExport([$banduserDataArray],
                    [$headingArray]);
            return Excel::download($export, 'banduser_transactions.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }

}
