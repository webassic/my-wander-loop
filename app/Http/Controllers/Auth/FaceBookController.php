<?php namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
use Illuminate\Auth\AuthServiceProvider;
use App\Repositories\UserRepository;
class FaceBookController extends Controller { 	 
use AuthenticatesUsers;
    /*** Where to redirect users after login. 
	** @var string 
	*/
//    protected $redirectTo = '/home';
//    
//    public function __construct() {
//        $this->middleware('guest')->except('logout');
//    }

    protected $user_repository;
    public function __construct(UserRepository $user_repository) {
        $this->user_repository = $user_repository;
    }
    public function redirectToFacebook() {
        \Log::info('Comes in redirect to facebook');
        return Socialite::driver('facebook')->redirect();
    }
    public function handleFacebookCallback() {
        try {
            $user = Socialite::driver('facebook')->user();
            \Log::info('$user Controller >> ' . print_r($user, true)); 
            $finduser = User::where('facebook_id', $user->id)->first();
            \Log::info('$finduser >> ' . print_r($finduser, true));
            if ($finduser) {
                \Log::info('In iff');
                Auth::login($finduser);
                return redirect('/home');
            } else {
                \Log::info('In else');
//                $newUser = User::create(['name' => $user->name, 'email' => $user->email, 'facebook_id' => $user->id]);
                $newUser = $this->user_repository->registerFaceBookUser($user);
                \Log::info('$newUser >> ' . print_r($newUser, true));
                Auth::login($newUser);
                return redirect()->back();
            }
        }
        catch(Exception $e) {
            return redirect('auth/facebook');
        }
    }
} ?>