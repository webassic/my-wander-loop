<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\RestController;

class LoginController extends RestController {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
        $this->middleware('checkstatus')->except('logout');
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function login(Request $request) {
        try {
            $checkEmailExist = \App\User::where('email', $request->email)->first();    
//            
            if(!empty($checkEmailExist)){
                if(empty($checkEmailExist['email_verification_code']) && $checkEmailExist['email_verify']=='yes'){
                    if($checkEmailExist['status'] == 'active'){
                        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                            $user = Auth::user();
                            $token = $user->createToken('MywanderWeb')->accessToken;
                            session([
                                'access_token' => $token
                            ]);
                            return redirect('/dashboard');
                        }else {
                            $error = 'Invalid credentials';
                            return redirect('/login')->with(['error' => $error]);
                        }
                    }else{
                        $error = 'Oops! Your account is inactive. Kindly contact us to know more';
                        return redirect('/login')->with(['error' => $error]);
                    }
                    
                }else{
                    $error = 'Oops! Your email verification is pending. Please check your registered email inbox for email verification mail.';
                    return redirect('/login')->with(['error' => $error]);
                }
            }else{
                $error = 'Your credentials are invalid  OR Your account is inactive. Kindly contact us to know more';
                    return redirect('/login')->with(['error' => $error]);
            }
            
            
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function logout(Request $request) {
        try {
            Auth::guard('web')->logout();
            return redirect("/login")->with('message', 'Successfully Logged Out');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
