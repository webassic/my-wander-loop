<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use App\Http\Controllers\API\GuardiansController;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_type=='User'){
            $exists = \App\Models\Guardian::where('user_id',Auth::user()->id)->count();
            return redirect('/guardians');
//            if(empty($exists)){
//                return redirect('/guardians');
//            }else{
//                return view('home');
//            }
        }
        return view('home');
        
    }
}
