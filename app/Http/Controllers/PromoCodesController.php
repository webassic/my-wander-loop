<?php

namespace App\Http\Controllers;

use App\Repositories\PromoCodesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use DB;
use Validator;

class PromoCodesController extends RestController {

    protected $promoCodesRepository;

    /**
     * 
     * @param PromoCodesRepository $promoCodesRepository
     */
    public function __construct(PromoCodesRepository $promoCodesRepository) {
        $this->promoCodesRepository = $promoCodesRepository;
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function index() {
        try {
            return view('promocodes.index');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     */
    public function add() {
        try {
            $usersList = $this->promoCodesRepository->getAllUsersList();
            $data = [];
            if (isset($usersList) && !empty($usersList)) {
                foreach ($usersList as $value) {
                    $data['usersList'][$value['id']] = $value['user_name'];
                }
            }
            return view('promocodes.add')->with(array('data' => $data));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $content = $request->getContent();
            if (is_string($request->getContent())) {
                $content = json_decode($request->getContent(), true);
            }
            $validator = Validator::make($content, [
                        'name' => 'required',
                        'valid_till' => 'required',
                        'discount_offered_type' => 'required',
                        'discount_offered' => 'required',
                        'user_id' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $return = $this->promoCodesRepository->storePromoCodeDetails($content);
            DB::commit();
            return $this->sendResponse($return, 'Promo Code Created Successfully.');
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return response(json_encode(['error' => 'Promo Code Name and Validity Date Combination Already Exists , Please enter valid details.']))
                                ->setStatusCode(409, $ex->getMessage());
            } else {
                return response(json_encode(['error' => $ex->getMessage()]))
                                ->setStatusCode($ex->getCode(), $ex->getMessage());
            }
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][5]['search']['value']) ? $_GET['columns'][5]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $response = $this->promoCodesRepository->getAllPromoCodes($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function show($id) {
        try {
            $response = $this->promoCodesRepository->showPromoCodeDetails($id);
            if (is_null($response)) {
                return $this->sendError('Promo Code Details not found.');
            }
            return $this->sendResponse($response, 'Promo Code Details not found.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function edit($id) {
        try {
            $usersList = $this->promoCodesRepository->getAllUsersList();
            $data = [];
            if (isset($usersList) && !empty($usersList)) {
                foreach ($usersList as $value) {
                    $data['usersList'][$value['id']] = $value['user_name'];
                }
            }
            $promoCodeDetails = $this->promoCodesRepository->showPromoCodeDetails($id);
            if (isset($promoCodeDetails) && !empty($promoCodeDetails)) {
                $usersId = array();
                if (!empty($promoCodeDetails['promo_code_users']) && isset($promoCodeDetails['promo_code_users'])) {
                    foreach ($promoCodeDetails['promo_code_users'] as $key => $val) {
                        $userIds[] = $val['users_id'];
                    }
                }
                $promoCodeDetails['user_ids'] = implode(",", $userIds);
            }
            return view('promocodes.edit')->with(array('promoCodeDetails' => $promoCodeDetails,
                        'data' => $data));
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return type
     */
    public function update(Request $request) {
        try {
            DB::beginTransaction();
            $content = $request->getContent();
            if (is_string($request->getContent())) {
                $content = json_decode($request->getContent(), true);
            }
            $validator = Validator::make($content, [
                        'name' => 'required',
                        'valid_till' => 'required',
                        'discount_offered' => 'required',
                        'user_id' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->promoCodesRepository->updatePromoCodeDetails($content);
            if (is_null($return)) {
                return $this->sendError('Promo Code Details not update.');
            }
            DB::commit();
            return $this->sendResponse($return, 'Promo Code Details has been updated successfully.');
        } catch (\Expectional $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return response(json_encode(['error' => 'Promo Code Name and Validity Date Combination Already Exists , Please enter valid details.']))
                                ->setStatusCode(409, $ex->getMessage());
            } else {
                return response(json_encode(['error' => $ex->getMessage()]))
                                ->setStatusCode($ex->getCode(), $ex->getMessage());
            }
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroy($id) {
        try {
            $this->promoCodesRepository->destroyPromoCodeDetails($id);

            return $this->sendResponse([], 'Promo Code Details has been deleted successfully.');
        } catch (\Exception $ex) {
            throw $ex;
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

//    public function checkValidPromoCode(Request $request) {
//        $paramsData = $request->all();
//        $return = $this->promoCodesRepository->checkValidPromoCode($paramsData['valid_promo_code']);
//        if (isset($return) && !empty($return)) {
//            $return = $return->toArray();
//            $totalAmount = explode(" ", $paramsData['total_amount']);
//            if ($return['discount_offered_type'] == '%') {
//                $percentAmt = ($return['discount_offered'] / 100) * $totalAmount[1];
//                $grandTotalAmt = ($totalAmount[1] - $percentAmt);
//            } else {
//                $grandTotalAmt = ($totalAmount[1] - $return['discount_offered']);
//            }
//            $return['grandTotalAmt'] = '₹ ' . $grandTotalAmt;
//            return $this->sendResponse($return, 'Promo Code apply successfully.');
//        } else {
//            $return['grandTotalAmt'] = $paramsData['total_amount'];
//            return $this->sendResponse($return, 'Invalid Promo Code.');
//        }
//    }

}
