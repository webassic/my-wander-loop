<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\API\RestController;
use Auth;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LeadsExport;
use App\Exports\ActiveCustomerExport;
use App\Exports\InactiveCustomerExport;
use App\Exports\AdminUserExport;

class UserController extends RestController {

    protected $successStatus = 200;
    protected $user_repository;
    protected $request_call;

    public function __construct(UserRepository $user_repository) {
        $this->user_repository = $user_repository;
    }

    /**
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register(Request $request) {
        try {

            $postData = $request->all();
//            dd($postData);
            $validator = Validator::make($request->all(), [
                        'firstname' => 'required|max:255',
                        'lastname' => 'required|max:255',
                        'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                        'mobile' => 'required|numeric|unique:users,mobile,NULL,id,deleted_at,NULL',
                        'password' => 'required',
                        'confirmed_password' => 'required|same:password',
                        'g-recaptcha-response' => 'required'
            ]);
            if ($validator->fails()) {
//                if($this->request_call){
                return $this->sendError('Validation Error.', $validator->errors());
//                }   
            }
            if (is_string($postData)) {
                $postData = json_decode($postData, true);
            }
            //Default user type is User
            $postData['user_type'] = 'User';
            $response = $this->user_repository->registerUser($postData);
            if (isset($response['success'])) {
                $this->successStatus = 200;
                $msgkey = 'success';
                $message = $response['success'];
                return $this->sendResponse([], $message);
            } else if (isset($response['error'])) {
                $this->successStatus = 400;
                $msgkey = 'error';
                $message = $response['error'];
                return $this->sendError($message, [], 400);
            } else {
                $this->successStatus = 400;
                $msgkey = 'error';
                $message = 'Something went wrong';
                return $this->sendError($message, [], 400);
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $postData = $request->all();
            $validator = Validator::make($postData, [
                        'firstname' => 'required',
                        'lastname' => 'required',
                        'email'=>'required|unique:users,email,'.$id.',id,deleted_at,NULL',
                        'mobile'=>'required|numeric|unique:users,mobile,'.$id.',id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            unset($postData['_token']);
            $return = $this->user_repository->UpdateUser($postData, $id);
            if (is_null($return)) {
                return $this->sendError('User not update.');
            }
            return $this->sendResponse($return, 'User has been updated successfully.');
        } catch (\Expectional $exc) {
            \Log::error($ex->getTraceAsString());
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function index() {
        try {
            return view('users.index');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function payingCustomerIndex() {
        try {
            return view('users.paying_customers');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    public function customerIndex() {
        try {
            return view('users.customers');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    public function inactiveCustomerIndex() {
        try {
            return view('users.inactive_customers');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function listings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][5]['search']['value']) ? $_GET['columns'][5]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $userTypFilter = isset($_GET['columns'][4]['search']['value']) ? $_GET['columns'][4]['search']['value'] : '';
            if (isset($userTypFilter) && !empty($userTypFilter) && $userTypFilter != '') {
                $paramsData['user_type'] = $userTypFilter;
            }
            $response = $this->user_repository->getAllAdminUsers($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function payingCustomerListings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][5]['search']['value']) ? $_GET['columns'][5]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $userTypFilter = isset($_GET['columns'][4]['search']['value']) ? $_GET['columns'][4]['search']['value'] : '';
            if (isset($userTypFilter) && !empty($userTypFilter) && $userTypFilter != '') {
                $paramsData['user_type'] = $userTypFilter;
            }
            $response = $this->user_repository->getAllUsers($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    public function customerListings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][5]['search']['value']) ? $_GET['columns'][5]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $userTypFilter = isset($_GET['columns'][4]['search']['value']) ? $_GET['columns'][4]['search']['value'] : '';
            if (isset($userTypFilter) && !empty($userTypFilter) && $userTypFilter != '') {
                $paramsData['user_type'] = $userTypFilter;
            }
            $response = $this->user_repository->getAllCustomers($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    /**
     * 
     * @param Request $request
     * @return type
     * @throws \Exception
     */
    public function inactiveCustomerListings(Request $request) {
        try {
            $paramsData = $request->all();
            $statusFilter = isset($_GET['columns'][5]['search']['value']) ? $_GET['columns'][5]['search']['value'] : '';
            if (isset($statusFilter) && !empty($statusFilter) && $statusFilter != '') {
                $paramsData['status'] = $statusFilter;
            }
            $userTypFilter = isset($_GET['columns'][4]['search']['value']) ? $_GET['columns'][4]['search']['value'] : '';
            if (isset($userTypFilter) && !empty($userTypFilter) && $userTypFilter != '') {
                $paramsData['user_type'] = $userTypFilter;
            }
            $response = $this->user_repository->getAllInactiveCustomers($paramsData);
            $datatable = [];
            $datatable['data'] = isset($response['records']) ? $response['records'] : [];
            $datatable['recordsTotal'] = isset($response['total_count']) ? $response['total_count'] : 0;
            $datatable['recordsFiltered'] = isset($response['total_count']) ? $response['total_count'] : 0;
            return response()->json($datatable);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function show($id) {
        try {
            $response = $this->user_repository->showUserDetails($id);
            if (is_null($response)) {
                return $this->sendError('User Details not found.');
            }
            return view('users.admin_users_view')->with('data', $response);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function authorizationErrorIndex() {
        try {
            return view('errors.authorization_error')->with('authenticationErrorMessage', 'You are not authorized to access this resource.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function adminProfileDetails($id) {
        try {
            $response = $this->user_repository->adminProfileDetails($id);
            if (is_null($response)) {
                return $this->sendError('User Details not found.');
            }
            return view('users.adminProfileView')->with('data', $response);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function userProfile() {
        try {

            return view('users.userProfileView');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function userProfileDetails($id) {
        try {
            $response = $this->user_repository->userProfileDetails($id);
            if (is_null($response)) {
                return $this->sendError('User Details not found.');
            }
            return $this->sendResponse($response, 'User Details has been fetched successfully.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @param type $status
     * @return type
     * @throws \Exception
     */
    public function updateStatus($id, $status) {
        try {
            $this->user_repository->updateUserStatus($id, $status);
            if ($status == 'active') {
                return $this->sendResponse([], 'Customer Activated Successfully.');
            } else if ($status == 'inactive') {
                return $this->sendResponse([], 'Customer Deactivated Successfully.');
            } else {
                return $this->sendResponse([], 'Customer Status Updated Successfully.');
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    /**
     * 
     * @param type $id
     * @param type $status
     * @return type
     * @throws \Exception
     */
    public function updateStatusForLeads($id, $status) {
        try {
            $this->user_repository->updateUserStatus($id, $status);
            if ($status == 'active') {
                return $this->sendResponse([], 'Leads Activated Successfully.');
            } else if ($status == 'inactive') {
                return $this->sendResponse([], 'Leads Deactivated Successfully.');
            } else {
                return $this->sendResponse([], 'Leads Status Updated Successfully.');
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     */
    public function getOtp() {
        $otp = '';
        $otp = $this->generateOtp();
        return $otp;
    }

    public function activateUserAccount($verification_code) {
        try {
            $response = $this->user_repository->getUserIdAndUpdate($verification_code);
            if ($response == 1) {

                return redirect('/login')->with('message', 'Your email id verifired successfully. Please login to your MyWanderLoop account.');
            }
            return redirect('/login')->with('message', 'Your email id verifired successfully. Please login to your MyWanderLoop account.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     */
    public function add() {
        try {
            return view('users.add');
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Add admin users
     * @return type
     * @throws \Exception
     */
    public function store(Request $request) {
        try {
            $postData = $request->all();
            $validator = Validator::make($postData, [
                        'firstname' => 'required',
                        'lastname' => 'required',
                        'email' => 'required|unique:users,email,NULL,id,deleted_at,NULL',
                        'mobile' => 'required|numeric|unique:users,mobile,NULL,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $return = $this->user_repository->storeAdminUser($postData);
            return $this->sendResponse($return, 'Admin User has been created successfully.');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     */
    public function edit($id) {
        try {
            $response = $this->user_repository->adminProfileDetails($id);
            if (is_null($response)) {
                return $this->sendError('User Details not found.');
            }
            return view('users.edit')->with('data', $response);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $return = $this->user_repository->destroyAdminUser($id);
            return $return;
        } catch (\Exception $ex) {
            return $this->sendError('Expectional Error.', $ex->getMessage(), 500);
        }
    }

    /**
     * Update MyAccount Password
     * @param Request $request
     */
    public function updatePassword(Request $request) {
        try {
            $data = $request->all();
            if (!empty($data)) {
                $response = $this->user_repository->updatePassword($data);
                if ($response['status_code'] == 200) {
                    echo $response;
                    exit;
                } else {
                    echo $response;
                    exit;
                }
            }
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }
    
    public function exportLeads(){
        try {
            $response = $this->user_repository->exportLeads();
            $leadsDataCollect = [];
            $leadsDataArray = [];
            if(!empty($response)){
                foreach($response as $value){
                $leadsDataCollect['Firstname'] = $value['firstname'];
                $leadsDataCollect['Lastname'] = $value['lastname'];
                $leadsDataCollect['Email'] = $value['email'];
                $leadsDataCollect['Mobile'] = $value['mobile'];
                $leadsDataCollect['Role'] = $value['user_type'];
                $leadsDataCollect['Status'] = $value['status'];
                $leadsDataCollect['Created On'] =  empty($value['created_at'])?'' : date('d/m/Y',strtotime($value['created_at']));
                $leadsDataArray[]= $leadsDataCollect;
                }
            }else{
                $leadsDataCollect['Firstname'] = '';
                $leadsDataCollect['Lastname'] ='';
                $leadsDataCollect['Email'] = '';
                $leadsDataCollect['Mobile'] = '';
                $leadsDataCollect['Role'] = '';
                $leadsDataCollect['Status'] = '';
                $leadsDataCollect['Created On'] =  '';
                $leadsDataArray[]= $leadsDataCollect;
            }
            
            
            $headingArray=[
                'Firstname',
                'Lastname',
                'Email',
                'Mobile',
                'Role',
                'Status',
                'Created On'
            ];
            $export = new LeadsExport([$leadsDataArray],
                        [$headingArray]);
           return Excel::download($export, 'leads.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }
    public function exportActiveCustomer(){
        try {
            $response = $this->user_repository->exportActiveCustomer();
            $leadsDataCollect = [];
            $leadsDataArray = [];
            if(!empty($response)){
                foreach($response as $value){
                $leadsDataCollect['Firstname'] = $value['firstname'];
                $leadsDataCollect['Lastname'] = $value['lastname'];
                $leadsDataCollect['Email'] = $value['email'];
                $leadsDataCollect['Mobile'] = $value['mobile'];
                $leadsDataCollect['Role'] = $value['user_type'];
                $leadsDataCollect['Status'] = $value['status'];
                $leadsDataCollect['Created On'] =  empty($value['created_at'])?'' : date('d/m/Y',strtotime($value['created_at']));
                $leadsDataArray[]= $leadsDataCollect;
                }
            }else{
                $leadsDataCollect['Firstname'] = '';
                $leadsDataCollect['Lastname'] ='';
                $leadsDataCollect['Email'] = '';
                $leadsDataCollect['Mobile'] = '';
                $leadsDataCollect['Role'] = '';
                $leadsDataCollect['Status'] = '';
                $leadsDataCollect['Created On'] =  '';
                $leadsDataArray[]= $leadsDataCollect;
            }
            
            
            $headingArray=[
                'Firstname',
                'Lastname',
                'Email',
                'Mobile',
                'Role',
                'Status',
                'Created On'
            ];
            $export = new ActiveCustomerExport([$leadsDataArray],
                        [$headingArray]);
           return Excel::download($export, 'activecustomers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }
    public function exportInactiveCustomer(){
        try {
            $response = $this->user_repository->exportInactiveCustomer();
            $leadsDataCollect = [];
            $leadsDataArray = [];
            if(!empty($response)){
                foreach($response as $value){
                $leadsDataCollect['Firstname'] = $value['firstname'];
                $leadsDataCollect['Lastname'] = $value['lastname'];
                $leadsDataCollect['Email'] = $value['email'];
                $leadsDataCollect['Mobile'] = $value['mobile'];
                $leadsDataCollect['Role'] = $value['user_type'];
                $leadsDataCollect['Status'] = $value['status'];
                $leadsDataCollect['Created On'] =  empty($value['created_at'])?'' : date('d/m/Y',strtotime($value['created_at']));
                $leadsDataArray[]= $leadsDataCollect;
                }
            }else{
                $leadsDataCollect['Firstname'] = '';
                $leadsDataCollect['Lastname'] ='';
                $leadsDataCollect['Email'] = '';
                $leadsDataCollect['Mobile'] = '';
                $leadsDataCollect['Role'] = '';
                $leadsDataCollect['Status'] = '';
                $leadsDataCollect['Created On'] =  '';
                $leadsDataArray[]= $leadsDataCollect;
            }
            
            
            $headingArray=[
                'Firstname',
                'Lastname',
                'Email',
                'Mobile',
                'Role',
                'Status',
                'Created On'
            ];
            $export = new InactiveCustomerExport([$leadsDataArray],
                        [$headingArray]);
           return Excel::download($export, 'admin_users.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }
    public function adminusersExport(){
        try {
            $response = $this->user_repository->adminusersExport();
            $leadsDataCollect = [];
            $leadsDataArray = [];
            if(!empty($response)){
                foreach($response as $value){
                $leadsDataCollect['Firstname'] = $value['firstname'];
                $leadsDataCollect['Lastname'] = $value['lastname'];
                $leadsDataCollect['Email'] = $value['email'];
                $leadsDataCollect['Mobile'] = $value['mobile'];
                $leadsDataCollect['Role'] = $value['user_type'];
                $leadsDataCollect['Status'] = $value['status'];
                $leadsDataCollect['Created On'] =  empty($value['created_at'])?'' : date('d/m/Y',strtotime($value['created_at']));
                $leadsDataArray[]= $leadsDataCollect;
                }
            }else{
                $leadsDataCollect['Firstname'] = '';
                $leadsDataCollect['Lastname'] ='';
                $leadsDataCollect['Email'] = '';
                $leadsDataCollect['Mobile'] = '';
                $leadsDataCollect['Role'] = '';
                $leadsDataCollect['Status'] = '';
                $leadsDataCollect['Created On'] =  '';
                $leadsDataArray[]= $leadsDataCollect;
            }
            
            
            $headingArray=[
                'Firstname',
                'Lastname',
                'Email',
                'Mobile',
                'Role',
                'Status',
                'Created On'
            ];
            $export = new AdminUserExport([$leadsDataArray],
                        [$headingArray]);
           return Excel::download($export, 'adminusers.xlsx');
        } catch (\Exception $e) {
            return response(json_encode(['error' => $e->getMessage()]))
                            ->setStatusCode($e->getCode(), $e->getMessage());
        }
    }
}
