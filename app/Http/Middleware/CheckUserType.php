<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckUserType {

    public function __construct(User $modelUser) {
        $this->modelUser = $modelUser;
    }

    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try {
            $route = $request->route();
            $actions = $route->getAction();
            $loggedInUserDetails = auth()->user();
            if (isset($loggedInUserDetails) && !empty($loggedInUserDetails)) {
                if (isset($loggedInUserDetails['user_type']) && !empty($loggedInUserDetails['user_type'])) {
                    if (in_array(strtolower($loggedInUserDetails['user_type']), $actions['middleware'])) {
                        return $next($request);
                    } else {
                        return redirect()->to('/authorization-error');
                    }
                } else {
                    return redirect()->to('/authorization-error');
                }
            } else {
                return redirect()->to('/authorization-error');
            }
        } catch (\Exception $ex) {
            return response(json_encode(['error' => $ex->getMessage()]))
                            ->setStatusCode(405, $ex->getMessage());
        }
        return response(json_encode(['error' => 'You are not authorized to access this resource.']))
                        ->setStatusCode(404, 'You are not authorized to access this resource.');
    }

}
