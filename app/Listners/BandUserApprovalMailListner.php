<?php

namespace App\Listners;

use App\Events\BandUserApprovalMailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail; 

class BandUserApprovalMailListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BandUserApprovalMailEvent  $event
     * @return void
     */
    public function handle(BandUserApprovalMailEvent $event)
    {
        $mailCheck = $event->mailCheck;
        //for sending mail
        if (!empty($mailCheck)) {
                $maildata = $mailCheck[0];
                $maildata['emailIds'] = $maildata['email'];
                $maildata['subject'] = 'Band User Approved Notification';
                Mail::send('emails.banduser_approved_notification_emails', ['maildata' => $maildata], function($message) use ($maildata) {
                    $message->subject($maildata['subject']);
                    $message->to($maildata['emailIds']);
                    $message->bcc('webassic3@gmail.com');
                });
            }
    }
}
