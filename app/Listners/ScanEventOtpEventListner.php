<?php

namespace App\Listners;

use App\Events\ScanEventOtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ScanEventOtpEventListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScanEventOtpEvent  $event
     * @return void
     */
    public function handle(ScanEventOtpEvent $event)
    {
        $userDetails = $event->userDetails;
        \Log::error('Scan Event OTP Data'.print_r($userDetails,true));
        // Account details
	$apiKey = urlencode(config('app.localtext_api_key'));
	
	// Message details
	$numbers = array($userDetails['mobile']);
	$sender = urlencode(config('app.localtext_sender'));
	$message = rawurlencode($userDetails['otp']." is your OTP to access MyWanderLoop's Band User information.");
 
	$numbers = implode(',', $numbers);
 
	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	
	// Process your response here
	
        $response  =json_decode($response,true);

        if(array_key_exists("errors",$response)){
            
             \Log::error('SMS Response Error : '.print_r($response,true));
        }else{
             
            \Log::info('SMS Response : '.print_r($response,true)); 
        }
	// Process your response here
         \Log::info('Otp --> Mobile '.$userDetails['mobile'].' Otp '.$userDetails['otp']);
    }
    
}
