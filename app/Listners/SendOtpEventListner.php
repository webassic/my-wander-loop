<?php

namespace App\Listners;

use App\Events\SendOtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOtpEventListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendOtpEvent  $event
     * @return void
     */
    public function handle(SendOtpEvent $event)
    {
        $userDetails = $event->userDetails;
        \Log::error('Data'.print_r($userDetails,true));
        // Account details
	$apiKey = urlencode(config('app.localtext_api_key'));
	
	// Message details
	$numbers = array($userDetails['mobile']);
	$sender = urlencode(config('app.localtext_sender'));
	$message = rawurlencode($userDetails['otp']." is your OTP for your MyWanderLoop mobile number verification");
 
	$numbers = implode(',', $numbers);
 
	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	
	// Process your response here
	
        $response  =json_decode($response,true);

        if(array_key_exists("errors",$response)){
            
             \Log::error('SMS Response Error : '.print_r($response,true));
        }else{
             
            \Log::info('SMS Response : '.print_r($response,true)); 
        }
	// Process your response here
         \Log::info('Otp --> Mobile '.$userDetails['mobile'].' Otp '.$userDetails['otp']);
    }
}
