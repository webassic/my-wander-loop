<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AadhaarInformation extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'aadhaar_information_capture';
    protected $resourceKey = 'aadhaar_information_capture';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banduser_id',
        'username',
        'dob',
        'gender',
        'address',
        'image',
        'status',
        'response'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
