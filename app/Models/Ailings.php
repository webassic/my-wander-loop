<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Ailings extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'ailings';
    protected $resourceKey = 'ailings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'created_by', 'modified_by', 'created_at', 'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
