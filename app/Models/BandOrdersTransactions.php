<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
//use Illuminate\Database\Eloquent\SoftDeletes;

class BandOrdersTransactions extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;
//    use SoftDeletes;
    protected $table = 'bandorders_transactions';
    protected $resourceKey = 'bandorders_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bandorders_id',
        'rzorder_id',
        'rzpayment_id',
        'rzsignature',
        'payment_status',
        'order_status',
        'worklfow_status',
        'created_by',
        'modified_by',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
