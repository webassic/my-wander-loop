<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class BandType extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $table = 'band_types';
    protected $resourceKey = 'band_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'band_name',
        'color',
        'glow_dark',
        'firstname_printed',
        'mobile_printed',
        'scanner_phone_number',
        'description',
        'status',
        'created_by',
        'modified_by'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
