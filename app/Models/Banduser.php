<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Banduser extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'bandusers';
    protected $resourceKey = 'bandusers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'guardians_id',
        'uniqueid',
        'title',
        'firstname',
        'lastname',
        'dob',
        'email',
        'alternate_email',
        'landline',
        'mobile',
        'alternate_mobile',
        'address',
        'locality',
        'city',
        'state',
        'pin',
        'blood_group',
        'relationship_with_applicant',
        'otp',
        'status',
        'aadhaar_card_file_name',
        'aadhaar_card_file_path',
//        'aadhaar_card_number',
        'medical_history_not_applicable',
        'wanderoff_history_not_applicable',
        'share_code',
        'languages_known',
        'package_name',
        'created_by',
        'modified_by'
    ];
    public $titleArray = [
        'Mr' => 'Mr',
        'Mrs' => 'Mrs',
        'Ms' => 'Ms',
        'Dr' => 'Dr',
        'Other' => 'Other'
    ];
    public $bloodGroupArray = [
        'A+' => 'A+',
        'A-' => 'A-',
        'B+' => 'B+',
        'B-' => 'B-',
        'O+' => 'O+',
        'O-' => 'O-',
        'AB+' => 'AB+',
        'AB-' => 'AB-',
        'Unknown' => 'Unknown'
    ];
    public $relationShipWithGuardian = [
        'Self' => 'Self',
        'Son' => 'Son',
        'Daughter' => 'Daughter',
        'Wife' => 'Wife',
        'Husband' => 'Husband',
        'Son-in-law' => 'Son-in-law',
        'Daughter-in-law' => 'Daughter-in-law',
        'Brother' => 'Brother',
        'Sister' => 'Sister',
        'Grand son' => 'Grand son',
        'Grand daughter' => 'Grand daughter',
        'Friend' => 'Friend',
        'Neighbour' => 'Neighbour',
        'Others' => 'Others'
    ];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
