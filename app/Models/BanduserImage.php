<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BanduserImage extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'bandusers_images';
    protected $resourceKey = 'bandusers_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'bandusers_id',
        'file_type',
        'file_name',
        'file_path',
        'created_by',
        'modified_by',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
