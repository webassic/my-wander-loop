<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BanduserPaymentTransactions extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'banduser_payment_transactions';
    protected $resourceKey = 'banduser_payment_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'banduser_id',
        'subscription_id',
        'payment_id',
        'invoice_number',
        'payment_date',
        'status',
        'signature',
        'is_renewal_payment',
        'start_date',
        'end_date',
        'created_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
