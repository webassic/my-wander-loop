<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BanduserSubscriptionTransactions extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'banduser_subscription_transactions';
    protected $resourceKey = 'banduser_subscription_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'guardian_subscription_id',
        'banduser_id',
        'subscription_id',
        'start_date',
        'end_date',
        'status',
        'inactive_on',
        'is_renewal_subscription'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
