<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BandusersAilings extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'bandusers_ailings';
    protected $resourceKey = 'bandusers_ailings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'bandusers_id',
        'hospital_registration_no',
        'ailing_id',
        'ailing_other',
        'since_when',
        'primary_physician',
        'secondary_physician',
        'hospital_preference',
        'hospital_city',
        'admitted_hospital',
        'recent_illness',
        'is_deleted',
        'modified_by',
        'created_by',
    ];
   

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
