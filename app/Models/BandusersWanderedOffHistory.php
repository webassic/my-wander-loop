<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class BandusersWanderedOffHistory extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'bandusers_wandered_off_history';
    protected $resourceKey = 'bandusers_wandered_off_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'bandusers_id',
        'lost_city',
        'lost_address',
        'lost_date',
        'found_city',
        'found_address',
        'found_date',
        'additional_comments',
        'is_deleted',
        'modified_by',
        'created_by',
    ];
   

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}

