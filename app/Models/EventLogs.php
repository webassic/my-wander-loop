<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EventLogs extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;
    protected $table = 'event_logs';
    protected $resourceKey = 'event_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'value',
        'record_id',
        'user_id',
        'module',
        'created_at',
        'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}


