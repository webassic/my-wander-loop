<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Guardian extends Model implements Auditable {

//    use SoftDeletes;

    use \OwenIt\Auditing\Auditable;

    protected $table = 'guardians';
    protected $resourceKey = 'guardians';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'dob',
        'address',
        'locality',
        'city',
        'state',
        'pin',
        'landline',
        'identity_type',
        'identity_number',
        'institution_name',
        'guardian_type',
        'social_media_login',
        'otp',
        'reg_no',
        'renew_date',
        'is_exist',
    ];
    public $identityTypeArray = [
//       'default', 
       'PAN' => 'PAN',
       'Aadhar' => 'Aadhar',
       'Driving License'=> 'Driving License'
    ];
    public $titleArray = [
       'Mr' => 'Mr',
        'Mrs' =>  'Mrs',
        'Ms' =>  'Ms',
        'Dr' => 'Dr',
        'Others' => 'Others'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
