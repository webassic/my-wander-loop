<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class GuardianSubscriptions extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'guardian_subscriptions';
    protected $resourceKey = 'guardian_subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'guardian_id',
        'subscription_id',
        'total_count',
        'status'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
