<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Institution extends Model implements Auditable {

    use SoftDeletes;

    use \OwenIt\Auditing\Auditable;

    protected $table = 'institutions';
    protected $resourceKey = 'institutions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'address',
        'locality',
        'city',
        'state',
        'pin',
        'landline'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
