<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class NewBandOrders extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'new_band_orders';
    protected $resourceKey = 'new_band_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banduser_id',
        'band_lost',
        'number_of_bands',
        'total_amount',
        'amount_paid',
        'transaction_id',
        'order_date',
        'order_processed',
        'order_processed_on',
        'order_shipped',
        'order_shipped_on',
        'delivered',
        'created_by',
        'modified_by'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
