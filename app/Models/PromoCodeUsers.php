<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromoCodeUsers extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'promo_code_users';
    protected $resourceKey = 'promo_code_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'promo_codes_id',
        'users_id',
        'created_at',
        'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

    public function users() {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function promo_codes() {
        return $this->belongsTo('App\Models\PromoCodes', 'id');
    }

}
