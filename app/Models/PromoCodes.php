<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PromoCodes extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'promo_codes';
    protected $resourceKey = 'promo_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'valid_till',
        'discount_offered_type',
        'discount_offered',
        'is_active',
        'created_by',
        'modified_by',
        'created_at',
        'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

    public function promo_code_users() {
        return $this->hasMany('App\Models\PromoCodeUsers', 'promo_codes_id');
    }

}
