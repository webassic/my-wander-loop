<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RazorpayCredentials extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'razorpay_credentials';
    protected $resourceKey = 'razorpay_credentials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_key_id', 
        'test_key_secret', 
        'test_basic_plan_id',
        'test_plan_id',
        'test_banduser_cost',
        'test_invoice_number_prefix',
        'test_invoice_number_start',
        'test_last_invoice_number',
        'test_gst_rate',
        'live_key_id', 
        'live_key_secret',
        'live_basic_plan_id',
        'live_plan_id',
        'live_banduser_cost',
        'live_invoice_number_prefix',
        'live_invoice_number_start',
        'live_last_invoice_number',
        'live_gst_rate',
        'last_modified', 
        'modified_by',
        'created_at',
        'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
