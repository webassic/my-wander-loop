<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RazorpayErrorsLog extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;
    protected $table = 'razorpayerrors_capture';
    protected $resourceKey = 'razorpayerrors_capture';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'action',
        'error_code',
        'error_message',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
