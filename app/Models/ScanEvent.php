<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
//use Illuminate\Database\Eloquent\SoftDeletes;

class ScanEvent extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;
//    use SoftDeletes;
    protected $table = 'scanevents';
    protected $resourceKey = 'scanevents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banduser_uniqueid',
        'banduser_name',
        'scanner_mobile',
        'ip_address',
        'latitude',
        'logitude',
        'location',
        'mobile_verify',
        'banduser_info_displayed',
        'scanned_on',
        'device_info',
        'json_data_by_location',
        'json_data_by_ip',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
