<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SupportTicket extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'support_tickets';
    protected $resourceKey = 'support_tickets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unique_ticket_id',
        'subject',
        'related_to',
        'message',
        'status',
        'created_by',
        'status_modified_by'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

    public function support_ticket_bandusers() {
        return $this->hasMany('App\Models\SupportTicketBanduser', 'support_ticket_id');
     }

}
