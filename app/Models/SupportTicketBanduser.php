<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SupportTicketBanduser extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'support_ticket_bandusers';
    protected $resourceKey = 'support_ticket_bandusers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'support_ticket_id',
        'banduser_id',
        'created_at',
        'updated_at'
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }
 
    public function bandusers() {
        return $this->belongsTo('App\Models\Banduser', 'banduser_id');
    }
    public function support_tickets() {
        return $this->belongsTo('App\Models\SupportTicket', 'id');
    }

}
