<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SupportTicketComment extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable;

    protected $table = 'support_ticket_comments';
    protected $resourceKey = 'support_ticket_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'support_ticket_id',
        'message',
        'message_by',
    ];

    public function generateTags(): array {
        return [
            $this->table,
        ];
    }

}
