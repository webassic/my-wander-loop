<?php

namespace App\Repositories;

use App\Models\Ailings;
use Auth;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class AilingRepository extends BaseRepository {

    private $selectFields = [
        'ailings.id',
        'ailings.name',
        'ailings.status',
        'users.firstname',
        'users.lastname'
    ];
    private $searchArray = array(
        'ailings.name',
        'ailings.status',
        'users.firstname',
        'users.lastname'
    );

    /**
     * 
     * @param Ailings $ailingModelObj
     */
    public function __construct(Ailings $ailingModelObj) {
        $this->ailingModelObj = $ailingModelObj;
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeAiling($postData) {
        try {
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
            $response = [];
            if (!empty($data)) {
                $ailing = $this->ailingModelObj;
                $ailing->name = isset($data['name']) ? trim($data['name']) : '';
                $ailing->status = isset($data['status']) ? $data['status'] : 'active';
                $ailing->created_by = auth()->user()->id;
                $response = $ailing->save();
                return $response;
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateAiling($request, $id) {
        try {
            $updateArray = array(
                'name' => isset($request['name']) ? trim($request['name']) : '',
                'status' => isset($request['status']) ? $request['status'] : '',
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                return \App\Models\Ailings::find($id)->update($updateArray);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showAiling($id) {
        try {
            $response = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $response = $this->ailingModelObj
                                ->where('id', '=', $id)
                                ->get()->toArray();
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyAiling($id) {
        try {
            $updateArray = array(
                'status' => 'inactive',
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                return \App\Models\Ailings::find($id)->update($updateArray);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllAilingDetails($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'ailings.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->ailingModelObj
                    ->select($this->selectFields)
                    ->join('users', function($join) {
                $join->on('users.id', '=', 'ailings.created_by');
            });

            if (!empty($conditions['status']) && isset($conditions['status'])) {
                $query->where('ailings.status', $conditions['status']);                
            }
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->where('ailings.name', '!=', 'Others'); // Hide remove Others option 
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $ailingsDetailsArray = $query->get()->toArray();
            $response['records'] = $ailingsDetailsArray;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * getAilingsList
     * @return type
     */
    public function getAilingsList() {
        try {
            return $this->ailingModelObj->select('id', 'name')
                            ->where(array('status' => 'active'))
                            ->orderBy('name', 'asc')
                            ->get()->toArray();
        } catch (Exception $ex) {
           throw $ex;
        }
    }

}
