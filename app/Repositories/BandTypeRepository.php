<?php

namespace App\Repositories;

use App\Models\BandType;
use DB;

class BandTypeRepository extends BaseRepository {

    protected $modelBandTypes;
    private $selectFields = [
        'band_types.id',
        'band_types.band_name',
        'band_types.color',
        'band_types.glow_dark',
        'band_types.firstname_printed',
        'band_types.mobile_printed',
        'band_types.scanner_phone_number',
        'band_types.description',
        'band_types.status',
        'band_types.created_by',
    ];
    private $searchArray = array(
        'band_types.band_name',
        'band_types.color',
        'band_types.glow_dark',
        'band_types.firstname_printed',
        'band_types.mobile_printed',
        'band_types.scanner_phone_number',
        'band_types.description',
        'band_types.status',
    );

    /**
     * 
     * @param PromoCodes $modelPromoCodes
     */
    public function __construct(BandType $modelBandTypes) {
        $this->modelBandTypes = $modelBandTypes;
       
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllBandTypes($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'is_active' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->modelBandTypes
                    ->select($this->selectFields);

            if (!empty($conditions['is_active']) && isset($conditions['is_active'])) {
                $query->where('band_types.status', $conditions['is_active']);
            }
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $bandTypesDetails = $query->get()->toArray();
            
            $response['records'] = $bandTypesDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }
    
    public function storeBandType($postData) {
        try{
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           
           $result= $this->modelBandTypes->create($data);
           return $result;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyBandType($id){
        try{
            $result = $this->modelBandTypes->where('id', $id)->delete();
            
           return $result;
        }catch(\Exception $ex){
           throw $ex;
        } 
    }
    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showBandType($id){
         try{
           $response = [];
            if(isset($id) && !empty($id) && $id > 0) {
                $response = $this->modelBandTypes->select($this->selectFields)
                        ->where('id', '=', $id)
                        ->get()->toArray();
                
            }
            return $response;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateBandType($postData,$id) {
       try{
            $data = array();
            $response=array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           
           $result= $this->modelBandTypes->where('id',$id)
                    ->update($data);
           if($result){
               $response = $this->modelBandTypes->find($id);
           }
           return $response;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }

}
