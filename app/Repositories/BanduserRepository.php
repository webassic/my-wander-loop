<?php

namespace App\Repositories;

use App\Models\Banduser;
use App\Models\BanduserImage;
use App\Models\BandusersAilings;
use App\Models\BandusersWanderedOffHistory;
use App\Repositories\GuardiansRepository;
use App\Repositories\RazorpayCredentialsRepository;
use Auth;
use DB;
use LaravelQRCode\Facades\QRCode;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\BadRequestError;
use App\Models\GuardianSubscriptions;
use App\Models\RazorpayErrorsLog;
use Illuminate\Support\Facades\Mail;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class BanduserRepository extends BaseRepository {

    private $selectFields = [
        'bandusers.id',
        'bandusers.uniqueid',
        'users.firstname as guardian_firstname',
        'users.lastname as guardian_lastname',
        'bandusers.firstname',
        'bandusers.lastname',
        'bandusers.email',
        'bandusers.title',
        'bandusers.dob',
        'bandusers.address',
        'bandusers.locality',
        'bandusers.city',
        'bandusers.state',
        'bandusers.pin',
        'bandusers.landline',
        'bandusers.mobile',
        'bandusers.blood_group',
        'bandusers.relationship_with_applicant',
        'bandusers.alternate_mobile',
        'bandusers.alternate_email',
        'bandusers.status',
        'bandusers.created_at',
        'bandusers.updated_at'
    ];
    
       private $selectFieldsForBandUserListing = [
        'bandusers.id',
        'bandusers.uniqueid',
        'users.firstname as guardian_firstname',
        'users.lastname as guardian_lastname',
        'bandusers.firstname',
        'bandusers.lastname',
        'bandusers.email',
        'bandusers.title',
        'bandusers.dob',
        'bandusers.address',
        'bandusers.locality',
        'bandusers.city',
        'bandusers.state',
        'bandusers.pin',
        'bandusers.landline',
        'bandusers.mobile',
        'bandusers.blood_group',
        'bandusers.relationship_with_applicant',
        'bandusers.alternate_mobile',
        'bandusers.alternate_email',
        'bandusers.status',
        'bandusers.package_name',
        'bandusers.created_at',
        'bandusers.updated_at',
        'banduser_subscription_transactions.end_date as package_expiry_date'
    ];
    private $searchGroupsArray = [
        'bandusers.firstname',
        'bandusers.lastname',
        'bandusers.email',
        'bandusers.mobile',
        'bandusers.package_name',
        'bandusers.status',
    ];
    private $searchTransactionGroupsArray = [
        'bandusers.firstname',
        'banduser_payment_transactions.payment_date',
        'banduser_payment_transactions.payment_id',
        'banduser_subscription_transactions.start_date',
        'banduser_subscription_transactions.end_date',        
        'banduser_payment_transactions.invoice_number',
        'bandusers.lastname'
    ];

    /**
     * 
     * @param Banduser $banduserModelObj
     */
    public function __construct(Banduser $banduserModelObj, BanduserImage $banduserImageModelObj, BandusersAilings $bandusersAilingsModelObj, BandusersWanderedOffHistory $bandusersWanderedOffHistoryMobelObj, GuardiansRepository $guardiansRepository, RazorpayCredentialsRepository $razorpayCredentialsRepository, GuardianSubscriptions $guardianSubscriptionsModelObj) {
        $this->banduserModelObj = $banduserModelObj;
        $this->bandusersAilingsModelObj = $bandusersAilingsModelObj;
        $this->bandusersWanderedOffHistoryMobelObj = $bandusersWanderedOffHistoryMobelObj;
        $this->guardiansRepository = $guardiansRepository;
        $this->banduserImageModelObj = $banduserImageModelObj;
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
        $this->guardianSubscriptionsModelObj = $guardianSubscriptionsModelObj;
    }

    public function generateRandomtring($length) {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $token = base64_encode(openssl_random_pseudo_bytes($length, $strong));
            if ($strong == TRUE)
                return strtr(substr($token, 0, $length), '+/=', '-_,'); //base64 is about 33% longer, so we need to truncate the result
        }
        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+';
        $charactersLength = strlen($characters) - 1;
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[mt_rand(0, $charactersLength)];
        }
        return $token;
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeBanduser($postData) {
        try {
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
            $getGuardianId = $this->guardiansRepository->getGuardianID(Auth::user()->id);
            $data['guardians_id'] = isset($getGuardianId) && !empty($getGuardianId) ? $getGuardianId : 0;
            $data['firstname'] = isset($data['firstname']) && !empty($data['firstname']) ? ucfirst($data['firstname']) : '';
            $data['lastname'] = isset($data['lastname']) && !empty($data['lastname']) ? ucfirst($data['lastname']) : '';
            $data['address'] = isset($data['address']) && !empty($data['address']) ? ucfirst($data['address']) : '';
            $data['locality'] = isset($data['locality']) && !empty($data['locality']) ? ucfirst($data['locality']) : '';
            $data['city'] = isset($data['city']) && !empty($data['city']) ? ucfirst($data['city']) : '';
            $data['state'] = isset($data['state']) && !empty($data['state']) ? ucfirst($data['state']) : '';
            $data['aadhaar_card_file_name'] = isset($data['aadhaar_card_original_file_name']) && !empty($data['aadhaar_card_original_file_name']) ? $data['aadhaar_card_original_file_name'] : '';
//            $data['aadhaar_card_file_name'] = $data['aadhaar_card_original_file_name'];
            $data['medical_history_not_applicable'] = isset($data['medicalHistoryCheckbox']) && !empty($data['medicalHistoryCheckbox']) ? $data['medicalHistoryCheckbox'] : 'off';
            $data['wanderoff_history_not_applicable'] = isset($data['wanderOffHistoryCheckbox']) && !empty($data['wanderOffHistoryCheckbox']) ? $data['wanderOffHistoryCheckbox'] : 'off';
            $data['languages_known'] = isset($data['languages_known']) && !empty($data['languages_known']) ? ucfirst($data['languages_known']) : '';
            $data['package_name'] = isset($data['package_name']) && !empty($data['package_name']) ? $data['package_name'] : '';
            $bandUserDetails = $this->banduserModelObj->create($data);

            if (isset($bandUserDetails->id) && !empty($bandUserDetails->id) && isset($data['aadhaar_card_file_name']) && !empty($data['aadhaar_card_file_name'])) {

                $baseFromJavascript = $postData['aadhaar_card_file_name'];
                // We need to remove the "data:image/png;base64,"
                $base_to_php = explode(',', $baseFromJavascript);
                // the 2nd item in the base_to_php array contains the content of the image
                $savedFilename = base64_decode($base_to_php[1]);
                $destinationPath = public_path("banduser_details/banduser_" . $bandUserDetails->id . "/" . $bandUserDetails->aadhaar_card_file_name);
                $checkDestinationPath = public_path("banduser_details/banduser_" . $bandUserDetails->id);
                if (\File::exists($checkDestinationPath)) {
                    @unlink(public_path($destinationPath));
                    \File::delete(public_path($destinationPath));
                } else {
                    $oldMask = umask(0);
                    mkdir(($checkDestinationPath), 0777, true);
                    umask($oldMask);
                }
                file_put_contents($destinationPath, $savedFilename);

                $updateData = [];
//                $updateData['aadhaar_card_file_path'] = "/banduser_details/banduser_" . $bandUserDetails->id . "/aadhar_card_details.zip";
                $updateData['aadhaar_card_file_path'] = "/banduser_details/banduser_" . $bandUserDetails->id . "/" . $bandUserDetails->aadhaar_card_file_name;
                $this->banduserModelObj->where('id', '=', $bandUserDetails->id)->update($updateData); // Update path
            }

            if (isset($bandUserDetails->id) && !empty($bandUserDetails->id) && isset($data['latestPhoto']) && !empty($data['latestPhoto'])) {
                // Store Band user images in bandusers_images table
                $bandUserImageArr = [];
                foreach ($data['latestPhoto'] as $photo) {
                    $baseFromJavascript = $photo; // $_POST['base64']; //your data in base64 'data:image/png....';
                    // We need to remove the "data:image/png;base64,"
                    $base_to_php = explode(',', $baseFromJavascript);
                    $imageExtentionExplode = explode('/', $base_to_php[0]);
                    $imageExtention = explode(';', $imageExtentionExplode[1]);
                    // the 2nd item in the base_to_php array contains the content of the image
                    $latestImg = base64_decode($base_to_php[1]);

                    $bandUserImageArr['bandusers_id'] = $bandUserDetails->id;
                    $bandUserImageArr['file_type'] = 'image';
                    $bandUserImageArr['file_name'] = $this->generateRandomtring(13) . "." . $imageExtention[0];
                    $bandUserImageArr['file_path'] = "banduser_details/banduser_" . $bandUserDetails->id . "/images/" . $bandUserImageArr['file_name'];
                    $bandUserImageArr['created_by'] = Auth::user()->id;

                    $checkImagepath = public_path("banduser_details/banduser_" . $bandUserDetails->id . "/images/" . $bandUserImageArr['file_name']);
                    $checkImgPath = public_path("banduser_details/banduser_" . $bandUserDetails->id . "/images");
                    if (\File::exists($checkImgPath)) {
                        @unlink(public_path($checkImagepath));
                        \File::delete(public_path($checkImagepath));
                    } else {
                        $oldMask = umask(0);
                        mkdir(($checkImgPath), 0777, true);
                        umask($oldMask);
                    }
                    // Save the image in a defined path
                    file_put_contents($checkImagepath, $latestImg);
                    \App\Models\BanduserImage::create($bandUserImageArr);
                }
            }
            if (isset($bandUserDetails->id) && !empty($bandUserDetails->id) && isset($data['medicalHistoryDataArr']) && !empty($data['medicalHistoryDataArr'])) {
                // Store Medical history in bandusers_ailings table
                $bandUserId = $bandUserDetails->id;
                $this->storeMedicalHistory($bandUserId, $data['medicalHistoryDataArr']);
            }

            if (isset($bandUserDetails->id) && !empty($bandUserDetails->id) && isset($data['wanderOffEventsHistoryDataArr']) && !empty($data['wanderOffEventsHistoryDataArr'])) {
                // Store Wander of events history in bandusers_wandered_off_history table
                $bandUserId = $bandUserDetails->id;
                $this->storeWanderOffEventsHistory($bandUserId, $data['wanderOffEventsHistoryDataArr']);
            }

            return $bandUserDetails;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * storeMedicalHistory
     * @param type $bandUserId
     * @param type $medicalHistoryArr
     * @return array
     */
    public function storeMedicalHistory($bandUserId, $medicalHistoryArr) {
        try {
            $response = [];
            if (!empty($bandUserId)) {
                foreach ($medicalHistoryArr as $key => $val) {
                    unset($val['id']);
                    if (isset($val['since_when']) && !empty($val['since_when'])) {
                        $old_date = explode("/", $val['since_when']);
                        if (isset($old_date[2]) && !empty($old_date)) {
                            $val['since_when'] = $old_date[2] . '-' . $old_date[1] . '-' . $old_date[0];
                        } else {
                            $val['since_when'] = $old_date[0];
                        }
                    }
                    $medicalHistoryArray = array(
                        'bandusers_id' => $bandUserId,
                        'hospital_registration_no' => $val['hospital_registration_no'],
                        'ailing_id' => $val['ailing_id'],
                        'ailing_other' => isset($val['ailing_other']) && !empty($val['ailing_other']) ? ucfirst($val['ailing_other']) : '',
                        'since_when' => $val['since_when'],
                        'primary_physician' => ucfirst($val['primary_physician']),
                        'secondary_physician' => ucfirst($val['secondary_physician']),
                        'admitted_hospital' => ucfirst($val['admitted_hospital']),
                        'hospital_city' => ucfirst($val['hospital_city']),
                        'hospital_preference' => ucfirst($val['hospital_preference']),
                        'recent_illness' => ucfirst($val['recent_illness']),
                        'created_by' => Auth::user()->id
                    );
                    \App\Models\BandusersAilings::create($medicalHistoryArray);
                }
            }
            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * storeWanderOffEventsHistory
     * @param type $bandUserId
     * @param type $wanderOffEventsHistoryDataArr
     * @return array
     */
    public function storeWanderOffEventsHistory($bandUserId, $wanderOffEventsHistoryDataArr) {
        try {
            $response = [];
            if (!empty($bandUserId)) {
                foreach ($wanderOffEventsHistoryDataArr as $key => $val) {
                    unset($val['id']);


                    if (isset($val['lost_date']) && !empty($val['lost_date'])) {
                        $old_date = explode("/", $val['lost_date']);
                        if (isset($old_date[2]) && !empty($old_date)) {
                            $val['lost_date'] = $old_date[2] . '-' . $old_date[1] . '-' . $old_date[0];
                        } else {
                            $val['lost_date'] = $old_date[0];
                        }
                    }
                    if (isset($val['found_date']) && !empty($val['found_date'])) {
                        $fold_date = explode("/", $val['found_date']);
                        if (isset($fold_date[2]) && !empty($fold_date)) {
                            $val['found_date'] = $fold_date[2] . '-' . $fold_date[1] . '-' . $fold_date[0];
                        } else {
                            $val['found_date'] = $fold_date[0];
                        }
                    }

                    $wanderOffEventsHistoryArray = array(
                        'bandusers_id' => $bandUserId,
                        'lost_city' => ucfirst($val['lost_city']),
                        'lost_address' => ucfirst($val['lost_address']),
                        'lost_date' => $val['lost_date'],
                        'found_city' => ucfirst($val['found_city']),
                        'found_address' => ucfirst($val['found_address']),
                        'found_date' => $val['found_date'],
                        'additional_comments' => isset($val['additional_comments']) && !empty($val['additional_comments']) ? ucfirst($val['additional_comments']) : '',
                        'created_by' => Auth::user()->id
                    );
                    \App\Models\BandusersWanderedOffHistory::create($wanderOffEventsHistoryArray);
                }
            }
            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showBanduser($id) {
        try {
            $bandUsersResponseArray = [];
            if (isset($id) && !empty($id)) {
                $bandUsersResponse = $this->banduserModelObj
                        ->select('bandusers.*', 'guardians.institution_name')
                        ->join('guardians', 'guardians.id', 'bandusers.guardians_id')
                        ->where('bandusers.id', '=', $id)
                        ->first();
                if (isset($bandUsersResponse) && !empty($bandUsersResponse) && $bandUsersResponse->count() > 0) {
                    $bandUsersResponseArray = $bandUsersResponse->toArray();
                    $bandUsersailings = $this->bandusersAilingsModelObj
                            ->select('bandusers_ailings.*', 'ailings.name as ailing_name')
                            ->leftjoin('ailings', 'ailings.id', 'bandusers_ailings.ailing_id')
                            ->where('bandusers_id', '=', $id)
                            ->get();
                    if (isset($bandUsersailings) && !empty($bandUsersailings) && $bandUsersailings->count() > 0) {
                        $bandUsersailingsArray = $bandUsersailings->toArray();
                        $bandUsersResponseArray['ailings_details'] = $bandUsersailingsArray;
                    }
                    $bandUsersWanderOff = $this->bandusersWanderedOffHistoryMobelObj
                            ->where('bandusers_id', '=', $id)
                            ->get();
                    if (isset($bandUsersWanderOff) && !empty($bandUsersWanderOff) && $bandUsersWanderOff->count() > 0) {
                        $bandUsersWanderOffArrray = $bandUsersWanderOff->toArray();
                        $bandUsersResponseArray['wanderoff_history'] = $bandUsersWanderOffArrray;
                    }

                    $bandUsersImages = \App\Models\BanduserImage::where('bandusers_id', '=', $id)->where('file_type', '=', 'image')->get();
                    if (isset($bandUsersImages) && !empty($bandUsersImages) && $bandUsersImages->count() > 0) {
                        $bandUsersImagesArrray = $bandUsersImages->toArray();
                        $bandUsersResponseArray['bandusers_images'] = $bandUsersImagesArrray;
                    }
                    $bandUsersQrCode = \App\Models\BanduserImage::where('bandusers_id', '=', $id)->where('file_type', '=', 'qr-code')->get();
                    if (isset($bandUsersQrCode) && !empty($bandUsersQrCode) && $bandUsersQrCode->count() > 0) {
                        $bandUsersQrCodeArrray = $bandUsersQrCode->toArray();
                        $bandUsersResponseArray['bandusers_qrcode'] = $bandUsersQrCodeArrray;
                    }
                    $bandUsersAadhaarInfo = \App\Models\AadhaarInformation::where('banduser_id', '=', $id)->get();
                    if (isset($bandUsersAadhaarInfo) && !empty($bandUsersAadhaarInfo) && $bandUsersAadhaarInfo->count() > 0) {
                        $bandUsersAadhaarInfoArrray = $bandUsersAadhaarInfo->toArray();
                        $bandUsersResponseArray['bandusers_aadhaar_info'] = $bandUsersAadhaarInfoArrray;
                    }
                }
            }
            return $bandUsersResponseArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $paramsData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateBanduser($paramsData) {
        try {
            $response = array();
            $createdById = Auth::user()->id;

            $bandUserData = $paramsData;
            unset($bandUserData['latestPhoto']);
            unset($bandUserData['aadhaar_card_original_file_name']);
            if($bandUserData['package_name'] == env('RAZORPAY_BASIC_PACKAGE')) {
                if ($bandUserData['medicalHistoryDataArr'] == NULL) {
                    unset($bandUserData['medicalHistoryDataArr']);
                }
                if ($bandUserData['wanderOffEventsHistoryDataArr'] == NULL) {
                    unset($bandUserData['wanderOffEventsHistoryDataArr']);
                }
            }
            if (isset($bandUserData['medicalHistoryCheckbox']) && !empty($bandUserData['medicalHistoryCheckbox'])) {
                unset($bandUserData['medicalHistoryCheckbox']);
            }
            if (isset($bandUserData['wanderOffHistoryCheckbox']) && !empty($bandUserData['wanderOffHistoryCheckbox'])) {
                unset($bandUserData['wanderOffHistoryCheckbox']);
            }
            if (isset($bandUserData['medicalHistoryDataArr']) && !empty($bandUserData['medicalHistoryDataArr'])) {
                unset($bandUserData['medicalHistoryDataArr']);
            }
             if (isset($bandUserData['wanderOffEventsHistoryDataArr']) && !empty($bandUserData['wanderOffEventsHistoryDataArr'])) {
                unset($bandUserData['wanderOffEventsHistoryDataArr']);
            }
            if (isset($bandUserData['banduserImageId']) && !empty($bandUserData['banduserImageId'])) {
                unset($bandUserData['banduserImageId']);
            }
            if (($bandUserData['status'] != 'active') && ($paramsData['status'] != "pending")) {
                $bandUserData['firstname'] = isset($bandUserData['firstname']) && !empty($bandUserData['firstname']) ? ucfirst($bandUserData['firstname']) : '';
                $bandUserData['lastname'] = isset($bandUserData['lastname']) && !empty($bandUserData['lastname']) ? ucfirst($bandUserData['lastname']) : '';
            }

            $old_aadhaar_card_file_name = $this->getAadhharCardDetails($bandUserData['id']); // Pass $old_aadhaar_card_file_name
            $bandUserData['address'] = isset($bandUserData['address']) && !empty($bandUserData['address']) ? ucfirst($bandUserData['address']) : '';
            $bandUserData['locality'] = isset($bandUserData['locality']) && !empty($bandUserData['locality']) ? ucfirst($bandUserData['locality']) : '';
            $bandUserData['city'] = isset($bandUserData['city']) && !empty($bandUserData['city']) ? ucfirst($bandUserData['city']) : '';
            $bandUserData['state'] = isset($bandUserData['state']) && !empty($bandUserData['state']) ? ucfirst($bandUserData['state']) : '';
            $bandUserData['medical_history_not_applicable'] = isset($bandUserData['medicalHistoryCheckbox']) && !empty($bandUserData['medicalHistoryCheckbox']) ? $bandUserData['medicalHistoryCheckbox'] : 'off';
            $bandUserData['wanderoff_history_not_applicable'] = isset($bandUserData['wanderOffHistoryCheckbox']) && !empty($bandUserData['wanderOffHistoryCheckbox']) ? $bandUserData['wanderOffHistoryCheckbox'] : 'off';
            $bandUserData['languages_known'] = isset($bandUserData['languages_known']) && !empty($bandUserData['languages_known']) ? ucfirst($bandUserData['languages_known']) : '';
            $bandUserData['aadhaar_card_file_name'] = isset($paramsData['aadhaar_card_original_file_name']) && !empty($paramsData['aadhaar_card_original_file_name']) ? $paramsData['aadhaar_card_original_file_name'] : $old_aadhaar_card_file_name['aadhaar_card_file_name'];
            $result = $this->banduserModelObj
                    ->where('id', '=', $bandUserData['id'])
                    ->where('created_by', '=', $createdById)
                    ->update($bandUserData);


            if (isset($paramsData['id']) && !empty($paramsData['id']) && isset($paramsData['aadhaar_card_file_name']) && !empty($paramsData['aadhaar_card_file_name'])) {

                $baseFromJavascript = $paramsData['aadhaar_card_file_name'];
                // We need to remove the "data:image/png;base64,"
                $base_to_php = explode(',', $baseFromJavascript);
                // the 2nd item in the base_to_php array contains the content of the image
                $savedFilename = base64_decode($base_to_php[1]);
                $oldDestinationPath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/" . $old_aadhaar_card_file_name['aadhaar_card_file_name']);
                $destinationPath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/" . $paramsData['aadhaar_card_original_file_name']);
                $checkDestinationPath = public_path("banduser_details/banduser_" . $paramsData['id']);

                if (\File::exists($checkDestinationPath)) {
                    unlink($oldDestinationPath);
                } else {
                    $oldMask = umask(0);
                    mkdir(($checkDestinationPath), 0777, true);
                    umask($oldMask);
                }
                file_put_contents($destinationPath, $savedFilename);

                $updateData = [];
                $updateData['aadhaar_card_file_path'] = "/banduser_details/banduser_" . $paramsData['id'] . "/" . $paramsData['aadhaar_card_original_file_name'];
                $updateData['aadhaar_card_file_name'] = isset($paramsData['aadhaar_card_original_file_name']) && !empty($paramsData['aadhaar_card_original_file_name']) ? $paramsData['aadhaar_card_original_file_name'] : $old_aadhaar_card_file_name['aadhaar_card_file_name'];

                $this->banduserModelObj->where('id', '=', $paramsData['id'])->update($updateData); // Update path
            }

            if (isset($paramsData['id']) && !empty($paramsData['id']) && isset($paramsData['latestPhoto']) && !empty($paramsData['latestPhoto'])) {
                if (isset($paramsData['banduserImageId']) && !empty($paramsData['banduserImageId'])) {
                    $getBandUserImages = $this->banduserImageModelObj->select('id', 'file_path')->where('bandusers_id', $bandUserData['id'])
                                    ->whereNotIn('id', $paramsData['banduserImageId'])
                                    ->get()->toArray();

                    $this->banduserImageModelObj->where('bandusers_id', $bandUserData['id'])
                            ->whereNotIn('id', $paramsData['banduserImageId'])
                            ->delete();
                    foreach ($getBandUserImages as $imagePath) {
                        $checkImagepath = public_path($imagePath['file_path']);
                        $checkImgPath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/images");
                        if (\File::exists($checkImgPath)) {
                            @unlink(public_path($checkImagepath));
                            \File::delete(public_path($checkImagepath));
                        }
                    }
                } else {
                    $getBandUserImages = $this->banduserImageModelObj->select('id', 'file_path')->where('bandusers_id', $bandUserData['id'])
                                    ->get()->toArray();
                    foreach ($getBandUserImages as $imagePath) {
                        $checkImagepath = public_path($imagePath['file_path']);
                        $checkImgPath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/images");
                        if (\File::exists($checkImgPath)) {
                            @unlink(public_path($checkImagepath));
                            \File::delete(public_path($checkImagepath));
                        }
                    }
                    $this->banduserImageModelObj->where('bandusers_id', '=', $bandUserData['id'])->delete();
                }

                // Store Band user images in bandusers_images table
                $bandUserImageArr = [];
                foreach ($paramsData['latestPhoto'] as $photo) {
                    $baseFromJavascript = $photo;
                    $base_to_php = explode(',', $baseFromJavascript);
                    $existingOrNew = explode(':', $base_to_php[0]);
                    if ($existingOrNew[0] == "data") {
                        $imageExtentionExplode = explode('/', $base_to_php[0]);
                        $imageExtention = explode(';', $imageExtentionExplode[1]);
                        // the 2nd item in the base_to_php array contains the content of the image
                        $latestImg = base64_decode($base_to_php[1]);

                        $bandUserImageArr['bandusers_id'] = $paramsData['id'];
                        $bandUserImageArr['file_type'] = 'image';
                        $bandUserImageArr['file_name'] = $this->generateRandomtring(13) . "." . $imageExtention[0];
                        $bandUserImageArr['file_path'] = "banduser_details/banduser_" . $paramsData['id'] . "/images/" . $bandUserImageArr['file_name'];
                        $bandUserImageArr['created_by'] = Auth::user()->id;

                        $checkImagepath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/images/" . $bandUserImageArr['file_name']);
                        $checkImgPath = public_path("banduser_details/banduser_" . $paramsData['id'] . "/images");
                        if (\File::exists($checkImgPath)) {
                            @unlink(public_path($checkImagepath));
                            \File::delete(public_path($checkImagepath));
                        } else {
                            $oldMask = umask(0);
                            mkdir(($checkImgPath), 0777, true);
                            umask($oldMask);
                        }
                        // Save the image in a defined path
                        file_put_contents($checkImagepath, $latestImg);
                        \App\Models\BanduserImage::create($bandUserImageArr);
                    } else {
                        
                    }
                }
            }

            // Medical History data
            if (isset($paramsData['id']) && !empty($paramsData['id']) && isset($paramsData['medicalHistoryDataArr']) && !empty($paramsData['medicalHistoryDataArr'])) {
                // Delete Medical history in bandusers_ailings table
                \App\Models\BandusersAilings::where('bandusers_id', '=', $paramsData['id'])->delete();
                // Store Medical history in bandusers_ailings table
                $this->storeMedicalHistory($paramsData['id'], $paramsData['medicalHistoryDataArr']);
            }

            // Wander Off Event History data
            if (isset($paramsData['id']) && !empty($paramsData['id']) && isset($paramsData['wanderOffEventsHistoryDataArr']) && !empty($paramsData['wanderOffEventsHistoryDataArr'])) {
                // Delete Wander Off Event History in bandusers_wandered_off_history table
                \App\Models\BandusersWanderedOffHistory::where('bandusers_id', '=', $paramsData['id'])->delete();
                // Store Wander Off Event History in bandusers_wandered_off_history table
                $this->storeWanderOffEventsHistory($paramsData['id'], $paramsData['wanderOffEventsHistoryDataArr']);
            }
            if ($result) {
                $response = $this->banduserModelObj->find($bandUserData['id']);
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function approveBandUserByAdmin($paramsData, $id) {
        try {
            $data = array();
            $response = array();
            $result = '';
            if (is_string($paramsData)) {
                $data = json_decode($paramsData, true);
            }
            if (empty($data)) {
                $data = $paramsData;
            }
            $data['modified_by'] = Auth::user()->id;
            $data['uniqueid'] = $this->generateUniqueId();
            $getPackageName = $this->banduserModelObj
                                ->select('bandusers.id', 'bandusers.package_name')
                                ->where('bandusers.id', '=', $id)
                                ->first()->toArray();

            if($getPackageName['package_name'] != env('RAZORPAY_BASIC_PACKAGE')){
                $qrcodeFileName = $this->generateQrCode($data['uniqueid'], $id);
                if (!empty($data['uniqueid']) && !empty($qrcodeFileName)) {
                    $getDetails = \App\Models\BanduserImage::where('bandusers_id', '=', $id)
                                    ->where('file_type', '=', 'qr-code')
                                    ->get()->toArray();

                    if (empty($getDetails)) {
                        $bandUserQrcodeArr['bandusers_id'] = $id;
                        $bandUserQrcodeArr['file_type'] = 'qr-code';
                        $bandUserQrcodeArr['file_name'] = $qrcodeFileName;
                        $bandUserQrcodeArr['file_path'] = "banduser_details/banduser_" . $id . "/qr_codes/" . $qrcodeFileName;
                        $bandUserQrcodeArr['created_by'] = Auth::user()->id;
                        \App\Models\BanduserImage::create($bandUserQrcodeArr);
                    } else {
                        \App\Models\BanduserImage::where('id', '=', $getDetails[0]['id'])
                                ->update([
                                    'file_path' => "banduser_details/banduser_" . $id . "/qr_codes/" . $qrcodeFileName,
                                    'file_name' => $qrcodeFileName,
                                    'modified_by' => Auth::user()->id
                        ]);
                    }


                    $result = $this->banduserModelObj
                            ->where('id', '=', $id)
                            ->update($data);
                }
            }else{
                $result = $this->banduserModelObj
                        ->where('id', '=', $id)
                        ->update($data);
            }


            if (!empty($result)) {
                $response = $this->banduserModelObj->find($id);
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function inactiveBandUserByAdmin($paramsData, $id) {
        try {
            $data = array();
            $response = array();
            $result = '';
            if (is_string($paramsData)) {
                $data = json_decode($paramsData, true);
            }
            if (empty($data)) {
                $data = $paramsData;
            }
            $data['modified_by'] = Auth::user()->id;
            $result = $this->banduserModelObj
                    ->where('id', '=', $id)
                    ->update($data);


            if (!empty($result)) {
                // Cancel subscription 
                $transactionResponse = \App\Models\BanduserSubscriptionTransactions:: select('banduser_subscription_transactions.id')
                        ->where('banduser_id', '=', $id)
                        ->first();

                if (isset($transactionResponse) && !empty($transactionResponse)) {
                    $cancel = $this->cancelSubscription($transactionResponse->id);
                }
                $response = $this->banduserModelObj->find($id);
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function generateQrCode($unqiueId, $id) {
        try {
            $string = 'https://app.mywanderloop.com/scan?x=' . $unqiueId;
            $fileName = 'qr' . md5($string) . strtotime(date("d.m.Y h:i:s")) . '.png';
            $filePath = public_path('banduser_details/banduser_' . $id . '/qr_codes/');
            if (!file_exists($filePath)) {
                $old = umask(0);
                mkdir(($filePath), 0777, true);
                umask($old);
            }
            $files = glob($filePath . '*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file))
                    unlink($file); // delete file
            }
            QRCode::text($string)->setOutfile($filePath . $fileName)->png();

            return $fileName;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyBanduser($id) {
        try {
            $checkStatus = $this->banduserModelObj
                            ->select('save_as_draft')
                            ->where('bandusers.id', '=', $id)
                            ->first()->toArray();
            if (isset($checkStatus) && !empty($checkStatus)) {
                if ($checkStatus['save_as_draft'] == 0) { // Hardcoded delete records
                    $result = $this->banduserModelObj->where('id', $id)->delete();
                    if ($result > 0) {
                        \App\Models\BandusersAilings::where('bandusers_id', '=', $id)->delete();
                        \App\Models\BandusersWanderedOffHistory::where('bandusers_id', '=', $id)->delete();
                        \App\Models\BanduserImage::where('bandusers_id', '=', $id)->delete();

                        // Delete band user folder with images
                        $checkImgPath = public_path("banduser_details/banduser_" . $id);
                        if (\File::exists($checkImgPath)) {
                            \File::deleteDirectory($checkImgPath);
                        }
                    }
                    return $result;
                }
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function bandusersListing($postData) {
        try {
            $result = [];
                        $conditions = array(
                'search' => isset($postData['search']) ? $postData['search'] : '',
                'start' => isset($postData['start']) ? $postData['start'] : '',
                'order' => isset($this->searchGroupsArray[$postData['order'][0]['column']]) ? $this->searchGroupsArray[$postData['order'][0]['column']] : 'bandusers.id',
                'order_dir' => isset($postData['order'][0]['dir']) ? $postData['order'][0]['dir'] : 'DESC',
                'limit' => isset($postData['length']) ? $postData['length'] : '',
            ); 
            $query = $this->banduserModelObj->select($this->selectFieldsForBandUserListing)
                    ->leftjoin('banduser_subscription_transactions', function($join) {
                        $join->on('banduser_subscription_transactions.banduser_id', '=', 'bandusers.id')
                        ->whereRaw('banduser_subscription_transactions.id IN (select MAX(a2.id) from banduser_subscription_transactions as a2 join bandusers as u2 on u2.id = a2.banduser_id group by u2.id)');
                    })
                    ->join('users', function($join) {
                $join->on('users.id', '=', 'bandusers.created_by');
            });
            $query = $query->where('created_by', '=', Auth::user()->id);
            $query = $query->orderBy('bandusers.id', 'DESC'); 
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchGroupsArray as $searchGroup) {
                         $query = $query->orWhere($searchGroup, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $result['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $bandUserDetails = $query->get()->toArray();
            $result['records'] = $bandUserDetails;
            $result['incomplete_results'] = false;
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @param type $userId
     * @return boolean
     * @throws \Exception
     */
    public function bandUsersListingsBasedOnGuardiansId($request, $guardiansId) {
        $response = [];
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'bandusers.uniqueid',
                'bandusers.title',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.email',
                'bandusers.mobile',
                'bandusers.relationship_with_applicant',
                'bandusers.package_name',
                'bandusers.status'
            );
            $searchFieldsArray = array(
                'bandusers.uniqueid',
                'bandusers.firstname',
                'bandusers.email',
                'bandusers.mobile',
                'bandusers.relationship_with_applicant',
                'bandusers.package_name',
                'bandusers.status'
            );

            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchFieldsArray[$request['order'][0]['column']]) ? $searchFieldsArray[$request['order'][0]['column']] : 'bandusers.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : ''
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->where(array('guardians.id' => $guardiansId));

            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions, $searchFieldsArray) {
                    foreach ($searchFieldsArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $response['records'] = $query->get()->toArray();
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * 
     * @param type $request
     * @param type $userId
     * @return boolean
     * @throws \Exception
     */
    public function bandUsersListingsForApproval($request) {
        $response = [];
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.title',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.status',
                'aadhaar_information_capture.status as aadhaar_verify_status',
                'bandusers.package_name',
            );
            $searchFieldsArray = array(
                'bandusers.uniqueid',
                'bandusers.firstname',
                'bandusers.status',
                'users.firstname',
                'bandusers.package_name',
            );
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchFieldsArray[$request['order'][0]['column']]) ? $searchFieldsArray[$request['order'][0]['column']] : 'bandusers.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : ''
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->leftjoin('aadhaar_information_capture', function($join) {
                        $join->on('aadhaar_information_capture.banduser_id', '=', 'bandusers.id');
                    })
                    ->where(array('bandusers.status' => 'pending', 'users.status' => 'active'))
                    ->where(array('bandusers.save_as_draft' => '1'));
            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchFieldsArray) {

                    foreach ($searchFieldsArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } elseif ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $data = $query->get()->toArray();
            foreach ($data as $key => $value) {
                if (isset($value['utitle']) && !empty($value['utitle']) && ($value['utitle'] == 'Others' || $value['utitle'] == 'Default')) {
                    $data[$key]['guardian_name'] = ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                } else {
                    $data[$key]['guardian_name'] = ucwords($value['utitle']) . ' ' . ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                }
                if (isset($value['title']) && !empty($value['title']) && ($value['title'] == 'Other' || $value['utitle'] == 'default')) {
                    $data[$key]['banduser_name'] = ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                } else {
                    $data[$key]['banduser_name'] = ucwords($value['title']) . ' ' . ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                }
            }

            $response['records'] = $data;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    public function getCountBanduserByUniqueId($uniqueId) {
        try {
            $response = $this->banduserModelObj->where('uniqueid', '=', $uniqueId)
                    ->count();
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function getBanduserByUniqueId($uniqueId) {
        try {
            $response = $this->banduserModelObj
                    ->select('bandusers.*',
                            'users.status as ustatus'
                    )
                    ->join('guardians', 'guardians.id', 'bandusers.guardians_id')
                    ->join('users', 'users.id', 'guardians.user_id')
                    ->where('bandusers.uniqueid', '=', $uniqueId)
                    ->where('bandusers.status', '=', 'active')
                    ->where('users.status', '=', 'active')
//                    ->where('bandusers.save_as_draft', '=', 1)
                    ->first();

            if (!empty($response)) {
                $result = $response->toArray();
                $result['fullname'] = ucfirst($result['title']) . '. ' . ucfirst($result['firstname']) . ' ' . ucfirst($result['lastname']);
                $result['mobile'] = !empty($result['mobile']) ? $result['mobile'] : '';
                $result['alternate_mobile'] = !empty($result['alternate_mobile']) ? $result['alternate_mobile'] : '';
                $result['fulladdress'] = '';
                if (!empty($result['address']))
                    $result['fulladdress'] = ucwords($result['address']) . ', ' . ucwords($result['locality']) . ', ' . ucwords($result['city']) . ', ' . ucwords($result['state']) . ', India';
                $ailingsDetails = $this->bandusersAilingsModelObj->select('bandusers_ailings.ailing_id',
                                        'bandusers_ailings.*',
                                        'ailings.name'
                                )
                                ->join('ailings', 'ailings.id', 'bandusers_ailings.ailing_id')
                                ->where('bandusers_ailings.bandusers_id', '=', $result['id'])->take(2)->get()->toArray();
                $ailignIds = array();
                $ailingsName = array();
                $emergencyMedication = array();
                $preferredDoctor = array();
                $hospitalPreference = array();
                $hospitalRegistration = array();
                if (!empty($ailingsDetails)) {
                    $index = 0;
                    foreach ($ailingsDetails as $key => $value) {
                        if (!in_array($value['ailing_id'], $ailignIds)) {
                            $ailignIds[$index] = $value['ailing_id'];
                            $ailingsName[$index] = $value['name'];
                            $emergencyMedication[$index] = $value['recent_illness'];
                            $preferredDoctor[$index] = $value['primary_physician'];
                            $hospitalPreference[$index] = $value['hospital_preference'];
                            $hospitalRegistration[$index] = $value['hospital_registration_no'];
                        }
                        $index++;
                    }
                }
                //profile image 
                $images = $this->banduserImageModelObj->where('bandusers_id', '=', $result['id'])
                        ->where('file_type', '!=', 'qr-code')
                        ->get();
                if (!empty($images)) {
                    $images = $images->toArray();
                    $result['profile_img'] = '';
                    foreach ($images as $imgkey => $imgvalue) {
                        if (!empty($imgvalue['file_path'])) {
                            if (empty($result['profile_img'])) {
                                $result['profile_img'] = url('/') . '/' . $imgvalue['file_path'];
                            }
                        }
                    }
                }
                $result['condition1'] = isset($ailingsName) && !empty($ailingsName) ? $ailingsName[0] : '';
                $result['condition2'] = isset($ailingsName) && !empty($ailingsName) ? @$ailingsName[1] : '';
                $result['emergencyMedication'] = isset($emergencyMedication) && !empty($emergencyMedication) ? implode(', ', $emergencyMedication) : '';
                $result['preferredDoctor'] = isset($preferredDoctor) && !empty($preferredDoctor) ? implode(', ', $preferredDoctor) : '';
                $result['hospitalPreference'] = isset($hospitalPreference) && !empty($hospitalPreference) ? implode(', ', $hospitalPreference) : '';
                $result['hospitalRegistration'] = isset($hospitalRegistration) && !empty($hospitalRegistration) ? implode(', ', $hospitalRegistration) : '';
                return $result;
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * confirmBandusersListing
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function confirmBandusersListing($postData) {
        try {
            $result = [];
            $conditions = array(
                'search' => isset($postData['search']) ? $postData['search'] : '',
                'start' => isset($postData['start']) ? $postData['start'] : '0',
                'limit' => isset($postData['length']) ? $postData['length'] : ''
            );
            $query = $this->banduserModelObj->select($this->selectFields)
                    ->join('users', function($join) {
                $join->on('users.id', '=', 'bandusers.created_by');
            });
            $query = $query->where('created_by', '=', Auth::user()->id);
            $query = $query->where('save_as_draft', '=', 0);
            $query = $query->where('bandusers.status', '=', 'draft');
            $query = $query->where('bandusers.package_name', '=', $postData['package_name']);

            $query->limit($conditions['limit'])->offset($conditions['start']);
            $result['total_count'] = $query->count();
            $result['records'] = $query->get()->toArray();
            $result['incomplete_results'] = false;
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * checkMobileNumber
     * @param type $mobileNumber
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function checkMobileNumber($mobileNumber, $id) {
        try {
            $response = $this->banduserModelObj->where('id', '=', $id)->where('mobile', '=', $mobileNumber)->count();
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * deleteAadhaarCardNumber
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function deleteAadhaarCardNumber($id) {
        try {
            $response = $this->banduserModelObj
                    ->where('id', '=', $id)
                    ->update(['aadhaar_card_number' => NULL]);
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * activeBandUsersListings
     * @param type $request
     * @param type $userId
     * @return boolean
     * @throws \Exception
     */
    public function activeBandUsersListings($request) {
        $response = [];
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.title',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.created_at',
                'bandusers.package_name',
                'banduser_payment_transactions.payment_date'
            );
            $searchFieldsArray = array(
                'bandusers.uniqueid',
                'bandusers.firstname',
                'bandusers.status',
                'users.firstname',
                'bandusers.mobile',
                'bandusers.package_name',
                'banduser_payment_transactions.payment_date'
            );
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchFieldsArray[$request['order'][0]['column']]) ? $searchFieldsArray[$request['order'][0]['column']] : 'bandusers.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : ''
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->leftjoin(
                        DB::raw("(
                                select * from banduser_subscription_transactions
                                where id in (
                                    select  max(id) max_id from banduser_subscription_transactions
                                    group by banduser_id
                                )
                            ) as MaxBandUserSubTrans"), function($join) {
                        $join->on("MaxBandUserSubTrans.banduser_id", "=", "bandusers.id");
                    })
                    ->leftjoin(
                                DB::raw("(
                                select subscription_id,max(id) max_payment_transaction_id from banduser_payment_transactions
                                group by subscription_id
                                ) as MaxBandUserPaymentTrans"), function($join) {
                            $join->on("MaxBandUserPaymentTrans.subscription_id", "=", "MaxBandUserSubTrans.subscription_id");
                    })  
                    ->leftjoin('banduser_payment_transactions', function($join) {
                        $join->on('banduser_payment_transactions.id', '=', 'MaxBandUserPaymentTrans.max_payment_transaction_id');
                    })
                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.status' => 'active'));
//                    below condition is comment for bypassing payment gateway after payment gateway integrate this condition must apply.
//                    ->where(array('bandusers.save_as_draft' => '1'));
            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchFieldsArray) {

                    foreach ($searchFieldsArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } elseif ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $data = $query->get()->toArray();
            foreach ($data as $key => $value) {
                if (isset($value['utitle']) && !empty($value['utitle']) && ($value['utitle'] == 'Others' || $value['utitle'] == 'Default')) {
                    $data[$key]['guardian_name'] = ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                } else {
                    $data[$key]['guardian_name'] = ucwords($value['utitle']) . ' ' . ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                }
                if (isset($value['title']) && !empty($value['title']) && ($value['title'] == 'Other' || $value['utitle'] == 'default')) {
                    $data[$key]['banduser_name'] = ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                } else {
                    $data[$key]['banduser_name'] = ucwords($value['title']) . ' ' . ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                }
            }

            $response['records'] = $data;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * inactiveBandUsersListings
     * @param type $request
     * @param type $userId
     * @return boolean
     * @throws \Exception
     */
    public function inactiveBandUsersListings($request) {
        $response = [];
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.title',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.package_name',
                'bandusers.created_at',
            );
            $searchFieldsArray = array(
                'bandusers.uniqueid',
                'bandusers.firstname',
                'bandusers.status',
                'users.firstname',
                'bandusers.mobile',
                'bandusers.package_name',
            );
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchFieldsArray[$request['order'][0]['column']]) ? $searchFieldsArray[$request['order'][0]['column']] : 'bandusers.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : ''
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->where(array('users.status' => 'inactive'))
                    ->orWhere(array('bandusers.status' => 'inactive'));
//                    below condition is comment for bypassing payment gateway after payment gateway integrate this condition must apply.
//                    ->where(array('bandusers.save_as_draft' => '1'));
            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchFieldsArray) {

                    foreach ($searchFieldsArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } elseif ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $data = $query->get()->toArray();
            foreach ($data as $key => $value) {
                if (isset($value['utitle']) && !empty($value['utitle']) && ($value['utitle'] == 'Others' || $value['utitle'] == 'Default')) {
                    $data[$key]['guardian_name'] = ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                } else {
                    $data[$key]['guardian_name'] = ucwords($value['utitle']) . ' ' . ucwords($value['ufirstname']) . ' ' . ucwords($value['ulastname']);
                }
                if (isset($value['title']) && !empty($value['title']) && ($value['title'] == 'Other' || $value['utitle'] == 'default')) {
                    $data[$key]['banduser_name'] = ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                } else {
                    $data[$key]['banduser_name'] = ucwords($value['title']) . ' ' . ucwords($value['firstname']) . ' ' . ucwords($value['lastname']);
                }
            }

            $response['records'] = $data;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * getBandUsersDetails
     * @param type $id
     */
    public function getBandUsersDetails($id) {
        try {
            $bandUsersResponseArray = [];
            $bandUsersResponse = $this->banduserModelObj
                    ->select('bandusers.*', 'users.mobile as guardian_mobile')
                    ->join('guardians', 'guardians.id', 'bandusers.guardians_id')
                    ->join('users', 'users.id', 'guardians.user_id')
                    ->where('bandusers.id', '=', $id)
                    ->first();
            if (isset($bandUsersResponse) && !empty($bandUsersResponse) && $bandUsersResponse->count() > 0) {
                $bandUsersResponseArray = $bandUsersResponse->toArray();
                $bandUsersQrCode = \App\Models\BanduserImage::where('bandusers_id', '=', $id)->where('file_type', '=', 'qr-code')->get();
                if (isset($bandUsersQrCode) && !empty($bandUsersQrCode) && $bandUsersQrCode->count() > 0) {
                    $bandUsersQrCodeArrray = $bandUsersQrCode->toArray();
                    $bandUsersResponseArray['bandusers_qrcode'] = $bandUsersQrCodeArrray;
                }
            }
            return $bandUsersResponseArray;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function getDetailsForMail($id) {
        try {
            $response = [];
            $response = $this->banduserModelObj->select('bandusers.uniqueid',
                                    'bandusers.firstname',
                                    'bandusers.lastname',
                                    'users.firstname as guardian_firstname',
                                    'users.lastname as guardian_lastname',
                                    'users.email',
                                    'bandusers.status'
                            )
                            ->join('guardians', 'guardians.id', 'bandusers.guardians_id')
                            ->join('users', 'users.id', 'guardians.user_id')
                            ->where('bandusers.id', '=', $id)
                            ->where('bandusers.status', '=', 'active')
                            ->where('bandusers.uniqueid', '!=', '')
                            ->get()->toArray();

            return $response;
        } catch (\Expectional $exc) {
            throw $ex;
        }
    }

    /**
     * createSubscription
     * @return string
     */
    public function createSubscription($data) {
        try {
            $result = [];
            try {
                // Get credentials 
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);
                if($data['package_name'] == env('RAZORPAY_BASIC_PACKAGE')){
                    $planDetails = $api->plan->fetch($credentials['basic_plan_id']);
                }else{
                    $planDetails = $api->plan->fetch($credentials['plan_id']);
                }
//                \Log::info('$planDetails >> ' .print_r($planDetails, true));exit;
                $subscription = $api->subscription->create(array(
                    'plan_id' => $planDetails->id,
                    'customer_notify' => 1,
                    'quantity' => $data['quantity'],
                    'total_count' => 6,
                        )
                );
                if (isset($subscription) && !empty($subscription)) {
                    $guardian_id = \App\Models\Guardian::where('user_id', Auth::user()->id)->get(['id'])->toArray();
                    if (isset($guardian_id) && !empty($guardian_id)) {
                        $guardianSubscriptionArray = array(
                            'guardian_id' => $guardian_id[0]['id'],
                            'subscription_id' => $subscription->id,
                            'total_count' => $data['quantity'],
                        );
                    }
                    $guardian_subscriptions = \App\Models\GuardianSubscriptions::create($guardianSubscriptionArray);
                    if (isset($guardian_subscriptions) && !empty($guardian_subscriptions)) {

                        if ($data['quantity'] > 1) {
                            $bandUserIds = explode(",", $data['banduser_ids']);
                            foreach ($bandUserIds as $bandUserId) {
                                $banduserSubscriptionTransactionsArray = array(
                                    'guardian_subscription_id' => $guardian_subscriptions->id,
                                    'banduser_id' => $bandUserId,
                                    'subscription_id' => $subscription->id,
                                );
                                \App\Models\BanduserSubscriptionTransactions::create($banduserSubscriptionTransactionsArray);
                            }
                        } else {
                            $banduserSubscriptionTransactionsArray = array(
                                'guardian_subscription_id' => $guardian_subscriptions->id,
                                'banduser_id' => $data['banduser_ids'],
                                'subscription_id' => $subscription->id,
                            );
                            \App\Models\BanduserSubscriptionTransactions::create($banduserSubscriptionTransactionsArray);
                        }
                    }
                }
                $result['planAmount'] = $planDetails['item']->amount;
                $result['subscription_id'] = $subscription->id;
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'BanduserRepository/createSubscription';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $result['rzerror'] = $errorLog['error_message'];
            }
            return $result;
        } catch (\Expection $ex) {
            $result = [];
            $result['error'] = $ex->getMessage();

            return $result;
        }
    }

    /**
     * saveTransactionDetails
     * @return string
     */
    public function saveTransactionDetails($data) {
        try {
            try {
                // Get credentials 
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);

                $subscriptionDetails = $api->subscription->fetch($data['subscription_id']);
                $updateBanduserData = [];
                $updateBanduserData['status'] = 'pending';
                $updateBanduserData['save_as_draft'] = 1;
                \App\Models\Banduser::whereIn('id', $data['banduser_ids'])->update($updateBanduserData);

                \App\Models\GuardianSubscriptions::where('subscription_id', '=', $subscriptionDetails->id)->update(array('status' => 'active'));

                $updateBanduserSubscriptionTransactionsData = [];
                $updateBanduserSubscriptionTransactionsData['start_date'] = date('Y-m-d', $subscriptionDetails->current_start);
                $updateBanduserSubscriptionTransactionsData['end_date'] = date('Y-m-d', $subscriptionDetails->current_end);
                $updateBanduserSubscriptionTransactionsData['status'] = 'active';

                \App\Models\BanduserSubscriptionTransactions::where('subscription_id', '=', $subscriptionDetails->id)->update($updateBanduserSubscriptionTransactionsData); // Update path            

                $paymentDetails = $api->payment->fetch($data['payment_id']); // Fetch Payment details

                $invoiceNumber = '';
                if (isset($credentials['last_invoice_number']) && !empty($credentials['last_invoice_number'])) {
                    $prefix = substr($credentials['last_invoice_number'], 0, 4);
                    $lastInvoiceNo = substr($credentials['last_invoice_number'], 4);
                    $addInvoiceNumber = $lastInvoiceNo + 1;
                    $invoiceNumber = $prefix . $addInvoiceNumber;
                }
                $banduserPaymentTransactionsArray = array(
                    'subscription_id' => $data['subscription_id'],
                    'payment_id' => $data['payment_id'],
                    'invoice_number' => $invoiceNumber,
                    'payment_date' => date("Y-m-d"),
                    'signature' => $data['signature'],
                    'status' => $paymentDetails->status,
                );
                $result = \App\Models\BanduserPaymentTransactions::create($banduserPaymentTransactionsArray);

                $this->razorpayCredentialsRepository->updateLastInvoiceNumber($invoiceNumber);

                if ($result) {
                    // Send email to guardian
                    $guardianSubID = \App\Models\GuardianSubscriptions::select('guardian_subscriptions.id')->where('subscription_id', '=', $subscriptionDetails->id)->first()->toArray();
                    $invoice = $this->downloadInvoice($guardianSubID['id']);
                    view()->share(compact('invoice'));
                    $pdf = \PDF::loadView('banduser_transactions.invoice');
                    $fileName = "Invoice-" . $invoice['invoiceNumber'] . ".pdf";
                    $dirName = storage_path('invoice_pdf');
                    if (!is_dir($dirName)) {
                        mkdir($dirName, 0777, true);
                    }
                    $pdf->save($dirName . '/' . $fileName);

                    $maildata = $invoice;
                    $maildata['subject'] = 'Your MyWanderLoop subscription';
                    $maildata['guardianName'] = $invoice['guardianName'];
                    if($data['package_name'] == env('RAZORPAY_BASIC_PACKAGE')){
                        $emailContent = 'emails.payment_received_notification_email_for_basic_package';
                    }else{
                        $emailContent = 'emails.payment_received_notification_email';                        
                    }
                    Mail::send($emailContent, ['maildata' => $maildata], function($message) use ($maildata, $fileName) {
                        $pdf_file = storage_path('invoice_pdf') . '/' . $fileName;
                        $message->attach($pdf_file);
                        $message->subject($maildata['subject']);
                        $message->to($maildata['guardianEmail']);
                        $message->bcc('webassic5@gmail.com');
                    });
                    if (count(Mail::failures()) == 0) { // Send mail successfully and remove pdf file 
                        unlink(storage_path('invoice_pdf') . '/' . $fileName);
                    }
                }
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'BanduserRepository/createSubscription';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $result['rzerror'] = $errorLog['error_message'];
            }
            return $result;
        } catch (\Expection $ex) {
            $result = [];
            $result['error'] = $ex->getMessage();
            return $result;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function activeBandUsersExport() {
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
//                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.email',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.created_at',
                'bandusers.package_name',
                'banduser_payment_transactions.payment_date'
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->leftjoin(
                        DB::raw("(
                                select * from banduser_subscription_transactions
                                where id in (
                                    select  max(id) max_id from banduser_subscription_transactions
                                    group by banduser_id
                                )
                            ) as MaxBandUserSubTrans"), function($join) {
                        $join->on("MaxBandUserSubTrans.banduser_id", "=", "bandusers.id");
                    })
                    ->leftjoin(
                                DB::raw("(
                                select subscription_id,max(id) max_payment_transaction_id from banduser_payment_transactions
                                group by subscription_id
                                ) as MaxBandUserPaymentTrans"), function($join) {
                            $join->on("MaxBandUserPaymentTrans.subscription_id", "=", "MaxBandUserSubTrans.subscription_id");
                    })  
                    ->leftjoin('banduser_payment_transactions', function($join) {
                        $join->on('banduser_payment_transactions.id', '=', 'MaxBandUserPaymentTrans.max_payment_transaction_id');
                    })
                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.status' => 'active'))
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function inactiveBandUsersExport() {
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
//                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.email',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.created_at',
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->where(array('users.status' => 'inactive'))
                    ->orWhere(array('bandusers.status' => 'inactive'))
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function bandusersExport($id) {
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
//                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.email',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.status',
                'bandusers.created_at',
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'guardians.user_id');
                    })
                    ->where(array('guardians.id' => $id))
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function exportUserBanduser() {
        try {
            $selectFieldsArray = array(
                'bandusers.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
//                'guardians.title as utitle',
                'bandusers.uniqueid',
                'bandusers.email',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.mobile',
                'bandusers.status',
                'bandusers.created_at',
            );
            $query = $this->banduserModelObj->select($selectFieldsArray)
                    ->join('users', function($join) {
                $join->on('users.id', '=', 'bandusers.created_by');
            });
            $query = $query->where('created_by', '=', Auth::user()->id);
            $query = $query->orderBy('bandusers.id', 'DESC')
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * bandusersTransactionListing
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function bandusersTransactionListing($postData) {
        try {
            $selectFieldsArray = array(
                'banduser_subscription_transactions.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.status',
                'bandusers.package_name',
                'guardian_subscriptions.id as guardian_subscription_id',
                'guardian_subscriptions.subscription_id',
                'banduser_subscription_transactions.start_date',
                'banduser_subscription_transactions.end_date',
                'banduser_subscription_transactions.status',
                'banduser_payment_transactions.payment_date',
                'banduser_payment_transactions.payment_id',
                'banduser_payment_transactions.invoice_number',
                'banduser_payment_transactions.start_date as payment_start_date',
                'banduser_payment_transactions.end_date as payment_end_date'
            );     
            $result = [];
            $conditions = array(
                'search' => isset($postData['search']) ? $postData['search'] : '',
                'start' => isset($postData['start']) ? $postData['start'] : '',
                'order' => isset($this->searchTransactionGroupsArray[$postData['order'][0]['column']]) ? 
                        $this->searchTransactionGroupsArray[$postData['order'][0]['column']] : 'bandusers.updated_at',
                'order_dir' => isset($postData['order'][0]['dir']) ? $postData['order'][0]['dir'] : 'ASC',
                'limit' => isset($postData['length']) ? $postData['length'] : '500'
            );           
            $query = \App\Models\BanduserSubscriptionTransactions::select($selectFieldsArray)
                            ->join('bandusers', function($join) {
                                $join->on('bandusers.id', '=', 'banduser_subscription_transactions.banduser_id');
                            })
                            ->join('guardian_subscriptions', function($join) {
                                $join->on('guardian_subscriptions.id', '=', 'banduser_subscription_transactions.guardian_subscription_id');
                            })
                            ->join('users', function($join) {
                                $join->on('users.id', '=', 'bandusers.created_by');
                            })
                            ->join('banduser_payment_transactions', function($join) {
                        $join->on('banduser_payment_transactions.subscription_id', '=', 'guardian_subscriptions.subscription_id');
                    });

            if (Auth::user()->user_type != 'Administration') {
                $query->where('bandusers.created_by', '=', Auth::user()->id);
            }           
            if (!empty($conditions['search']['value'])) {
                $searchFieldsArray = $this->searchTransactionGroupsArray;
                $query->where(function($query) use ($conditions, $searchFieldsArray) {
                    foreach ($this->searchTransactionGroupsArray as $searchGroup) {
                        if ($searchGroup == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } elseif ($searchGroup == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query = $query->orWhere($searchGroup, 'like', "%" . $conditions['search']['value'] . "%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $result['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $result['records'] = $query->get()->toArray();
            foreach($result['records'] as $key => &$val){
                // start date replace
                if($val['payment_start_date'] != null){
                    $val['start_date'] = $val['payment_start_date'];
                } 

                // end date replace 
                if($val['payment_end_date'] != null){
                    $val['end_date'] = $val['payment_end_date'];
                } 
            }
            $result['incomplete_results'] = false;
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * cancelSubscription
     * @return string
     */
    public function cancelSubscription($id) {
        try {
            $transactionResponse = \App\Models\BanduserSubscriptionTransactions:: select('banduser_subscription_transactions.*')
                            ->where('id', '=', $id)
                            ->first()->toArray();

            if (isset($transactionResponse) && !empty($transactionResponse)) {
                // Get credentials 
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);

                $subscriptionDetails = $api->subscription->fetch($transactionResponse['subscription_id']);
                \Log::info('$transactionResponse >> ' .print_r($transactionResponse, true));
                \Log::info('$subscriptionDetails >> ' .print_r($subscriptionDetails, true));
                $getGuardianSub = \App\Models\GuardianSubscriptions::select('guardian_subscriptions.total_count')->where('subscription_id', '=', $subscriptionDetails->id)->first()->toArray();
                \Log::info('$getGuardianSub >> ' .print_r($getGuardianSub, true));
                if ($getGuardianSub['total_count'] > 1) {
                    \Log::info('in iff');
                    $quantity = $getGuardianSub['total_count'] - 1;

                    $subscriptionData = array(
                        'plan_id' => $subscriptionDetails->plan_id,
                        'schedule_change_at' => "cycle_end",
                        'customer_notify' => 1,
                        'quantity' => $quantity,
                    );
                    $updateSubscription = $this->get_curl_update_subscriptions($credentials, $transactionResponse['subscription_id'], $subscriptionData);
                    \Log::info('$updateSubscription >> ' .print_r($updateSubscription, true));
                } else {
                    \Log::info('in else');
                    $subscriptionData = array(
                        'cancel_at_cycle_end' => 1
                    );
                    $updateSubscription = $api->subscription->fetch($transactionResponse['subscription_id'])->cancel($subscriptionData);
                    \Log::info('$updateSubscription >> ' .print_r($updateSubscription, true));
                }
                $updateBanduserSubscriptionTransactionsData = [];
                $updateBanduserSubscriptionTransactionsData['status'] = 'cancel';
                $updateBanduserSubscriptionTransactionsData['cancelled_on'] = date('Y-m-d');
                $updateBanduserSubscriptionTransactionsData['end_date'] = date('Y-m-d', $subscriptionDetails->current_end);
                \App\Models\BanduserSubscriptionTransactions::where('id', '=', $transactionResponse['id'])->update($updateBanduserSubscriptionTransactionsData); // Update path            
                $result = \App\Models\GuardianSubscriptions::where('subscription_id', '=', $subscriptionDetails->id)->update(array('total_count' => ($getGuardianSub['total_count'] - 1)));
                \Log::info('$result >> ' .print_r($result, true));
                return $result;
            }
        } catch (\Expection $ex) {
            $result = [];
            $result['error'] = $ex->getMessage();
            return $result;
        }
    }

    // initialized cURL Request subscription
    private function get_curl_update_subscriptions($credentials, $subscription_id, $subscriptionData) {
        $data_string = json_encode($subscriptionData);
        $api_key = $credentials['key_id'];
        $password = $credentials['key_secret'];
        $url = 'https://api.razorpay.com/v1/subscriptions/' . $subscription_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $password);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json')
        );

        if (curl_exec($ch) === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
        $errors = curl_error($ch);
        $result = curl_exec($ch);
        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $finalResult = json_decode($result);
        return $finalResult;
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function exportBanduserTransactions() {
        try {
            $selectFieldsArray = array(
                'banduser_subscription_transactions.id',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.status',
                'guardian_subscriptions.subscription_id',
                'banduser_subscription_transactions.start_date',
                'banduser_subscription_transactions.end_date',
                'banduser_payment_transactions.payment_id',
                'banduser_payment_transactions.invoice_number',
                'banduser_payment_transactions.payment_date',
                'banduser_payment_transactions.start_date as payment_start_date',
                'banduser_payment_transactions.end_date as payment_end_date'
            );
            $query = \App\Models\BanduserSubscriptionTransactions::select($selectFieldsArray)
                    ->join('bandusers', function($join) {
                        $join->on('bandusers.id', '=', 'banduser_subscription_transactions.banduser_id');
                    })
                    ->join('guardian_subscriptions', function($join) {
                        $join->on('guardian_subscriptions.id', '=', 'banduser_subscription_transactions.guardian_subscription_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'bandusers.created_by');
                    })
                    ->join('banduser_payment_transactions', function($join) {
                $join->on('banduser_payment_transactions.subscription_id', '=', 'guardian_subscriptions.subscription_id');
            });
            if (Auth::user()->user_type != 'Administration') {
                $query->where('bandusers.created_by', '=', Auth::user()->id);
            }
            // $query->where('bandusers.save_as_draft', '=', 1);
            // $query->where('bandusers.status', '=', 'pending');
            $query = $query->orderBy('bandusers.id', 'DESC')
                    ->get()
                    ->toArray();
            if(isset($query) && !empty($query)){
                foreach($query as $k => &$v){
                    // start date replace
                    if($v['payment_start_date'] != null){
                        $v['start_date'] = $v['payment_start_date'];
                    }

                    // end date replace 
                    if($v['payment_end_date'] != null){
                        $v['end_date'] = $v['payment_end_date'];
                    }
                }
            }       
            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * downloadInvoice
     * @return string
     */
    public function downloadInvoice($id,$paymentId) {
        try {
            $selectFieldsArray = array(
                'banduser_subscription_transactions.id',
                'bandusers.firstname',
                'bandusers.lastname',
                'bandusers.package_name',
                'users.firstname as ufirstname',
                'users.lastname as ulastname',
                'users.email',
                'guardian_subscriptions.subscription_id',
                'guardians.address',
                'guardians.locality',
                'guardians.city',
                'guardians.state',
                'banduser_subscription_transactions.start_date',
                'banduser_subscription_transactions.end_date',
                'banduser_payment_transactions.invoice_number',
                'banduser_payment_transactions.created_at as invoice_date',
                'banduser_payment_transactions.payment_date',
                'banduser_payment_transactions.payment_id',
                'banduser_payment_transactions.start_date as payment_start_date',
                'banduser_payment_transactions.end_date as payment_end_date'
            );
            $query = \App\Models\GuardianSubscriptions::select($selectFieldsArray)
                    ->join('banduser_subscription_transactions', function($join) {
                        $join->on('banduser_subscription_transactions.guardian_subscription_id', '=', 'guardian_subscriptions.id');
                    })
                    ->join('bandusers', function($join) {
                        $join->on('bandusers.id', '=', 'banduser_subscription_transactions.banduser_id');
                    })
                    ->join('guardians', function($join) {
                        $join->on('guardians.id', '=', 'bandusers.guardians_id');
                    })
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'bandusers.created_by');
                    })
                    ->join('banduser_payment_transactions', function($join) {
                $join->on('banduser_payment_transactions.subscription_id', '=', 'guardian_subscriptions.subscription_id');
            });
            $query->where(array('guardian_subscriptions.id' => $id,
                                'banduser_payment_transactions.payment_id' => $paymentId));

            $resultArr = $query->get()->toArray();
            $finalResult = [];
            $bandUser = [];
            if(isset($resultArr) && !empty($resultArr)){
                foreach($resultArr as $key => &$val){
                    // start date replace
                    if($val['payment_start_date'] != null){
                        $val['start_date'] = $val['payment_start_date'];
                    } 
    
                    // end date replace 
                    if($val['payment_end_date'] != null){
                        $val['end_date'] = $val['payment_end_date'];
                    } 
                }
            }
            
            if (isset($resultArr) && !empty($resultArr)) {
                // Get credentials 
                $credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
                $api = new Api($credentials['key_id'], $credentials['key_secret']);
                if($resultArr[0]['package_name'] == env('RAZORPAY_BASIC_PACKAGE')){
                    $planDetails = $api->plan->fetch($credentials['basic_plan_id']);
                }else{
                    $planDetails = $api->plan->fetch($credentials['plan_id']);
                }

                $planAmount = (substr($planDetails['item']->amount, 0, -2));
                $currency = $planDetails['item']->currency;
                $finalResult['paymentStartDate'] = date('d F Y', strtotime($resultArr[0]['start_date']));
                $finalResult['paymentEndDate'] = date('d F Y', strtotime($resultArr[0]['end_date']));
                $finalResult['invoiceNumber'] = $resultArr[0]['invoice_number'];
                $finalResult['invoiceDate'] = date('d F Y', strtotime($resultArr[0]['invoice_date']));
                $finalResult['guardianName'] = $resultArr[0]['ufirstname'] . " " . $resultArr[0]['ulastname'];
                $finalResult['guardianEmail'] = $resultArr[0]['email'];
                $finalResult['guardianAddress'] = $resultArr[0]['address'] . ", " . $resultArr[0]['locality'] . ", " . $resultArr[0]['city'] . ", " . $resultArr[0]['state'] . ", " . "India";
                $finalResult['guardianState'] = strtolower($resultArr[0]['state']);
                foreach ($resultArr as $key => $result) {
                    $bandUser[$key]['name'] = $result['firstname'] . " " . $result['lastname'];
                }
                $finalResult['bandUserList'] = $bandUser;
                $finalResult['quantity'] = count($bandUser);
                $finalResult['amount'] = $planAmount;
                $finalResult['currency'] = $currency;
                $finalResult['totalAmount'] = count($bandUser) * $planAmount;
                $finalResult['orignalCost'] = round($planAmount * 100 / (100 + $credentials['gst_rate']));
                $finalResult['gstAmount'] = $planAmount - $finalResult['orignalCost'];
                $finalResult['gstRate'] = $credentials['gst_rate'];
                $finalResult['packageName'] = ucfirst($resultArr[0]['package_name']);
            }
            return $finalResult;
        } catch (\Expection $ex) {
            $result = [];
            $result['error'] = $ex->getMessage();
            return $result;
        }
    }

    /**
     * getAadhharDetails
     * @param type $id
     */
    public function getAadhharCardDetails($id) {
        try {
            if (isset($id) && !empty($id)) {
                $getDetails = $this->banduserModelObj
                                ->select('bandusers.id', 'bandusers.share_code', 'bandusers.aadhaar_card_file_name', 'bandusers.aadhaar_card_file_path')
                                ->where('bandusers.id', '=', $id)
                                ->first()->toArray();
                return $getDetails;
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * saveAadhaarInformations
     * @param type $id
     * @param type $data
     * @param type $response
     * @return type
     */
    public function saveAadhaarInformations($id, $data, $response) {
        try {
            if (isset($data) && !empty($data)) {
                if (isset($data['image']) && !empty($data['image'])) {
                    $checkImagepath = public_path("banduser_details/banduser_" . $id . "/images/aadhaar_img.jpg");
                    $storeImagepath = "banduser_details/banduser_" . $id . "/images/aadhaar_img.jpg";
                    $checkImgPath = public_path("banduser_details/banduser_" . $id . "/images");
                    $latestImg = base64_decode($data['image']);
                    if (\File::exists($checkImgPath)) {
                        @unlink(($checkImagepath));
                    } else {
                        $oldMask = umask(0);
                        mkdir(($checkImgPath), 0777, true);
                        umask($oldMask);
                    }
                    // Save the image in a defined path
                    file_put_contents($checkImagepath, $latestImg);
                }

                $aadhaarInfoArr = array(
                    'banduser_id' => $id,
                    'username' => isset($data['name']) && !empty($data['name']) ? $data['name'] : '',
                    'dob' => isset($data['dob']) && !empty($data['dob']) ? $data['dob'] : NULL,
                    'gender' => isset($data['gender']) && !empty($data['gender']) ? $data['gender'] : '',
                    'address' => isset($data['address']) && !empty($data['address']) ? $data['address'] : '',
                    'image' => isset($data['image']) && !empty($data['image']) ? $storeImagepath : '',
                    'status' => $data['status'],
                    'response' => $response,
                );

                $getDetails = \App\Models\AadhaarInformation::select('aadhaar_information_capture.*')
                        ->where('aadhaar_information_capture.banduser_id', '=', $id)
                        ->first();
                if (isset($getDetails) && !empty($getDetails)) {
                    $saveUpdateDetails = \App\Models\AadhaarInformation::where('banduser_id', '=', $id)->update($aadhaarInfoArr);
                } else {
                    $saveUpdateDetails = \App\Models\AadhaarInformation::create($aadhaarInfoArr);
                }
            }
            return $saveUpdateDetails;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function getCountOfActiveBanduserByUniqueId($uniqueId) {
        try {
            $response = $this->banduserModelObj->where('uniqueid', '=', $uniqueId)
                    ->where('status', '=', 'active')
                    ->count();
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    
    /**
     * getPackageTypeDetailCountOfBandUsers
     * 
     */
    public function getPackageTypeDetailCountOfBandUsers() {
        try {
            $bandUsersResponseArray = [];
            $bandUsersResponse = $this->banduserModelObj
                    ->select('bandusers.package_name', DB::raw('count(*) as total'))
                    ->where('bandusers.save_as_draft', '=', 0)
                    ->where('bandusers.status', '=', 'draft')
                    ->where('created_by', '=', Auth::user()->id)
                    ->groupBy('package_name')
                    ->get()->toArray();
            if(isset($bandUsersResponse) && !empty($bandUsersResponse)){
                $bandUsersResponseArray = $bandUsersResponse;
            }
            return $bandUsersResponseArray;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}
