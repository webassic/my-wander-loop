<?php

namespace App\Repositories;

use App\Models\Banduserhistory;
use Auth;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class BanduserhistoryRepository extends BaseRepository { 
     /**
     * 
     * @param Banduserhistory $banduserhistoryModelObj
     */
    public function __construct(Banduserhistory $banduserhistoryModelObj) {
        $this->banduserhistoryModelObj = $banduserhistoryModelObj;
    }
    
}