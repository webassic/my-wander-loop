<?php

namespace App\Repositories;

use App\Models\Guardian;
use App\User;
use Auth;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class GuardiansRepository extends BaseRepository {

    private $selectFields = [
        'guardians.id',
        'users.firstname as firstname',
        'users.lastname as lastname',
        'users.email',
        'users.mobile',
        'guardians.institution_name',
        'guardians.guardian_type',
        'guardians.title',
        'guardians.dob',
        'guardians.address',
        'guardians.locality',
        'guardians.city',
        'guardians.state',
        'guardians.pin',
        'guardians.landline',
        'guardians.identity_type',
        'guardians.identity_number',
        'guardians.social_media_login',
        'guardians.renew_date',
        'guardians.is_exist',
        'guardians.created_at',
        'guardians.updated_at'
    ];

    /**
     * 
     * @param Assets $assetsModelObj
     */
    public function __construct(Guardian $guardianModelObj, User $userModelObj) {
        $this->guardianModelObj = $guardianModelObj;
        $this->userModelObj = $userModelObj;
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeGuardian($postData) {
        try {
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
            $data['dob'] = isset($data['dob']) && !empty($data['dob']) ? date('Y/m/d ', strtotime($data['dob'])) : '';
            if(!empty($data['dob']))
                 $data['dob'] =  str_replace('/', '-', $data['dob']);
                
            $result = $this->guardianModelObj->create($data);
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateGuardian($postData, $id) {
        try {
            $data = array();
            $userDetails=[];
            $response = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
//          
            $data['address'] = isset($data['address']) && !empty($data['address']) ? ucfirst($data['address']) : '';
            $data['locality'] = isset($data['locality']) && !empty($data['locality']) ? ucfirst($data['locality']) : '';
            $data['city'] = isset($data['city']) && !empty($data['city']) ? ucfirst($data['city']) : '';
            $data['state'] = isset($data['state']) && !empty($data['state']) ? ucfirst($data['state']) : '';
            if (empty($data['dob']))
                unset($data['dob']);
            if(isset($data['firstname']) && !empty( $data['firstname'])){
                $userDetails['firstname'] = $data['firstname'];
                unset($data['firstname']);
            }
            if(isset($data['lastname']) && !empty( $data['lastname'])){
                $userDetails['lastname'] = $data['lastname'];
                 unset($data['lastname']);
            }
            if(isset($data['mobile']) && !empty( $data['mobile'])){
                $userDetails['mobile'] = $data['mobile'];
                unset($data['mobile']);
            }
            if(!empty($userDetails)){
                $user_id = $this->guardianModelObj->where('id', $id)
                    ->get(['user_id'])->toArray();
            
                $this->userModelObj->where('id',$user_id)
                            ->update($userDetails);
            }
            
            
            $result = $this->guardianModelObj->where('id', $id)
                    ->update($data);
            if ($result) {
                $response = $this->guardianModelObj->find($id);
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showGuardian($userId) {
        try {
            $response = [];
            if (isset($userId) && !empty($userId) && $userId > 0) {
                $result = $this->guardianModelObj->select($this->selectFields)
                        ->join('users', function($join) {
                            $join->on('users.id', '=', 'guardians.user_id');
                        })
                        ->where('user_id', '=', $userId)
                        ->first();
                if (isset($result) && !empty($result) && $result->count() > 0) {
                    $response = $result->toArray();
//                   
                    $response['dob'] = !empty($response['dob']) ? date('d-m-Y ', strtotime($response['dob'])) : '';
                    $response['dob'] =  str_replace('-', '/', $response['dob']);
                    
                }
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyGuardian($id) {
        try {
            $result = $this->guardianModelObj->where('id', $id)->delete();

            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function recordExist($userId) {
        try {
            $result = $this->guardianModelObj->where('user_id', '=', $userId)
                    ->where('guardian_type', '!=', null)
                    ->count();
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function recordExistGuardianType($userId) {
        try {
            $result = $this->guardianModelObj->where('user_id', '=', $userId)
                            ->get()->toArray();
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function storeGuardianType($postData) {
        try {
            $result = $this->guardianModelObj->create($postData);

            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * getGuardianID
     * @param type $userId
     * @return type
     * @throws \Exception
     */
    public function getGuardianID($userId) {
        try {
            $response = [];
            if (isset($userId) && !empty($userId) && $userId > 0) {
                $result = $this->guardianModelObj->select('guardians.id')
                        ->where('user_id', '=', $userId)
                        ->first();
                if (isset($result) && !empty($result)) {
                    $finalArr = $result->toArray();
                    $response = $finalArr['id'];
                }
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
