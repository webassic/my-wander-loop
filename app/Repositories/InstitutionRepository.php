<?php

namespace App\Repositories;

use App\Models\Institution;
use Auth;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class InstitutionRepository extends BaseRepository { 
    
    private $selectFields = [
        'institutions.id',
        'users.firstname as firstname',
        'users.lastname as lastname',
        'institutions.name',
        'institutions.address',
        'institutions.locality',
        'institutions.city',
        'institutions.state',
        'institutions.pin',
        'institutions.landline',
        'institutions.created_at',
        'institutions.updated_at'
        ];
   
    /**
     * 
     * @param Assets $assetsModelObj
     */
    public function __construct(Institution $institutionModelObj) {
        $this->institutionModelObj = $institutionModelObj;
    }
    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeInstitution($postData) {
       try{
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           
           $result= $this->institutionModelObj->create($data);
           return $result;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateInstitution($postData,$id) {
       try{
            $data = array();
            $response=array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           foreach($data as $key => $value){
                $updatedKey= trim(str_replace('u', '', $key));
                $updatedData[$updatedKey]=$value;
           }
           $result= $this->institutionModelObj->where('id',$id)
                   ->update($updatedData);
           if($result){
               $response = $this->institutionModelObj->find($id);
           }
           return $response;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showInstitution($userId){
         try{
            $result = $this->institutionModelObj->select($this->selectFields)
                    ->join('users', function($join) {
                        $join->on('users.id', '=', 'institutions.user_id');
                    })
                    ->where('user_id','=',$userId)
                    ->get();
//            dd($result);
           return $result;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyInstitution($id){
        try{
            $result = $this->institutionModelObj->where('id', $id)->delete();
            
           return $result;
        }catch(\Exception $ex){
           throw $ex;
        } 
    }
    
    public function recordExist($userId){
        try{
            $result = $this->institutionModelObj->where('user_id','=',$userId)
                    ->count();
//            dd($result);
           return $result;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
}