<?php

namespace App\Repositories;

use App\Models\NewBandOrders;
use Auth;
use App\Repositories\BanduserRepository;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\BadRequestError;
use App\Repositories\RazorpayCredentialsRepository;
use App\Models\BandOrdersTransactions;
use App\Models\RazorpayErrorsLog;
use DB;
use Illuminate\Support\Facades\Mail;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class NewBandOrdersRepository extends BaseRepository {

    private $selectFields = [
        'new_band_orders.id',
        'new_band_orders.banduser_id',
        'new_band_orders.band_lost',
        'new_band_orders.number_of_bands',
        'new_band_orders.total_amount',
        'new_band_orders.amount_paid',
        'new_band_orders.order_processed',
        'new_band_orders.order_date',
        'new_band_orders.order_shipped',
        'new_band_orders.delivered',
        'new_band_orders.created_at',
        'new_band_orders.updated_at',
        'bandusers.firstname',
        'bandusers.lastname',
        'bandusers.package_name',
        'users.firstname as ufirstname',
        'users.lastname as ulastname',
        'users.email',
        'guardians.address',
        'guardians.locality',
        'guardians.city',
        'guardians.state',
        'bandorders_transactions.rzpayment_id',
        'bandorders_transactions.rzorder_id',
        'bandorders_transactions.invoice_number',
    ];
    private $searchArray = array(
        'band_lost' => 'new_band_orders.band_lost',
        'number_of_bands' => 'new_band_orders.number_of_bands',
        'total_amount' => 'new_band_orders.total_amount',
        'firstname' => 'bandusers.firstname',
        'ufirstname' => 'users.firstname',
        'rzpayment_id' => 'bandorders_transactions.rzpayment_id',
    );
    private $api;
    private $razorpayCredentialsRepository;
    private $bandOrdersTransactions;
    private $credentials;
    private $newBandOrdersObj;

    /**
     * 
     * @param NewBandOrders $newBandOrdersObj
     */
    public function __construct(NewBandOrders $newBandOrdersObj, BanduserRepository $banduserRepository, RazorpayCredentialsRepository $razorpayCredentialsRepository, BandOrdersTransactions $bandOrdersTransactions) {
        $this->newBandOrdersObj = $newBandOrdersObj;
        $this->banduserRepository = $banduserRepository;
        $this->razorpayCredentialsRepository = $razorpayCredentialsRepository;
        $this->bandOrdersTransactions = $bandOrdersTransactions;
        $this->credentials = $this->razorpayCredentialsRepository->getRazorpayCredentials();
    }

    /**
     * 
     * @param type $param
     * @return type
     * @throws \Exception
     */
    public function storeNewBandOrder($param) {
        try {

            try {
                $api = new Api($this->credentials['key_id'], $this->credentials['key_secret']);
                $order = $api->order->create(array(
                    'receipt' => '123',
                    'amount' => $param['total_amount'] . '00',
                    'payment_capture' => 1,
                    'currency' => 'INR'
                        )
                );
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'NewBandOrdersRepository/storeNewBandOrder';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $error['rzerror'] = 'Opps! Order not proceed. Something went wrong.';
                return $error;
            }
            if (isset($param['band_lost']) && $param['band_lost'] == 'Yes') {
                $data['modified_by'] = Auth::user()->id;
                $data['uniqueid'] = $this->generateUniqueId();
                $qrcodeFileName = $this->banduserRepository->generateQrCode($data['uniqueid'], $param['banduser_id']);
                $id = $param['banduser_id'];
                if (!empty($data['uniqueid']) && !empty($qrcodeFileName)) {
                    $getDetails = \App\Models\BanduserImage::where('bandusers_id', '=', $id)
                                    ->where('file_type', '=', 'qr-code')
                                    ->get()->toArray();

                    if (empty($getDetails)) {
                        $bandUserQrcodeArr['bandusers_id'] = $id;
                        $bandUserQrcodeArr['file_type'] = 'qr-code';
                        $bandUserQrcodeArr['file_name'] = $qrcodeFileName;
                        $bandUserQrcodeArr['file_path'] = "banduser_details/banduser_" . $id . "/qr_codes/" . $qrcodeFileName;
                        $bandUserQrcodeArr['created_by'] = Auth::user()->id;
                        \App\Models\BanduserImage::create($bandUserQrcodeArr);
                    } else {
                        \App\Models\BanduserImage::where('id', '=', $getDetails[0]['id'])
                                ->update([
                                    'file_path' => "banduser_details/banduser_" . $id . "/qr_codes/" . $qrcodeFileName,
                                    'file_name' => $qrcodeFileName,
                                    'modified_by' => Auth::user()->id
                        ]);
                    }


                    $result = \App\Models\Banduser::where('id', '=', $id)
                            ->update($data);
                }
            }
            $create = $this->newBandOrdersObj->create($param);
            $transaxtionArray = [];
            if ((isset($create['id']) && !empty($create['id'])) && (isset($order['id']) && !empty($order['id']))) {
                $transaxtionArray['bandorders_id'] = $create['id'];
                $transaxtionArray['rzorder_id'] = $order['id'];
                $transaxtionArray['created_by'] = Auth::user()->id;
                $this->bandOrdersTransactions->create($transaxtionArray);
                $create['rzorder_id'] = $order['id'];
            }
            return $create;
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function adminNewBandOrdersListings($request) {
        try {
            $searchUsersArray = $this->searchArray;
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']]) ? $searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']] : 'new_band_orders.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'No'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'));
            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                }
            }
            $response['records'] = $allData;
            $response['incomplete_results'] = false;
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function exportAdminBandOrders() {
        try {
            $searchUsersArray = $this->searchArray;

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'No'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'));




            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                    $orderStatus = '';
                    if ($value['order_processed'] == 'No') {
                        $orderStatus = 'New Order';
                    } else if ($value['order_processed'] == 'Yes') {
                        $orderStatus = 'Order Processed';
                    }
                    if ($value['order_shipped'] == 'Yes') {
                        $orderStatus = 'Order Shipped';
                    }
                    if ($value['delivered'] == 'Yes') {
                        $orderStatus = 'Order Delivered';
                    }
                    $allData[$key]['order_status'] = $orderStatus;
                }
            }

            return $allData;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function exportAdminDeliveredOrders() {
        try {

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'Yes'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'));




            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                    $orderStatus = '';
                    if ($value['order_processed'] == 'No') {
                        $orderStatus = 'New Order';
                    } else if ($value['order_processed'] == 'Yes') {
                        $orderStatus = 'Order Processed';
                    }
                    if ($value['order_shipped'] == 'Yes') {
                        $orderStatus = 'Order Shipped';
                    }
                    if ($value['delivered'] == 'Yes') {
                        $orderStatus = 'Order Delivered';
                    }
                    $allData[$key]['order_status'] = $orderStatus;
                }
            }

            return $allData;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function exportUserBandOrders() {
        try {

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'No'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->where(array('users.id' => Auth::user()->id));




            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                    $orderStatus = '';
                    if ($value['order_processed'] == 'No') {
                        $orderStatus = 'New Order';
                    } else if ($value['order_processed'] == 'Yes') {
                        $orderStatus = 'Order Processed';
                    }
                    if ($value['order_shipped'] == 'Yes') {
                        $orderStatus = 'Order Shipped';
                    }
                    if ($value['delivered'] == 'Yes') {
                        $orderStatus = 'Order Delivered';
                    }
                    $allData[$key]['order_status'] = $orderStatus;
                }
            }

            return $allData;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function exportUserDeliveredOrders() {
        try {

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'Yes'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->where(array('users.id' => Auth::user()->id));




            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                    $orderStatus = '';
                    if ($value['order_processed'] == 'No') {
                        $orderStatus = 'New Order';
                    } else if ($value['order_processed'] == 'Yes') {
                        $orderStatus = 'Order Processed';
                    }
                    if ($value['order_shipped'] == 'Yes') {
                        $orderStatus = 'Order Shipped';
                    }
                    if ($value['delivered'] == 'Yes') {
                        $orderStatus = 'Order Delivered';
                    }
                    $allData[$key]['order_status'] = $orderStatus;
                }
            }

            return $allData;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function userNewBandOrdersListings($request) {
        try {
            $searchUsersArray = $this->searchArray;
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']]) ? $searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']] : 'new_band_orders.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'No'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->where(array('users.id' => Auth::user()->id));


            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                }
            }
            $response['records'] = $allData;
            $response['incomplete_results'] = false;
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function adminDeliveredListings($request) {
        try {

            $searchUsersArray = $this->searchArray;
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']]) ? $searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']] : 'new_band_orders.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
            );

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'Yes'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'));


            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                }
            }
            $response['records'] = $allData;
            $response['incomplete_results'] = false;
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function userDeliveredListings($request) {
        try {

            $searchUsersArray = $this->searchArray;
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']]) ? $searchUsersArray[$request['columns'][$request['order'][0]['column']]['name']] : 'new_band_orders.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
            );

            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('new_band_orders.delivered' => 'Yes'))
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
//                    ->where(array('bandusers.status' => 'active'))
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->where(array('users.id' => Auth::user()->id));


            if (!empty($conditions['search']['value'])) {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'bandusers.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`bandusers`.`firstname`, ' ', `bandusers`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $allData = $query->get()->toArray();
            if (!empty($allData)) {
                foreach ($allData as $key => $value) {
                    $allData[$key]['price_per_band'] = $this->credentials['banduser_cost'];
                    $allData[$key]['order_date'] = empty($value['order_date']) ? date('d M Y', strtotime($value['created_at'])) : date('d M Y', strtotime($value['order_date']));
                }
            }
            $response['records'] = $allData;
            $response['incomplete_results'] = false;
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateStatus($request, $id) {
        try {
            $getDetails = $this->newBandOrdersObj->where('id', $id)->get()->toArray();
            if (!empty($request) && isset($request['order_processed'])) {
                $request['order_processed_on'] = date('Y-m-d H:i:s');
            }
            if (!empty($request) && isset($request['order_shipped'])) {
                if (!empty($getDetails) && $getDetails[0]['order_processed'] == 'No') {
                    $request['order_processed'] = 'Yes';
                }
                if (!empty($getDetails) && empty($getDetails[0]['order_processed_on'])) {
                    $request['order_processed_on'] = date('Y-m-d H:i:s');
                }
                $request['order_shipped_on'] = date('Y-m-d H:i:s');
            }
            if (!empty($request) && isset($request['delivered'])) {
                if (!empty($getDetails) && $getDetails[0]['order_processed'] == 'No') {
                    $request['order_processed'] = 'Yes';
                }
                if (!empty($getDetails) && empty($getDetails[0]['order_processed_on'])) {
                    $request['order_processed_on'] = date('Y-m-d H:i:s');
                }
                if (!empty($getDetails) && $getDetails[0]['order_shipped'] == 'No') {
                    $request['order_shipped'] = 'Yes';
                }
                if (!empty($getDetails) && empty($getDetails[0]['order_shipped_on'])) {
                    $request['order_shipped_on'] = date('Y-m-d H:i:s');
                }
                $request['delivered_on'] = date('Y-m-d H:i:s');
            }
            $request['modified_by'] = Auth::user()->id;
            $update = $this->newBandOrdersObj->where('id', $id)
                    ->update($request);
            return $update;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function updateTransactionDetails($request) {
        try {
            $getTransactionalDetails = $this->bandOrdersTransactions->where('rzorder_id', $request['rzorder_id'])->get()->toArray();
            try {
                $api = new Api($this->credentials['key_id'], $this->credentials['key_secret']);

                $payments = $api->order->fetch($getTransactionalDetails[0]['rzorder_id'])->payments();

                $orderDetails = $api->order->fetch($getTransactionalDetails[0]['rzorder_id']);
            } catch (BadRequestError $rzex) {
                $errorLog = [];
                $errorLog['user_id'] = Auth::user()->id;
                $errorLog['action'] = 'NewBandOrdersRepository/updateTransactionDetails';
                $errorLog['error_code'] = $rzex->getCode();
                $errorLog['error_message'] = $rzex->getMessage();
                RazorpayErrorsLog::create($errorLog);
                $error['rzerror'] = 'Opps! Order not proceed. Something went wrong.';
                return $error;
            }

            if (!empty($getTransactionalDetails)) {

                $updatedData = [];
                $updatedData['order_status'] = $orderDetails->status;
                $updatedData['payment_status'] = $payments->items[0]['status'];
                if ($updatedData['order_status'] == 'paid' && $updatedData['payment_status'] == 'captured') {
                    $updatedData['workflow_status'] = 'Success';
                }
                $updatedData['payment_status'] = $payments->items[0]['status'];
                $updatedData['rzpayment_id'] = $request['rzpayment_id'];
                $updatedData['rzsignature'] = $request['rzsignature'];
                $updatedData['modified_by'] = Auth::user()->id;

//              Start  Generate Invoice 
                $invoiceNumber = '';
                if (isset($this->credentials['last_invoice_number']) && !empty($this->credentials['last_invoice_number'])) {
                    $prefix = substr($this->credentials['last_invoice_number'], 0, 4);
                    $lastInvoiceNo = substr($this->credentials['last_invoice_number'], 4);
                    $addInvoiceNumber = $lastInvoiceNo + 1;
                    $invoiceNumber = $prefix . $addInvoiceNumber;
                }

//                $invoiceNumber='';
//                if (isset($getLastInvoiceNumber) && !empty($getLastInvoiceNumber) && !empty($getLastInvoiceNumber['invoice_number'])) {
//                    $prefix = substr($getLastInvoiceNumber['invoice_number'], 0, 4);
//                    $lastInvoiceNo = substr($getLastInvoiceNumber['invoice_number'], 4);
//                    $addInvoiceNumber = $lastInvoiceNo + 1;
//                    $invoiceNumber = $prefix . $addInvoiceNumber;
//                } else {
//                    $addInvoiceNumber = $this->credentials['invoice_number_start'] + 1;
//                    $invoiceNumber = $this->credentials['invoice_number_prefix'] . $addInvoiceNumber;
//                }


                if (isset($invoiceNumber) && !empty($invoiceNumber)) {
                    $this->razorpayCredentialsRepository->updateLastInvoiceNumber($invoiceNumber);
                    $updatedData['invoice_number'] = $invoiceNumber;
                }
//              End Generate Invoice

                $this->bandOrdersTransactions->where('rzorder_id', $request['rzorder_id'])
                        ->update($updatedData);
                $getNewOrderId = $this->bandOrdersTransactions->where('rzorder_id', $request['rzorder_id'])->get()->toArray();
                if (!empty($getNewOrderId)) {
                    $amountPaid = $orderDetails->amount_paid / 100;
                    $this->newBandOrdersObj->where('id', $getNewOrderId[0]['bandorders_id'])
                            ->update([
                                'amount_paid' => $amountPaid
                    ]);
                }

                // Send email to guardian
                $invoice = $this->downloadInvoice($getNewOrderId[0]['bandorders_id']);
                view()->share(compact('invoice'));
                $pdf = \PDF::loadView('new_band_orders.invoice');
                $fileName = "Invoice-" . $invoice['invoiceNumber'] . ".pdf";

                $dirName = storage_path('new_band_order_invoice_pdf');
                if (!is_dir($dirName)) {
                    mkdir($dirName, 0777, true);
                }
                $pdf->save($dirName . '/' . $fileName);

                $maildata = $invoice;
                $maildata['subject'] = 'Payment Received New Band Order';
                $maildata['guardianName'] = $invoice['guardianName'];
                Mail::send('emails.payment_received_new_band_order_email', ['maildata' => $maildata], function($message) use ($maildata, $fileName) {
                    $pdf_file = storage_path('new_band_order_invoice_pdf') . '/' . $fileName;
                    $message->attach($pdf_file);
                    $message->subject($maildata['subject']);
                    $message->to($maildata['guardianEmail']);
                    $message->bcc('webassic3@gmail.com');
                });
                if (count(Mail::failures()) == 0) { // Send mail successfully and remove pdf file 
                    unlink(storage_path('new_band_order_invoice_pdf') . '/' . $fileName);
                }
            } else {
                $updatedData['order_status'] = $orderDetails->status;
                $updatedData['payment_status'] = $payments->items[0]['status'];
                if ($updatedData['order_status'] == 'paid' && $updatedData['payment_status'] == 'captured') {
                    $updatedData['workflow_status'] = 'Unknown';
                } else {
                    $updatedData['workflow_status'] = 'Unknown';
                }
                $updatedData['payment_status'] = $payments->items[0]['status'];
                $updatedData['bandorders_id'] = 0;
                $updatedData['rzorder_id'] = $request['rzorder_id'];
                $updatedData['rzpayment_id'] = $request['rzpayment_id'];
                $updatedData['rzsignature'] = $request['rzsignature'];
                $updatedData['created_by'] = Auth::user()->id;
                $this->bandOrdersTransactions->created($updatedData);
            }
            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * downloadInvoice
     * @return string
     */
    public function downloadInvoice($id) {
        try {

            $api = new Api($this->credentials['key_id'], $this->credentials['key_secret']);
            $query = $this->newBandOrdersObj->select($this->selectFields)
                    ->join('bandusers', 'bandusers.id', '=', 'new_band_orders.banduser_id')
                    ->join('bandorders_transactions', 'bandorders_transactions.bandorders_id', '=', 'new_band_orders.id')
                    ->join('guardians', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->join('users', 'users.id', '=', 'guardians.user_id')
                    ->where(array('bandorders_transactions.workflow_status' => 'Success'))
                    ->where(array('new_band_orders.id' => $id))
                    ->get()
                    ->toArray();
            $payments = $api->payment->fetch($query[0]['rzpayment_id']);

            if (!empty($payments) && $payments['status'] == 'captured') {

                $payments_amt = ($payments['amount']) / 100;
                if (($payments_amt != $query[0]['total_amount']) && empty($query[0]['amount_paid'])) {
                    $this->newBandOrdersObj->where(array('id' => $id))
                            ->update(['amount_paid' => $payments_amt]);
                }
            }

            $finalResult = [];
            if (isset($query) && !empty($query)) {

                $finalResult['invoiceNumber'] = isset($query[0]['invoice_number']) && !empty($query[0]['invoice_number']) ? $query[0]['invoice_number'] : 'NA';
                $finalResult['invoiceDate'] = !empty($query[0]['order_date']) ? date('d F Y', strtotime($query[0]['order_date'])) : date('d F Y', strtotime($query[0]['created_at']));
                $finalResult['guardianName'] = $query[0]['ufirstname'] . " " . $query[0]['ulastname'];
                $finalResult['guardianAddress'] = $query[0]['address'] . " , " . $query[0]['locality'] . " , " . $query[0]['city'] . " , " . $query[0]['state'] . " , " . "India";
                $finalResult['guardianState'] = strtolower($query[0]['state']);
                $finalResult['bandUserName'] = $query[0]['firstname'] . " " . $query[0]['lastname'];
                $finalResult['guardianEmail'] = $query[0]['email'];
                $finalResult['quantity'] = $query[0]['number_of_bands'];
                $finalResult['amount'] = empty($query[0]['amount_paid']) ? round($query[0]['total_amount'], 2) : round($query[0]['amount_paid'], 2);
                $finalResult['currency'] = isset($payments['currency']) && !empty($payments['currency']) ? $payments['currency'] : 'INR';
                $finalResult['totalAmount'] = round($query[0]['total_amount'], 2);
                $finalResult['price_per_band'] = ($query[0]['total_amount']) / ($query[0]['number_of_bands']);
                $finalResult['orignalCost'] = round($finalResult['price_per_band'] * 100 / (100 + $this->credentials['gst_rate']), 2);
                $finalResult['gstAmount'] = round($finalResult['price_per_band'] - $finalResult['orignalCost'], 2);
                $finalResult['gstRate'] = $this->credentials['gst_rate'];
                $discountAmt = $discountPercent = '';
                if ($finalResult['totalAmount'] != $finalResult['amount']) {
                    $discountAmt = $query[0]['total_amount'] - $query[0]['amount_paid'];
                    $discountPercent = ($discountAmt * 100) / $query[0]['total_amount'];
                    $discountAmt = round($finalResult['orignalCost']- ($finalResult['orignalCost']*($discountPercent/100)),2);
                
                    $finalResult['gstAmount'] =round($discountAmt*($this->credentials['gst_rate']/100),2);
                }
                $finalResult['discountRate'] = empty($discountPercent) ? '0' : $discountPercent;
                $finalResult['discountAmount'] = empty($discountAmt) ? '0' : $discountAmt;
                $finalResult['packageName'] = ucfirst($query[0]['package_name']);
                
            }
            return $finalResult;
        } catch (\Expection $ex) {
            throw $ex;
        }
    }

}
