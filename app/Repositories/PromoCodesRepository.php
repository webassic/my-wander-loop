<?php

namespace App\Repositories;

use App\Models\PromoCodeUsers;
use App\Models\PromoCodes;
use App\User;
use DB;

class PromoCodesRepository extends BaseRepository {

    protected $modelPromoCodes;
    protected $modelUser;
    protected $modelPromoCodeUsers;
    private $selectFields = [
        'promo_codes.id',
        'promo_codes.name',
        'promo_codes.valid_till',
        'promo_codes.discount_offered_type',
        'promo_codes.discount_offered',
        'promo_codes.is_active'
    ];
    private $searchArray = array(
        'promo_codes.name',
        'promo_codes.valid_till',
        'promo_codes.discount_offered_type',
        'promo_codes.discount_offered',
        'promo_codes.is_active'
    );

    /**
     * 
     * @param PromoCodes $modelPromoCodes
     */
    public function __construct(PromoCodes $modelPromoCodes, PromoCodeUsers $modelPromoCodeUsers, User $modelUser) {
        $this->modelPromoCodes = $modelPromoCodes;
        $this->modelPromoCodeUsers = $modelPromoCodeUsers;
        $this->modelUser = $modelUser;
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllPromoCodes($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'is_active' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->modelPromoCodes
                    ->select($this->selectFields);

            if (!empty($conditions['is_active']) && isset($conditions['is_active'])) {
                $query->where('promo_codes.is_active', $conditions['is_active']);
            }
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $promoCodeDetails = $query->get();
            foreach ($promoCodeDetails as $val) {
                $val->promo_code_users;
                foreach ($val->promo_code_users as $value) {
                    $value->users;
                }
            }
            $promoCodeDetailsArray = $promoCodeDetails->toArray();
            $finalArray = [];
            if (!empty($promoCodeDetailsArray) && isset($promoCodeDetailsArray)) {
                foreach ($promoCodeDetailsArray as $promoCodeKey => $promoCodeValue) {
                    $userNames = [];
                    $usersArray = isset($promoCodeValue['promo_code_users']) ? $promoCodeValue['promo_code_users'] : '';
                    if (isset($usersArray) && !empty($usersArray)) {
                        foreach ($usersArray as $userVal) {
                            $userNames[] = $userVal['users']['firstname'] . ' ' . $userVal['users']['lastname'];
                        }
                        // Final array for user names
                        $finalArray[] = array(
                            'user_names' => implode(", ", array_unique($userNames)),
                        );
                    }
                }
            }
            if (isset($promoCodeDetailsArray) && !empty($promoCodeDetailsArray) && !empty($finalArray)) {
                foreach ($promoCodeDetailsArray as $key => $value) {
                    $promoCodeDetailsArray[$key]['user_names'] = ucwords($finalArray[$key]['user_names']);
                }
            }
            $response['records'] = $promoCodeDetailsArray;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    public function getAllUsersList() {
        try {
            $usersList = $this->modelUser->select('id', 'firstname', 'lastname')
                    ->where(array('status' => 'active', 'user_type' => 'User'))
                    ->orderBy('firstname', 'asc')
                    ->get()
                    ->toArray();

            $newArray = [];
            if (isset($usersList) && !empty($usersList)) {
                foreach ($usersList as $key => &$val) {
                    $newArray[$key]['id'] = $val['id'];
                    $newArray[$key]['user_name'] = $val['firstname'] . ' ' . $val['lastname'];
                }
            }
            return $newArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storePromoCodeDetails($postData) {
        try {
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
            $response = [];
            $promocodeUsersResponse = [];
            if (!empty($data)) {
                $promoCodeDetails = $this->modelPromoCodes;
                $promoCodeDetails->name = isset($data['name']) ? trim($data['name']) : null;
                $promoCodeDetails->valid_till = isset($data['valid_till']) ? date('Y-m-d', strtotime($data['valid_till'])) : null;
                $promoCodeDetails->discount_offered_type = isset($data['discount_offered_type']) ? $data['discount_offered_type'] : '%';
                $promoCodeDetails->discount_offered = isset($data['discount_offered']) ? $data['discount_offered'] : null;
                $promoCodeDetails->is_active = isset($data['is_active']) ? $data['is_active'] : 'active';
                $promoCodeDetails->created_by = auth()->user()->id;
                $promoCodeDetails->save();

                if (isset($promoCodeDetails->id) && !empty($promoCodeDetails->id)) {
                    // Store Promo Code User Details 
                    $promoCodesId = $promoCodeDetails->id;
                    $promocodeUsersResponse = $this->storePromoCodeUserDetails($promoCodesId, $data['user_id']);
                }
                return $promocodeUsersResponse;
            }
            return $promocodeUsersResponse;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $promoCodesId
     * @param type $usersIds
     * @return type
     * @throws \Exception
     */
    public function storePromoCodeUserDetails($promoCodesId, $usersIds) {
        try {
            $response = [];
            if (!empty($promoCodesId)) {
                foreach ($usersIds as $key => $val) {
                    $promocodeUsersArray = array(
                        'promo_codes_id' => $promoCodesId,
                        'users_id' => $val,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    \App\Models\PromoCodeUsers::create($promocodeUsersArray);
                }
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showPromoCodeDetails($id) {
        try {
            $promoCodeDetails = [];
            $promoCodeDetailsArray = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $promoCodeDetails = $this->modelPromoCodes
                        ->where('id', '=', $id)
                        ->get();

                foreach ($promoCodeDetails as &$val) {
                    $val->promo_code_users;
                    foreach ($val->promo_code_users as $value) {
                        $value->users;
                    }
                }
            }
            if (isset($promoCodeDetails) && !empty($promoCodeDetails)) {
                $promoCodeDetailsArray = $promoCodeDetails->first()->toArray();
            }
            return $promoCodeDetailsArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updatePromoCodeDetails($request) {
        try {
            $id = isset($request['id']) && !empty($request['id']) ? $request['id'] : 0;
            $promocodeUsersResponse = [];
            $updateArray = array(
                'name' => isset($request['name']) ? trim($request['name']) : '',
                'valid_till' => isset($request['valid_till']) ? date('Y-m-d', strtotime($request['valid_till'])) : '',
                'discount_offered_type' => isset($request['discount_offered_type']) ? $request['discount_offered_type'] : '%',
                'discount_offered' => isset($request['discount_offered']) ? $request['discount_offered'] : '',
                'is_active' => isset($request['is_active']) ? $request['is_active'] : 'active',
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                \App\Models\PromoCodes::find($id)->update($updateArray);

                // delete existing entry from promo code users table 
                $this->modelPromoCodeUsers
                        ->where('promo_codes_id', $id)
                        ->delete();

                // insert new entry in promo code users table
                if (isset($request['user_id']) && !empty($request['user_id'])) {
                    $promocodeUsersResponse = $this->storePromoCodeUserDetails($id, $request['user_id']);
                }
            } else {
                return;
            }
            return $promocodeUsersResponse;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyPromoCodeDetails($id) {
        try {
            $updateArray = array(
                'is_active' => 'inactive',
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                return \App\Models\PromoCodes::find($id)->update($updateArray);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $promoCode
     * @return type
     * @throws \Exception
     */
//    public function checkValidPromoCode($promoCode) {
//        try {
//            if (isset($promoCode) && !empty($promoCode)) {
//                $promoCodeDetails = $this->modelPromoCodes->select($this->selectFields)
//                                ->join('promo_code_users', function($join) {
//                                    $join->on('promo_code_users.promo_codes_id', '=', 'promo_codes.id');
//                                    $join->where('users_id', '=', auth()->user()->id);
//                                })
//                                ->where('name', '=', strtolower($promoCode))
//                                ->where('is_active', '=', 'active')
//                                ->where('valid_till', '>=', date('Y-m-d'))
//                                ->first();
//            }
//            return $promoCodeDetails;
//        } catch (\Exception $ex) {
//            throw $ex;
//        }
//    }

}
