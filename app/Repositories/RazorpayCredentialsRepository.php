<?php

namespace App\Repositories;

use App\Models\RazorpayCredentials;
use Auth;

/**
 * Interface RazorpayCredentialsRepository.
 *
 * @package namespace App\Repositories;
 */
class RazorpayCredentialsRepository extends BaseRepository {

    /**
     * 
     * @param RazorpayCredentials $razorpayCredentials
     */
    public function __construct(RazorpayCredentials $razorpayCredentials) {
        $this->razorpayCredentials = $razorpayCredentials;
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showRazorpayCredentials($id) {
        try {
            $response = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $response = $this->razorpayCredentials
                                ->where('id', '=', $id)
                                ->first()->toArray();
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $request
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateRazorpayCredentials($request, $id) {
        try {
            $updateArray = array(
                'test_key_id' => isset($request['test_key_id']) ? trim($request['test_key_id']) : '',
                'test_key_secret' => isset($request['test_key_secret']) ? trim($request['test_key_secret']) : '',
                'test_basic_plan_id' => isset($request['test_basic_plan_id']) ? trim($request['test_basic_plan_id']) : '',
                'test_plan_id' => isset($request['test_plan_id']) ? trim($request['test_plan_id']) : '',
                'test_banduser_cost' => isset($request['test_banduser_cost']) ? $request['test_banduser_cost'] : '',
                'test_invoice_number_prefix' => isset($request['test_invoice_number_prefix']) ? $request['test_invoice_number_prefix'] : '',
                'test_invoice_number_start' => isset($request['test_invoice_number_start']) ? $request['test_invoice_number_start'] : '',
                'test_last_invoice_number' => isset($request['test_invoice_number_start']) ? $request['test_invoice_number_prefix'] . $request['test_invoice_number_start'] : '',
                'test_gst_rate' => isset($request['test_gst_rate']) ? $request['test_gst_rate'] : '',
                'live_key_id' => isset($request['live_key_id']) ? $request['live_key_id'] : '',
                'live_key_secret' => isset($request['live_key_secret']) ? $request['live_key_secret'] : '',
                'live_basic_plan_id' => isset($request['live_basic_plan_id']) ? $request['live_basic_plan_id'] : '',
                'live_plan_id' => isset($request['live_plan_id']) ? $request['live_plan_id'] : '',
                'live_banduser_cost' => isset($request['live_banduser_cost']) ? $request['live_banduser_cost'] : '',
                'live_invoice_number_prefix' => isset($request['live_invoice_number_prefix']) ? $request['live_invoice_number_prefix'] : '',
                'live_invoice_number_start' => isset($request['live_invoice_number_start']) ? $request['live_invoice_number_start'] : '',
                'live_last_invoice_number' => isset($request['test_invoice_number_start']) ? $request['live_invoice_number_prefix'] . $request['live_invoice_number_start'] : '',
                'live_gst_rate' => isset($request['live_gst_rate']) ? $request['live_gst_rate'] : '',
                'last_modified' => date('Y-m-d'),
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                return \App\Models\RazorpayCredentials::find($id)->update($updateArray);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type 
     * @return type
     * @throws \Exception
     */
    public function getRazorpayCredentials() {
        try {
            $response = $this->razorpayCredentials
                            ->first()->toArray();
            $result = [];
            if (\Config::get('app.razorpay_mode_flag') == 0) {
                $result['mode'] = $response['test_mode'];
                $result['key_id'] = $response['test_key_id'];
                $result['key_secret'] = $response['test_key_secret'];
                $result['plan_id'] = $response['test_plan_id'];
                $result['basic_plan_id'] = $response['test_basic_plan_id'];
                $result['banduser_cost'] = $response['test_banduser_cost'];
                $result['invoice_number_prefix'] = $response['test_invoice_number_prefix'];
                $result['invoice_number_start'] = $response['test_invoice_number_start'];
                $result['last_invoice_number'] = $response['test_last_invoice_number'];
                $result['gst_rate'] = $response['test_gst_rate'];
            } else {
                $result['mode'] = $response['live_mode'];
                $result['key_id'] = $response['live_key_id'];
                $result['key_secret'] = $response['live_key_secret'];
                $result['plan_id'] = $response['live_plan_id'];
                $result['basic_plan_id'] = $response['live_basic_plan_id'];
                $result['banduser_cost'] = $response['live_banduser_cost'];
                $result['invoice_number_prefix'] = $response['live_invoice_number_prefix'];
                $result['invoice_number_start'] = $response['live_invoice_number_start'];
                $result['last_invoice_number'] = $response['live_last_invoice_number'];
                $result['gst_rate'] = $response['live_gst_rate'];
            }
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * updateLastInvoiceNumber
     * @param type $invoiceNumber
     */
    public function updateLastInvoiceNumber($invoiceNumber) {
        $response = $this->razorpayCredentials
                        ->first()->toArray();
        if (\Config::get('app.razorpay_mode_flag') == 0) {
            return \App\Models\RazorpayCredentials::where('id', '=', $response['id'])->update(array('test_last_invoice_number' => $invoiceNumber));
        } else {
            return \App\Models\RazorpayCredentials::where('id', '=', $response['id'])->update(array('live_last_invoice_number' => $invoiceNumber));
        }
    }

}
