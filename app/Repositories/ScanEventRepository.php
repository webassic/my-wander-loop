<?php

namespace App\Repositories;

use App\Models\ScanEvent;
use App\Models\EventLogs;
use App\Models\Guardian;
use App\Models\Banduser;
use App\User;
use DB;
use Auth;

class ScanEventRepository extends BaseRepository {

    protected $modelScanEvent;
    private $selectFields = [
        'scanevents.id',
        'bandusers.guardians_id',
        'bandusers.email as banduser_email',
        'bandusers.mobile as banduser_mobile',
        'bandusers.alternate_email as banduser_alternate_email',
        'guardians.user_id',
        'users.id as userid',
        'users.firstname',
        'users.lastname',
        'users.email',
        'users.mobile',
        'scanevents.banduser_uniqueid',
        'scanevents.banduser_name',
        'scanevents.scanner_mobile',
        'scanevents.ip_address',
        'scanevents.latitude',
        'scanevents.logitude',
        'scanevents.location',
        'scanevents.lat_long_error',
        'scanevents.mobile_verify',
        'scanevents.banduser_info_displayed',
        'scanevents.scanned_on',
        'scanevents.device_info',
    ];
    private $searchArray = array(
        'scanevents.banduser_uniqueid',
        'scanevents.banduser_name',
        'scanevents.scanner_mobile',
        'scanevents.scanned_on',
        'users.firstname'
    );

    /**
     * 
     * @param PromoCodes $modelPromoCodes
     */
    public function __construct(ScanEvent $modelScanEvent,EventLogs $eventLogs,Guardian $guardian,Banduser $banduser,User $user) {
        $this->modelScanEvent = $modelScanEvent;
        $this->modelEventLog = $eventLogs;
        $this->modelGuardian = $guardian;
        $this->modelBanduser = $banduser;
        $this->modelUser = $user;
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllScanEvent($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'is_active' => isset($request['status']) ? $request['status'] : '',
            );
            $query = $this->modelBandTypes
                    ->select($this->selectFields);

            if (!empty($conditions['is_active']) && isset($conditions['is_active'])) {
                $query->where('band_types.status', $conditions['is_active']);
            }
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $bandTypesDetails = $query->get()->toArray();
            
            $response['records'] = $bandTypesDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }
    
    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
     public function userScanEventsList($request) {
         $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'scanevents.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
            );
            $query = $this->modelScanEvent
                    ->select($this->selectFields)
                    ->join('bandusers','bandusers.uniqueid','scanevents.banduser_uniqueid')
                    ->join('guardians','guardians.id','bandusers.guardians_id')
                    ->join('users','users.id','guardians.user_id')
                    ->where('users.id','=',Auth::user()->id);
            
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        if($search=='scanevents.scanned_on'){
                            $date = date('Y-m-d', strtotime($conditions['search']['value']));
                            
                           $query->orWhere($search, 'like', "%{$date}%"); 
                        }else{
                            
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
             $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $scanEventsDetails = $query->get()->toArray();
            
            
            $response['records'] = $scanEventsDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
         
     }
    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function adminScanEventsList($request) {
         $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'scanevents.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
            );
//            dd($conditions);
            $query = $this->modelScanEvent
                    ->select($this->selectFields)
                    ->join('bandusers','bandusers.uniqueid','scanevents.banduser_uniqueid')
                    ->join('guardians','guardians.id','bandusers.guardians_id')
                    ->join('users','users.id','guardians.user_id');
                    
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        if($search=='scanevents.scanned_on'){
                            $date = date('Y-m-d', strtotime($conditions['search']['value']));
                            
                           $query->orWhere($search, 'like', "%{$date}%"); 
                        }else if($search=='users.firstname'){ 
                           
                        $query->orWhere(DB::raw("CONCAT(users.firstname, ' ', users.lastname)"), 'like', "%{$conditions['search']['value']}%");
                        
                        }else{ 
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
             $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $scanEventsDetails = $query->get()->toArray();
            
            
            $response['records'] = $scanEventsDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
         
     }
     
      /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showScanEvents($id){
         try{
           $response = [];
            if(isset($id) && !empty($id) && $id > 0) {
                $response = $this->modelScanEvent->select($this->selectFields)
                        ->join('bandusers','bandusers.uniqueid','scanevents.banduser_uniqueid')
                        ->join('guardians','guardians.id','bandusers.guardians_id')
                        ->join('users','users.id','guardians.user_id')
                        ->where('scanevents.id', '=', $id)
                        ->get()->toArray();
                
            }
            return $response;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    
    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeScanEvent($postData) {
        try{
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           
           $result= $this->modelScanEvent->create($data);
           return $result;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    
    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function updateScanEvent($postData,$id) {
        try{
            $data = array();
            $response=array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
           
           $result= $this->modelScanEvent->where('id',$id)
                    ->update($data);
           if($result){
               $response = $this->modelScanEvent->find($id);
           }
           return $response;
       }catch(\Exception $ex){
           throw $ex;
       } 
    }
    
     /**
     * Display the eventlogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEventLogs(){
        try {  
            $data = [];
            $userDetails = Auth::user();          
            
            $eventLogCount ='';
            if($userDetails['user_type'] == 'User'){
              
                $scanEventDetails = $this->modelScanEvent
                         ->where('user_id',$userDetails['id'])
                         ->limit(1)
                         ->orderBy('id', 'desc')
                         ->get()->toArray();
                
            }
            if($userDetails['user_type'] == 'Administration'){
  
                $scanEventDetails = $this->modelScanEvent
                    ->where('user_id','!=',null)
                    ->limit(1)
                    ->orderBy('id', 'desc')
                    ->get()->toArray();
               
                
            }
            $scanEventId = '';
            if(!empty($scanEventDetails)){
                foreach ($scanEventDetails as $key => $value) {
                    $scanEventId= $value['id'];
               
                }  
                $eventLogCount = $this->modelEventLog
                             ->where('record_id','=',$scanEventId)
                             ->where('name','=','scanevent_viewed')
                             ->where('user_id','=',$userDetails['id'])
                             ->where('module','=','scan event')
                             ->count();
            }
            return $eventLogCount;
            

        } catch (\Expectional $exc) {
           throw $ex;
        }
    }
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getDetailsForMail($id) {
       try {  
            
            $response = $this->modelScanEvent->select($this->selectFields)
                        ->join('bandusers','bandusers.uniqueid','scanevents.banduser_uniqueid')
                        ->join('guardians','guardians.id','bandusers.guardians_id')
                        ->join('users','users.id','guardians.user_id')
                        ->where('scanevents.id', '=', $id)
                        ->get()->toArray();
            if(!empty($response)){
              if(isset($response[0]['scanned_on'])){
                  
                  $response[0]['scanned_on'] = date("d M Y H:i:s", strtotime($response[0]['scanned_on']));
              }
            }
           return $response;

        } catch (\Expectional $exc) {
           throw $ex;
        }
    }
    
    public function loggedEvent(){
         try {  
            $data = [];
            $userDetails = Auth::user();          
            $eventLogData =[
                   'name'=>'scanevent_viewed',
                   'module'=>'scan event',
                   'user_id'=>$userDetails['id'], 
                ];
            if($userDetails['user_type'] == 'User'){
              
                $scanEventDetails = $this->modelScanEvent 
                    ->where('user_id','=',$userDetails['id'])
                    ->limit(10)
                    ->orderBy('id', 'desc')
                    ->get()->toArray();

            }
            if($userDetails['user_type'] == 'Administration'){
  
                $scanEventDetails = $this->modelScanEvent
                    ->where('user_id','!=','null')
                    ->limit(20)
                    ->orderBy('id', 'desc')
                    ->get()->toArray();
                
                
            }
            if(!empty($scanEventDetails)){
                foreach ($scanEventDetails as $key => $value) {
                    
                    $eventLogCount = $this->modelEventLog
                             ->where('record_id','=',$value['id'])
                             ->where('name','=','scanevent_viewed')
                             ->where('user_id','=',$userDetails['id'])
                             ->where('module','=','scan event')
                             ->count();
                              
                    if($eventLogCount == 0 ){
                        $eventLogData['record_id'] =$value['id'];
                        $this->modelEventLog->create($eventLogData);
                    }
                  

                }  
            }
            
           return true;

        } catch (\Expectional $exc) {
           throw $ex;
        }
    }
    
    public function exportAdminScanEvent(){
        try{
            $query = $this->modelScanEvent
                    ->select($this->selectFields)
                    ->join('bandusers','bandusers.uniqueid','scanevents.banduser_uniqueid')
                    ->join('guardians','guardians.id','bandusers.guardians_id')
                    ->join('users','users.id','guardians.user_id')
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
