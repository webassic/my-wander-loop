<?php

namespace App\Repositories;

use App\Models\SupportTicket;
use DB;
use Auth;

class SupportTicketRepository extends BaseRepository {

    protected $modelSupportTicket;
    private $selectFields = [
        'support_tickets.id',
        'support_tickets.unique_ticket_id',
        'support_tickets.subject',
        'support_tickets.related_to',
        'support_tickets.message',
        'support_tickets.status',
        'support_tickets.status_modified_by',
        'support_tickets.created_by',
        'support_tickets.created_at',
        'support_tickets.updated_at',
//        'users.firstname',
//        'users.lastname'
    ];
    private $adminSelectFields = [
        'support_tickets.id',
        'support_tickets.unique_ticket_id',
        'support_tickets.subject',
        'support_tickets.related_to',
        'support_tickets.message',
        'support_tickets.status',
        'support_tickets.status_modified_by',
        'support_tickets.created_by',
        'support_tickets.created_at',
        'support_tickets.updated_at',
        'users.firstname',
        'users.lastname'
    ];
    private $adminSelectFieldsForShow = [
        'support_tickets.id',
        'support_tickets.unique_ticket_id',
        'support_tickets.subject',
        'support_tickets.related_to',
        'support_tickets.message',
        'support_tickets.status',
        'support_tickets.status_modified_by',
        'support_tickets.created_by',
        'support_tickets.created_at',
        'support_tickets.updated_at',
        'users.firstname',
        'users.lastname',
        'bandusers.title as title',
        'bandusers.firstname as banduser_firstname',
        'bandusers.lastname as banduser_lastname',
    ];
    private $searchArray = array(
        'support_tickets.status',
        'support_tickets.unique_ticket_id',
        'support_tickets.subject',
    );
    private $searchAdminArray = array(
        'support_tickets.status',
        'support_tickets.unique_ticket_id',
        'support_tickets.subject',
        'users.firstname',
        'users.lastname'
    );

    /**
     * 
     * @param $modelSupportTicket
     */
    public function __construct(SupportTicket $modelSupportTicket) {
        $this->modelSupportTicket = $modelSupportTicket;
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function adminSuportTicketsList($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchAdminArray[$request['order'][0]['column']]) ? $this->searchAdminArray[$request['order'][0]['column']] : 'support_tickets.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
            );
//            dd($conditions);
            $query = $this->modelSupportTicket
                    ->select($this->adminSelectFields)
                    ->join('users', 'users.id', 'support_tickets.created_by');

//            Log::info('>>>>>>>>>>'. print_r($query->toArray()));
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchAdminArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $supportTicketDetails = $query->get()->toArray();


            $response['records'] = $supportTicketDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    public function adminShowSupportTickets($id) {
        try {
            $response = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $response = $this->modelSupportTicket->select($this->adminSelectFields)
                                ->join('users', 'users.id', 'support_tickets.created_by')
                                ->where('support_tickets.id', '=', $id)
                                ->get()->toArray();
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

//    public function adminShowSupportTickets($id) {
//        try {
//            $response = [];
//            if (isset($id) && !empty($id) && $id > 0) {
//                $response = $this->modelSupportTicket->select($this->adminSelectFieldsForShow)
//                                ->join('users', 'users.id', 'support_tickets.created_by')
//                                ->join('support_ticket_bandusers', 'support_tickets.id', 'support_ticket_bandusers.support_ticket_id')                                 
//                                ->leftjoin('bandusers', 'bandusers.id', 'support_ticket_bandusers.banduser_id')                                 
//                                ->where('support_tickets.id', '=', $id)
//                                ->get()->toArray();
//            }
//            return $response;
//        } catch (\Exception $ex) {
//            throw $ex;
//        }
//    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function userSupportTicketsList($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'support_tickets.id',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'DESC',
                'limit' => isset($request['length']) ? $request['length'] : '',
            );
            $query = $this->modelSupportTicket
                    ->select($this->selectFields)
//                    ->join('support_ticket_bandusers')
                    ->where('created_by', '=', Auth::user()->id);
//            dd($query->get());
            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $supportTicketsDetails = $query->get()->toArray();

            $response['records'] = $supportTicketsDetails;
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * getAllBandUserList
     * @return string
     * @throws \Exception
     */
    public function getAllBandUserList() {
        try {
            $usersList = \App\Models\Banduser::select('id', 'firstname', 'lastname')->where('created_by', '=', Auth::user()->id)
                    ->orderBy('firstname', 'asc')
                    ->get()
                    ->toArray();

            $newArray = [];
            if (isset($usersList) && !empty($usersList)) {
                foreach ($usersList as $key => &$val) {
                    $newArray[$key]['id'] = $val['id'];
                    $newArray[$key]['user_name'] = $val['firstname'] . ' ' . $val['lastname'];
                }
            }
            return $newArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * storeSupportTicketDetails
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeSupportTicketDetails($data) {
        try {
            if (!empty($data)) {

                $data['unique_ticket_id'] = $this->generateUniqueId();
                $data['subject'] = isset($data['subject']) ? trim(ucfirst($data['subject'])) : '';
                $data['message'] = isset($data['message']) ? ucfirst($data['message']) : '';
                $data['created_by'] = auth()->user()->id;
                $supportTicketsResponse = $this->modelSupportTicket->create($data);

                if (isset($supportTicketsResponse->id) && !empty($supportTicketsResponse->id) && $data['bandusers_id'] != "") {
                    // Store Suppot Ticket User Details 
                    $suppotTicketId = $supportTicketsResponse->id;
                    $supportTicketsResponse = $this->storeSupportTicketBandUserDetails($suppotTicketId, $data['bandusers_id']);
                }
                return $supportTicketsResponse;
            }
            return $supportTicketsResponse;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $suppotTicketId
     * @param type $usersIds
     * @return type
     * @throws \Exception
     */
    public function storeSupportTicketBandUserDetails($suppotTicketId, $bandUserID) {
        try {
            $response = [];
            if (!empty($suppotTicketId)) {
//                $bandUserID = explode(",", $usersIds);
//                if ($bandUserID['0'] == 'select_all') {
//                    unset($bandUserID[0]);
//                }
                foreach ($bandUserID as $val) {
                    $suppotTicketBandUsersArray = array(
                        'support_ticket_id' => $suppotTicketId,
                        'banduser_id' => $val,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    \App\Models\SupportTicketBanduser::create($suppotTicketBandUsersArray);
                }
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showSupportTicketDetails($id) {
        try {
            $supportTicketDetails = [];
            $supportTicketDetailsArray = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $supportTicketDetails = $this->modelSupportTicket
                        ->where('id', '=', $id)
                        ->get();

                foreach ($supportTicketDetails as &$val) {
                    $val->support_ticket_bandusers;
                    foreach ($val->support_ticket_bandusers as $value) {
                        $value->bandusers;
                    }
                }
            }
            if (isset($supportTicketDetails) && !empty($supportTicketDetails)) {
                $supportTicketDetailsArray = $supportTicketDetails->first()->toArray();
            }
            return $supportTicketDetailsArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * updateResolved OR Closed Status
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function updateResolvedStatus($postData) {
        try {
            $updateArray = array(
                'status' => $postData['supportTicketStatus'],
                'status_modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                $explodeIds = explode(',', $postData['supportTicketId']);
                return \App\Models\SupportTicket::whereIn('id', $explodeIds)->update($updateArray);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    
     /**
     * Display the eventlogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEventLog(){
        try {  
            $data = [];
            $userDetails = Auth::user();          
            
            $eventLogCount ='';
            if($userDetails['user_type'] == 'Administration'){
  
                $scanEventDetails = $this->modelSupportTicket
                    ->where('user_id','!=',null)
                    ->limit(1)
                    ->orderBy('id', 'desc')
                    ->get()->toArray();
               
                
            }
            $scanEventId = '';
            if(!empty($scanEventDetails)){
                foreach ($scanEventDetails as $key => $value) {
                    $scanEventId= $value['id'];
               
                }  
                $eventLogCount = $this->modelEventLog
                             ->where('record_id','=',$scanEventId)
                             ->where('name','=','scanevent_viewed')
                             ->where('user_id','=',$userDetails['id'])
                             ->where('module','=','scan event')
                             ->count();
            }
            return $eventLogCount;
            

        } catch (\Expectional $exc) {
           throw $ex;
        }
    }
    public function exportAdminHelpDesk(){
        try{
            $query = $this->modelSupportTicket
                    ->select($this->adminSelectFields)
                    ->join('users', 'users.id', 'support_tickets.created_by')
                    ->get()
                    ->toArray();
            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}
