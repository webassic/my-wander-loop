<?php

namespace App\Repositories;

use App\Models\SupportTicketComment;
use DB;
use Auth;

class SupportTicketsCommentsRepository extends BaseRepository {

    protected $modelSupportTicketComment;
    private $selectFields = [
        'support_ticket_comments.id',
        'support_ticket_comments.support_ticket_id',
        'support_ticket_comments.message',
        'support_ticket_comments.message_by',
        'support_ticket_comments.created_at',
        'support_ticket_comments.updated_at',
        'users.firstname',
        'users.lastname',
    ];
    private $searchArray = array(
        'support_ticket_comments.message',
    );

    /**
     * 
     * @param $modelSupportTicket
     */
    public function __construct(SupportTicketComment $modelSupportTicketComment) {
        $this->modelSupportTicketComment = $modelSupportTicketComment;
    }

    /**
     * storeSupportTicketCommentDetails
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeSupportTicketCommentDetails($postData) {
        try {
            $data = array();
            if (is_string($postData)) {
                $data = json_decode($postData, true);
            }
            if (empty($data)) {
                $data = $postData;
            }
            if (!empty($data)) {
                $data['support_ticket_id'] = isset($data['support_ticket_id']) ? $data['support_ticket_id'] : '';
                $data['message'] = isset($data['message']) ? ucfirst($data['message']) : '';
                $data['message_by'] = auth()->user()->id;
                $supportTicketsCommentResponse = $this->modelSupportTicketComment->create($data);
                return $supportTicketsCommentResponse;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function getSupportTicketCommentsList($id) {
        try {
            $response = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $response = $this->modelSupportTicketComment->select($this->selectFields)
                                ->join('users', 'users.id', 'support_ticket_comments.message_by')
                                ->where('support_ticket_comments.support_ticket_id', '=', $id)
                                ->get()->toArray();
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function getUserDetails($id) {
        try {
            $response = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $response = \App\Models\SupportTicket::join('users', 'users.id', 'support_tickets.created_by')
                        ->where('support_tickets.id', '=', $id)->get()->toArray();
            }
            return $response;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

}
