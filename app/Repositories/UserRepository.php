<?php

namespace App\Repositories;

use App\User;
use App\Models\Banduser;
use App\Repositories\BanduserRepository;
use App\Models\Guardian;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Hash;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class UserRepository extends BaseRepository {

    protected $user;
    private $selectFields = [
        'users.id',
        'users.firstname',
        'users.lastname',
        'users.email',
        'users.mobile',
        'users.user_type',
        'users.status',
        'users.created_at',
        'users.updated_at'];
    private $searchArray = array(
        'users.firstname',
        'users.email',
        'users.mobile',
        'users.user_type',
        'users.status',
        'users.created_at',
        'users.updated_at');

    /**
     * 
     * @param User $user
     */
    public function __construct(User $user, BanduserRepository $banduserRepository) {
        $this->user = $user;
        $this->banduserRepository = $banduserRepository;
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function registerUser($postData) {
        try {
            $data = [];
            $postData['firstname'] = ucfirst($postData['firstname']);
            $postData['lastname'] = ucfirst($postData['lastname']);
            $postData['password'] = bcrypt($postData['password']);
            $postData['email_verification_code'] = $this->generateRandomtring(16);
            $user = $this->user->create($postData);
//            dd($user);
            if (!empty($user)) {
                if (!empty($user['email_verification_code'])) {
                    Mail::send('emails.email_verification', ['maildata' => $user], function($message) use ($user) {

                        $message->subject("MyWanderLoop Email Verification");
                        $message->to($user['email']);
                    });
                }
            }

            if ($user) {
                $data['data'] = $user;
                $data['success'] = 'Your registeration has been done successfully.';
                return $data;
            } else {
                return ['error' => 'Registeration has been failed.'];
            }
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    /**
     * 
     * @param type $postData
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function UpdateUser($postData, $id) {
        try {
            $data = array();
            $response = array();
            $data['firstname'] = isset($postData['firstname']) && !empty($postData['firstname']) ? ucfirst($postData['firstname']) : '';
            $data['lastname'] = isset($postData['lastname']) && !empty($postData['lastname']) ? ucfirst($postData['lastname']) : '';
            $data['email'] = isset($postData['email']) && !empty($postData['email']) ? $postData['email'] : '';
            $data['mobile'] = isset($postData['mobile']) && !empty($postData['mobile']) ? $postData['mobile'] : '';
            $result = $this->user->where('id', '=', $id)
                    ->update($data);
            if ($result) {
                $response = $this->user->find($id);
            }
            return $response;
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllAdminUsers($request) {
        $response = [];
        try {
            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($this->searchArray[$request['order'][0]['column']]) ? $this->searchArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
                'user_type' => isset($request['user_type']) ? $request['user_type'] : '',
            );
            $query = $this->user->select($this->selectFields)
                    ->where(array('users.user_type' => 'Administration'));

            if (!empty($conditions['user_type']) && isset($conditions['user_type'])) {
                $query->where('users.user_type', $conditions['user_type']);
            }
            if (!empty($conditions['status']) && isset($conditions['status'])) {
                $query->where('users.status', $conditions['status']);
            }

            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions) {
                    foreach ($this->searchArray as $search) {
                        if ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $response['total_count'] = $query->count();
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $response['records'] = $query->get()->toArray();
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    public function getAllUsers($request) {
        $response = [];
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at'];
            $searchUsersArray = array(
                'users.firstname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at');

            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['order'][0]['column']]) ? $searchUsersArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
                'user_type' => isset($request['user_type']) ? $request['user_type'] : '',
            );

            $query = $this->user->select($selectUsersFields)
                    ->leftJoin('guardians', 'users.id', '=', 'guardians.user_id')
                    ->leftJoin('bandusers', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->where(function($query) {
                        $query->orWhere('bandusers.status', '=', 'draft')
                        ->orWhere('bandusers.id', '=', null);
                    })
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->distinct('users.id');
            if (!empty($conditions['user_type']) && isset($conditions['user_type'])) {
                $query->where('users.user_type', $conditions['user_type']);
            }
            if (!empty($conditions['status']) && isset($conditions['status'])) {
                $query->where('users.status', $conditions['status']);
            }

            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $response['records'] = $query->get()->toArray();
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllCustomers($request) {
        $response = [];
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at'];
            $searchUsersArray = array(
                'users.firstname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at');

            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['order'][0]['column']]) ? $searchUsersArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
                'user_type' => isset($request['user_type']) ? $request['user_type'] : '',
            );

            $query = $this->user->select($selectUsersFields)
                    ->leftJoin('guardians', 'users.id', '=', 'guardians.user_id')
                    ->leftJoin('bandusers', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->where(function($query) {
                        //save as draft and status pending should check after payment gateway implemented
                        $query->orWhere(array('bandusers.status' => 'active'))
                        ->orWhere(array('bandusers.status' => 'pending'));
                    })
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->distinct('users.id');
            if (!empty($conditions['user_type']) && isset($conditions['user_type'])) {
                $query->where('users.user_type', $conditions['user_type']);
            }
            if (!empty($conditions['status']) && isset($conditions['status'])) {
                $query->where('users.status', $conditions['status']);
            }

            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $response['records'] = $query->get()->toArray();
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * 
     * @param type $request
     * @return boolean
     * @throws \Exception
     */
    public function getAllInactiveCustomers($request) {
        $response = [];
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at'];
            $searchUsersArray = array(
                'users.firstname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at',
                'users.updated_at');

            $conditions = array(
                'search' => isset($request['search']) ? $request['search'] : '',
                'start' => isset($request['start']) ? $request['start'] : '',
                'order' => isset($searchUsersArray[$request['order'][0]['column']]) ? $searchUsersArray[$request['order'][0]['column']] : 'users.updated_at',
                'order_dir' => isset($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'ASC',
                'limit' => isset($request['length']) ? $request['length'] : '',
                'status' => isset($request['status']) ? $request['status'] : '',
                'user_type' => isset($request['user_type']) ? $request['user_type'] : '',
            );

            $query = $this->user->select($selectUsersFields)
                    ->withTrashed()
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'inactive'));
            if (!empty($conditions['user_type']) && isset($conditions['user_type'])) {
                $query->where('users.user_type', $conditions['user_type']);
            }
            if (!empty($conditions['status']) && isset($conditions['status'])) {
                $query->where('users.status', $conditions['status']);
            }

            if (!empty($conditions['search']['value']) && $conditions['search']['value'] != 'all') {
                $query->where(function($query) use ($conditions, $searchUsersArray) {
                    foreach ($searchUsersArray as $search) {
                        if ($search == 'users.firstname') {
                            $query->orWhere(DB::raw("CONCAT(`users`.`firstname`, ' ', `users`.`lastname`)"), 'like', "%{$conditions['search']['value']}%");
                        } else {
                            $query->orWhere($search, 'like', "%{$conditions['search']['value']}%");
                        }
                    }
                });
            }
            $response['total_count'] = $query->count();
            $query->orderBy($conditions['order'], $conditions['order_dir']);
            $query->limit($conditions['limit'])->offset($conditions['start']);
            $response['records'] = $query->get()->toArray();
            $response['incomplete_results'] = false;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $response;
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function showUserDetails($id) {
        try {
            $userDetailsArray = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $userDetails = $this->user
                        ->select('users.firstname',
                                'users.lastname',
                                'users.email',
                                'users.mobile',
                                'users.user_type',
                                'users.status',
                                'guardians.*')
                        ->leftjoin('guardians', function($join) {
                            $join->on('guardians.user_id', '=', 'users.id');
                        })
                        ->withTrashed()
                        ->where('users.id', '=', $id)
                        ->first();

                if (isset($userDetails) && !empty($userDetails) && $userDetails->count() > 0) {
                    $userDetailsArray = $userDetails->toArray();
                }
            }
            return $userDetailsArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function adminProfileDetails($id) {
        try {
            $userDetailsArray = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $userDetails = $this->user
                        ->select('users.*')
                        ->where(array('users.id' => $id, 'users.user_type' => 'Administration'))
                        ->first();

                if (isset($userDetails) && !empty($userDetails) && $userDetails->count() > 0) {
                    $userDetailsArray = $userDetails->toArray();
                }
            }
            return $userDetailsArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function userProfileDetails($id) {
        try {
            $userDetailsArray = [];
            if (isset($id) && !empty($id) && $id > 0) {
                $userDetails = $this->user
                        ->select('users.*')
                        ->where(array('users.id' => $id, 'users.user_type' => 'User'))
                        ->first();

                if (isset($userDetails) && !empty($userDetails) && $userDetails->count() > 0) {
                    $userDetailsArray = $userDetails->toArray();
                }
            }
            return $userDetailsArray;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @param type $status
     * @return type
     * @throws \Exception
     */
    public function updateUserStatus($id, $status) {
        try {
            $updateArray = array(
                'status' => $status,
                'modified_by' => auth()->user()->id
            );
            if (isset($updateArray) && !empty($updateArray)) {
                $update = \App\User::find($id)->update($updateArray);
                if ($update == true && $status == 'inactive') {
                    $this->user->where('id', $id)->delete();
                    $guardian_id = Guardian::where('user_id', $id)->get(['id'])->toArray();
                    if (isset($guardian_id) && !empty($guardian_id)) {

                        // Cancel Subscriptions 
                        $getBandUserIDs = \App\Models\Banduser::select('banduser_subscription_transactions.id')
                                        ->join('banduser_subscription_transactions', function($join) {
                                            $join->on('banduser_subscription_transactions.banduser_id', '=', 'bandusers.id');
                                        })
                                        ->where('guardians_id', '=', $guardian_id[0]['id'])->get()->toArray();

                        if (isset($getBandUserIDs) && !empty($getBandUserIDs)) {
                            foreach ($getBandUserIDs as $getBandUserID) {
                                $cancelSub = $this->banduserRepository->cancelSubscription($getBandUserID);
                            }
                        }

                        \App\Models\Banduser::where('guardians_id', $guardian_id[0]['id'])
                                ->update(['status' => 'inactive']);
                    }
                }
                return $update;
            } else {
                return;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function getUserIdAndUpdate($verification_code) {
        try {
            $result = 0;
            $getUserID = $this->user->where('email_verification_code', $verification_code)->get(['id'])->toArray();
            if (isset($getUserID[0]['id']) && !empty($getUserID[0]['id'])) {
                $updateUserAccount = $this->user
                        ->where('id', $getUserID)
                        ->update(array('email_verify' => 'yes', 'email_verification_code' => '', 'status' => 'active'));
                if ($updateUserAccount)
                    return $updateUserAccount;
                else
                    return $result;
            }
            return $result;
        } catch (\Exception $ex) {
            return response(json_encode(['error' => $ex->getMessage()]))
                            ->setStatusCode(500, $ex->getMessage());
        }
    }

    /**
     * storeAdminUser
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function storeAdminUser($postData) {
        try {
            $data = [];
            $data['firstname'] = isset($postData['firstname']) && !empty($postData['firstname']) ? ucfirst($postData['firstname']) : '';
            $data['lastname'] = isset($postData['lastname']) && !empty($postData['lastname']) ? ucfirst($postData['lastname']) : '';
            $data['email'] = isset($postData['email']) && !empty($postData['email']) ? $postData['email'] : '';
            $data['mobile'] = isset($postData['mobile']) && !empty($postData['mobile']) ? $postData['mobile'] : '';
            $data['password'] = isset($postData['password']) && !empty($postData['password']) ? bcrypt($postData['password']) : '';
            $data['user_type'] = 'Administration';
            $data['status'] = 'active';
            $data['email_verify'] = 'yes';
            $result = $this->user->create($data);
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function destroyAdminUser($id) {
        try {
            $adminUserCnt = $this->user
                    ->select('users.*')
                    ->where(array('users.user_type' => 'Administration', 'users.deleted_at' => NULL))
                    ->count();
            if ($adminUserCnt > 1) {
                $result = $this->user->where('id', $id)->delete();
            } else {
                $result = 2;
            }
            return $result;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $content
     * @return type
     * @throws \Exception
     */
    public function updatePassword($content) {
        try {
            $getUserDetails = $this->user->where('id', $content['id'])->first();
            if (Hash::check($content['password'], $getUserDetails->password)) {
                if ($content['new_password'] === $content['repeat_new_password']) {
                    $details = $this->user
                            ->where('id', $content['id'])
                            ->update(array(
                        'password' => bcrypt($content['new_password'])
                    ));
                    return 200;
                } else {
                    return 201;
                }
            } else {
                return 202;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function exportLeads() {
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at'];
            $query = $this->user->select($selectUsersFields)
                    ->leftJoin('guardians', 'users.id', '=', 'guardians.user_id')
                    ->leftJoin('bandusers', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->where(function($query) {
                        $query->orWhere('bandusers.status', '=', 'draft')
                        ->orWhere('bandusers.id', '=', null);
                    })
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->distinct('users.id')->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function exportActiveCustomer() {
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at'];
            $query = $this->user->select($selectUsersFields)
                    ->leftJoin('guardians', 'users.id', '=', 'guardians.user_id')
                    ->leftJoin('bandusers', 'guardians.id', '=', 'bandusers.guardians_id')
                    ->where(function($query) {
                        //save as draft and status pending should check after payment gateway implemented
                        $query->orWhere(array('bandusers.status' => 'active'))
                        ->orWhere(array('bandusers.status' => 'pending'));
                    })
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'active'))
                    ->distinct('users.id')
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function exportInactiveCustomer() {
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at'];
            $query = $this->user->select($selectUsersFields)
                    ->withTrashed()
                    ->where(array('users.user_type' => 'User'))
                    ->where(array('users.status' => 'inactive'))
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function adminusersExport() {
        try {
            $selectUsersFields = [
                'users.id',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'users.user_type',
                'users.status',
                'users.created_at'];
            $query = $this->user->select($selectUsersFields)
                    ->where(array('users.user_type' => 'Administration'))
                    ->get()
                    ->toArray();

            return $query;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * 
     * @param type $postData
     * @return type
     * @throws \Exception
     */
    public function registerFaceBookUser($postData) {
        try {
            $data = [];
            $name = explode(" ", $postData->name);            
            $data['firstname'] = ucfirst($name[0]);
            $data['lastname'] = ucfirst($name[1]);
            $data['email'] = $postData->email;
            $data['mobile'] = '9999999999';
            $data['password'] = bcrypt('12345678');
            $data['email_verification_code'] = '11111111';
            $data['status'] = 'active';
            $data['email_verify'] = 'yes';
            $data['facebook_id'] = $postData->id;
            $data['user_type'] = 'User';
            \Log::info('$data >> ' . print_r($data, true)); 
            $user = $this->user->create($data);
            \Log::info('$user Model >> ' . print_r($user, true));       
            
            if ($user) {
                $data['data'] = $user;
                $data['success'] = 'Your registeration has been done successfully.';
                return $data;
            } else {
                return ['error' => 'Registeration has been failed.'];
            }
        } catch (\Exception $exc) {
            throw $exc;
        }
    }

}
