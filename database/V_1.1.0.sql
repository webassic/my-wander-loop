CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- 2020-05-21
CREATE TABLE `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` bigint(20) unsigned NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci,
  `new_values` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(1023) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audits_auditable_type_auditable_id_index` (`auditable_type`,`auditable_id`),
  KEY `audits_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

DROP TABLE `guardians`;
CREATE TABLE `guardians` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` enum('Mr','Mrs','Ms','Dr','default') NOT NULL DEFAULT 'default',
  `user_id` int(11) DEFAULT NULL,
  `guardian_type` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `landline` varchar(45) DEFAULT NULL,
  `identity_type` enum('default','PAN','Aadhar','Driving License') DEFAULT 'default',
  `identity_number` varchar(255) DEFAULT NULL,
  `social_media_login` text,
  `otp` int(11) DEFAULT NULL,
  `reg_no` varchar(255) DEFAULT NULL,
  `renew_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Removed this becase we store this information in same table guardians 
DROP TABLE `institutions`;

-- Currently ailing from - Diseases List - Master 
DROP TABLE IF EXISTS `ailings`;
CREATE TABLE `ailings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` INT(11) NOT NULL,
  `modified_by` INT(11) DEFAULT NULL,  
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Bands Master 
DROP TABLE IF EXISTS `band_types`;
CREATE TABLE `band_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `band_name` VARCHAR(255) NOT NULL,
  `color` VARCHAR(255) NOT NULL,
  `glow_dark` enum('yes','no') DEFAULT 'no',  
  `firstname_printed` enum('yes','no') DEFAULT 'no',
  `mobile_printed` enum('yes','no') DEFAULT 'no',
  `scanner_phone_number` enum('yes','no') DEFAULT 'no',
  `description` VARCHAR(255) NOT NULL,
  `status` ENUM('active','inactive') DEFAULT 'active',  
  `created_by` INT(11) NOT NULL,
  `modified_by` INT(11) DEFAULT NULL,  
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `bands_images`;
CREATE TABLE `bands_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `band_types_id` int(11) NOT NULL,
  `file_name` varchar(255),
  `file_path` varchar(1024), 
  `created_by` INT(11) NOT NULL,
  `modified_by` INT(11) DEFAULT NULL,  
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Band Users 
DROP TABLE IF EXISTS `bandusers`;
CREATE TABLE `bandusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guardians_id` int(11) NOT NULL,
  `uniqueid` int(8) DEFAULT NULL,
  `title` enum('Mr','Mrs','Ms','Dr','default') DEFAULT 'default',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `landline` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `mobile2` varchar(45) DEFAULT NULL,
  `address` varchar(250),
  `locality` varchar(250),
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `blood_group` varchar(45) DEFAULT NULL,
  `relationship_with_applicant` varchar(255) DEFAULT NULL,
  `otp` varchar(45) DEFAULT NULL,
--   `qr_code` varchar(255) DEFAULT NULL,
--   `file_path` varchar(1024), 
  `status` ENUM('active','inactive') DEFAULT 'active',  
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- DROP TABLE IF EXISTS `bandusers_qr_code_details`;
-- CREATE TABLE `bandusers_qr_code_details` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `bandusers_id` int(11) NOT NULL,
--   `qr_code` varchar(255) DEFAULT NULL,
--   `file_path` varchar(1024),  
--   `created_by` int(11) DEFAULT NULL,
--   `modified_by` INT(11) DEFAULT NULL,
--   `created_at` timestamp NULL DEFAULT NULL,
--   `updated_at` timestamp NULL DEFAULT NULL
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `bandusers_images`;
CREATE TABLE `bandusers_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandusers_id` int(11) NOT NULL,
  `file_name` varchar(255),
  `file_path` varchar(1024),  
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `bandusers_ailings`;
CREATE TABLE `bandusers_ailings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandusers_id` int(11) NOT NULL,
  `ailing_id` int(11) DEFAULT NULL,
  `since_when` date DEFAULT NULL,
  `primary_physician` varchar(255) DEFAULT NULL,
  `secondary_physician` varchar(255) DEFAULT NULL,
  `hospital_preference` text,
  `hospital_city` varchar(100) DEFAULT NULL,
  `admitted_hospital` varchar(255) DEFAULT NULL,
  `recent_illness` varchar(255) DEFAULT NULL, 
  `is_deleted` ENUM('yes','no') DEFAULT 'no',  
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `bandusers_wandered_off_history`;
CREATE TABLE `bandusers_wandered_off_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandusers_id` int(11) NOT NULL,
  `lost_city` varchar(100) DEFAULT NULL,
  `lost_address` varchar(100) DEFAULT NULL,
  `found_city` varchar(100) DEFAULT NULL,
  `found_address` varchar(100) DEFAULT NULL,
  `is_deleted` ENUM('yes','no') DEFAULT 'no',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `bandusers_bands`;
CREATE TABLE `bandusers_bands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandusers_id` int(11) NOT NULL,
  `package_details` ENUM('standard','premium') DEFAULT 'standard',
  `band_types_id` int(11) NOT NULL,  
  `status`  ENUM('pending','approved','damage','lost') DEFAULT 'pending', 
  `is_delivered` ENUM('yes','no') DEFAULT 'no',  
  `is_active` ENUM('active','inactive') DEFAULT 'active', 
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `promo_codes`;
CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `valid_till` date NOT NULL,
  `discount_offered_type` ENUM('','%','rs') DEFAULT '',
  `discount_offered` int(11) NOT NULL,
  `is_active` ENUM('active','inactive') DEFAULT 'active', 
  `created_by` int(11) DEFAULT NULL,
  `modified_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `promo_code_users`;
CREATE TABLE `promo_code_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_code_users_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL, 
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `guardians` ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `renew_date`;
ALTER TABLE `guardians` ADD COLUMN `modified_by` int(11) DEFAULT NULL AFTER `created_by`;

ALTER TABLE `guardians` 
ADD COLUMN `institution_name` VARCHAR(255) NULL AFTER `user_id`;

ALTER TABLE `users` 
CHANGE COLUMN `user_type` `user_type` ENUM('', 'Administration', 'User') NULL DEFAULT '' ;

ALTER TABLE `guardians` 
CHANGE COLUMN `guardian_type` `guardian_type` ENUM('Individual', 'Institution') NULL DEFAULT 'Individual' ;

ALTER TABLE `promo_codes` 
CHANGE COLUMN `discount_offered_type` `discount_offered_type` ENUM('', '%', 'Rs') NULL DEFAULT '' ;

ALTER TABLE `promo_code_users` 
CHANGE COLUMN `promo_code_users_id` `promo_codes_id` INT(11) NOT NULL ;

-- 25 May 2020

ALTER TABLE `guardians` 
ADD COLUMN `is_exist` TINYINT NULL DEFAULT 0 AFTER `renew_date`;

-- 27 May 2020
ALTER TABLE `ailings` ADD COLUMN `status` ENUM('active', 'inactive') NULL DEFAULT 'active' AFTER `name`;

ALTER TABLE `band_types` 
ADD COLUMN `deleted_at` DATETIME NULL AFTER `modified_by`;

-- 28 May 2020
ALTER TABLE promo_codes
ADD CONSTRAINT UC_name UNIQUE (name,valid_till);

-- 29 May 2020
ALTER TABLE `bands_images` 
RENAME TO `band_types_images` ;

CREATE TABLE `guardians_dummy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=458 DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE DEFINER=`root`@`%` TRIGGER `guardians_reg_no_insert` BEFORE INSERT ON `guardians`
 FOR EACH ROW BEGIN
  INSERT INTO guardians_dummy VALUES (NULL);
  SET NEW.reg_no = CONCAT('REG', CONCAT(LPAD(LAST_INSERT_ID(), 5, '0')));
END
--  01 June 2020
ALTER TABLE `bandusers` 
CHANGE COLUMN `status` `status` ENUM('active', 'inactive', 'pending', 'approved') NULL DEFAULT 'pending' ;

-- 02 June 2020
ALTER TABLE `bandusers` 
CHANGE COLUMN `email2` `alternate_email` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `bandusers` 
CHANGE COLUMN `mobile2` `alternate_mobile` VARCHAR(45) NULL DEFAULT NULL ;

ALTER TABLE `bandusers` 
ADD COLUMN `save_as_draft` INT(11) NULL DEFAULT 0 AFTER `modified_by`;

ALTER TABLE `bandusers_wandered_off_history` 
ADD COLUMN `lost_date` DATE NULL DEFAULT NULL AFTER `lost_address`,
ADD COLUMN `found_date` DATE NULL DEFAULT NULL AFTER `found_address`;

ALTER TABLE `bandusers_ailings` 
ADD COLUMN `ailing_other` VARCHAR(255) NULL AFTER `ailing_id`;

-- 04 June 2020
CREATE TABLE `scanevents` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `banduser_uniqueid` VARCHAR(255) NULL,
  `banduser_name` VARCHAR(255) NULL,
  `scanner_mobile` VARCHAR(45) NULL,
  `ip_address` VARCHAR(255) NULL,
  `latitude` VARCHAR(255) NULL,
  `logitude` VARCHAR(255) NULL,
  `location` TEXT NULL,
  `mobile_verify` ENUM('yes', 'no') NULL DEFAULT 'no',
  `banduser_info_displayed` ENUM('yes', 'no') NULL DEFAULT 'no',
  `scanned_on` DATETIME NULL,
  `device_info` TEXT NULL,
  `json_data` JSON NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `bandusers_images` 
ADD COLUMN `file_type` VARCHAR(45) NULL DEFAULT NULL COMMENT 'file_type = image or qr_code' AFTER `bandusers_id`;

-- 08 June 2020

ALTER TABLE `bandusers` 
ADD COLUMN `aadhaar_card_number` INT(11) NULL COMMENT 'Capture last 4 digit aadhaar card number' AFTER `modified_by`;

ALTER TABLE `bandusers` 
ADD COLUMN `aadhaar_card_file_name` VARCHAR(255) NULL AFTER `modified_by`,
ADD COLUMN `aadhaar_card_file_path` VARCHAR(1024) NULL AFTER `aadhaar_card_file_name`,
ADD COLUMN `share_code` INT(11) NULL COMMENT 'Capture 4 digit share code for open file' AFTER `aadhaar_card_number`;

ALTER TABLE `bandusers` 
CHANGE COLUMN `status` `status` ENUM('active', 'inactive', 'pending') NULL DEFAULT 'pending' ;


ALTER TABLE `scanevents` 
ADD COLUMN `json_data_by_location` JSON NULL DEFAULT NULL AFTER `json_data_by_ip`,
CHANGE COLUMN `json_data` `json_data_by_ip` JSON NULL DEFAULT NULL ;

-- 09 June 2020
ALTER TABLE `scanevents` 
ADD COLUMN `lat_long_error` TEXT NULL AFTER `json_data_by_location`;

-- 15 June 2020

CREATE TABLE `support_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_ticket_id` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `related_to` enum('banduser_name','general_account','select_all') DEFAULT 'banduser_name',
  `message` varchar(45) DEFAULT NULL,
  `status` enum('active','inactive','pending') DEFAULT 'pending',
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


CREATE TABLE `support_ticket_bandusers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `support_ticket_id` INT(11) NULL,
  `banduser_id` INT(11) NULL,
  `created_at` TIMESTAMP NULL DEFAULT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `bandusers` 
ADD COLUMN `medical_history_not_applicable` VARCHAR(10) NULL DEFAULT 'off' AFTER `save_as_draft`,
ADD COLUMN `wanderoff_history_not_applicable` VARCHAR(10) NULL DEFAULT 'off' AFTER `medical_history_not_applicable`;

ALTER TABLE `support_tickets` 
CHANGE COLUMN `status` `status` ENUM('pending', 'pending_by_guardian', 'resolved') NULL DEFAULT 'pending' ,
CHANGE COLUMN `modified_by` `resolved_by` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `support_tickets` 
CHANGE COLUMN `created_by` `created_by` INT(11) NULL DEFAULT NULL AFTER `status`;

CREATE TABLE `support_ticket_comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `support_ticket_id` INT NOT NULL,
  `message` TEXT NOT NULL,
  `message_by` INT NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));

-- 16 June 2020

CREATE TABLE `event_logs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `value` TEXT NULL,
  `record_id` INT NULL,
  `user_id` INT NULL,
  `module` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));

-- 17 June 2020

ALTER TABLE `scanevents` 
ADD COLUMN `user_id` INT(11) NULL AFTER `lat_long_error`;

-- 20 June 2020

ALTER TABLE `support_tickets` 
CHANGE COLUMN `resolved_by` `status_modified_by` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `support_tickets` 
CHANGE COLUMN `status` `status` ENUM('pending', 'resolved') NULL DEFAULT 'pending' ;

-- 25 June 2020
ALTER TABLE `guardians` 
CHANGE COLUMN `title` `title` ENUM('Mr', 'Mrs', 'Ms', 'Dr', 'Default') NOT NULL DEFAULT 'default' ;

--29 June 2020
ALTER TABLE `users` 
ADD COLUMN `email_verification_code` VARCHAR(255) NULL AFTER `status`,
ADD COLUMN `email_verify` ENUM('yes', 'no') NULL DEFAULT 'no' AFTER `email_verification_code`;

-- 30 June 2020
ALTER TABLE `users` 
CHANGE COLUMN `status` `status` ENUM('active', 'inactive') NULL DEFAULT 'inactive' ;

ALTER TABLE `guardians` 
CHANGE COLUMN `title` `title` ENUM('Mr', 'Mrs', 'Ms', 'Dr', 'Others', 'Default') NOT NULL DEFAULT 'Default' ;

-- 01 July 2020
ALTER TABLE `bandusers_ailings` 
ADD COLUMN `hospital_registration_no` VARCHAR(100) NULL AFTER `bandusers_id`;

-- 02 July 2020
ALTER TABLE `users` 
ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;

-- 07 July 2020
ALTER TABLE `bandusers` 
CHANGE COLUMN `title` `title` ENUM('Mr', 'Mrs', 'Ms', 'Dr', 'default', 'Other') NULL DEFAULT 'default' ;

--13 July 2020
ALTER TABLE `bandusers` 
ADD COLUMN `languages_known` VARCHAR(250) NULL DEFAULT NULL AFTER `wanderoff_history_not_applicable`;

ALTER TABLE `bandusers_wandered_off_history` 
ADD COLUMN `additional_comments` VARCHAR(250) NULL DEFAULT NULL AFTER `modified_by`;

-- 15 July 2020

CREATE TABLE `razorpay_credentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_mode` varchar(45) DEFAULT NULL COMMENT 'TEST  mode',
  `test_key_id` varchar(255) DEFAULT NULL,
  `test_key_secret` varchar(255) DEFAULT NULL,
  `test_plan_id` varchar(255) DEFAULT NULL,
  `test_banduser_cost` varchar(255) DEFAULT NULL,
  `live_mode` varchar(45) DEFAULT NULL,
  `live_key_id` varchar(255) DEFAULT NULL,
  `live_key_secret` varchar(255) DEFAULT NULL,
  `live_plan_id` varchar(255) DEFAULT NULL,
  `live_banduser_cost` varchar(45) DEFAULT NULL,
  `last_modified` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `razorpay_credentials` VALUES (1,' TEST MODE','rzp_test_CIqPbmfIu3Go1h','bWwBNrkuMHWVNVMOo3T9PPEI','plan_F9abyoi3tyDm9U','249','LIVE MODE','rzp_test_CIqPbmfIu3Go1h','bWwBNrkuMHWVNVMOo3T9PPEI','live_plan','500','2020-07-15',14,'2020-07-15 10:19:52','2020-07-15 08:39:55');

--- 16 July 2020

ALTER TABLE `bandusers` 
CHANGE COLUMN `status` `status` ENUM('draft', 'active', 'inactive', 'pending') NULL DEFAULT 'pending' ;

-- 23 July 2020

CREATE TABLE `guardian_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guardian_id` int(11) DEFAULT NULL,
  `subscription_id` varchar(255) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL COMMENT 'Total bandusers count',
  `status` enum('active','inactive') DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `banduser_subscription_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guardian_subscription_id` int(11) DEFAULT NULL,
  `banduser_id` int(11) DEFAULT NULL,
  `subscription_id` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  `inactive_on` datetime DEFAULT NULL COMMENT 'Capture date when subscription will cancelled',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `razorpay_credentials` 
ADD COLUMN `test_invoice_number_prefix` VARCHAR(45) NULL DEFAULT NULL AFTER `test_banduser_cost`,
ADD COLUMN `test_invoice_number_start` INT(11) NULL DEFAULT NULL AFTER `test_invoice_number_prefix`,
ADD COLUMN `test_last_invoice_number` VARCHAR(250) NULL DEFAULT NULL AFTER `test_invoice_number_start`,
ADD COLUMN `live_invoice_number_prefix` VARCHAR(45) NULL DEFAULT NULL AFTER `live_banduser_cost`,
ADD COLUMN `live_invoice_number_start` INT(11) NULL DEFAULT NULL AFTER `live_invoice_number_prefix`,
ADD COLUMN `live_last_invoice_number` VARCHAR(250) NULL DEFAULT NULL AFTER `live_invoice_number_start`;


CREATE TABLE `new_band_orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `banduser_id` INT NOT NULL,
  `band_lost` ENUM('Yes', 'No') NULL DEFAULT 'No',
  `number_of_bands` INT NOT NULL,
  `total_amount` VARCHAR(255) NULL,
  `transaction_id` VARCHAR(255) NULL,
  `order_date` DATETIME NULL,
  `order_processed` ENUM('Yes', 'No') NULL DEFAULT 'No',
  `order_processed_on` DATETIME NULL,
  `order_shipped` ENUM('Yes', 'No') NULL DEFAULT 'No',
  `order_shipped_on` DATETIME NULL,
  `delivered` ENUM('Yes', 'No') NULL DEFAULT 'No',
  `deleted_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `new_band_orders` 
ADD COLUMN `created_by` INT NULL AFTER `delivered`,
ADD COLUMN `modified_by` INT NULL AFTER `created_by`;

-- 24 July 2020

CREATE TABLE `banduser_payment_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` varchar(255) DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `new_band_orders` 
ADD COLUMN `delivered_on` DATETIME NULL AFTER `delivered`;

ALTER TABLE `banduser_subscription_transactions` 
ADD COLUMN `invoice_number` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_id`;

CREATE TABLE `bandorders_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandorders_id` int(11) NOT NULL,
  `rzorder_id` varchar(255) DEFAULT NULL,
  `rzpayment_id` varchar(255) DEFAULT NULL,
  `rzsignature` text,
  `payment_status` varchar(255) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `workflow_status` enum('Initiated','Success','Failed','Unknown') DEFAULT 'Initiated',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `razorpayerrors_capture` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `action` VARCHAR(255) NULL,
  `user_id` VARCHAR(45) NULL,
  `error_code` VARCHAR(255) NULL,
  `error_message` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));

-- 25 July 2020

ALTER TABLE `banduser_subscription_transactions` 
CHANGE COLUMN `status` `status` ENUM('active', 'inactive', 'cancel') NULL DEFAULT 'inactive' ,
CHANGE COLUMN `inactive_on` `cancelled_on` DATETIME NULL DEFAULT NULL COMMENT 'Capture date when subscription will cancelled' ;

-- 27 July 2020 

ALTER TABLE `banduser_payment_transactions` 
ADD COLUMN `invoice_number` VARCHAR(255) NULL DEFAULT NULL AFTER `subscription_id`;

ALTER TABLE `banduser_subscription_transactions` 
DROP COLUMN `invoice_number`;

ALTER TABLE `razorpay_credentials` 
ADD COLUMN `test_gst_rate` VARCHAR(45) NULL DEFAULT NULL AFTER `test_last_invoice_number`,
ADD COLUMN `live_gst_rate` VARCHAR(45) NULL DEFAULT NULL AFTER `live_last_invoice_number`;

ALTER TABLE `bandorders_transactions` 
ADD COLUMN `invoice_number` VARCHAR(255) NULL AFTER `workflow_status`;

-- 30 July 2020

ALTER TABLE `bandusers` 
CHANGE COLUMN `status` `status` ENUM('draft', 'active', 'inactive', 'pending') NULL DEFAULT 'draft' ;

ALTER TABLE `new_band_orders` 
ADD COLUMN `amount_paid` VARCHAR(255) NULL AFTER `total_amount`;

-- 31 July 2020

CREATE TABLE `aadhaar_information_capture` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `banduser_id` INT(11) NULL,
  `username` VARCHAR(255) NULL,
  `dob` DATE NULL,
  `gender` VARCHAR(45) NULL,
  `address` VARCHAR(255) NULL,
  `image` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `aadhaar_information_capture` 
ADD COLUMN `status` VARCHAR(45) NULL AFTER `image`,
ADD COLUMN `response` TEXT NULL AFTER `status`;


ALTER TABLE `aadhaar_information_capture` 
CHANGE COLUMN `image` `image` TEXT NULL DEFAULT NULL ;

-- 07 August 2020

ALTER TABLE `bandusers` 
CHANGE COLUMN `aadhaar_card_number` `aadhaar_card_number` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Capture last 4 digit aadhaar card number' ;

ALTER TABLE `bandusers` 
CHANGE COLUMN `share_code` `share_code` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Capture 4 digit share code for open file' ;

-- 17 August 2020

ALTER TABLE `razorpay_credentials` 
CHANGE COLUMN `test_invoice_number_start` `test_invoice_number_start` VARCHAR(250) NULL DEFAULT NULL ;

ALTER TABLE `razorpay_credentials` 
CHANGE COLUMN `live_invoice_number_start` `live_invoice_number_start` VARCHAR(250) NULL DEFAULT NULL ;

-- 15 December 2020

ALTER TABLE `razorpay_credentials` 
ADD COLUMN `test_basic_plan_id` VARCHAR(255) NULL DEFAULT NULL AFTER `test_plan_id`;

ALTER TABLE `razorpay_credentials` 
ADD COLUMN `live_basic_plan_id` VARCHAR(255) NULL DEFAULT NULL AFTER `live_plan_id`;


ALTER TABLE `bandusers` 
ADD COLUMN `package_name` VARCHAR(50) NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `bandusers` 
CHANGE COLUMN `package_name` `package_name` VARCHAR(50) NULL DEFAULT NULL AFTER `languages_known`;

UPDATE `bandusers` SET `package_name` = 'standard';

-- 23 December 2020

ALTER TABLE `users` 
ADD COLUMN `facebook_id` VARCHAR(150) NULL AFTER `email_verify`;

-- 21 December 2021 - This is done to find out which records are created via cron job for renewal subscriptions workflow
create table bk_banduser_payment_transactions_20210925
select * from banduser_payment_transactions;

create table bk_banduser_subscription_transactions_20210925
select * from banduser_subscription_transactions;

ALTER TABLE `banduser_payment_transactions` 
ADD COLUMN `is_renewal_payment` ENUM('No','Yes') NULL DEFAULT 'No';

ALTER TABLE `banduser_payment_transactions` 
ADD COLUMN `start_date` date DEFAULT NULL AFTER `signature`,
ADD COLUMN `end_date` date DEFAULT NULL AFTER `start_date`;