/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  adminrb
 * Created: 5 May, 2020
 */

ALTER TABLE `reunite_update`.`bandusers` 
CHANGE COLUMN `guardian_id` `created_by` INT(11) NULL DEFAULT NULL ;


ALTER TABLE `reunite_update`.`bandusers` 
CHANGE COLUMN `deleted_at` `deleted_at` TIMESTAMP NULL DEFAULT NULL ;
