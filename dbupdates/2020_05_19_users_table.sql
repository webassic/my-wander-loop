
ALTER TABLE `users` ADD COLUMN `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL AFTER `password`;

ALTER TABLE `users` ADD COLUMN `status` ENUM('active','inactive') NULL DEFAULT 'active'  AFTER `remember_token`;
