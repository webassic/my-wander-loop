$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#ailings-list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[0, "asc"]],
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: false,
            ajax: {
                url: '/api/ailings/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
             },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && data == 'active') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'inactive') {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'firstname', name: 'firstname'},
                {data: 'created_at', name: 'created_at',
                    render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editAilingDetails(' + data.id + ')"  title="Edit/Update"><i class="flaticon-edit"></i></a>';
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                name = aData.firstname != '' ? aData.firstname + " " + aData.lastname : '';
                $('td:eq(2)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
        $('#listing_status').delegate('#status', 'change', function () {
            otable.columns(1).search(this.value).draw();
        });
        $('#ailings-list_filter input').prop('title', 'In case you are trying to search based on a Date, then use YYYY-MM-DD format.');
    });


    $("#addAilingSubmitBtnId").click(function () {
        $("#addAilingFormId").validate({
            rules: {
                name: {
                    required: true,
                },
                color: "required",
                description: "required",

            },
            submitHandler: function (form) {
                var url = $("#addAilingFormId").attr('action');
                var data = $('#addAilingFormId').serializeArray();
                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });
                $('#addAilingSubmitBtnId').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(item),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $("#success-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#success-msg").text(response.message);
                        $("#addAilingFormId").get(0).reset();
                        $('#addAilingsModal').modal('toggle');
                        $('#addAilingsModal').modal('toggle');
                        $('#ailings-list').DataTable().ajax.reload();
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');
                            $('#addAilingSubmitBtnId').attr('disabled', false);
                        }, 3000);

                    },
                    error: function (error) {
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    $("#" + dkey + "_error").text(dvalue[0]);
                                    $("#" + dkey + "_error").removeClass('text-muted');
                                    $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                });
                                $('#addAilingSubmitBtnId').attr('disabled', false);
                                $('.errorMessage').delay(6000).fadeOut();
                            }
                        });
                    }
                });
            }
        });
    });

    $('.model-close').click(function (e) {
        var form_name = $(this).attr('data-formname');
        $("form[name='" + form_name + "']")[0].reset();
        validator = $("form[name='" + form_name + "']").validate({});
        validator.resetForm();
        $(".error").css("display", "none");
    });
    $('.close').click(function () {
        var form_name = $(this).attr('data-formname');
        $("form[name='" + form_name + "']")[0].reset();
        validator = $("form[name='" + form_name + "']").validate({});
        validator.resetForm();
        $(".error").css("display", "none");
    });

    $("#editAilingSubmitBtnId").click(function () {
        $("#editAilingFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                name: {
                    required: true,
                },
                color: "required",
                description: "required",

            },
            submitHandler: function (form) {
                var data = $('#editAilingFormId').serializeArray();
                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });
                var url = $("#editAilingFormId").attr('action');
                var id = $("#eid").val();
                $('#editAilingSubmitBtnId').attr('disabled', true);
                $.ajax({
                    type: "PUT",
                    url: url + '/' + id,
                    data: JSON.stringify(item),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $("#success-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#success-msg").text(response.message);
                        $("#editAilingFormId").get(0).reset();
                        $('#editAilingsModal').modal('toggle');
                        $('#ailings-list').DataTable().ajax.reload();
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');
                            $('#editAilingSubmitBtnId').attr('disabled', false);
                        }, 3000);
                    },
                    error: function (error) {
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    $("#e" + dkey + "_error").text(dvalue[0]);
                                    $("#e" + dkey + "_error").removeClass('text-muted');
                                    $("#e" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                });
                                $('#editAilingSubmitBtnId').attr('disabled', false);
                                $('.errorMessage').delay(6000).fadeOut();
                            }

                        });

                    }
                });
            }
        });
    });
});
function editAilingDetails(id) {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: '/api/ailings/' + id,
        dataType: "json",
        success: function (response) {
            if (response.data !== '') {
                $('#editAilingsModal').modal('show');
                $.each(response.data, function (key, value) {
                    $("#eid").val(value.id);
                    $("#e_name").val(value.name);
                    $('#estatus').val(value.status);
                });

            }
        },
        error: function (error) {
            console.log(error);
        }
    });

}