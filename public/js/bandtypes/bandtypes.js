$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    $(document).ready(function () {
        
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#band-types-list').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[1, "desc"]],
            ajax: {
                url: '/api/bandtypes/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
             },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'band_name', name: 'band_name'},
                {data: 'color', name: 'color'},
                {data: 'description', name: 'description'},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && data == 'active') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'inactive') {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'created_at', name: 'created_at',
                    render: function (d) {
                        return moment(d).format("DD MM YYYY") != 'Invalid date' ? moment(d).format("DD MMM YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="getBandTypeDetails('+ data.id +')" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showBandTypeDetails('+ data.id +')"><i class="flaticon2-information"></i></a><a href="javascript:void(0);" data-toggle="modal" data-toggle="modal" data-target="#deleteBandTypesModal" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteBandType('+data.id+')"><i class="flaticon-delete"></i></a>';
//                        return '<a href="javascript:void(0);" onclick="getInventoryTransactionDetails(' + data.id + ')"  class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="/inventory-transactions/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a><a href="javascript:void(0);"  onclick="deleteInventoryTransaction(' + data.id + ')" data-toggle="modal" data-target="#deleteInventoryTransactionModal" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="flaticon-delete"></i></a>';
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
              
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
        $('#listing_status').delegate('#bandTypeStatus', 'change', function () {
             otable.columns(1).search(this.value).draw();
        });
         $("#addBandTypeSubmitBtnId").click(function(){
            $("#addBandTypeFormId").validate({
                rules: {
                    // simple rule, converted to {required:true}
                    band_name: {
                        required: true,
                    },
                    color: "required",
                    description :"required",
                    
                },
                submitHandler: function(form) {
                    var url = $("#addBandTypeFormId").attr('action');
                   
                    $('#addBandTypeSubmitBtnId').attr('disabled',true);
                    $.ajax({
                        type: "POST",
                        url: url,
                        data:$("#addBandTypeFormId").serialize(),
                        dataType: "json",
                        ContentType :'application/json',
                        success: function (response) {
                          $("#success-alert").css('display','block');
                          $(".error").css('display','none');
                            $("#success-msg").text(response.message);
                            $("#addBandTypeFormId").get(0).reset();
                            $('#addBandTypesModal').modal('toggle');
                           $('#addBandTypesModal').modal('toggle');
                           $('#band-types-list').DataTable().ajax.reload();
                            setInterval(function(){
                                 $("#success-alert").fadeOut('slow');
                                 $('#addBandTypeSubmitBtnId').attr('disabled',false);
                            }, 3000);
                            
                        },
                        error: function (error) {
                            console.log(error);
                           $.each( error, function( key, value ) {
                                if(key=='responseJSON'){
                                    console.log(value.success);
                                     $.each( value.data, function( dkey, dvalue ) {
                                         console.log('Key' + dkey);
                                         console.log('Value' + dvalue[0]);
                                        $("#"+dkey+"_error").text(dvalue[0]);
                                            $("#"+dkey+"_error").removeClass('text-muted'); 
                                            $("#"+dkey+"_error").css({'display':'block','color':'red'}); 
                                     });
                                     $('#addBandTypeSubmitBtnId').attr('disabled',false);
                                     $('.errorMessage').delay(6000).fadeOut();
                                }
                                   
                            });
                                
                        }
                    });
                }
            });
        });
         $("#editBandTypeSubmitBtnId").click(function(){
            $("#editBandTypeFormId").validate({
                rules: {
                    // simple rule, converted to {required:true}
                    band_name: {
                        required: true,
                    },
                    color: "required",
                    description :"required",
                    
                },
                submitHandler: function(form) {
                    var url = $("#editBandTypeFormId").attr('action');
                   var id = $("#eid").val();
                    $('#editBandTypeSubmitBtnId').attr('disabled',true);
                    $.ajax({
                        type: "PUT",
                        url: url+'/'+id,
                        data:$("#editBandTypeFormId").serialize(),
                        dataType: "json",
                        ContentType :'application/json',
                        success: function (response) {
                          $("#success-alert").css('display','block');
                          $(".error").css('display','none');
                            $("#success-msg").text(response.message);
                            $("#editBandTypeFormId").get(0).reset();
                            $('#editBandTypesModal').modal('toggle');
                          
                           $('#band-types-list').DataTable().ajax.reload();
                            setInterval(function(){
                                 $("#success-alert").fadeOut('slow');
                                 $('#editBandTypeSubmitBtnId').attr('disabled',false);
                            }, 3000);
                            
                        },
                        error: function (error) {
                            console.log(error);
                           $.each( error, function( key, value ) {
                                if(key=='responseJSON'){
                                    console.log(value.success);
                                     $.each( value.data, function( dkey, dvalue ) {
                                         console.log('Key' + dkey);
                                         console.log('Value' + dvalue[0]);
                                        $("#e"+dkey+"_error").text(dvalue[0]);
                                            $("#e"+dkey+"_error").removeClass('text-muted'); 
                                            $("#e"+dkey+"_error").css({'display':'block','color':'red'}); 
                                     });
                                     $('#editBandTypeSubmitBtnId').attr('disabled',false);
                                     $('.errorMessage').delay(6000).fadeOut();
                                }
                                   
                            });
                                
                        }
                    });
                }
            });
        });
    });
    $("#deleteBandTypeSubmitBtnId").click(function(){
        $("#deleteBandTypeFormId").validate({
            submitHandler: function(form) {
                var url = $("#deleteBandTypeFormId").attr('action');
                var id = $("#bandTypeId").val();
                $.ajax({
                    type: "DELETE",
                    url: url+'/'+id,
                    dataType: "json",
                    ContentType :'application/json',
                    success: function (response) {
                      $("#success-alert").css('display','block');
                      $(".error").css('display','none');
                        $("#success-msg").text(response.message);
                        $("#deleteBandTypeFormId").get(0).reset();
                        $('#deleteBandTypesModal').modal('toggle');
                       $('#band-types-list').DataTable().ajax.reload();
                        setInterval(function(){
                             $("#success-alert").fadeOut('slow');

                        }, 3000);

                    },
                    error: function (error) {
                        console.log(error);


                    }
                });
            }
        });
        
    });
    $('#addBandTypesModal').on('hidden.bs.modal', function () {
    $('#addBandTypeFormId')[0].reset();
    });
    
   
});
function showBandTypeDetails(id) {
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl +'/api/bandtypes/' + id,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.data!==''){
                $('#viewBandTypesModal').modal('show');
                 $.each( response.data, function( key, value ) {
                     $("#vid").val(value.id);
                    $("#vband_name").val(value.band_name);
                    $("#vcolor").val(value.color);
                    $("#vdescription").val(value.description);
                    $('#vstatus').val(value.status);
                    $("input[name=glow_dark][value='"+value.glow_dark+"']").prop("checked",true);
                    $("input[name=firstname_printed][value='"+value.firstname_printed+"']").prop("checked",true);
                    $("input[name=mobile_printed][value='"+value.mobile_printed+"']").prop("checked",true);
                    $("input[name=scanner_phone_number][value='"+value.scanner_phone_number+"']").prop("checked",true);
                 });

            }
        },
        error: function (error) {
            console.log(error);


        }
    });
}
function getBandTypeDetails(id) {
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl +'/api/bandtypes/' + id,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.data!==''){
                $('#editBandTypesModal').modal('show');
                 $.each( response.data, function( key, value ) {
                     $("#eid").val(value.id);
                    $("#eband_name").val(value.band_name);
                    $("#ecolor").val(value.color);
                    $("#edescription").val(value.description);
                    $('#estatus').val(value.status);
                    $("input[name=glow_dark][value='"+value.glow_dark+"']").prop("checked",true);
                    $("input[name=firstname_printed][value='"+value.firstname_printed+"']").prop("checked",true);
                    $("input[name=mobile_printed][value='"+value.mobile_printed+"']").prop("checked",true);
                    $("input[name=scanner_phone_number][value='"+value.scanner_phone_number+"']").prop("checked",true);
                 });

            }
        },
        error: function (error) {
            console.log(error);


        }
    });
}
function openEditMOdal(){
   var bandTypeId =  $("#vid").val();
   $('#viewBandTypesModal').modal('toggle');
   setTimeout(function(){  
    getBandTypeDetails(bandTypeId);
   },1000);
}
function deleteBandType(id){
    $("#bandTypeId").val(id);
 }