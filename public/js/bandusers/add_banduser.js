(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);
var KTBootstrapSelect = function () {

    // Private functions
    var demos = function () {
        // minimum setup
        $('.kt-selectpicker').selectpicker();
    }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();
var KTBootstrapDatepicker = function () {

    var arrows;

    // Private functions
    var demos = function () {

        // enable clear button
        $('#kt_datepicker_banduser,#kt_datepicker_lost_date,#kt_datepicker_found_date,#edit_kt_datepicker_lost_date,#edit_kt_datepicker_found_date,#kt_datepicker_since_when,#edit_kt_datepicker_since_when').datepicker({
            rtl: KTUtil.isRTL(),
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();
var medicalHistoryDataArr = [];
var wanderOffEventsHistoryDataArr = [];
$(document).ready(function ()
{
    
    var fileupload = $("#aadhaar_card_file");
    var button = $("#btnFileUpload");
    button.click(function () {
       fileupload.click();
    });

    $('form input').on('keypress', function (e) {
        return e.which !== 13;
    });

    $("#aadhaar_card_file").change(function (e) { 
        $("#spnFilePath").css('display','block');
        $("#spnFilePath").html($('#aadhaar_card_file')[0].files[0].name);

        $('#aadhaar_card_file_name_id').remove();
        $('#aadhaar_card_original_file_name').remove();
        var divLength = $('#aadhharCardDiv').find('input').length;
        if (divLength > 2) {
            $(this).parent("input").remove();
        } else {
            var file_size = $('#aadhaar_card_file')[0].files[0].size;
            var file_name = $('#aadhaar_card_file')[0].files[0].name;
            console.log('file_size >> ' + file_size);
            if (file_size > 2097152) {
                console.log('error');
                $("#uploadFileError").css("display", "block");
                $("#uploadFileError").html("Allowed format: zip. Max file size: 2 MB.");
                $("#bandUsersSubmitId").attr("disabled", true);
            } else {
                $("#uploadFileError").css("display", "none");
                $("#bandUsersSubmitId").attr("disabled", false);
                console.log('success');
                var files = e.target.files,
                        filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    var fileReader = new FileReader();
                    fileReader.onload = (function (e) {
                        console.log('ddd >> ' + e.target.result);
                        $("<input type=\"hidden\" id=\"aadhaar_card_file_name_id\" name=\"aadhaar_card_file_name\" value=\"" + e.target.result + "\"/>").insertAfter("#aadhaar_card_file");
                        $("<input type=\"hidden\" id=\"aadhaar_card_original_file_name\" name=\"aadhaar_card_original_file_name\" value=\"" + file_name + "\"/>").insertAfter("#aadhaar_card_file");
                    });
                    fileReader.readAsDataURL(f);
                }
            }
        }
    });
    KTBootstrapSelect.init();
    KTBootstrapDatepicker.init();
    $("#bandUsersSubmitId").click(function () {
        var date = $('#date').children("option:selected").val();
        var month = $('#month').children("option:selected").val();
        var year = $('#year').children("option:selected").val();
        if (date == '' && month == '' && year == '') {
            $("#validatedob").text('This field is required');

        } else if (date == '' || month == '' || year == '') {
            $("#validatedob").text('Please enter valid format');
        } else {
            if (month == 'Feb' && date > 29) {
                $("#validatedob").text('Date cannot greater than 29');
            } else {

                $("#validatedob").text('');
            }
        }

        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#bandUserFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                title: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                    noSpace: true
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                    noSpace: true
                },
                alternate_mobile: {
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                    noSpace: true
                },
                landline: {
                    digits: true,
                    minlength: 6,
                    maxlength: 15,
                    noSpace: true
                },
                firstname: {
                    required: true,
                    noSpace: true
                },
                lastname: {
                    required: true,
                    noSpace: true
                },
                locality: {
                    required: true,
                    noSpace: true
                },
                city: {
                    required: true,
                    noSpace: true
                },
                state: {
                    required: true,
                    noSpace: true
                },
                address: {
                    required: true,
                    noSpace: true
                },
                blood_group: "required",
                relationship_with_applicant: "required",
                "files[]": {
                    required: true,
                    extension: "jpg|jpeg|png",
                },
                aadhaar_card_file: {
                    required: true
                },
                pin: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength: 6,
                    noSpace: true
                },
//                aadhaar_card_number: {
//                    required: true,
//                    digits: true,
//                    minlength: 4,
//                    maxlength: 4,
//                    noSpace: true
//                },
                share_code: {
                    required: true,
                    digits: true,
                    minlength: 4,
                    maxlength: 4,
                    noSpace: true
                },
                languages_known: {
                    noSpace: true
                },
            },
            messages: {
                aadhaar_card_file: "Please upload .ZIP file",
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                    $('#error_banduser_status').html('<div class="alert alert-danger">There are some validation errors please correct them and re-submit.</div>');
                    $('#below_error_banduser_status').html('<div class="alert alert-danger">There are some validation errors please correct them and re-submit.</div>');
                }
            },
            submitHandler: function (form) {
                var date = $('#date').children("option:selected").val();
                var month = $('#month').children("option:selected").val();
                var year = $('#year').children("option:selected").val();
                if (date == '' && month == '' && year == '') {
                    $("#validatedob").text('This field is required');
                    $('#date').focus();
                } else if (date == '' || month == '' || year == '') {
                    $('#date').focus();
                    $("#validatedob").text('Please enter valid format');
                } else {
                    if (month == 'Feb' && date > 29) {
                        W
                        $("#validatedob").text('Date cannot greater than 29');
                    } else {

                        $("#validatedob").text('');
                    }

                    $("#error_banduser_status").fadeOut('slow');
                    $("#below_error_banduser_status").fadeOut('slow');
                    var data = $("#bandUserFormId").serializeFormJSON();
                    $('.loadingImageLoader').css('display', 'block');
                    var mobile = data.mobile;
                    $("#addMobileNumber").html('We have sent an OTP to your Mobile Number – <b>' + mobile + '</b>. Please verify your mobile number.');
                    var id = 'null';
                    $.ajax({
                        type: "GET",
                        url: '/api/banduser_getotp/' + mobile + '/' + id,
                        dataType: "json",
                        ContentType: 'application/json',
                        success: function (response) {
                            $('.loadingImageLoader').css('display', 'none');
                            $("#bandUserVerifyOtpModal").modal('show');
//                        $("#banduser_otp").val(response.data);
                            $("#hidden_banduser_otp").val(btoa(response.data));
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }


            }
        });
    });
// Verify OTP and submit form 
    $("#bandUserVerifyOTP").click(function (e) {
        $("#bandUserVerifyOtpFormId").validate({
            submitHandler: function (form) {
                var otp = $("#banduser_otp").val();
                var hiddenOtp = atob($("#hidden_banduser_otp").val());
                if (hiddenOtp == otp) {
                    $('#bandUserOTPMsg').html('<div class="alert alert-success">OTP has been verified successfully.</div>');
                    $('.loadingImageLoader').css('display', 'block');
                    setTimeout(function () {
                        $('#bandUserOTPMsg').html('');
                        $("#bandUserVerifyOtpModal").modal('hide');
                        var saveDataArray = {};
                        var data = $("#bandUserFormId").serializeFormJSON();
                        saveDataArray = Object.assign(saveDataArray, data);
                        if (data.medicalHistoryCheckbox == "on" && data.wanderOffHistoryCheckbox == "on") {
                            saveDataArray = Object.assign(saveDataArray, {medicalHistoryDataArr: [], wanderOffEventsHistoryDataArr: []});
                        } else if (data.medicalHistoryCheckbox == "on") {
                            saveDataArray = Object.assign(saveDataArray, {medicalHistoryDataArr: [], wanderOffEventsHistoryDataArr: wanderOffEventsHistoryDataArr});
                        } else if (data.wanderOffHistoryCheckbox == "on") {
                            saveDataArray = Object.assign(saveDataArray, {medicalHistoryDataArr: medicalHistoryDataArr, wanderOffEventsHistoryDataArr: []});
                        } else {
                            saveDataArray = Object.assign(saveDataArray, {medicalHistoryDataArr: medicalHistoryDataArr, wanderOffEventsHistoryDataArr: wanderOffEventsHistoryDataArr});
                        }
                        $.ajax({
                            type: "POST",
                            url: "/api/bandusers",
                            data: saveDataArray,
                            dataType: "json",
                            ContentType: 'application/json',
                            success: function (response) {
                                $("#showHideBandUserDetail").css("display", "block");
                                $('.loadingImageLoader').css('display', 'none');
                                $('#email_error').css('display', 'none');
                                $('#addImagesDiv').find('.pip').remove(); // Remove images span
                                var finalBandUserCount = parseInt($("#finalBandUserCount").text());
                                var bandUserCount = parseInt($("#bandUserCount").text());
                                $("#bandUserFormId")[0].reset();
                                $('.kt-selectpicker').selectpicker('refresh');
                                medicalHistoryDataArr = [];
                                wanderOffEventsHistoryDataArr = [];
                                if (finalBandUserCount > bandUserCount && bandUserCount > 0) {
                                    $("#banduser_otp").val('');
                                    var newValue = bandUserCount + 1;
                                    $("#bandUserCount").text(newValue);
                                    $('html, body').animate({
                                        scrollTop: ($('#bandUserCount').offset().top - 300)
                                    }, 2000);
                                    if (newValue == finalBandUserCount) {
                                        $("#bandUsersSubmitId").val('Confirm Band Users');
                                    }
                                } else {
                                    $("#banduser_otp").val('');
                                    $("#confirmBandUsersModal").modal('show');
                                    $("#confirmBandUsersBtn").trigger("click");
//                                    $("#showHideBandUserDetail").css("display", "none");
//                                    $("#showConfirmBandUserBtn").css("display", "block");
//                                    $("#bandUsersSubmitId").addClass("displayNone");
//                                    $("#confirmBandUsersBtn").removeClass("displayNone");
                                }
                                $('#add_banduser_status').html('<div class="alert alert-success">' + response.message + '</div>');
                                setInterval(function () {
                                    $("#add_banduser_status").fadeOut('slow');
                                    $('#bandUsersSubmitId').attr('disabled', false);
                                }, 6000);
                                $('#medicalHistoryDataTable tbody tr').remove();
                                $('#medicalHistoryDataTable tbody').append('<tr id="noRecordsMedicalHistory"><td colspan="6">No records!!</td></tr>');
                                $('#wanderOffEventHistoryDataTable tbody tr').remove();
                                $('#wanderOffEventHistoryDataTable tbody').append('<tr id="noRecordsWandeOffHistory"><td colspan="7">No records!!</td></tr>');
                            },
                            error: function (error) {
                                $.each(error, function (key, value) {
                                    $('.loadingImageLoader').css('display', 'none');
                                    if (key == 'responseJSON') {
                                        $.each(value.data, function (dkey, dvalue) {
                                            $("#" + dkey + "_error").text(dvalue[0]);
                                            $("#" + dkey + "_error").removeClass('text-muted');
                                            $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                        });
                                        $('html, body').animate({
                                            scrollTop: ($('.error').offset().top - 300)
                                        }, 2000);
                                        $('#below_error_banduser_status').html('<div class="alert alert-danger">There are some validation errors please correct them and re-submit.</div>');
                                    }

                                });

                            }
                        });
                    }, 3000);
                } else {
                    $('#bandUserOTPMsg').html('<div class="alert alert-danger">Incorrect OTP, Please try again!</div>');
                }
            }
        });
    });


    $("#addMedicalHistoryLink").click(function (e) {
        if ($("#medicalHistoryCheckbox").prop('checked') == true) {
            $('#errorMedicalHistory').css('display', 'block');
            $('#errorMedicalHistory').html('<div class="alert alert-danger">To Add the record, Please uncheck the checkbox of "Not Applicable" before.</div>');
            setInterval(function () {
                $("#errorMedicalHistory").fadeOut('slow');
            }, 6000);
            $("#addMedicalHistoryModal").modal('hide');
            $("#addMedicalHistoryLink").prop('disabled', true);
            e.preventDefault();
        } else {
            $('#errorMedicalHistory').css('display', 'none');
            $("#addMedicalHistoryModal").modal('show');
            $("#addMedicalHistoryLink").prop('disabled', false);
        }
    });

    $("#medicalHistoryCheckbox").click(function (e) {
        if ($("#medicalHistoryCheckbox").prop('checked') == true) {
            $("#addMedicalHistoryLink").prop('disabled', true);
        } else {
            $("#addMedicalHistoryLink").prop('disabled', false);
        }
    });

    $("#addwanderOffEventHistoryLink").click(function (e) {
        if ($("#wanderOffHistoryCheckbox").prop('checked') == true) {
            $("#addwanderOffEventHistoryLink").prop('disabled', true);
            $('#errorWanderOffHistory').css('display', 'block');
            $('#errorWanderOffHistory').html('<div class="alert alert-danger">To Add the record, Please uncheck the checkbox of "Not Applicable" before.</div>');
            setInterval(function () {
                $("#errorWanderOffHistory").fadeOut('slow');
            }, 6000);
            $("#addWanderOffEventsHistoryModal").modal('hide');
            $("#addwanderOffEventHistoryLink").prop('disabled', true);
            e.preventDefault();
        } else {
            $('#errorWanderOffHistory').css('display', 'none');
            $("#addWanderOffEventsHistoryModal").modal('show');
            $("#addwanderOffEventHistoryLink").prop('disabled', false);
        }
    });

    $("#wanderOffHistoryCheckbox").click(function (e) {
        if ($("#wanderOffHistoryCheckbox").prop('checked') == true) {
            $("#addwanderOffEventHistoryLink").prop('disabled', true);
        } else {
            $("#addwanderOffEventHistoryLink").prop('disabled', false);
        }
    });
// Save and Save more medical history
    var i = 0;
    $("#saveMedicalHistoryBtn, #saveAndMoreMedialHistoryBtn").click(function (e) {
        var btnFlag = $(this).attr('value');
        $("#checkButtonFlag").val(btnFlag);
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#addMedicalHistoryForm").validate({
            rules: {
                // simple rule, converted to {required:true}
                hospital_registration_no: {noSpace: true},
                ailing_id: {required: true},
                primary_physician: {noSpace: true},
                secondary_physician: {noSpace: true},
                admitted_hospital: {noSpace: true},
                hospital_city: {noSpace: true},
                hospital_preference: {noSpace: true},
                recent_illness: {noSpace: true},
            },
            submitHandler: function (form) {
                i++;
                medicalArrayData = {
                    id: i,
                    hospital_registration_no: $('#hospital_registration_no').val(),
                    ailing_id: $('#ailing_id').val(),
                    ailing_name: $("#ailing_id option:selected").text(),
                    ailing_other: $('#ailing_other').val(),
                    since_when: $('#kt_datepicker_since_when').val(),
                    primary_physician: $('#primary_physician').val(),
                    secondary_physician: $('#secondary_physician').val(),
                    admitted_hospital: $('#admitted_hospital').val(),
                    hospital_city: $('#hospital_city').val(),
                    hospital_preference: $('#hospital_preference').val(),
                    recent_illness: $('#recent_illness').val()
                },
                        medicalHistoryDataArr.push(medicalArrayData);
                if ($("#checkButtonFlag").val() == "Save") {
                    $("#addMedicalHistoryModal").modal('hide');
                }
                resetForm();
                if (medicalHistoryDataArr != '') {

                    $('#medicalHistoryDataTable tbody tr').remove();
                    $.each(medicalHistoryDataArr, function (key, val) {
                        var sinceWhen = "NA";
                        var primaryPhysician = "NA";
                        var secondaryPhysician = "NA";
                        var admittedHospital = "NA";
                        if (val.since_when != "") {
                            sinceWhen = val.since_when;
                        }
                        if (val.primary_physician != "") {
                            primaryPhysician = val.primary_physician;
                        }
                        if (val.secondary_physician != "") {
                            secondaryPhysician = val.secondary_physician;
                        }
                        if (val.admitted_hospital != "") {
                            admittedHospital = val.admitted_hospital;
                        }
                        var medicalHistoryRow = `<tr id="medical_history_row_${key}">
                                    <td>${val.ailing_name}</td>
                                    <td>${sinceWhen}</td>
                                    <td>${primaryPhysician}</td>
                                    <td>${secondaryPhysician}</td>
                                    <td>${admittedHospital}</td>                                     
                                    <td><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editMedicalHistory(${val.id})" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showMedicalHistory(${val.id})"><i class="flaticon2-information"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteMedicalHistory(this)" data-value="${val.id}" data-key="${key}"><i class="flaticon-delete"></i></a></td>
                                    </tr>`;
                        $('#medicalHistoryDataTable').append(medicalHistoryRow);
                    });
                }
            }
        });
    });
// update medical history    
    $("#updateMedicalHistoryBtn").click(function (e) {
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#editMedicalHistoryForm").validate({
            rules: {
                hospital_registration_no: {noSpace: true},
                ailing_id: {required: true},
                primary_physician: {noSpace: true},
                secondary_physician: {noSpace: true},
                admitted_hospital: {noSpace: true},
                hospital_city: {noSpace: true},
                hospital_preference: {noSpace: true},
                recent_illness: {noSpace: true},
            },
            submitHandler: function (form) {
                var formData = $("#editMedicalHistoryForm").serializeFormJSON();
                objIndex = medicalHistoryDataArr.findIndex((obj => obj.id == formData.id));
                delete formData['_token'];
                medicalHistoryDataArr[objIndex] = formData;
                $("#editMedicalHistoryModal").modal("hide");
                if (medicalHistoryDataArr != '') {

                    $('#medicalHistoryDataTable tbody tr').remove();
                    $.each(medicalHistoryDataArr, function (key, val) {
                        var sinceWhen = "NA";
                        var primaryPhysician = "NA";
                        var secondaryPhysician = "NA";
                        var admittedHospital = "NA";
                        if (val.since_when != "") {
                            sinceWhen = val.since_when;
                        }
                        if (val.primary_physician != "") {
                            primaryPhysician = val.primary_physician;
                        }
                        if (val.secondary_physician != "") {
                            secondaryPhysician = val.secondary_physician;
                        }
                        if (val.admitted_hospital != "") {
                            admittedHospital = val.admitted_hospital;
                        }
                        var medicalHistoryRow = `<tr id="medical_history_row_${key}">
                                    <td>${val.ailing_name}</td>
                                    <td>${sinceWhen}</td>
                                    <td>${primaryPhysician}</td>
                                    <td>${secondaryPhysician}</td>
                                    <td>${admittedHospital}</td>                                       
                                    <td><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editMedicalHistory(${val.id})" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showMedicalHistory(${val.id})"><i class="flaticon2-information"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteMedicalHistory(this)" data-value="${val.id}" data-key="${key}"><i class="flaticon-delete"></i></a></td>
                                    </tr>`;
                        $('#medicalHistoryDataTable').append(medicalHistoryRow);
                    });
                }
            }
        });
    });
    function resetForm() {
        $("#addMedicalHistoryForm")[0].reset();
//        var form_name = $(this).attr('data-formname');
//        $("form[name='" + form_name + "']")[0].reset();
//        validator = $("form[name='" + form_name + "']").validate({});
//        console.log('validator >>  ' + validator);
//        validator.resetForm();
//        $(".error").css("display", "none");
    }
    function resetWandeOffEventForm() {
        $("#addWanderOffEventHistoryForm")[0].reset();
    }

    $("#edit_ailing_id").change(function (e) { // Check Other option 
        $("#edit_ailing_name").val($("#edit_ailing_id option:selected").text());
        if ($("#edit_ailing_id option:selected").text() == "Other") { // Need to change dynamic
            $('#editOtherOptionDiv').removeClass("displayNone");
        } else {
            $('#editOtherOptionDiv').addClass("displayNone");
            $("#edit_ailing_other").val('');
        }
    });
    var j = 0;
    $("#saveWanderOffHistoryBtn, #saveAndMoreWanderOffHistoryBtn").click(function (e) {
        var btnWanderOffFlag = $(this).attr('value');
        $("#checkWanderOffButtonFlag").val(btnWanderOffFlag);
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#addWanderOffEventHistoryForm").validate({
            rules: {
                lost_city: {required: true, noSpace: true},
                lost_address: {required: true, noSpace: true},
                lost_date: {required: true},
                found_city: {required: true, noSpace: true},
                found_address: {required: true, noSpace: true},
                found_date: {required: true},
                additional_comments: {noSpace: true},
            },
            submitHandler: function (form) {
                j++;
                wanderOffEventsArrayData = {
                    id: j,
                    lost_city: $('#lost_city').val(),
                    lost_address: $('#lost_address').val(),
                    lost_date: $('#kt_datepicker_lost_date').val(),
                    found_city: $('#found_city').val(),
                    found_address: $('#found_address').val(),
                    found_date: $('#kt_datepicker_found_date').val(),
                    additional_comments: $('#additional_comments').val(),
                },
                        wanderOffEventsHistoryDataArr.push(wanderOffEventsArrayData);
                if ($("#checkWanderOffButtonFlag").val() == "Save") {
                    $("#addWanderOffEventsHistoryModal").modal('hide');
                }
                resetWandeOffEventForm();
                if (wanderOffEventsHistoryDataArr != '') {

                    $('#wanderOffEventHistoryDataTable tbody tr').remove();
                    $.each(wanderOffEventsHistoryDataArr, function (key, val) {
                        var wanderOffEventsHistoryRow = `<tr id="wander_off_event_history_row_${key}">
                                    <td>${val.lost_city}</td>
                                    <td>${val.lost_address}</td>
                                    <td>${val.lost_date}</td>
                                    <td>${val.found_city}</td>
                                    <td>${val.found_address}</td>                                     
                                    <td>${val.found_date}</td>                                     
                                    <td><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editWanderOffEventHistory(${val.id})" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showWanderOffEventHistory(${val.id})"><i class="flaticon2-information"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteWanderOffEventHistory(this)" data-value="${val.id}" data-key="${key}"><i class="flaticon-delete"></i></a></td>
                                    </tr>`;
                        $('#wanderOffEventHistoryDataTable').append(wanderOffEventsHistoryRow);
                    });
                }
            }
        });
    });
// update Wander Off History Btn
    $("#updateWanderOffHistoryBtn").click(function (e) {
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#editWanderOffEventHistoryForm").validate({
            rules: {
                lost_city: {required: true, noSpace: true},
                lost_address: {required: true, noSpace: true},
                lost_date: {required: true},
                found_city: {required: true, noSpace: true},
                found_address: {required: true, noSpace: true},
                found_date: {required: true},
                additional_comments: {noSpace: true},
            },
            submitHandler: function (form) {
                var wanderOffFormData = $("#editWanderOffEventHistoryForm").serializeFormJSON();
                wanderOfdobjIndex = wanderOffEventsHistoryDataArr.findIndex((obj => obj.id == wanderOffFormData.id));
                delete wanderOffFormData['_token'];
                wanderOffEventsHistoryDataArr[wanderOfdobjIndex] = wanderOffFormData;
                $("#editWanderOffEventHistoryModal").modal("hide");
                if (wanderOffEventsHistoryDataArr != '') {
                    $('#wanderOffEventHistoryDataTable tbody tr').remove();
                    $.each(wanderOffEventsHistoryDataArr, function (key, val) {
                        var wanderOffEventsHistoryRow = `<tr id="wander_off_event_history_row_${key}">
                                    <td>${val.lost_city}</td>
                                    <td>${val.lost_address}</td>
                                    <td>${val.lost_date}</td>
                                    <td>${val.found_city}</td>
                                    <td>${val.found_address}</td>                                     
                                    <td>${val.found_date}</td>                                     
                                    <td><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editWanderOffEventHistory(${val.id})" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showWanderOffEventHistory(${val.id})"><i class="flaticon2-information"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteWanderOffEventHistory(this)" data-value="${val.id}" data-key="${key}"><i class="flaticon-delete"></i></a></td>
                                    </tr>`;
                        $('#wanderOffEventHistoryDataTable').append(wanderOffEventsHistoryRow);
                    });
                }
            }
        });
    });
    if (window.File && window.FileList && window.FileReader) {
        $("#files").on("change", function (e) {
            var spanLength = $('#addImagesDiv').find('.pip').length;
            if (spanLength > 0) {
                $("#fileError").css("display", "block");
            } else {
                $("#fileError").css("display", "none");
                var files = e.target.files,
                        filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    var fileReader = new FileReader();
                    fileReader.onload = (function (e) {
                        var file = e.target;
                        $("<span class=\"pip\"><span class=\"remove\"><i class='icon-2x text-dark-50 flaticon-close'></i></span>" +
                                "<input type=\"hidden\"  name=\"latestPhoto[]\" value=\"" + e.target.result + "\"/><img class=\"imageThumb\" src=\"" + e.target.result + "\" name='fileName'/>" +
                                "</span>").appendTo("#addImagesDiv");
                        $(".remove").click(function () {
                            $(this).parent(".pip").remove();
                        });
                    });
                    fileReader.readAsDataURL(f);
                }
            }
        });
    } else {
        alert("Your browser doesn't support to File API")
    }
    $('#confirmBandUsersBtn').on('click', function () {        
        var table = $('#kt_confirm_bandusers');
        var baseUrl = $("#baseUrl").val();
        $("#selected_package_name").val($("#package_name").val());
        // begin first table
        table.DataTable({
            destroy: true,
            scrollY: 'auto',
            searching: false,
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthChange": false,
            ajax: {
                url: baseUrl + '/api/confirm_bandusers_datatable',
                type: 'GET',
                data: function (d) {
                    d.package_name = $("#package_name").val();
                }
            },
            columns: [
                {data: null, render: function (data, type, row) {
                        return '<label class="kt-checkbox" style="margin-bottom: 10px !important;"><input class="band_user_list" value="' + data.id + '" name="bandUserCheckbox" type="checkbox" class="custom-checkbox"><span></span></label>';
                    }, orderable: false, sClass: "classDataTable"},
                {data: 'name'},
                {data: 'email'},
                {data: 'mobile'},
                {data: 'subscriptionAmt'},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editBandUser(' + data.id + ')" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteBandUser(' + data.id + ')"><i class="flaticon-delete"></i></a>';
                    }}
            ],
            "drawCallback": function (settings) {
//                $("#totalAmount").html(settings.json.totalAmount);
                if (settings.json.recordsTotal < 1) {
                    $("#confirmAndGoToPaymentBtn").attr("disabled", true);
                }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\₹,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                        '₹ ' + total
                        );
            }
        });
    }
    );
    
    $("#checkPackageTypeCnt").click(function () {
            $.ajax({
                type: "GET",
                url: "/api/get_package_type_detail_count",
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $("#error_confirm_payment_banduser").fadeOut('slow');
                    if(response.length > 1){
                        $("#selectPackageModal").modal('show');
                        $("#confirmBandUsersModal").modal('hide');                        
                    }else{                        
                        if(response.length >=1){
                            var packageName = response[0].package_name;
                            $("#selected_package_name").val(packageName);
                            $("#selectPackageModal").modal('hide');
                            $("#confirmBandUsersModal").modal('show');
                            var table = $('#kt_confirm_bandusers');
                            var baseUrl = $("#baseUrl").val();

                            // begin first table
                            table.DataTable({
                                destroy: true,
                                scrollY: 'auto',
                                searching: false,
                                responsive: true,
                                paging: true,
                                pagingType: 'full_numbers',
                                processing: true,
                                serverSide: true,
                                "pageLength": 10,
                                "lengthChange": false,
                                ajax: {
                                    url: baseUrl + '/api/confirm_bandusers_datatable',
                                    type: 'GET',
                                    data: function (d) {
                                        d.package_name = packageName;
                                    }
                                },
                                columns: [
                                    {data: null, render: function (data, type, row) {
                                            return '<label class="kt-checkbox" style="margin-bottom: 10px !important;"><input class="band_user_list" value="' + data.id + '" name="bandUserCheckbox" type="checkbox" class="custom-checkbox"><span></span></label>';
                                        }, orderable: false, sClass: "classDataTable"},
                                    {data: 'name'},
                                    {data: 'email'},
                                    {data: 'mobile'},
                                    {data: 'subscriptionAmt'},
                                    {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                                            return '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editBandUser(' + data.id + ')" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteBandUser(' + data.id + ')"><i class="flaticon-delete"></i></a>';
                                        }}
                                ],
                                "drawCallback": function (settings) {
                                    if (settings.json.recordsTotal < 1) {
                                        $("#confirmAndGoToPaymentBtn").attr("disabled", true);                            
                                    }else{
                                        $("#confirmAndGoToPaymentBtn").attr("disabled", false);
                                    }
                                },
                                "footerCallback": function (row, data, start, end, display) {
                                    var api = this.api(), data;

                                    // Remove the formatting to get integer data for summation
                                    var intVal = function (i) {
                                        return typeof i === 'string' ?
                                                i.replace(/[\₹,]/g, '') * 1 :
                                                typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total = api
                                            .column(4)
                                            .data()
                                            .reduce(function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0);

                                    // Update footer
                                    $(api.column(4).footer()).html(
                                            '₹ ' + total
                                            );
                                }
                            });
                        }else{
                            $("#error_confirm_payment_banduser").css('display', 'block');                            
                            setInterval(function () {
                                $("#error_confirm_payment_banduser").fadeOut('slow');
                            }, 6000);
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
});
function showMedicalHistory(id) {
    $.each(medicalHistoryDataArr, function (key, val) {
        if (id == val.id) {
            var hospitalPreference = "NA";
            var hospitalRegNo = "NA";
            var hospitalCity = "NA";
            var sinceWhen = "NA";
            var primaryPhysician = "NA";
            var secondaryPhysician = "NA";
            var admittedHospital = "NA";
            var recentIllness = "NA";
            if (val.hospital_preference != "") {
                hospitalPreference = val.hospital_preference;
            }
            if (val.hospital_city != "") {
                hospitalCity = val.hospital_city;
            }
            if (val.since_when != "" && val.since_when != null) {
                sinceWhen = dateFormatConvert(val.since_when);
            }
            if (val.hospital_registration_no != "" && val.hospital_registration_no != null) {
                hospitalRegNo = val.hospital_registration_no;
            }
            if (val.admitted_hospital != "") {
                admittedHospital = val.admitted_hospital;
            }
            if (val.recent_illness != "") {
                recentIllness = val.recent_illness;
            }
            if (val.primary_physician != "") {
                primaryPhysician = val.primary_physician;
            }
            if (val.secondary_physician != "") {
                secondaryPhysician = val.secondary_physician;
            }
            $("#showMedicalHistoryModal").modal("show");
            $("#txtHospitalRrgistrationNo").text(hospitalRegNo);
            $("#txtCurrentlyAilingFrom").text(val.ailing_name);
            $("#txtSinceWhen").text(sinceWhen);
            $("#txtPrimaryPhysicianName").text(primaryPhysician);
            $("#txtSecondaryPhysicianName").text(secondaryPhysician);
            $("#txtLastadmittedHospitalName").text(admittedHospital);
            $("#txtCity").text(hospitalCity);
            $("#txtHospitalPreference").text(hospitalPreference);
            $("#txtAnyMajorRecentIllness").text(recentIllness);
        } else {
            $("#showMedicalHistoryModal").modal("hide");
        }
    });
}

function editMedicalHistory(id) {
    $.each(medicalHistoryDataArr, function (key, val) {
        if (id == val.id) {
            $("#editMedicalHistoryModal").modal("show");
            if (val.ailing_name == "Other" && val.ailing_other != "") {
                $('#editOtherOptionDiv').removeClass("displayNone");
                $("#edit_ailing_other").val(val.ailing_other);
            } else {
                $('#editOtherOptionDiv').addClass("displayNone");
                $("#edit_ailing_other").val('');
            }
            $("#edit_id").val(val.id);
            $("#edit_hospital_registration_no").val(val.hospital_registration_no);
            $("#edit_ailing_id").val(val.ailing_id);
            $("#edit_ailing_name").val(val.ailing_name);
            $("#edit_kt_datepicker_since_when").val(dateFormatConvert(val.since_when));
            $("#edit_primary_physician").val(val.primary_physician);
            $("#edit_secondary_physician").val(val.secondary_physician);
            $("#edit_admitted_hospital").val(val.admitted_hospital);
            $("#edit_hospital_city").val(val.hospital_city);
            $("#edit_hospital_preference").val(val.hospital_preference);
            $("#edit_recent_illness").val(val.recent_illness);
        } else {
            $("#editMedicalHistoryModal").modal("hide");
        }
    });
}

function deleteMedicalHistory(obj) {
    $("#deleteMedicalWanderOffHistoryModal").modal('show');
    $("#deleteMedicalWanderOffHistoryBtn").attr("onclick", "confirmDeleteMedicalHistory()");
    var idVal = obj.getAttribute('data-value');
    var idKey = obj.getAttribute('data-key');
    $("#hiddenMedicalWanderOffId").val(idVal);
    $("#hiddenMedicalWanderOffKey").val(idKey);
    $("#spanHeadingName").text("Medical History");
    $("#spanDeleteMsg").text("Medical History");
}
function confirmDeleteMedicalHistory() {
    var idVal = $("#hiddenMedicalWanderOffId").val();
    var idKey = $("#hiddenMedicalWanderOffKey").val();
    for (var i = 0; i < medicalHistoryDataArr.length; i++) {
        if (medicalHistoryDataArr[i]['id'] == idVal) {
            medicalHistoryDataArr.splice(i, 1);
        }
    }
    $('#errorMedicalHistory').css('display', 'block');
    $('#errorMedicalHistory').html('<div class="alert alert-danger">Medical History deleted successfully.</div>');
    setInterval(function () {
        $("#errorMedicalHistory").fadeOut('slow');
        $('#errorMedicalHistory').css('display', 'none');
    }, 3000);
    $("#deleteMedicalWanderOffHistoryModal").modal('hide');
    $('#medical_history_row_' + idKey).remove();
    if (medicalHistoryDataArr.length < 1) {
        $('#medicalHistoryDataTable tbody').append('<tr id="noRecordsMedicalHistory"><td colspan="6">No records!!</td></tr>');
    }
}


function showWanderOffEventHistory(id) {
    $.each(wanderOffEventsHistoryDataArr, function (key, val) {
        if (id == val.id) {
            $("#showWanderOffEventHistoryModal").modal("show");
            $("#txtLostCity").text(val.lost_city);
            $("#txtLostLocation").text(val.lost_address);
            $("#txtLostDate").text(dateFormatConvert(val.lost_date));
            $("#txtFoundCity").text(val.found_city);
            $("#txtFoundLocation").text(val.found_address);
            $("#txtFoundDate").text(dateFormatConvert(val.found_date));
            $("#txtAdditionalComments").text(val.additional_comments);
        } else {
            $("#showWanderOffEventHistoryModal").modal("hide");
        }
    });
}

function editWanderOffEventHistory(id) {
    $.each(wanderOffEventsHistoryDataArr, function (key, val) {
        if (id == val.id) {
            $("#editWanderOffEventHistoryModal").modal("show");
            $("#edit_wander_off_id").val(val.id);
            $("#edit_lost_city").val(val.lost_city);
            $("#edit_lost_address").val(val.lost_address);
            $("#edit_kt_datepicker_lost_date").val(dateFormatConvert(val.lost_date));
            $("#edit_found_city").val(val.found_city);
            $("#edit_found_address").val(val.found_address);
            $("#edit_kt_datepicker_found_date").val(dateFormatConvert(val.found_date));
            $("#edit_additional_comments").val(val.additional_comments);
        } else {
            $("#editWanderOffEventHistoryModal").modal("hide");
        }
    });
}

//function deleteWanderOffEventHistory(obj) {
//    var idVal = obj.getAttribute('data-value');
//    for (var i = 0; i < wanderOffEventsHistoryDataArr.length; i++) {
//        if (wanderOffEventsHistoryDataArr[i]['id'] == idVal) {
//            wanderOffEventsHistoryDataArr.splice(i, 1);
//        }
//    }
//    $(obj).closest('tr').remove();
//    if (wanderOffEventsHistoryDataArr.length < 1) {
//        $('#wanderOffEventHistoryDataTable tbody').append('<tr id="noRecordsWandeOffHistory"><td colspan="7">No records!!</td></tr>');
//    }
//}

function deleteWanderOffEventHistory(obj) {
    $("#deleteMedicalWanderOffHistoryModal").modal('show');
    $("#deleteMedicalWanderOffHistoryBtn").attr("onclick", "confirmDeleteWanderOffEventHistory()");
    var idVal = obj.getAttribute('data-value');
    var idKey = obj.getAttribute('data-key');
    $("#hiddenMedicalWanderOffId").val(idVal);
    $("#hiddenMedicalWanderOffKey").val(idKey);
    $("#spanHeadingName").text("Wander Off Event History");
    $("#spanDeleteMsg").text("Wander Off Event History");
}

function confirmDeleteWanderOffEventHistory() {
    var idVal = $("#hiddenMedicalWanderOffId").val();
    var idKey = $("#hiddenMedicalWanderOffKey").val();
    for (var i = 0; i < wanderOffEventsHistoryDataArr.length; i++) {
        if (wanderOffEventsHistoryDataArr[i]['id'] == idVal) {
            wanderOffEventsHistoryDataArr.splice(i, 1);
        }
    }
    $('#errorWanderOffHistory').css('display', 'block');
    $('#errorWanderOffHistory').html('<div class="alert alert-danger">Wander Off Event deleted successfully.</div>');
    setInterval(function () {
        $("#errorWanderOffHistory").fadeOut('slow');
        $('#errorWanderOffHistory').css('display', 'none');
    }, 3000);
    $("#deleteMedicalWanderOffHistoryModal").modal('hide');
    $('#wander_off_event_history_row_' + idKey).remove();
    if (wanderOffEventsHistoryDataArr.length < 1) {
        $('#wanderOffEventHistoryDataTable tbody').append('<tr id="noRecordsWandeOffHistory"><td colspan="7">No records!!</td></tr>');
    }
}

function deleteBandUser(id) {
    $.ajax({
        type: "DELETE",
        url: "/api/bandusers" + '/' + id,
        dataType: "json",
        ContentType: 'application/json',
        success: function (response) {
            $("#successMsgAlert").css('display', 'block');
            $("#successMsg").text(response.message);
            $('#kt_confirm_bandusers').DataTable().ajax.reload();
            setInterval(function () {
                $("#successMsgAlert").fadeOut('slow');
            }, 5000);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function editBandUser(id) {
    window.location.href = "/bandusers/edit/" + id;
}

function dateFormatConvert(date) {
    var dateAr = date.split('-');
    if (dateAr[2]) {
        var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    } else {
        var newDate = dateAr[0];
    }
    return newDate;
}

$(".dobvalidate").on('change', function () {
    var month = $('#month').children("option:selected").val();
    var year = $('#year').children("option:selected").val();
    if (year !== '' && month !== '') {
        $.ajax({
            type: "GET",
            url: '/api/dobvaliidate/' + month + '/' + year,
            dataType: "json",
            ContentType: 'application/json',
            success: function (response) {
                if (response.data !== '') {
                    //empty dropdown
                    var dateFilter = $('#date');
                    dateFilter.selectpicker('val', '');
                    dateFilter.find('option').remove();
                    dateFilter.selectpicker("refresh");
                    //append new days depend on month and year
                    $.each(response.data, function (key, value) {
                        var options = "<option " + "value='" + value + "'>" + value + "";
                        $("#date").append(options);
                    });
                    $('#date').selectpicker('refresh');
                }

            },
            error: function (error) {
                console.log(error);

            }
        });
    }
});


//$('#confirmAndGoToPaymentBtn').on('click', function (evt) {
//    var r = confirm("Work In Progress");
//    if (r == true) {
//        window.location.href = "/bandusers/";
//    }
//    evt.preventDefault();
//});

//function checkPromoCode() {
//    var valid_promo_code = $("#valid_promo_code").val();
//    if (valid_promo_code != "") {
//        $("#errorPromoCode").css('display', 'none');
//        $("#valid_promo_code").css('border', '1px solid gray');
//        $.ajax({
//            type: "POST",
//            url: '/api/check_valid_promo_code',
//            data: 'valid_promo_code=' + valid_promo_code +
//                    '&total_amount=' + $("#totalAmount").html(),
//            dataType: "json",
//            ContentType: 'application/json',
//            success: function (response) {
//                if (response.data.id != null) {
//                    $("#grandTotalAmt").html(response.data.grandTotalAmt);
//                    $("#successPromoCode").html(response.message);
//                    $("#successPromoCode").css('display', 'block');
//                    $("#errorPromoCode").html('');
//                    $("#errorPromoCode").css('display', 'none');
//                    $("#valid_promo_code").css('border', '1px solid gray');
//                    setTimeout(function () {
//                        $("#successPromoCode").css('display', 'none');
//                    }, 3000);
//                } else {
//                    $("#grandTotalAmt").html(response.data.grandTotalAmt);
//                    $("#successPromoCode").html('');
//                    $("#successPromoCode").css('display', 'none');
//                    $("#errorPromoCode").html(response.message);
//                    $("#errorPromoCode").css('display', 'block');
//                    $("#valid_promo_code").css('border', '1px solid red');
//                }
//            }
//        });
//    } else {
//        $("#errorPromoCode").html('This is required.');
//        $("#errorPromoCode").css('display', 'block');
//        $("#valid_promo_code").css('border', '1px solid red');
//    }
//}
// Razorpay integration
var userDetails;
var planAmount;
$(document).ready(function () {
    $("#confirmAndGoToPaymentBtn").click(function (e) {
        var banduserIds = [];
        $("input:checkbox[name=bandUserCheckbox]:checked").each(function () {
            banduserIds.push($(this).val());
        });
        var packageName = $("#selected_package_name").val();
        if (banduserIds.length > 0) {
            var userDetail = JSON.parse(userDetails);
            $('.loadingImageLoader').css('display', 'block');
            $.ajax({
                url: '/api/create_subscription',
                method: 'POST',
                data: 'banduser_ids=' + banduserIds +
                        '&quantity=' + banduserIds.length +
                        '&package_name=' + $("#selected_package_name").val(),
                dataType: "json",
                success: function (result) {
                    $('.loadingImageLoader').css('display', 'none');
                    var finalPlanAmt = (result.planAmount * banduserIds.length);
                    if (result.subscription_id) {
                        var options =
                                {
                                    "key": rzKey, // Enter the Key ID generated from the Dashboard
                                    "subscription_id": result.subscription_id,
                                    "amount": finalPlanAmt, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                                    "currency": "INR",
                                    "name": "MyWanderLoop",
                                    "description": "Securing your freedom",
                                    "image": rzimage,
                                    "handler": function (response) {
                                        console.log('response >> ' + JSON.stringify(response));
                                        $('.loadingImageLoader').css('display', 'block');
                                        setTimeout(function () {
                                            if (response.razorpay_payment_id !== '' && response.razorpay_subscription_id !== '' && response.razorpay_signature !== '') {
                                                var data = {
                                                    "payment_id": response.razorpay_payment_id,
                                                    "subscription_id": response.razorpay_subscription_id,
                                                    "signature": response.razorpay_signature,
                                                    "banduser_ids": banduserIds,
                                                    "package_name": packageName
                                                }

                                                $.ajaxSetup({
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                });
                                                $.ajax({

                                                    url: '/api/save_transaction_details',
                                                    method: 'POST',
                                                    data: data,
                                                    dataType: "json",
                                                    success: function (response) {
                                                        $('.loadingImageLoader').css('display', 'none');
//                                                        console.log('save_payment --> ' + JSON.stringify(response));
                                                        if (response.status == "captured") {
                                                            $('#successMsgAlert').css('display', 'block');
                                                            $('#successMsg').text('You have been successfully subscribed and a payment has been made.');
                                                            setTimeout(function () {
                                                                $("#successMsgAlert").fadeOut('slow');
                                                                window.location.href = '/bandusers';
                                                            }, 3000);
                                                        }
                                                    }
                                                });

                                            } else {
                                                alert("Oops something went wrong!");
                                            }
                                        }, 5000);
                                    },
                                    "prefill": {
                                        "name": userDetail.firstname,
                                        "email": userDetail.email,
                                        "contact": userDetail.mobile
                                    },
                                    "readonly": {
                                        "email": true,
                                        "contact": true
                                    },
                                    "theme": {
                                        "color": "#F6C648"
                                    }
                                };
                        var rzp1 = new Razorpay(options);
                        rzp1.open();
                        e.preventDefault();

                    } else if (result.error) {
                        alert('Oops something went wrong!');
                    } else {
                        alert('Oops something went wrong!');
                    }

                }
            });
        } else {
            alert('Please Select Band Users');
        }
    });

    // multipal checkbox checked
    $('.multiple_checkall').on('click', function () {
        if (this.checked) {
            $('.band_user_list').each(function () {
                this.checked = true;
            });
        } else {
            $('.band_user_list').each(function () {
                this.checked = false;
            });
        }
    });

    $("#pin").on("keydown keyup change", function (e) {
        $('#pin-error').remove();
        var value = $(this).val();
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            if (value.length < 6) {
                //display error message
                $(this).after('<div id="pin-error" class="error invalid-feedback">Please enter at only digits.</div>');
                $("#pin-error").css("display", "block");
                return false;
            }
        } else {
            if (value.length < 6 || value.length > 6) {
                $('#bandUsersSubmitId').attr('disabled', true);
                $(this).after('<div id="pin-error" class="error invalid-feedback">Please enter at least 6 characters.</div>');
                $("#pin-error").css("display", "block");
            } else {
                $('#bandUsersSubmitId').attr('disabled', false);
            }
        }
    });

    $('.modal').on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    });
    
    $("#confirmMakePaymentBandUserBtn").click(function () {
        $("#selectPackageModalID").validate({
            rules: {
                select_package_name: {
                    required: true
                }
            },
            submitHandler: function (form) {                                                   
                var packageName = $('#select_package_name').find("option:selected").text().toLowerCase();
                $("#selectPackageModal").modal('hide');
                $("#confirmBandUsersModal").modal('show');
                var table = $('#kt_confirm_bandusers');
                var baseUrl = $("#baseUrl").val();
                $("#selected_package_name").val(packageName);
                // begin first table
                table.DataTable({
                    destroy: true,
                    scrollY: 'auto',
                    searching: false,
                    responsive: true,
                    paging: true,
                    pagingType: 'full_numbers',
                    processing: true,
                    serverSide: true,
                    "pageLength": 10,
                    "lengthChange": false,
                    ajax: {
                        url: baseUrl + '/api/confirm_bandusers_datatable',
                        type: 'GET',
                        data: function (d) {
                            d.package_name = packageName;
                        }
                    },
                    columns: [
                        {data: null, render: function (data, type, row) {
                                return '<label class="kt-checkbox" style="margin-bottom: 10px !important;"><input class="band_user_list" value="' + data.id + '" name="bandUserCheckbox" type="checkbox" class="custom-checkbox"><span></span></label>';
                            }, orderable: false, sClass: "classDataTable"},
                        {data: 'name'},
                        {data: 'email'},
                        {data: 'mobile'},
                        {data: 'subscriptionAmt'},
                        {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                                return '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editBandUser(' + data.id + ')" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteBandUser(' + data.id + ')"><i class="flaticon-delete"></i></a>';
                            }}
                    ],
                    "drawCallback": function (settings) {
                        if (settings.json.recordsTotal < 1) {
                            $("#confirmAndGoToPaymentBtn").attr("disabled", true);                            
                        }else{
                            $("#confirmAndGoToPaymentBtn").attr("disabled", false);
                        }
                    },
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\₹,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(4)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(4).footer()).html(
                                '₹ ' + total
                                );
                    }
                });
            }
        });
    });
});

function cancelButton(){
      window.location.href = '/bandusers';
}