$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    var atable;
    $(document).ready(function () {

        otable = $('#all_band_users_list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[1, "desc"]],
            ajax: {
                url: '/api/all/bandusers/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'guardian_name', name: 'guardian_name'},
                {data: 'banduser_name', name: 'banduser_name'},
                {data: 'uniqueid', name: 'uniqueid'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                             return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                        }},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && (data == 'active')) {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'inactive' || data == 'pending')) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        console.log('package_name >> ' + data.package_name);
                        if (data != null && (data.aadhaar_verify_status == 'success' && data.package_name == 'standard') || (data.package_name == 'basic')) {
                            return '<a href="/admin/band-users/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a> \n\
                                <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Approve" onclick="changeStatus(' + data.id + ",'" + data.status + "'" + ')"><i class="flaticon2-settings"></i></a>';
                        } else {
                            return '<a href="/admin/band-users/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a>';
                        }
                    }}
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            }

        });

        //Redirect to view page
        $('#all_band_users_list').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 5) {
                var data = otable.row(this).data();
                document.location.href = '/admin/band-users/view/' + data.id;
            }
        });

        atable = $('#active_band_users_list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[1, "desc"]],
            ajax: {
                url: '/api/active/bandusers/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'uniqueid', name: 'uniqueid'},
                {data: 'banduser_name', name: 'banduser_name'},
                {data: 'guardian_name', name: 'guardian_name'},
                {data: 'mobile', name: 'mobile'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                             return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                        }},
                {data: 'payment_date', name: 'payment_date', render: function (d) {
                            return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                        }},
                {data: 'created_at', name: 'created_at', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/admin/band-users/view/' + data.id + '" title="View"><i class="flaticon2-information"></i> View</a>';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users/' + data.id + '" title="Download Pdf"><i class="la la-download" style="font-size:20px;"></i>Download PDF</a>';
                        actionData += '<a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#activateDeactivateUserModal" onclick="inactiveBanduser(' + data.id + ')" title="Inactive"><i class="flaticon-settings-1"></i> Inactive</a>';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users-address/' + data.id + '" title="Download Address"><i class="la la-download" style="font-size:20px;"></i>Download Address</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    }}
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            }

        });

        //Redirect to view page
        $('#active_band_users_list').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 7) {
                var data = atable.row(this).data();
                document.location.href = '/admin/band-users/view/' + data.id;
            }
        });
    });

    $("form[name='pending_approve_band_user']").validate({
        submitHandler: function (form) {
            $('.loadingImageLoader').css('display', 'block');
            var id = $("#userId").val();
            var status = $("#currentStatus").val();

            if (status == 'pending') {
                var updateStatus = 'active';
            }
            $.ajax({
                type: "PUT",
                url: '/api/changeBandUserStatus/' + id,
                dataType: "json",
                data: {
                    status: updateStatus
                },
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#pendingApproveBandUserFormId").get(0).reset();
                    $('#approvedBandUserModal').modal('toggle');
                    $('#all_band_users_list').DataTable().ajax.reload();
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                    }, 3000);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

    $("form[name='activate_deactivate_user']").validate({
        submitHandler: function (form) {
            $('.loadingImageLoader').css('display', 'block');
            var id = $("#userId").val();
            var status = $("#currentStatus").val();

            var updateStatus = 'inactive';
            $.ajax({
                type: "PUT",
                url: '/api/inactiveBandUserStatus/' + id,
                dataType: "json",
                data: {
                    status: updateStatus
                },
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#activateDeactivateUserFormId").get(0).reset();
                    $('#activateDeactivateUserModal').modal('toggle');
                    $('#active_band_users_list').DataTable().ajax.reload();
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                    }, 3000);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });



});
function changeStatus(id, status) {

    if (status == 'pending') {
        $("#userId").val(id);
        $("#currentStatus").val(status);
        $("#approvedBandUserModal").modal('show');
        $("#pendingApproveBandUserContent").text('Are you sure you want to Approve this Band User ?');
        $('#pendingApproveBandUserModalLabel').text('Approve Band User');

    }

}
function inactiveBanduser(id) {
//    alert(status);
    $("#userId").val(id);
    $("#currentStatus").val("inactive");

    $('#activateDeactivateUserModalLabel').text('Inactive Band User');
    $('#activateDeactivateContent').text('If you marked band users is an inactive then this action cannot be undone. Do you want to continue?');

}