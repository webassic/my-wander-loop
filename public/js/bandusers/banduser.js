/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var KTDatatablesDataSourceAjaxClient = function () {

    var initTable1 = function () {
        var baseUrl = $("#baseUrl").val();
        // begin first table
        var table;
        table = $('#kt_table_banduser').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[1, "desc"]],
            ajax: {
                url: baseUrl + '/api/bandusers_datatable',
                data: {
                },
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'uniqueid', name: 'uniqueid', render: function (data, type, row) {
                        if (data != null && (data != '')) {
                            return  data;
                        }
                        return 'NA';
                    }
                },
                {data: 'name'},
                {data: 'email'},
                {data: 'mobile'},
                {data: 'package_name'},
                {data: 'package_expiry_date', name: 'package_expiry_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '-';
                    }},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && (data == 'active')) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'inactive' || data == 'pending')) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return  '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        }
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],
            columnDefs: [
                {
                    "targets": 1,
                    "data": 'name',
                    "render": function (data, type, row) {
                        return row.firstname + ' ' + row.lastname;
                    }
                },
                {
                    "targets": 4,
                    "data": 'package_name',
                    "render": function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }
                },
                {
                    targets: -1,
                    title: 'Edit, View, Delete,<br/> Buy New Band',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/bandusers/edit/' + row.id + '"><i class="la la-edit"></i> Edit Details</a>';
                        actionData += ' <a class="dropdown-item" href="/bandusers/view/' + row.id + '"><i class="flaticon2-information"></i> View Details</a>';
                        if (row.status != 'active') {
                            actionData += ' <a href="javascript:void(0);" data-toggle="modal" data-toggle="modal" data-target="#deleteBandUserModal" class="dropdown-item" title="Delete" onclick="deleteBandUser(' + row.id + ')"><i class="la la-trash"></i> Delete</a>';
                        }
                        if (row.status == 'active') {
                            actionData += '<a href="javascript:void(0);" data-toggle="modal" data-toggle="modal" data-target="#buyNewBandModal" class="dropdown-item" title="Buy New Band" onclick="buyNewBandFunction(' + row.id + ')"><i class="fas fa-shopping-cart"></i>Buy New Band</a>';
                        }
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            },
        });

        //Redirect to view page
        $('#kt_table_banduser').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 7) {
                var data = table.row(this).data();
                document.location.href = '/bandusers/view/' + data.id;
            }
        });

    };

    return {

        //main function to initiate the module
        init: function () {
            initTable1();
        }
    };
}();

jQuery(document).ready(function () {
    KTDatatablesDataSourceAjaxClient.init();
    $("#cancelBuyNewBandsBtn").click(function () {

        $("#buyNewBandsId")[0].reset();
    });
    $("#buyNewBandsBtnId").click(function (e) {
        $("#buyNewBandsId").validate({
            rules: {
                // simple rule, converted to {required:true}
                bands_number: {
                    required: true,
                },
                total_amount: {
                    required: true,
                },

            },
            messages: {
                total_price: "Total price cannot be zero."
            },
            submitHandler: function (form) {
                $('.loadingImageLoader').css('display', 'block');
                var data = $("#buyNewBandsId").serializeFormJSON();
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: '/api/buy_new_band',
                    dataType: "json",
                    ContentType: 'application/json',
                    data: data,
                    success: function (response) {
                        $('.loadingImageLoader').css('display', 'none');
                        $("#buyNewBandModal").modal('hide');
                        $("#buyNewBandsId")[0].reset();
                        $('html, body').animate({
                            scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
                        }, 2000);
                        console.log(response.data);
                        if (response.data.hasOwnProperty('rzorder_id')) {

                            razorPaycheckout(response.data.total_amount, response.data.rzorder_id);

                        }

//                        $.each(response.data, function( key, value ) {
//                            if(key=='total_amount'){
//                                total_amount =value;
//                            }
//                            if(key == 'rzorder_id'){
//                                razorPaycheckout(value,);
//                            }
//                        });
//                        $('#success-alert').css('display', 'block');
//                        $('#success-msg').text(response.message);
//                        setInterval(function () {
//                            $("#success-alert").fadeOut('slow');
//                           
//                        }, 3000);

                    },
                    error: function (error) {
                        $('.loadingImageLoader').css('display', 'none');
                        console.log(error);

                        $.each(error.responseJSON, function (key, value) {
                            console.log(key + ">>");
                            console.log(value + " >>");
                            if (key == 'data') {
                                $.each(value, function (innerkey, innervalue) {
                                    if (innerkey == 'rzerror') {
                                        $("#buyNewBandModal").modal('hide');
                                        $("#buyNewBandsId")[0].reset();
                                        $('#danger-alert').css('display', 'block');
                                        $('#error-msg').text(innervalue);

                                        setInterval(function () {
                                            $("#danger-alert").fadeOut('slow');

                                        }, 3000);
                                    }
                                });

                            }
//                            

                        });


                    }
                });
            }
        });
    });
    
     $("#package_name").change(function () {
            $('.loadingImageLoader').css('display', 'block');
            var planID = $('#package_name').find("option:selected").val();            
            if(planID !=""){
            $.ajax({
                type: "POST",
                url: "/api/get_package_details",
                data: 'planID='+planID,
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#packageInformation").css('display', 'block');
                    $("#packageDescription").html(response.description);
                    $("#packageName").html(response.name);
                    $("#packageAmount").html(response.subscriptionAmt);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }else{
            $('.loadingImageLoader').css('display', 'none');
            $("#packageInformation").css('display', 'none');
        }
        });
        
        
     
    
});

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    $(document).ready(function () {
        var guardiansId = $('#guardiansId').val();

        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        if (guardiansId != '') {
            otable = $('#band_users-list').DataTable({
                responsive: true,
                paging: true,
                pagingType: 'full_numbers',
                processing: true,
                serverSide: true,
                "pageLength": 10,
                "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                "aaSorting": [[0, "asc"]],
                ajax: {
                    url: '/api/bandusers/listings/' + guardiansId,
                    data: function (d) {
                    }
                },
                "language": {
                    "paginate": {
                        "first": "First page",
                        "last": "Last page",
                        "previous": "Previous ",
                        "next": "Next "
                    }
                },
                oLanguage: {
                    sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
                },
                columns: [
                    {data: 'uniqueid', name: 'uniqueid'},
                    {data: 'firstname', name: 'firstname'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'relationship_with_applicant', name: 'relationship_with_applicant'},                   
                    {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                             return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                        }},
                    {data: 'status', name: 'status', render: function (data, type, row) {
                            if (data != null && (data == 'active')) {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                            } else if (data != null && (data == 'inactive' || data == 'pending')) {
                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                            } else {
                                return 'NA';
                            }
                        }},
                    {data: 'created_at', name: 'created_at',
                        render: function (d) {
                            return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                        }},
                    {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                            return '<a href="/admin/band-users/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a>';
                        }}
                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    name = aData.firstname != '' ? aData.firstname + " " + aData.lastname : '';
                    $('td:eq(1)', nRow).html(name);
                },
                fnInitComplete: function (data, res) {
                    if (res.hasOwnProperty("errors")) {
                        $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                        setTimeout(function () {
                            $('#authenticate-error-message').html('');
                        }, 5000);
                    }
                    $('#opacitylow').css({'opacity': 2.0});
                    $('.loader').css({'opacity': 0.5});
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).css('cursor', 'pointer');
                }
            });

        }

        //Redirect to view page
        $('#band_users-list').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 8) {
                var data = otable.row(this).data();
                document.location.href = '/admin/band-users/view/' + data.id;
            }
        });

        $('#listing_status').delegate('#status', 'change', function () {
            otable.columns(5).search(this.value).draw();
        });
        $('#band_users-list_filter input').prop('title', 'In case you are trying to search based on a Date, then use YYYY-MM-DD format.');
    });
});

function deleteBandUser(id) {
    $("#bandUserId").val(id);
}

function deleteBandUserDetails() {
    var id = $("#bandUserId").val();
    $.ajax({
        type: "DELETE",
        url: "/api/bandusers" + '/' + id,
        dataType: "json",
        ContentType: 'application/json',
        success: function (response) {
            $("#successMsgAlert").css('display', 'block');
            $("#successMsg").text(response.message);
            $('#kt_table_banduser').DataTable().ajax.reload();
            setInterval(function () {
                $("#successMsgAlert").fadeOut('slow');
            }, 5000);
        },
        error: function (error) {
            console.log(error);
        }
    });
}
//function calculatePrice() {
//    var number = $("#bands_number").val();
//    var price_per_band = $("#price_per_band").val();
//    var total_amount = number * price_per_band;
//    $("#total_amount").val(total_amount);
//}

function buyNewBandFunction(id) {
    $("#banduser_id").val(id);
}
//alert(rzname);

function razorPaycheckout(totalAmt, orderId) {
    var userDetail = JSON.parse(userDetails);
    var name = userDetail.firstname + ' ' + userDetail.lastname;
    var email = userDetail.email;
    var mobile = userDetail.mobile;

    var options = {
        "key": rzKey, // Enter the Key ID generated from the Dashboard
        "amount": totalAmt, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "INR",
        "name": "MyWanderLoop",
        "description": "Securing your freedom",
        "image": rzimage,
        "order_id": orderId,
        "handler": function (response) {
            saveTransactionalDetails(response);
        },
        "prefill": {
            "name": name,
            "email": email,
            "contact": mobile
        },
        "notes": {
            "address": "Razorpay Corporate Office"
        },
        "theme": {
            "color": "#F6C648"
        }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();

}

function saveTransactionalDetails(response) {

    var rzorder_id = response.razorpay_order_id;
    var rzpayment_id = response.razorpay_payment_id;
    var rzsignature = response.razorpay_signature;
    $('.loadingImageLoader').css('display', 'block');
    $.ajax({
        type: "PUT",
        url: '/api/band_orders/razorpay_callback',
        dataType: "json",
        ContentType: 'application/json',
        data: {
            "rzorder_id": rzorder_id,
            "rzpayment_id": rzpayment_id,
            "rzsignature": rzsignature
        },
        success: function (response) {
            $('.loadingImageLoader').css('display', 'none');
            $('html, body').animate({
                scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
            }, 2000);
            $('#success-alert').css('display', 'block');
            $('#success-msg').text(response.message);
            setInterval(function () {
                $("#success-alert").fadeOut('slow');

            }, 3000);
            $('#kt_table_banduser').DataTable().ajax.reload();


        },
        error: function (error) {
            console.log(error);
            $('.loadingImageLoader').css('display', 'none');
        }
    });
}