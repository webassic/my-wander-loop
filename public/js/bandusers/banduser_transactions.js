/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var KTDatatablesDataSourceAjaxClient = function () {
    var initTable1 = function () {
        var baseUrl = $("#baseUrl").val();
        // begin first table
        var table;
        table = $('#banduser_transactions').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[4, "desc"]],
            ajax: {
                url: baseUrl + '/api/transaction_datatable',
                type: 'GET',
                data: function (d) {
                },
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            columns: [
                {data: 'name'},
                {data: 'payment_date', name: 'payment_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'payment_id'},
                {data: 'start_date', name: 'start_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'end_date', name: 'end_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'invoice_number'},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && (data == 'inactive')) {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'cancel')) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'active')) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],
            columnDefs: [
                {
                    "targets": 0,
                    "data": 'name',
                    "render": function (data, type, row) {
                        return row.firstname + ' ' + row.lastname;
                    }
                },
                {
                    targets: -1,
                    title: 'Cancel Subscriptions/<br/>Download Invoice',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        if (row.status == 'active') {
                            actionData += '<a class="dropdown-item" href="/transactions/cancel_subscription/' + row.id + '"><i class="la la-trash"></i> Cancel Subscription</a>';
                        }
                        actionData += ' <a class="dropdown-item" href="/transactions/download_invoice/' + row.guardian_subscription_id + '/'+row.payment_id+'"><i class="la la-download"></i> Download Invoice</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ]
        });
    };

    var initTable2 = function () {
        var baseUrl = $("#baseUrl").val();
        // begin first table
        var table;
        table = $('#admin_banduser_transactions').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[4, "desc"]],
            ajax: {
                url: baseUrl + '/api/admin_transaction_datatable',
                type: 'GET',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            columns: [
                {data: 'guardian_name'},
                {data: 'name'},
                {data: 'payment_date', name: 'payment_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'payment_id'},
                {data: 'start_date', name: 'start_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'end_date', name: 'end_date', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'invoice_number'},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && (data == 'inactive')) {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'cancel')) {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && (data == 'active')) {
                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],
            columnDefs: [
                {
                    "targets": 0,
                    "data": 'guardian_name',
                    "render": function (data, type, row) {
                        return row.ufirstname + ' ' + row.ulastname;
                    }
                },
                {
                    "targets": 1,
                    "data": 'name',
                    "render": function (data, type, row) {
                        return row.firstname + ' ' + row.lastname;
                    }
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += ' <a class="dropdown-item" href="/admin-transactions/download_invoice/' + row.guardian_subscription_id + '/'+row.payment_id+'"><i class="la la-download"></i> Download Invoice</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ],
        });
    };

    return {

        //main function to initiate the module
        init: function () {
            initTable1();
            initTable2();
        }
    };
}();

jQuery(document).ready(function () {
    KTDatatablesDataSourceAjaxClient.init();
});