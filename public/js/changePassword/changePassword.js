$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.model-close').click(function (e) {
        var form_name = $(this).attr('data-formname');
        $("form[name='" + form_name + "']")[0].reset();
        validator = $("form[name='" + form_name + "']").validate({});
        validator.resetForm();
        $(".error").css("display", "none");
    });
    $('.close').click(function () {
        var form_name = $(this).attr('data-formname');
        $("form[name='" + form_name + "']")[0].reset();
        validator = $("form[name='" + form_name + "']").validate({});
        validator.resetForm();
        $(".error").css("display", "none");
    });
    $(document).ready(function () {
        $.validator.addMethod("regx", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "Please enter 8 characters including only Alphabets and Numbers.");
        $("form[name='change_password']").validate({
            rules: {
                new_password: {
                    required: true,
                    maxlength: 10,
                    minlength: 8,
                    regx: /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/
                },
                new_confirm_password: {
                    equalTo: "#new_password",
                    maxlength: 10,
                    minlength: 8,
                    regx: /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/
                }
            },
            messages: {
                new_password: {
                    required: "password is required"
                }
            },
            submitHandler: function (form) {
                var data = $('#change_password').serializeArray();
                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });
                ajaxIcondisplay('changePasswordFormIcon', true);
                $.ajax({
                    type: "POST",
                    url: '/api/password/update',
                    dataType: "json",
                    data: JSON.stringify(item),
                    success: function (returnData) {
                        ajaxIcondisplay('changePasswordFormIcon', false);
                        if (returnData.status == 0) {
                            resetformdata('change_password');
                            $('#change_password_status').html('<div class="alert alert-success">' + returnData.message + '</div>');
                            setTimeout(function () {
                                $('#changePasswordModal').modal('hide');
                                $('#change_password_status').html('');
                            }, 8000);
                            window.location.href = "/logout";
                        } else if (returnData.status == 1) {
                            resetformdata('change_password');
                            $('#change_password_status').html('<div class="alert alert-danger">' + returnData.message + '</div>');
                            setTimeout(function () {
                                $('#change_password_status').html('');
                            }, 4000);
                        } else {
                            resetformdata('change_password');
                            $('#change_password_status').html('<div class="alert alert-danger">' + returnData.message + '</div>');
                            setTimeout(function () {
                                $('#change_password_status').html('');
                            }, 4000);
                        }
                    }
                }).fail(function (jqXHR, textStatus, xhr) {
                    ajaxIcondisplay('changePasswordFormIcon', false);
                    resetformdata('change_password');
                    $('#change_password_status').html('<div class="alert alert-danger">' + jqXHR.responseJSON.error + '</div>');
                    setTimeout(function () {
                        $('#changePasswordModal').modal('hide');
                        $('#change_password_status').html('');
                    }, 4000);
                });
            }
        });
    });
    function ajaxIcondisplay(divid, status) {
        if (status) {
            $("#" + divid).show();
        } else {
            $("#" + divid).hide();
        }
    }

    function resetformdata(form_name) {
        $('#' + form_name).each(function () {
            this.reset();
        });
    }
});