// Class definition

var KTBootstrapSelect = function () {

    // Private functions
    var demos = function () {
        // minimum setup
        $('.kt-selectpicker').selectpicker();
    }

    return {
        // public functions
        init: function () {
            demos();
        }
    };
}();


jQuery(document).ready(function () {

    KTBootstrapSelect.init();

    $("#guardianSubmitId").click(function () {
        
           var date = $('#date').children("option:selected").val();
           var month = $('#month').children("option:selected").val();
           var year = $('#year').children("option:selected").val();
           if(date=='' && month =='' && year ==''){
               $("#validatedob").text('This field is required');
              
           }else if(date=='' || month =='' || year ==''){
               $("#validatedob").text('Please enter valid format');
           }else{
              if(month=='Feb' && date > 29){
                  $("#validatedob").text('Date cannot greater than 29');
              }else{
                  
               $("#validatedob").text('');
              }
           }
        
        $("#guardianFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                title: {
                    required: true,
                    noSpace: true

                },
                firstname: {
                    required: true,
                    noSpace: true,
                    noSpaceForName: true
                },
                lastname: {
                    required: true,
                    noSpace: true,
                    noSpaceForName: true
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15,
                    noSpace: true
                },
                address: {
                    required: true,
                    noSpace: true
                },
                city: {
                    required: true,
                    noSpace: true
                },
                locality: {
                    required: true,
                    noSpace: true
                },
                state: {
                    required: true,
                    noSpace: true
                },
//                dob: {
//                    required: true,
//
//                },
                pin: {
                    required: true,
                    noSpace: true,
//                    minlength: 6,
                    maxlength: 10

                },
                landline: {
//                      noSpace:true,
                      digits: true,
                    minlength: 6,
                    maxlength: 15
                },
            },
            submitHandler: function (form) {
                var url = $("#guardianFormId").attr('action');
                var id = $("#gid").val();
                $('#guardianSubmitId').attr('disabled', true);
                $.ajax({
                    type: "PUT",
                    url: url + '/' + id,
                    data: $("#guardianFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        localStorage.setItem("successMessage", response.message);
                        location.href = '/guardians';

                    },
                    error: function (error) {
                        console.log(error);
                        $.each(error, function (key, value) {
                            $('#guardianSubmitId').attr('disabled', false);
                            if (key == 'responseJSON') {

                                console.log(value.success);
                                $.each(value.data, function (dkey, dvalue) {
                                    console.log('Key' + dkey);
                                    console.log('Value' + dvalue[0]);

                                    $("#" + dkey + "error").text(dvalue[0]);
                                    $("#" + dkey + "error").css('display', 'block');


                                });
                            }

                        });

                    }
                });
            }
        });
    });
    $("#institutionSubmitId").click(function () {

        $("#institutionFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                address: {
                    required: true,
                    noSpace: true
                },
                city: {
                    required: true,
                    noSpace: true
                },
                 mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15,
                    noSpace: true
                },
                locality: {
                    required: true,
                    noSpace: true
                },
                state: {
                    required: true,
                    noSpace: true
                },
                landline: {
//                      noSpace:true,
                    digits: true,
                    minlength: 6,
                    maxlength: 15
                },
                pin: {
                    required: true,
                    noSpace: true,
//                    minlength: 6,
                    maxlength: 10
                }
            },
            submitHandler: function (form) {
                var url = $("#institutionFormId").attr('action');
                var id = $("#gid").val();
                $('#institutionSubmitId').attr('disabled', true);
                $.ajax({
                    type: "PUT",
                    url: url + '/' + id,
                    data: $("#institutionFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        localStorage.setItem("successMessage", response.message);
                        location.href = '/guardians';
                    },
                    error: function (error) {
                        console.log(error);
                        $('#institutionSubmitId').attr('disabled', false);
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {

                                console.log(value.success);
                                $.each(value.data, function (dkey, dvalue) {
                                    console.log('Key' + dkey);
                                    console.log('Value' + dvalue[0]);
                                    if (dkey == 'dob') {
                                        $("#kt_datepicker_guardianerror").text(dvalue[0]);
                                        $("#kt_datepicker_guardianerror").css('display', 'block');
                                    } else {
                                        $("#" + dkey + "error").text(dvalue[0]);
                                        $("#" + dkey + "error").css('display', 'block');
                                    }

                                });
                            }

                        });

                    }
                });
            }
        });
    });
    $("#editIndividualSubmitId").click(function () {
        
        $("#editIndividualFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15,
                    noSpace: true
                },
                address: {
                    required: true,
                    noSpace: true
                },
                city: {
                    required: true,
                    noSpace: true
                },
                locality: {
                    required: true,
                    noSpace: true
                },
                state: {
                    required: true,
                    noSpace: true
                },
                pin: {
                    required: true,
                    noSpace: true,
//                    minlength: 6,
                    maxlength: 10

                },
                landline: {
//                      noSpace:true,
                      digits: true,
                    minlength: 6,
                    maxlength: 15
                },
            },
            submitHandler: function (form) {
                var url = $("#editIndividualFormId").attr('action');
                var id = $("#gid").val();
                $('#editIndividualSubmitId').attr('disabled', true);
                $.ajax({
                    type: "PUT",
                    url: url + '/' + id,
                    data: $("#editIndividualFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        localStorage.setItem("successMessage", response.message);
                        location.href = '/guardians';

                    },
                    error: function (error) {
                        console.log(error);
                        $.each(error, function (key, value) {
                            $('#editIndividualSubmitId').attr('disabled', false);
                            if (key == 'responseJSON') {

                                console.log(value.success);
                                $.each(value.data, function (dkey, dvalue) {
                                    console.log('Key' + dkey);
                                    console.log('Value' + dvalue[0]);

                                    $("#" + dkey + "error").text(dvalue[0]);
                                    $("#" + dkey + "error").css('display', 'block');


                                });
                            }

                        });

                    }
                });
            }
        });
    });
    $("#editInstitutionSubmitId").click(function () {

        $("#editInstitutionFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                address: {
                    required: true,
                    noSpace: true
                },
                city: {
                    required: true,
                    noSpace: true
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15,
                    noSpace: true
                },
                locality: {
                    required: true,
                    noSpace: true
                },
                state: {
                    required: true,
                    noSpace: true
                },
                landline: {
//                      noSpace:true,
                    digits: true,
                    minlength: 6,
                    maxlength: 15
                },
                pin: {
                    required: true,
                    noSpace: true,
//                    minlength: 6,
                    maxlength: 10
                }
            },
            submitHandler: function (form) {
                var url = $("#editInstitutionFormId").attr('action');
                var id = $("#gid").val();
                $('#editInstitutionSubmitId').attr('disabled', true);
                $.ajax({
                    type: "PUT",
                    url: url + '/' + id,
                    data: $("#editInstitutionFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        localStorage.setItem("successMessage", response.message);
                        location.href = '/guardians';
                    },
                    error: function (error) {
                        console.log(error);
                        $('#editInstitutionSubmitId').attr('disabled', false);
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {

                                console.log(value.success);
                                $.each(value.data, function (dkey, dvalue) {
                                    console.log('Key' + dkey);
                                    console.log('Value' + dvalue[0]);
                                    if (dkey == 'dob') {
                                        $("#kt_datepicker_guardianerror").text(dvalue[0]);
                                        $("#kt_datepicker_guardianerror").css('display', 'block');
                                    } else {
                                        $("#" + dkey + "error").text(dvalue[0]);
                                        $("#" + dkey + "error").css('display', 'block');
                                    }

                                });
                            }

                        });

                    }
                });
            }
        });
    });

    $("#guardianCancelId").click(function () {
        location.href = $("#guardianCancelId").attr('data-id');
    });
    $("#guardianUpdateCancelId").click(function () {
        location.href = $("#guardianUpdateCancelId").attr('data-id');
    });
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value == '' || value.trim().length != 0;
    }, "Please enter valid data.");
    jQuery.validator.addMethod("noSpaceForName", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No space allowed");
   
        
});


 $(".dobvalidate").on('change',function(){
        var month = $('#month').children("option:selected").val();
           var year = $('#year').children("option:selected").val();
           if(year!=='' && month!==''){
               $.ajax({
                    type: "GET",
                    url:  '/api/dobvaliidate/' + month +'/'+year,
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        console.log(response);
                        if(response.data!==''){
                            //empty dropdown
                           var dateFilter = $('#date');
                            dateFilter.selectpicker('val', '');
                            dateFilter.find('option').remove();
                            dateFilter.selectpicker("refresh");
                            //append new days depend on month and year
                            $.each(response.data, function (key, value) {
                                var options = "<option " + "value='" + value + "'>" + value + "";
                                $("#date").append(options);
                            });
                            $('#date').selectpicker('refresh');
                        }

                    },
                    error: function (error) {
                        console.log(error);
                        
                    }
                });
           }
    });