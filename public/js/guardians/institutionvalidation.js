/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function() {        
        $("#institutionSubmitId").click(function(){
            $("#institutionFormId").validate({
                rules: {
                    name: "required",
                    locality: "required",
                    city: "required",
                    state: "required",
                    address:"required",
                    pin: {
                      required: true,
                      digits: true,
                      minlength:6,
                      maxlength:6,
                    }
                },
                submitHandler: function(form) {
                    var url = $("#institutionFormId").attr('action');
                    
                    $.ajax({
                        type: "POST",
                        url: url,
                        data:$("#institutionFormId").serialize(),
                        dataType: "json",
                        ContentType :'application/json',
                        success: function (response) {
                            $("#success-alert").css('display','block');
                            $("#success-msg").text(response.message);
                            $("#institutionFormId").get(0).reset();
                            setInterval(function(){location.reload(); }, 3000);
                        },
                        error: function (error) {
                            console.log(error);
                            $.each( error, function( key, value ) {
                                if(key=='responseJSON'){
                                    console.log(value.success);
                                     $.each( value.data, function( dkey, dvalue ) {
                                         console.log('Key ' + dkey);
                                         console.log('Value ' + dvalue[0]);
                                       
                                            $("#"+dkey+"error").text(dvalue[0]);
                                            $("#"+dkey+"error").css('display','block'); 
                                       
                                        
                                     });
                                }
                                   
                            });
                                
                        }
                    });
                }
            });
        });
        $("#institutionUpdateSubmitId").click(function(){
            var url = $("#institutionUpdateFormId").attr('action');
                   
            $("#institutionUpdateFormId").validate({
                rules: {
                    uname: "required",
                    ulocality: "required",
                    ucity: "required",
                    ustate: "required",
                    uaddress:"required",
                    upin: {
                       required: true,
                       digits: true,
                       minlength:6,
                       maxlength:6,
                     }
                },
                submitHandler: function(form) {
                    var url = $("#institutionUpdateFormId").attr('action');
                    var id = $("#uid").val();
                   
                    $.ajax({
                        type: "PUT",
                        url: url+'/'+id,
                        data:$("#institutionUpdateFormId").serialize(),
                        dataType: "json",
                        ContentType :'application/json',
                        success: function (response) {
                          $("#success-alert").css('display','block');
                            $("#success-msg").text(response.message);
                           $(".error").css('display','none'); 

                        },
                        error: function (error) {
                            console.log(error);
                            $.each( error, function( key, value ) {
                                if(key=='responseJSON'){
                                    console.log(value.success);
                                     $.each( value.data, function( dkey, dvalue ) {
                                         console.log('Key' + dkey);
                                         console.log('Value' + dvalue[0]);
                                       
                                            $("#u"+dkey+"error").text(dvalue[0]);
                                            $("#u"+dkey+"error").css('display','block'); 
                                       
                                        
                                     });
                                }
                                   
                            });
                                
                        }
                    });
                }
            });
        });
       if($("#exists").val()!=0){
            var userId = $("#userId").val();
            $.ajax({
            type: "GET",
            url: '/api/institutions/'+ userId,
            dataType: "json",
            ContentType :'application/json',
            success: function (response) {
              // location.reload();
//              console.log(response);
              $.each(response, function( key, value ) {
                    if(key=='data'){
                        $.each( value[0], function( dkey, dvalue ) {
                               $("#u"+dkey).val(dvalue);
                        });
                    }

                });
            },
            error: function (error) {
//                

            }
        });
        }
        
});