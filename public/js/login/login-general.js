"use strict";

// Class Definition
var KTLoginGeneral = function () {

    var login = $('#kt_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">' + msg + '</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    var handleSignInFormSubmit = function () {
        $('#kt_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $("#signInFormId");
            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                submitHandler: function (form) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    form.submit();
                }
            });
        });
    }

    var handleSignUpFormSubmit = function () {
        $('#kt_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');
            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true
                    },
                    agree: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        form.clearForm();
                        form.validate().resetForm();

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
                    }, 2000);
                }
            });
        });
    }

    var handleForgotFormSubmit = function () {
        $('#kt_login_forgot_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Cool! Password recovery instruction has been sent to your email.');
                    }, 2000);
                }
            });
        });
    }

    // Public Functions
    return {
        // public functions
        init: function () {
//            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgotFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function () {
    KTLoginGeneral.init();
    var baseUrl = $("#baseUrl").val();
    $("#registerFormSubmitId").click(function () {
        $.validator.addMethod("regx", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "Please enter 8 characters and including only Alphabets and Numbers.");
        $("#registerFormId").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                email: {
                    required: true,
                    email: true,
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:15
                },
                password: {
                    required: true,
//                    regx: /^(?=.*\d)(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*?]{8,25}$/
                    regx: /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/
                },
                confirmed_password: {
                    equalTo: "#password"
                },
                hiddenRecaptcha: {
                    required: function () {
                        var response = grecaptcha.getResponse();
                        if (response.length == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                agree: "required",

            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                   
                }
            },
            submitHandler: function (form) {
                $('.loadingImageLoader').css('display', 'block');
                var url = $("#registerFormId").attr('action');
                 $('#registerFormSubmitId').attr('disabled',true);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#registerFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('.loadingImageLoader').css('display', 'none');
                         $('#registerFormSubmitId').attr('disabled',false);
                        $('.servererror').hide();
                        $("#success-alert").css('display', 'block');
                        grecaptcha.reset();
                        $("html, body").animate( 
                                             { scrollTop: "0" }, 1000); 
                        $("#success-msg").html('You have successfully signed up. Please check your email for a verification link to complete your Registration.<a href="/login" style="color:#fff;font-weight:bold;text-decoration:underline;">Click Here</a> to go to Login to your account');
                        $("#registerFormId").get(0).reset();

                    },
                    error: function (error) {   
                        $('.loadingImageLoader').css('display', 'none');
                          $('#registerFormSubmitId').attr('disabled',false);
                          $("html, body").animate( 
                                             { scrollTop: "0" }, 1000); 
                          
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    console.log('Key : ' + dkey);
                                    console.log('Value :' + dvalue[0]);

                                    $("#" + dkey + "error").text(dvalue[0]);
                                    $("#" + dkey + "error").css('display', 'block');
                                    $("#" + dkey + "error").css('color', 'red');


                                });
                            }
                            
                        });
                        $('.servererror').delay(5000).fadeOut();

                    }
                });
            }
        });
    });
});

$(document).ready(function () {
    //single new password input validations..        
    $(document).on('blur', '#password', function (event) {
        if ($("#password").val() == "") {
            $('#passworderror').css("display", "block");
            $(this).parent().parent().parent().addClass('validate is-invalid');
            $('#passworderror').addClass('error invalid-feedback');
            $('#passworderror').html('This field is required.');
            $("#registerFormSubmitId").attr('disabled', true);
            event.preventDefault();
            return false;
        } else {
//            var filter = /^(?=.*\d)(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*?]{8,25}$/;
            var filter = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
            if (!filter.test($("#password").val())) {
                $('#passworderror').css("display", "block");
                $('#passworderror').addClass('error invalid-feedback');
                $(this).parent().parent().parent().addClass('validate is-invalid');
                $("#passworderror").html('Please enter 8 characters including only Alphabets and Numbers.');
                $("#registerFormSubmitId").attr('disabled', true);
                return false;
            } else {
                if (($("#password").val().length < 8)) {
                    $('#passworderror').css("display", "block");
                    $('#passworderror').addClass('error invalid-feedback');
                    $(this).parent().parent().parent().addClass('validate is-invalid');
                    $("#passworderror").html('Please enter 8 characters including only Alphabets and Numbers.');
                    $("#registerFormSubmitId").attr('disabled', true);
                    return false;
                } else {
                    $('#passworderror').css("display", "none");
                    $('#passworderror').html('');
                    $("#registerFormSubmitId").attr('disabled', false);
                }
            }
        }
    });

    //single new repeat password input validations..        
    $(document).on('keyup', '#confirmed_password', function (event) {
        if ($("#confirmed_password").val() == "") {
            $(this).parent().parent().parent().addClass('validate is-invalid');
            $('#confirmed_passworderror').addClass('error invalid-feedback');
            $('#confirmed_passworderror').css("display", "block");
            $('#confirmed_passworderror').html('This field is required.');
            $("#registerFormSubmitId").attr('disabled', true);

            event.preventDefault();
            return false;
        } else {
            var filter = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
            if (!filter.test($("#confirmed_password").val())) {
                $('#confirmed_passworderror').css("display", "block");
                $('#confirmed_passworderror').addClass('error invalid-feedback');
                $(this).parent().parent().parent().addClass('validate is-invalid');
                $("#confirmed_passworderror").html('Please enter 8 characters including only Alphabets and Numbers.');
                $("#registerFormSubmitId").attr('disabled', true);
                return false;
            } else {
                if (($("#confirmed_password").val().length < 8)) {
                    $('#confirmed_passworderror').css("display", "block");
                    $('#confirmed_passworderror').addClass('error invalid-feedback');
                    $(this).parent().parent().parent().addClass('validate is-invalid');
                    $("#confirmed_passworderror").html('Please enter 8 characters including only Alphabets and Numbers.');
                    $("#registerFormSubmitId").attr('disabled', true);
                    return false;
                } else {
                    if ($("#password").val() !== $("#confirmed_password").val()) {
                        $('#confirmed_passworderror').css("display", "block");
                        $('#confirmed_passworderror').addClass('error invalid-feedback');
                        $(this).parent().parent().parent().addClass('validate is-invalid');
                        $('#confirmed_passworderror').html('The new passwords did not match, please try again.');
                        $("#registerFormSubmitId").attr('disabled', true);
                    } else {
                        $('#confirmed_passworderror').css("display", "none");
                        $('#confirmed_passworderror').html('');
                        $("#registerFormSubmitId").attr('disabled', false);
                    }
                }
            }
        }
    });
//     setInterval(function(){ grecaptcha.reset(); }, 5 * 60 * 1000 ); 
});
   