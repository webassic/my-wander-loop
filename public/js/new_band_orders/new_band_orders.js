/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    var ttable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#admin-new-band-orders-list').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[9, "desc"]],
            ajax: {
                url: '/api/admin-new-band-orders/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [

                {data: 'firstname', name: 'firstname'},
                {data: 'ufirstname', name: 'ufirstname'},
                {data: 'number_of_bands', name: 'number_of_bands'},
                {data: 'band_lost', name: 'band_lost', render: function (d) {
                        if (d == 'Yes') {
                            return '<span ><i class="flaticon2-check-mark icon-lg text-success"></i></span>';
                        } else {
                            return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>';
                        }
                    },
                    className: "text-center"
                },
                {data: 'price_per_band', name: 'price_per_band', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'total_amount', name: 'total_amount', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'amount_paid', name: 'amount_paid'},
                {data: 'rzpayment_id', name: 'rzpayment_id'},
                {data: 'order_status', name: 'order_status'},
                {data: 'order_date', name: 'order_date'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],

            columnDefs: [
                {"visible": false, "targets": [10]},
                {
                    targets: 6,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        if (row.amount_paid == '' || row.amount_paid == null) {
                            return "Rs. " + row.total_amount;
                        } else {
                            return "Rs. " + row.amount_paid;
                        }
                    },
                },
                {
                    targets: 8,
                    title: 'Order Status',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var orderStatus;

                        if (row.order_processed == 'No') {

                            orderStatus = 'New Order';
                        } else if (row.order_processed == 'Yes') {
                            orderStatus = 'Order Processed';
                        }
                        if (row.order_shipped == 'Yes') {

                            orderStatus = 'Order Shipped';
                        }
                        if (row.delivered == 'Yes') {
                            orderStatus = 'Order Delivered';
                        }

                        return orderStatus;
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, row, meta) {

                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users/' + row.banduser_id + '"><i class="la la-download"></i> Download PDF</a>';
                        actionData += '<a class="dropdown-item" href="/bandorders/download_invoice/' + row.id + '" title="Download Invoice"><i class="la la-download" style="font-size:20px;"></i>Download Invoice</a>';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users-address/' + row.banduser_id + '" title="Download Address"><i class="la la-download" style="font-size:20px;"></i>Download Address</a>';

                        if (row.order_processed == 'No') {

                            actionData += ' <a href="javascript:void(0);" class="dropdown-item" data-toggle="modal" data-toggle="modal" data-target="#markAsProcessedModal" title="Mark As Processed" onclick="processedJsFunction(' + row.id + ')"><i class="flaticon2-check-mark "></i> Mark As Processed</a>';
                        } else {
                            actionData += ' <a href="javascript:void(0);" class="dropdown-item text-success"  title="Processed" ><i class="far fa-check-circle text-success"></i>Processed</a>';
                        }
                        if (row.order_shipped == 'No') {

                            actionData += ' <a href="javascript:void(0);" data-toggle="modal" data-toggle="modal" data-target="#markAsShippedModal" class="dropdown-item" title="Mark As Shipped" onclick="shippedJsFunction(' + row.id + ')"><i class="flaticon2-check-mark "></i>Mark As Shipped </a>';
                        } else {
                            actionData += ' <a href="javascript:void(0);" class="dropdown-item text-success"  title="Shipped" ><i class="far fa-check-circle text-success"></i>Shipped</a>';
                        }
                        if (row.delivered == 'No') {
                            actionData += '<a href="javascript:void(0);" data-toggle="modal" data-toggle="modal" data-target="#markAsDeliveredModal" class="dropdown-item" title="Mark As Delivered" onclick="deliveredJsFunction(' + row.id + ')"><i class="flaticon2-check-mark "></i>Mark As Delivered </a>';
                        } else {
                            actionData += ' <a href="javascript:void(0);" class="dropdown-item text-success"  title="Delivered" ><i class="far fa-check-circle text-success"></i>Delivered</a>';
                        }
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var uname = aData.ufirstname != '' ? aData.ufirstname.charAt(0).toUpperCase() + aData.ufirstname.slice(1) + " " + aData.ulastname.charAt(0).toUpperCase() + aData.ulastname.slice(1) : '';
                var name = aData.firstname != '' ? aData.firstname.charAt(0).toUpperCase() + aData.firstname.slice(1) + " " + aData.lastname.charAt(0).toUpperCase() + aData.lastname.slice(1) : '';
                $('td:eq(1)', nRow).html(uname);
                $('td:eq(0)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
        ttable = $('#admin-delivered-orders-list').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[9, "desc"]],
            ajax: {
                url: '/api/admin-delivered-orders/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [

                {data: 'firstname', name: 'firstname'},
                {data: 'ufirstname', name: 'ufirstname'},
                {data: 'number_of_bands', name: 'number_of_bands'},
                {data: 'band_lost', name: 'band_lost', render: function (d) {
                        if (d == 'Yes') {
                            return '<span ><i class="flaticon2-check-mark icon-lg text-success"></i></span>';
                        } else {
                            return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>';
                        }
                    },
                    className: "text-center"
                },
                {data: 'price_per_band', name: 'price_per_band', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'total_amount', name: 'total_amount', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'amount_paid', name: 'amount_paid'},
                {data: 'rzpayment_id', name: 'rzpayment_id'},
                {data: 'order_status', name: 'order_status'},
                {data: 'order_date', name: 'order_date'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],
            columnDefs: [
                {"visible": false, "targets": [10]},
                {
                    targets: 6,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        if (row.amount_paid == '' || row.amount_paid == null) {
                            return "Rs. " + row.total_amount;
                        } else {
                            return "Rs. " + row.amount_paid;
                        }
                    },
                },
                {
                    targets: 8,
                    title: 'Order Status',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var orderStatus;

                        if (row.order_processed == 'No') {

                            orderStatus = 'New Order';
                        } else if (row.order_processed == 'Yes') {
                            orderStatus = 'Order Processed';
                        }
                        if (row.order_shipped == 'Yes') {

                            orderStatus = 'Order Shipped';
                        }
                        if (row.delivered == 'Yes') {
                            orderStatus = 'Order Delivered';
                        }

                        return orderStatus;
                    },
                },
                {
                    targets: -1,
                    title: 'Action',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users/' + row.banduser_id + '"><i class="la la-download"></i> Download PDF</a>';
                        actionData += '<a class="dropdown-item" href="/bandorders/download_invoice/' + row.id + '" title="Download Invoice"><i class="la la-download" style="font-size:20px;"></i>Download Invoice</a>';
                        actionData += '<a class="dropdown-item" href="/admin/download-band-users-address/' + row.banduser_id + '" title="Download Address"><i class="la la-download" style="font-size:20px;"></i>Download Address</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
//                        var actionData ='';
//                        if(row.delivered == 'Yes'){
//                            actionData +='<a class="dropdown-item" href="/bandorders/download_invoice/' + row.id + '" title="Download Invoice"><i class="la la-download text-danger" style="font-size:20px;"></i></a>';
//                        }else{
//                             actionData +='<span class="text-danger">NA</span>';
//                        }
//                       
//                        return actionData;
                    },
                },
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var uname = aData.ufirstname != '' ? aData.ufirstname.charAt(0).toUpperCase() + aData.ufirstname.slice(1) + " " + aData.ulastname.charAt(0).toUpperCase() + aData.ulastname.slice(1) : '';
                var name = aData.firstname != '' ? aData.firstname.charAt(0).toUpperCase() + aData.firstname.slice(1) + " " + aData.lastname.charAt(0).toUpperCase() + aData.lastname.slice(1) : '';
                $('td:eq(1)', nRow).html(uname);
                $('td:eq(0)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
    });

});


$("form[name='mark_as_processed']").validate({
    submitHandler: function (form) {
        $('.loadingImageLoader').css('display', 'block');
        var url = $("#markAsProcessedFromId").attr('action');
        var id = $("#markAsProcessedInput").val();
        var data = {
            "banduser_id": id,
            "order_processed": "Yes",
        }
        $.ajax({
            type: "PUT",
            url: url,
            dataType: "json",
            data: data,
            ContentType: 'application/json',
            success: function (response) {
                console.log(response);
                $('.loadingImageLoader').css('display', 'none');
                $("#markAsProcessedModal").modal('hide');
                $("#markAsProcessedFromId")[0].reset();
                $('html, body').animate({
                    scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
                }, 2000);
                $('#admin-new-band-orders-list').DataTable().ajax.reload();
                $('#success-alert').css('display', 'block');
                $('#success-msg').text('Order processed successfully.');
                setInterval(function () {
                    $("#success-alert").fadeOut('slow');

                }, 3000);
            },
            error: function (error) {
                $('.loadingImageLoader').css('display', 'none');
                console.log(error);
            }
        });
    }
});
$("form[name='mark_as_shipped']").validate({
    submitHandler: function (form) {
        $('.loadingImageLoader').css('display', 'block');
        var url = $("#markAsShippedFromId").attr('action');
        var id = $("#markAsShippedInput").val();
        var data = {
            "banduser_id": id,
            "order_shipped": "Yes",
        }
        $.ajax({
            type: "PUT",
            url: url,
            dataType: "json",
            data: data,
            ContentType: 'application/json',
            success: function (response) {
                console.log(response);
                $('.loadingImageLoader').css('display', 'none');
                $("#markAsShippedModal").modal('hide');
                $("#markAsShippedFromId")[0].reset();
                $('html, body').animate({
                    scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
                }, 2000);
                $('#admin-new-band-orders-list').DataTable().ajax.reload();
                $('#success-alert').css('display', 'block');
                $('#success-msg').text('Order shipped successfully.');
                setInterval(function () {
                    $("#success-alert").fadeOut('slow');

                }, 3000);
            },
            error: function (error) {
                $('.loadingImageLoader').css('display', 'none');
                console.log(error);
            }
        });
    }
});
$("form[name='mark_as_delivered']").validate({
    submitHandler: function (form) {
        $('.loadingImageLoader').css('display', 'block');
        var url = $("#markAsDeliveredFromId").attr('action');
        var id = $("#markAsDeliveredInput").val();
        var data = {
            "banduser_id": id,
            "delivered": "Yes",
        }
        $.ajax({
            type: "PUT",
            url: url,
            dataType: "json",
            data: data,
            ContentType: 'application/json',
            success: function (response) {
                console.log(response);
                $('.loadingImageLoader').css('display', 'none');
                $("#markAsDeliveredModal").modal('hide');
                $("#markAsDeliveredFromId")[0].reset();
                $('html, body').animate({
                    scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
                }, 2000);
                $('#admin-new-band-orders-list').DataTable().ajax.reload();
                $('#success-alert').css('display', 'block');
                $('#success-msg').text('Order delivered successfully.');
                setInterval(function () {
                    $("#success-alert").fadeOut('slow');

                }, 3000);
            },
            error: function (error) {
                $('.loadingImageLoader').css('display', 'none');
                console.log(error);
            }
        });
    }
});

function processedJsFunction(id) {
    $("#markAsProcessedInput").val(id);
}
function shippedJsFunction(id) {
    $("#markAsShippedInput").val(id);
}
function deliveredJsFunction(id) {
    $("#markAsDeliveredInput").val(id);
}