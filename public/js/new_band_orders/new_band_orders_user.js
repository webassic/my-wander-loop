/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    var ttable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#user-new-band-orders-list').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[8, "desc"]],
            ajax: {
                url: '/api/user-new-band-orders/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [

                {data: 'firstname', name: 'firstname'},
                {data: 'number_of_bands', name: 'number_of_bands'},
                {data: 'band_lost', name: 'band_lost', render: function (d) {
                        if (d == 'Yes') {
                            return '<span ><i class="flaticon2-check-mark icon-lg text-success"></i></span>';
                        } else {
                            return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>';
                        }
                    },
                    className: "text-center"
                },
                {data: 'price_per_band', name: 'price_per_band', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'total_amount', name: 'total_amount', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'amount_paid', name: 'amount_paid'},
                {data: 'rzpayment_id', name: 'rzpayment_id'},
                {data: 'order_status', name: 'order_status'},
                {data: 'order_date', name: 'order_date'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],

            columnDefs: [
                {"visible": false, "targets": [9]},
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        if (row.amount_paid == '' || row.amount_paid == null) {
                            return "Rs. " + row.total_amount;
                        } else {
                            return "Rs. " + row.amount_paid;
                        }
                    },
                },
                {
                    targets: 7,
                    title: 'Order Status',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var orderStatus;

                        if (row.order_processed == 'No') {

                            orderStatus = 'New Order';
                        } else if (row.order_processed == 'Yes') {
                            orderStatus = 'Order Processed';
                        }
                        if (row.order_shipped == 'Yes') {

                            orderStatus = 'Order Shipped';
                        }
                        if (row.delivered == 'Yes') {
                            orderStatus = 'Order Delivered';
                        }

                        return orderStatus;
                    },
                },
                {
                    targets: -1,
                    title: 'Download Invoice',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/user_bandorders/download_invoice/' + row.id + '" title="Download Invoice"><i class="la la-download" style="font-size:20px;"></i>Download Invoice</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var name = aData.firstname != '' ? aData.firstname.charAt(0).toUpperCase() + aData.firstname.slice(1) + " " + aData.lastname.charAt(0).toUpperCase() + aData.lastname.slice(1) : '';
                $('td:eq(0)', nRow).html(name);

            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
        ttable = $('#user-delivered-orders-list').DataTable({
            "dom": '<"pull-left"f><"pull-right"l>tip',
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[8, "desc"]],
            ajax: {
                url: '/api/user-delivered-orders/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [

                {data: 'firstname', name: 'firstname'},

                {data: 'number_of_bands', name: 'number_of_bands'},
                {data: 'band_lost', name: 'band_lost', render: function (d) {
                        if (d == 'Yes') {
                            return '<span ><i class="flaticon2-check-mark icon-lg text-success"></i></span>';
                        } else {
                            return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>';
                        }
                    },
                    className: "text-center"
                },
                {data: 'price_per_band', name: 'price_per_band', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'total_amount', name: 'total_amount', render: function (d) {
                        if (d == '' || d == null) {
                            return 'NA';
                        } else {
                            return 'Rs. ' + d;
                        }
                    }},
                {data: 'amount_paid', name: 'amount_paid'},
                {data: 'rzpayment_id', name: 'rzpayment_id'},
                {data: 'order_status', name: 'order_status'},
                {data: 'order_date', name: 'order_date'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'package_name', name: 'package_name', render: function (data, type, row) {
                        return row.package_name.charAt(0).toUpperCase() + row.package_name.slice(1);
                    }},
                {data: 'Actions', responsivePriority: -1},
            ],
            columnDefs: [
                {"visible": false, "targets": [9]},
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        if (row.amount_paid == '' || row.amount_paid == null) {
                            return "Rs. " + row.total_amount;
                        } else {
                            return "Rs. " + row.amount_paid;
                        }
                    },
                },
                {
                    targets: 7,
                    title: 'Order Status',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var orderStatus;

                        if (row.order_processed == 'No') {

                            orderStatus = 'New Order';
                        } else if (row.order_processed == 'Yes') {
                            orderStatus = 'Order Processed';
                        }
                        if (row.order_shipped == 'Yes') {

                            orderStatus = 'Order Shipped';
                        }
                        if (row.delivered == 'Yes') {
                            orderStatus = 'Order Delivered';
                        }

                        return orderStatus;
                    },
                },
                {
                    targets: -1,
                    title: 'Action',
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var actionData = '';
                        actionData += '<span class="dropdown">';
                        actionData += '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">\
                              <i class="la la-ellipsis-h"></i>\
                            </a>';
                        actionData += ' <div class="dropdown-menu dropdown-menu-right">';
                        actionData += '<a class="dropdown-item" href="/user_bandorders/download_invoice/' + row.id + '" title="Download Invoice"><i class="la la-download" style="font-size:20px;"></i>Download Invoice</a>';
                        actionData += '</div>';
                        actionData += '</span>';
                        return actionData;
                    },
                },
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                var name = aData.firstname != '' ? aData.firstname.charAt(0).toUpperCase() + aData.firstname.slice(1) + " " + aData.lastname.charAt(0).toUpperCase() + aData.lastname.slice(1) : '';
                $('td:eq(0)', nRow).html(name);

            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
    });

});


