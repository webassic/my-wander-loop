$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No space please and don't leave it empty");

    $('#valid_till').datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom left",
        minDate: new Date()
    }).on('changeDate', function (ev) {
        if ($('#valid_till').val() != '') {
            $('#valid_till').removeClass('error');
            $('#valid_till-error').css('display', 'none');
        }
    });

    var otable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#promo-codes-list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[1, "desc"]],
            ajax: {
                url: '/api/promocodes/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
             },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'name', name: 'name', render: function (data, type, row) {
                        return data.toUpperCase();
                    }},
                {data: 'valid_till', name: 'valid_till', render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: 'discount_offered', name: 'discount_offered'},
                {data: 'user_names', name: 'user_names', orderable: false},
                {data: 'is_active', name: 'is_active', render: function (data, type, row) {
                        if (data != null && data == 'active') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'inactive') {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'created_at', name: 'created_at',
                    render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="/promo-codes/edit/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="javascript:void(0);" data-toggle="modal" data-target="#deletePromoCodesModal" onclick="deletePromoCode(' + data.id + ')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="flaticon-delete"></i></a>';
//                        return '<a href="javascript:void(0);" onclick="getInventoryTransactionDetails(' + data.id + ')"  class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a><a href="/inventory-transactions/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a><a href="javascript:void(0);"  onclick="deleteInventoryTransaction(' + data.id + ')" data-toggle="modal" data-target="#deleteInventoryTransactionModal" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="flaticon-delete"></i></a>';
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                name = aData.discount_offered != '' ? aData.discount_offered + " " + aData.discount_offered_type : '';
                $('td:eq(2)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
        $('#listing_status').delegate('#status', 'change', function () {
            otable.columns(5).search(this.value).draw();
        });
        $('#promo-codes-list_filter input').prop('title', 'In case you are trying to search based on a Date, then use YYYY-MM-DD format.');
    });

    $(document).ready(function () {
        $("form[name='promo_codes']").validate({
            rules: {
                name: {
                    required: true,
                    noSpace: true,
                    minlength: 5,
                    maxlength: 10
                },
                valid_till: {
                    required: true
                },
                users_id: {
                    required: true
                },
                discount_offered: {
                    required: true
                }
            },
            submitHandler: function (form) {
                var url = $("#promoCodeFormId").attr('action');
                var data = $('#promoCodeFormId').serializeArray();
                data.push({'name': 'user_id', "value": $('#users_id').select2("val")});

                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });

                $('#addPromoCodesSubmitBtnId').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(item),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('#add_promocode_status').html('<div class="alert alert-success">Promo Code Created Successfully.</div>');
                        setInterval(function () {
                            $("#add_promocode_status").fadeOut('slow');
                            $('#addPromoCodesSubmitBtnId').attr('disabled', false);
                        }, 6000);
                        window.location.href = "/promo-codes";
                    },
                    error: function (error) {
                        $('#error_promocode_status').html('<div class="alert alert-danger">' + error.responseJSON.error + '</div>');
                        setInterval(function () {
                            $("#error_promocode_status").fadeOut('slow');
                        }, 6000);

                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    $("#" + dkey + "_error").text(dvalue[0]);
                                    $("#" + dkey + "_error").removeClass('text-muted');
                                    $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                });
                                $('#addPromoCodesSubmitBtnId').attr('disabled', false);
                                $('.errorMessage').delay(6000).fadeOut();
                            }
                        });
                    }
                });
            }
        });

        $("form[name='edit_promo_codes']").validate({
            rules: {
                name: {
                    required: true,
                    noSpace: true,
                    minlength: 5,
                    maxlength: 10
                },
                valid_till: {
                    required: true
                },
                users_id: {
                    required: true
                },
                discount_offered: {
                    required: true
                }
            },
            submitHandler: function (form) {
                var url = $("#editPromoCodeFormId").attr('action');
                var data = $('#editPromoCodeFormId').serializeArray();
                data.push({'name': 'user_id', "value": $('#users_id').select2("val")});

                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });
                $('#editPromoCodesSubmitBtnId').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: "/api/promocodes/update",
                    data: JSON.stringify(item),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('#edit_promocode_status').html('<div class="alert alert-success">Promo Code Updated Successfully.</div>');
                        setInterval(function () {
                            $("#edit_promocode_status").fadeOut('slow');
                            $('#editPromoCodesSubmitBtnId').attr('disabled', false);
                        }, 6000);
                        window.location.href = "/promo-codes";
                    },
                    error: function (error) {
                        $('#edit_error_promocode_status').html('<div class="alert alert-danger">' + error.responseJSON.error + '</div>');
                        setInterval(function () {
                            $("#edit_error_promocode_status").fadeOut('slow');
                        }, 6000);
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    $("#" + dkey + "_error").text(dvalue[0]);
                                    $("#" + dkey + "_error").removeClass('text-muted');
                                    $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                });
                                $('#editPromoCodesSubmitBtnId').attr('disabled', false);
                                $('.errorMessage').delay(6000).fadeOut();
                            }
                        });
                    }
                });
            }
        });

        $("form[name='delete_promocode']").validate({
            submitHandler: function (form) {
                var url = $("#deletePromoCodeFormId").attr('action');
                var id = $("#promoCodeId").val();
                $.ajax({
                    type: "DELETE",
                    url: url + '/' + id,
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $("#success-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#success-msg").text(response.message);
                        $("#deletePromoCodeFormId").get(0).reset();
                        $('#deletePromoCodesModal').modal('toggle');
                        $('#promo-codes-list').DataTable().ajax.reload();
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');

                        }, 3000);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    });
});
function deletePromoCode(id) {
    $("#promoCodeId").val(id);
}