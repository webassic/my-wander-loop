$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    $(document).ready(function () {

    });
    $("#editRazorPaySubmitBtnId").click(function () {
        jQuery.validator.addMethod("accept", function (value, element, param) {
            return value.match(new RegExp("." + param + "$"));
        }, "Please allow only characters.");
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");
        $("#editRazorPayFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                test_key_id: {
                    required: true,
                    noSpace: true
                },
                test_key_secret: {
                    required: true,
                    noSpace: true
                },
                test_basic_plan_id: {
                    required: true,
                    noSpace: true
                },
                test_plan_id: {
                    required: true,
                    noSpace: true
                },
                test_banduser_cost: {
                    required: true,
                    digits: true,
                    noSpace: true
                },
                test_invoice_number_prefix: {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    accept: "[a-zA-Z]+",
                    noSpace: true
                },
                test_invoice_number_start: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength: 10,
                    noSpace: true
                },
                test_gst_rate: {
                    required: true,
                    digits: true,
                    noSpace: true
                },
                live_key_id: {
                    required: true,
                    noSpace: true
                },
                live_key_secret: {
                    required: true,
                    noSpace: true
                },
                live_basic_plan_id: {
                    required: true,
                    noSpace: true
                },
                live_plan_id: {
                    required: true,
                    noSpace: true
                },
                live_banduser_cost: {
                    required: true,
                    digits: true,
                    noSpace: true
                },
                live_invoice_number_prefix: {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    accept: "[a-zA-Z]+",
                    noSpace: true
                },
                live_invoice_number_start: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength: 10,
                    noSpace: true
                },
                live_gst_rate: {
                    required: true,
                    digits: true,
                    noSpace: true
                },
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            submitHandler: function (form) {
                var data = $('#editRazorPayFormId').serializeArray();
                var url = $("#editRazorPayFormId").attr('action');
                var id = $("#eid").val();
                $('#editRazorPaySubmitBtnId').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: url + '/' + id,
                    data: data,
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $("#success-alert").css('display', 'block');
                        $('html, body').animate({
                            scrollTop: ($('.kt-portlet__head-title').offset().top - 300)
                        }, 2000);
                        $('#success-alert').html('<div class="alert alert-success">' + response.message + '</div>');
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');
                            $('#editRazorPaySubmitBtnId').attr('disabled', false);
                            window.location.href = "/razorpay_credentials/" + id;
                        }, 3000);
                    },
                    error: function (error) {
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
                                $.each(value.data, function (dkey, dvalue) {
                                    $("#e" + dkey + "_error").text(dvalue[0]);
                                    $("#e" + dkey + "_error").removeClass('text-muted');
                                    $("#e" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                                });
                                $('#editRazorPaySubmitBtnId').attr('disabled', false);
                            }

                        });

                    }
                });
            }
        });
    });
}); 