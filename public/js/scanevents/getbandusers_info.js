/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('#errorMessage').delay(3000).fadeOut();
    $('#successMessage').delay(3000).fadeOut();
    getLocation();

    $("#verifyMobile").click(function () {

        $("#scanEventsFormId").validate({
            rules: {
                // simple rule, converted to {required:true}
                uniqueid: {
                    required: true,
                    digits: true,
                    minlength: 8,
                    maxlength: 8,
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                },
            },
            submitHandler: function (form) {
                $('.loadingImageLoader').css('display', 'block');
                var uniqueId = $("#uniqueid").val();
                var mobile = $("#mobile").val();
                $("#storeMobileStore").val(mobile);
                $("#hiddenUid").val(uniqueId);
                $('#verifyMobile').attr('disabled', true);


                $.ajax({
                    type: "GET",
                    url: '/api/banduser_exist/' + uniqueId,
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('.loadingImageLoader').css('display', 'none');
                        if (response.data > 0) {
                            $("#scanEventsFormId").get(0).reset();
                            $("#successMessage").css('display', 'block').text("Enter OTP to verify your mobile number");
                            setInterval(function () {
                                $("#successMessage").fadeOut('slow');
                                $('#verifyMobile').attr('disabled', false);
                            }, 3000);

                            $("#scanEventsFormId").toggle();
                            $("#mobileVerificationFormId").toggle();
//                       console.log($("#storeMobileStore").val());
                            if ($("#scaneventId").val() != '') {
                                var dataForMobileAndUid = {
                                    'scanner_mobile': mobile,
                                    'banduser_uniqueid': uniqueId
                                };
                                updateScanEvents(dataForMobileAndUid);
                            }

                            generateOtp();
                        }

//                  console.log(response);

                    },
                    error: function (error) {
                        $('.loadingImageLoader').css('display', 'none');
//                   console.log(error);   
                        $.each(error, function (key, value) {
                            if (key == 'responseJSON') {
//                           console.log(value.message);
                                $("#errorMessage").css('display', 'block').text(value.message);
                                setInterval(function () {
                                    $("#errorMessage").fadeOut('slow');
                                    $('#verifyMobile').attr('disabled', false);
                                }, 3000);
                            }

                        });
                    }
                });
            }
        });
    });
    $("#mobileVerificationBtnId").click(function () {
        $("#mobileVerificationFormId").validate({

            submitHandler: function (form) {
                $('.loadingImageLoader').css('display', 'block');
                var uniqueId = $("#hiddenUid").val();
                var otp = $("#otp").val();
                var generatedOtp = atob($("#generatedOtp").val());
                $('#mobileVerificationBtnId').attr('disabled', true);
                if (generatedOtp == otp) {
                    var dataForMobileVerify = {
                        'mobile_verify': 'yes',

                    };
                    updateScanEvents(dataForMobileVerify);
                    $.ajax({
                        type: "GET",
                        url: '/api/banduser_data/' + uniqueId,
                        dataType: "json",
                        ContentType: 'application/json',
                        success: function (response) {
                            console.log('responsessss >> ' + JSON.stringify(response));
                            $('.loadingImageLoader').css('display', 'none');
                            var dataForBandUserDisplay = {
                                'banduser_info_displayed': 'yes',

                            };
                            updateScanEvents(dataForBandUserDisplay);
                            $("#mobileVerificationFormId").hide();
                            $("#banduserDetailsContainer").show();
                            $.each(response, function (key, value) {
                                console.log(value);
                                if (key == 'data') {
                                    if (value.fullname != '') {
                                        $("#banduser_name").text(value.fullname);
                                    } else {
                                        $("#banduser_name").text('Not Available');
                                    }
                                    if (value.dob != '') {
                                        $("#date_of_birth").text(dateFormatConvert(value.dob));
                                    } else {
                                        $("#date_of_birth").text('Not Available');
                                    }
                                    if (value.fulladdress != '') {
                                        $("#fulladdress").text(value.fulladdress);
                                    } else {
                                        $("#fulladdress").text('Not Available');
                                    }
                                    if (value.blood_group != '') {
                                        $("#bloodgroup").text(value.blood_group);
                                    } else {
                                        $("#bloodgroup").text('Not Available');
                                    }
                                    if (value.mobile != '') {
                                        $("#primary_mobile").text(value.mobile);
                                    } else {
                                        $("#primary_mobile").text('Not Available');
                                    }
                                    if (value.condition1 != '' && value.condition1 != null) {
                                        $("#condition1").text(value.condition1);
                                    } else {
                                        $("#condition1").text('Not Available');
                                    }
                                    if (value.condition2 != '' && value.condition2 != null) {
                                        $("#condition2").text(value.condition2);
                                    } else {
                                        $("#condition2").text('Not Available');
                                    }
                                    if (value.emergencyMedication != '') {
                                        $("#emergencyMedication").text(value.emergencyMedication);
                                    } else {
                                        $("#emergencyMedication").text('Not Available');
                                    }
                                    if (value.preferredDoctor != '') {
                                        $("#preferredDoctor").text(value.preferredDoctor);
                                    } else {
                                        $("#preferredDoctor").text('Not Available');
                                    }
                                    if (value.hospitalPreference != '') {
                                        $("#hospitalPreference").text(value.hospitalPreference);
                                    } else {
                                        $("#hospitalPreference").text('Not Available');
                                    }
                                    if (value.hospitalRegistration != '') {
                                        $("#hospitalRegistration").text(value.hospitalRegistration);
                                    } else {
                                        $("#hospitalRegistration").text('Not Available');
                                    }
                                    if (value.languages_known != '') {
                                        $("#preferredLanguages").text(value.languages_known);
                                    } else {
                                        $("#preferredLanguages").text('Not Available');
                                    }
                                    if (value.profile_img != '') {
                                        profile_img = value.profile_img;
                                        $("#profile_img").attr('src', profile_img);
                                    }
                                }
                            });


                        },
                        error: function (error) {
                            $('.loadingImageLoader').css('display', 'none');
                            var dataForBandUserDisplay = {
                                'banduser_info_displayed': 'no',

                            };
                            updateScanEvents(dataForBandUserDisplay);

                            $("#mobileVerificationFormId").hide();
                            $("#banduserDetailsContainer").show();
//                        console.log(error);   
                            $.each(error, function (key, value) {
                                if (key == 'responseJSON') {
                                    console.log(value.message);
                                    $("#detailsContainer").css('display', 'none');
                                    html = '<h4 class="text-center">Band User is not active.</h4> For more details please get in touch with support :';
                                    html += '<a href="tel:+91-945-5550-650" class="text-danger">+91-945-5550-650</a>'
                                    $("#detailsErrorContainer").css('display', 'block');
                                    $("#detailsErrorMessageDiv").css('display', 'block').html(html);

                                }

                            });
                        }
                    });
                } else {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#errorMessage").css('display', 'block').text("Invalid OTP!!");
                    setInterval(function () {
                        $("#errorMessage").fadeOut('slow');
                        $('#mobileVerificationBtnId').attr('disabled', false);
                    }, 3000);
                }

            }
        });
    });
});

function generateOtp() {
    var mobile = $("#storeMobileStore").val();
    $.ajax({
        type: "GET",
        url: '/api/scanevent_getotp/' + mobile,
        dataType: "json",
        ContentType: 'application/json',
        success: function (response) {
//           console.log(response);
//           $("#otp").val(response.data);
            $("#generatedOtp").val(btoa(response.data));
        },
        error: function (error) {
//            console.log(error);   

        }
    });
}
//    $.ajax({
//        type: "POST",
//        url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDSYbGPyLqLSR_55pUJhZH50rSbSBdTk-8',
//        dataType: "json",
//        ContentType: 'application/json',
//        success: function (response) {          
//            console.log(response);
//
//        },
//        error: function (error) {
//            console.log(error);   
//
//        }
//    });
$.get("https://ipinfo.io", function (data) {
    var dataArray = {};
    dataArray = Object.assign(dataArray, data);
    dataArray = Object.assign(dataArray, {uniqueID: $("#uniqueid").val()});
    $('.loadingImageLoader').css('display', 'block');
    $.ajax({
        type: "POST",
        url: '/api/capture_user_info',
        data: dataArray,
        dataType: "json",
        ContentType: 'application/json',
        success: function (response) {
            $('.loadingImageLoader').css('display', 'none');
            if (response.data == 0 || $("#uniqueid").val() == "") {
                $("#scanEventsFormId").css("display", "none");
                $("#invalidUniqueErrorMessage").css("display", "block");
            } else {
                if (response.data.id != '') {
                    $("#scanEventsFormId").css("display", "block");
                    $("#invalidUniqueErrorMessage").css("display", "none");
                    $("#scaneventId").val(response.data.id);

                    var uid = $("#uniqueid").val();
                    var lat = $("#latitude").val();
                    var long = $("#logitude").val();
                    var errorlatlog = $("#lat_long_error").val();
                    if (uid != '') {
                        var data = {"banduser_uniqueid": uid,
                            "lat_long_error": errorlatlog,
                            "latitude": lat,
                            "logitude": long,

                        }
                        updateScanEvents(data);
                    }

                }
            }
        },
        error: function (error) {
            //            console.log(error);   

        }
    });
}, "jsonp");
//window.onload = function() {
//  
//  var geoOptions = {
//      maximumAge: 5 * 60 * 1000,
//      timeout: 10 * 1000,
//    enableHighAccuracy: true
//  }
//
//  var geoSuccess = function(position) {
//   
//    $("#latitude").val(position.coords.latitude);
//    $("#logitude").val(position.coords.longitude);
//  };
//  var geoError = function(error) {
//      console.log(error);
//    $("#lat_long_error").val(error.message);
//    // error.code can be:
//    //   0: unknown error
//    //   1: permission denied
//    //   2: position unavailable (error response from location provider)
//    //   3: timed out
//  };
//
//  navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
//};
//


function getLocation() {
//console.log(navigator.geolocation);
    var geoOptions = {
        maximumAge: 5 * 60 * 1000,
        timeout: 10 * 1000,
        enableHighAccuracy: true
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError, geoOptions);
    } else {
        var data = {"lat_long_error": "Geolocation is not supported by this browser."};
        $("#lat_long_error").val("Geolocation is not supported by this browser.");
        var scanId = $("#scaneventId").val();
        if (scanId != '') {
            updateScanEvents(data);

        }
//        console.log(data);
    }
}

function showPosition(position) {

    console.log(position);
    var data = {
        "latitude": position.coords.latitude,
        "logitude": position.coords.longitude
    };
    $("#latitude").val(position.coords.latitude);
    $("#logitude").val(position.coords.longitude);
    var scanId = $("#scaneventId").val();
    if (scanId != '') {
        updateScanEvents(data);

    }
//    updateScanEvents(data);
}
function showError(error) {
    var locationError;
    switch (error.code) {
        case error.PERMISSION_DENIED:
            locationError = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            locationError = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            locationError = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            locationError = "An unknown error occurred."
            break;
    }

    var data = {"lat_long_error": locationError};
    $("#lat_long_error").val(locationError);
    var scanId = $("#scaneventId").val();
    if (scanId != '') {
        updateScanEvents(data);

    }
//    updateScanEvents(data);
}



function updateScanEvents(data) {
    var id = $("#scaneventId").val();
    if (id != '') {
        $.ajax({
            type: "PUT",
            url: '/api/update_scan_event/' + id,
            data: data,
            dataType: "json",
            ContentType: 'application/json',
            success: function (response) {
                if (response.data.id != '') {
                    $("#scaneventId").val(response.data.id);
                }
//                   console.log(response.data);

            },
            error: function (error) {
                //            console.log(error);   

            }
        });
    }

}

function dateFormatConvert(date) {
    var dateAr = date.split('-');
    if (dateAr[2]) {
        var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    } else {
        var newDate = dateAr[0];
    }
    return newDate;
}
