$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

     var otable;
    $(document).ready(function () {
        
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
         otable = $('#scanevents_listing').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[3, "desc"]],
            ajax: {
                url: '/api/scanevents/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
             },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'banduser_uniqueid', name: 'banduser_uniqueid'},
                {data: 'banduser_name', name: 'banduser_name'},
                {data: 'scanner_mobile', name: 'scanner_mobile'},
                {data: 'scanned_on', name: 'scanned_on',
                render: function (d) {
                        return moment(d).format("DD MM YYYY HH:mm:ss") != 'Invalid date' ? moment(d).format("DD MMM YYYY HH:mm:ss") : '';
                    }},
                {data: 'mobile_verify', name: 'mobile_verify',
                render: function (d) {
                    if(d=='yes'){
                        return '<span ><i class="flaticon2-check-mark ticon-lg text-success"></i></span>';
                    }else{
                       return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>'; 
                    }
                    },
                    className: "text-center"
                },              
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="scanEventsDetails('+ data.id +')"><i class="flaticon2-information"></i></a>';                      
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
              
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });
         ptable = $('#admin_scanevent_list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[3, "desc"]],
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: false,
            ajax: {
                url: '/api/admin-scanevents/',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>',
               
            },
            columns: [
                {data: 'firstname', name: 'firstname'},
                {data: 'banduser_uniqueid', name: 'banduser_uniqueid'},
                {data: 'banduser_name', name: 'banduser_name'},
                {data: 'scanner_mobile', name: 'scanner_mobile'},
                {data: 'scanned_on', name: 'scanned_on',
                render: function (d) {
                        return moment(d).format("DD MMM YYYY HH:mm:ss") != 'Invalid date' ? moment(d).format("DD MMM YYYY HH:mm:ss") : '';
                    }},
                {data: 'mobile_verify', name: 'mobile_verify',
                render: function (d) {
                    if(d=='yes'){
                        return '<span ><i class="flaticon2-check-mark ticon-lg text-success"></i></span>';
                    }else{
                       return '<span><i class="flaticon2-cross icon-lg text-danger"></i></span>'; 
                    }
                    },
                    className: "text-center"
                },              
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        console.log(data);
                        return '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="adminScanEventsDetails('+ data.id +')"><i class="flaticon2-information"></i></a>';                      
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
               name = aData.firstname != '' ? aData.firstname + " " + aData.lastname : '';
                $('td:eq(0)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            }
        });

});

});

function scanEventsDetails(id) {
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl +'/api/scanevents/' + id,
        dataType: "json",
        success: function (response) {
            
            if(response.data!==''){
                $('#viewScanEventsModal').modal('show');
                 $.each( response.data, function( key, value ) {
                    if(value.banduser_name){
                        $('#banduser_name').text(value.banduser_name);
                    }else{
                         $('#banduser_name').text('---');
                    }
                    if(value.banduser_uniqueid){
                        $('#banduser_uniqueid').text(value.banduser_uniqueid);
                    }else{
                         $('#banduser_uniqueid').text('---');
                    }
                    if(value.location){
                        $('#location').text(value.location);
                    }else{
                         $('#location').text('---');
                    }
                    if(value.scanner_mobile){
                        $('#scanner_mobile').text(value.scanner_mobile);
                    }else{
                         $('#scanner_mobile').text('---');
                    }
                    var scannedOn = moment(value.scanned_on).format("DD MM YYYY HH:mm:ss") != 'Invalid date' ? moment(value.scanned_on).format("DD MMM YYYY HH:mm:ss") : '' ;
                    $('#scanned_on').text(scannedOn);
                    if(value.mobile_verify=='yes'){
                        
                        $('#mobile_verify').html('<i class="flaticon2-check-mark ticon-lg text-success"></i>');
                        
                    }else{
                        $('#mobile_verify').html('<i class="flaticon2-cross ticon-lg text-danger"></i>');
                    }
                    if(value.banduser_info_displayed=='yes'){
                        
                        $('#banduser_info').html('<i class="flaticon2-check-mark ticon-lg text-success"></i>');
                        
                    }else{
                        $('#banduser_info').html('<i class="flaticon2-cross ticon-lg text-danger"></i>');
                    }
                    
                 });
//
            }
        },
        error: function (error) {
            console.log(error);


        }
    });
}
function adminScanEventsDetails(id) {
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl +'/api/admin-scanevents/' + id,
        dataType: "json",
        success: function (response) {
            
            if(response.data!==''){
                $('#adminViewScanEventsModal').modal('show');
                 $.each( response.data, function( key, value ) {
                     var name = value.firstname != '' ? value.firstname + " " + value.lastname : '--';
                    var scannedOn = moment(value.scanned_on).format("DD MM YYYY HH:mm:ss") != 'Invalid date' ? moment(value.scanned_on).format("DD MMM YYYY HH:mm:ss") : '' ;
                    $("#guardian_name").text(name);
                    if(value.email){
                        $('#guardian_email').text(value.email);
                    }else{
                         $('#guardian_email').text('---');
                    }
                    if(value.mobile){
                        $('#guardian_mobile').text(value.mobile);
                    }else{
                         $('#guardian_mobile').text('---');
                    }
                    if(value.banduser_name){
                        $('#banduser_name').text(value.banduser_name);
                    }else{
                         $('#banduser_name').text('---');
                    }
                    if(value.banduser_uniqueid){
                        $('#banduser_uniqueid').text(value.banduser_uniqueid);
                    }else{
                         $('#banduser_uniqueid').text('---');
                    }
                    if(value.location){
                        $('#location').text(value.location);
                    }else{
                         $('#location').text('---');
                    }
                    if(value.scanner_mobile){
                        $('#scanner_mobile').text(value.scanner_mobile);
                    }else{
                         $('#scanner_mobile').text('---');
                    }
                    if(value.lat_long_error){
                        $('#latlog_error').text(value.lat_long_error);
                    }else{
                         $('#latlog_error').text('---');
                    }
                    $('#scanned_on').text(scannedOn);
                    if(value.mobile_verify=='yes'){
                        
                        $('#mobile_verify').html('<i class="flaticon2-check-mark ticon-lg text-success"></i>');
                        
                    }else{
                        $('#mobile_verify').html('<i class="flaticon2-cross ticon-lg text-danger"></i>');
                    }
                    if(value.banduser_info_displayed=='yes'){
                        
                        $('#banduser_info').html('<i class="flaticon2-check-mark ticon-lg text-success"></i>');
                        
                    }else{
                        $('#banduser_info').html('<i class="flaticon2-cross ticon-lg text-danger"></i>');
                    }
                    
                 });
//
            }
        },
        error: function (error) {
            console.log(error);


        }
    });
}