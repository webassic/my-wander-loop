$(document).ready(function () {
    $("#loadSupportTicketCommentsDiv").load('/api/get_support_ticket_comments_list/' + supportTicketId, function () {
    });
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
    $("form[id='addSupportTicketCommentFormId']").validate({
        rules: {
            message: {
                required: true,
                noSpace: true
            },
        },
        submitHandler: function (form) {
            $('.loadingImageLoader').css('display', 'block');
            var url = $("#addSupportTicketCommentFormId").attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: $("#addSupportTicketCommentFormId").serialize(),
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#addSupportTicketCommentFormId").get(0).reset();
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                    }, 3000);
                    $("#loadSupportTicketCommentsDiv").load('/api/get_support_ticket_comments_list/' + supportTicketId, function () {
                    });
                },
                error: function (error) {
                }
            });
        }
    });
});