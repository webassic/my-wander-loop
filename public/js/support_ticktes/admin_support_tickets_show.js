/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl + '/api/admin-supporttickets/' + supportTicketId,
        dataType: "json",
        success: function (response) {
            if (response.data !== '') {
                console.log(response.data);
                $.each(response.data, function (key, value) {
                    if (value.status == 'resolved') {
                        $("#checkStatusForShowForm").css('display', 'none');
                    }
                    if (value.firstname) {
                        $('#gardian_name').text(value.firstname + ' ' + value.lastname);
                    } else {
                        $('#gardian_name').text('---');
                    }
                    if (value.status) {
                        $('#showStatus').text(value.status);
                        if (value.status == 'pending') {
                            $("#checkSupportTicketStatus").css('display', 'block');
                            $('#showStatus').addClass('kt-badge--unified-danger');
                        } else if (value.status == 'resolved') {
                            $('#showStatus').addClass('kt-badge--unified-success');
                        } else {
                            $('#showStatus').addClass('kt-badge--unified-danger');
                        }
                    } else {
                        $('#showStatus').text('---');
                    }
                    if (value.unique_ticket_id) {
                        $('#unique_ticket_id').text('#' + value.unique_ticket_id);
                    } else {
                        $('#unique_ticket_id').text('---');
                    }
                    if (value.subject) {
                        $('#subject').text(value.subject);
                    } else {
                        $('#subject').text('---');
                    }
                    if (value.message) {
                        $('#message').text(value.message);
                    } else {
                        $('#message').text('---');
                    }
                    if (value.related_to == 'select_all' || value.related_to == 'banduser_name') {
                        $('#related_type').text('Band Users');
                    } else {
                        $('#related_type').text('General');
                    }
                    if (value.related_to == 'select_all' || value.related_to == 'banduser_name') {
                        var finalArr = [];
                        $.each(response.data, function (key, value) {
                            finalArr.push(value.title + " " + value.banduser_firstname + " " + value.banduser_lastname);
                        });
                        $('#related_to').text(finalArr.join(", "));
                    } else {
                        $('#related_to').text('---');
                    }

                });
            }
        },
        error: function (error) {
            console.log(error);


        }
    });

    $("#loadSupportTicketCommentsDiv").load('/api/get_admin_support_ticket_comments_list/' + supportTicketId, function () {
    });
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
    $("form[id='addSupportTicketCommentFormId']").validate({
        rules: {
            message: {
                required: true,
                noSpace: true
            },
        },
        submitHandler: function (form) {
            $('.loadingImageLoader').css('display', 'block');
            var url = $("#addSupportTicketCommentFormId").attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: $("#addSupportTicketCommentFormId").serialize(),
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#addSupportTicketCommentFormId").get(0).reset();
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                    }, 3000);
                    $("#loadSupportTicketCommentsDiv").load('/api/get_admin_support_ticket_comments_list/' + supportTicketId, function () {
                    });
                },
                error: function (error) {
                }
            });
        }
    });


    $("form[name='resolved_support_ticket']").validate({
        submitHandler: function (form) {
            $('.loadingImageLoader').css('display', 'block');
            var url = $("#resolvedSupportTicketFormId").attr('action');
            var id = $("#supportTicketId").val();
            $.ajax({
                type: "POST",
                url: url + '/' + id,
                data: $("#resolvedSupportTicketFormId").serialize(),
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('.loadingImageLoader').css('display', 'none');
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#resolvedSupportTicketFormId").get(0).reset();
                    $('#markAsResolvedSupportTicketModal').modal('toggle');
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                        window.location.reload();
                    }, 3000);
                },
                error: function (error) {
                }
            });
        }
    });

});

function resolvedSupportTicket(id, status) {
    $("#supportTicketId").val(id);
    $("#supportTicketStatus").val(status);
    $("#statusTitle").html(status.charAt(0).toUpperCase() + status.slice(1));
    $("#statusMessage").html(status);

}