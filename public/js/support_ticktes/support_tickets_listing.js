
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    var support_listing_table;
    $(document).ready(function () { 
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#support_ticket_management_listing').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[3, "desc"]],
            ajax: {
                url: '/api/admin-supporttickets',
                dataSrc: function (json) {
                    rowCount = json.recordsTotal;
                    if (rowCount >= 1) {
                        $("#markAsResolvedSTBtn").css("display", "block");
//                        $('#markAsResolvedSTBtn').attr('disabled', true);
                    }
                    return json.data;
                },
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: null, render: function (data, type, row) {
                        return '<label class="kt-checkbox" style="margin-bottom: 10px !important;"><input class="support_tickets_list" value="' + data.id + '" name="markAsResolvedCheckbox" type="checkbox" class="custom-checkbox"><span></span></label>';
                    }, orderable: false, sClass: "classDataTable"},
                {data: 'unique_ticket_id', name: 'unique_ticket_id'},
                {data: 'firstname'},
                {data: 'subject', name: 'subject'},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && data == 'resolved') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        }
                    }},
                {data: null, searchable: false, orderable: false}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var name = aData.firstname != '' ? aData.firstname.charAt(0).toUpperCase() + aData.firstname.slice(1) + " " + aData.lastname.charAt(0).toUpperCase() + aData.lastname.slice(1) : '';

                $('td:eq(2)', nRow).html(name);
                $('td:eq(5)', nRow).html('<a href="/support_tickets/show/' + aData.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" ><i class="flaticon2-information"></i></a>');

            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            }
        });

        //Redirect to view page
        $('#support_ticket_management_listing').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 0 && cell.index() != 5) {
                var data = otable.row(this).data();
                document.location.href = '/support_tickets/show/' + data.id;
            }
        });

        // multipal checkbox checked
        $('.multiple_checkall').on('click', function () {
            if (this.checked) {
                $('.support_tickets_list').each(function () {
                    this.checked = true;
                });
            } else {
                $('.support_tickets_list').each(function () {
                    this.checked = false;
                });
            }
        });

        $("form[name='resolved_support_ticket']").validate({
            submitHandler: function (form) {
                $('.loadingImageLoader').css('display', 'block');
                var url = $("#resolvedSupportTicketFormId").attr('action');
                var id = $("#supportTicketId").val();
                $.ajax({
                    type: "POST",
                    url: url + '/' + id,
                    data: $("#resolvedSupportTicketFormId").serialize(),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('.loadingImageLoader').css('display', 'none');
                        $("#success-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#success-msg").text(response.message);
                        $("#resolvedSupportTicketFormId").get(0).reset();
                        $('#markAsResolvedSupportTicketModal').modal('toggle');
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (error) {
                    }
                });
            }
        });
    });

    overlayList = $('<div></div>').prependTo('body').attr('id', 'overlayList');
    support_listing_table = $('#support_tickets_listing').DataTable({
        responsive: true,
        paging: true,
        pagingType: 'full_numbers',
        processing: true,
        serverSide: true,
        "pageLength": 10,
        "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
        "aaSorting": [[3, "desc"]],
        ajax: {
            url: '/api/support_tickets/listings',
            data: function (d) {
            }
        },
        "language": {
            "paginate": {
                "first": "First page",
                "last": "Last page",
                "previous": "Previous ",
                "next": "Next "
            }
        },
        oLanguage: {
            sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
        },
        columns: [
            {data: 'unique_ticket_id', name: 'unique_ticket_id'},
            {data: 'subject', name: 'subject'},
            {data: 'status', name: 'status', render: function (data, type, row) {
                    if (data != null && data == 'resolved') {
                        return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                    } else {
                        return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                    }
                }},
            {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                    return '<a href="/support_tickets/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a>';
                }}
        ],
        "fnCreatedRow": function (nRow, aData, iDataIndex) {

        },
        fnInitComplete: function (data, res) {
            if (res.hasOwnProperty("errors")) {
                $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                setTimeout(function () {
                    $('#authenticate-error-message').html('');
                }, 5000);
            }
            $('#opacitylow').css({'opacity': 2.0});
            $('.loader').css({'opacity': 0.5});
            overlayList.remove();
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).css('cursor', 'pointer');
        },
    });

    //Redirect to view page
    $('#support_tickets_listing').on('click', 'tbody tr', function (evt) {
        var cell = $(evt.target).closest('td');
        if (cell.index() != 3) {
            var data = support_listing_table.row(this).data();
            document.location.href = '/support_tickets/' + data.id;
        }
    });

    $("#relatedTo").change(function () {
        if (this.value != "") {
            $("#addSupportTicketSubmitBtnId").attr('disabled', false);
        }
    });

    // Add support tickets
    $("#addSupportTicketSubmitBtnId").click(function (e) {
        var relatedType = $('input[name="related_to"]:checked').val();

        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "No space please and don't leave it empty");

        if (relatedType == "banduser_name" && $("#relatedTo option:selected").val() == undefined) {
            $("#bandUsersListError").css('display', 'block');
            $("#addSupportTicketSubmitBtnId").attr('disabled', true);
            e.preventDefault();
        } else {
            $("#bandUsersListError").css('display', 'none');
            $("#addSupportTicketSubmitBtnId").attr('disabled', false);
        }
        $("#addSupportTicketsFormId").validate({
            rules: {
                subject: {
                    required: true,
                    noSpace: true
                },
                message: {
                    required: true,
                    noSpace: true
                },
            },
            submitHandler: function (form) {
                var data = $('#addSupportTicketsFormId').serializeArray();
                data.push({'name': 'bandusers_id', "value": $('#relatedTo').select2("val")});

                item = {};
                $.each(data, function (index, value) {
                    item[value.name] = value.value;
                });
                var url = $("#addSupportTicketsFormId").attr('action');
                $('.loadingImageLoader').css('display', 'block');
                $.ajax({
                    type: "POST",
                    url: url,
//                    data: $("#addSupportTicketsFormId").serialize(),
                    data: JSON.stringify(item),
                    dataType: "json",
                    ContentType: 'application/json',
                    success: function (response) {
                        $('.loadingImageLoader').css('display', 'none');
                        $('#add_support_ticket_status').html('<div class="alert alert-success">Support Tickets Created Successfully.</div>');
                        setInterval(function () {
                            $("#add_support_ticket_status").fadeOut('slow');
                        }, 6000);
                        window.location.href = "/support_tickets";
                    },
                    error: function (error) {
                        $('.loadingImageLoader').css('display', 'none');
                        $('#error_support_ticket_status').html('<div class="alert alert-danger">' + error.responseJSON.error + '</div>');
                        setInterval(function () {
                            $("#error_support_ticket_status").fadeOut('slow');
                        }, 6000);

                    }
                });
            }
        });
    });
});

/**
 * markAsResolvedSupportTickets
 * @returns {undefined}
 */
function markAsResolvedSupportTickets() {
    var supportTicketIds = [];
    $("input:checkbox[name=markAsResolvedCheckbox]:checked").each(function () {
        supportTicketIds.push($(this).val());
    });
    if (supportTicketIds.length > 0) {
        var status = 'resolved';
        $("#markAsResolvedSupportTicketModal").modal('show');
        $("#supportTicketId").val(supportTicketIds);
        $("#supportTicketStatus").val(status);
        $("#statusTitle").html(status.charAt(0).toUpperCase() + status.slice(1));
        $("#statusMessage").html(status);
    } else {
        $("#markAsResolvedSupportTicketModal").modal('hide');
        alert('Please select support tickets.');
    }
}