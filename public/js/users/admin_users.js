$(document).ready(function () {
    $.validator.addMethod("noSpace", function (value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
    $.validator.addMethod("regx", function (value, element, regexpr) {
        return regexpr.test(value);
    }, "Please enter 8 characters and including only Alphabets and Numbers.");
    $("form[name='admin_users']").validate({
        rules: {
            firstname: {
                required: true,
                noSpace: true,
            },
            lastname: {
                required: true,
                noSpace: true
            },
            email: {
                required: true,
                email: true,
                noSpace: true
            },
            mobile: {
                required: true,
                digits: true,
                minlength:6,
                maxlength:15,
                noSpace: true
            },
            password: {
                required: true,
                regx: /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/
            },
            confirmed_password: {
                required: true,
                equalTo: "#password"
            },
        },
        submitHandler: function (form) {
            var url = $("#adminUserFormId").attr('action');
            var data = $('#adminUserFormId').serializeArray();
            $('#addAdminUserSubmitBtnId').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('#add_adminuser_status').html('<div class="alert alert-success">Admin User Created Successfully.</div>');
                    setInterval(function () {
                        $("#add_adminuser_status").fadeOut('slow');
                        $('#addAdminUserSubmitBtnId').attr('disabled', false);
                    }, 6000);
                    window.location.href = "/admin-users";
                },
                error: function (error) {
                    $.each(error, function (key, value) {
                        if (key == 'responseJSON') {
                            $.each(value.data, function (dkey, dvalue) {
                                $("#" + dkey + "_error").text(dvalue[0]);
                                $("#" + dkey + "_error").removeClass('text-muted');
                                $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                            });
                            $('#addAdminUserSubmitBtnId').attr('disabled', false);
                            $('.errorMessage').delay(6000).fadeOut();
                        }

                    });
                }
            });
        }
    });

    $("form[name='edit_admin_users']").validate({
        rules: {
            firstname: {
                required: true,
                noSpace: true,
            },
            lastname: {
                required: true,
                noSpace: true
            },
            email: {
                required: true,
                email: true,
                noSpace: true
            },
            mobile: {
                required: true,
//                number: true,
                digits: true,
                minlength:6,
                maxlength:15,
                noSpace: true
            }
        },
        submitHandler: function (form) {
            var url = $("#editAdminUserFormId").attr('action');
            var data = $('#editAdminUserFormId').serializeArray();
            $('#editAdminUserSubmitBtnId').attr('disabled', true);
            var id = $("#eid").val();
            $.ajax({
                type: "POST",
                url: url + '/' + id,
                data: data,
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $('#add_adminuser_status').html('<div class="alert alert-success">Admin User Created Successfully.</div>');
                    setInterval(function () {
                        $("#add_adminuser_status").fadeOut('slow');
//                        $('#editAdminUserSubmitBtnId').attr('disabled', false);
                    }, 6000);
                    window.location.href = "/admin-users";
                },
                error: function (error) {
                    $.each(error, function (key, value) {
                        if (key == 'responseJSON') {
                            $.each(value.data, function (dkey, dvalue) {
                                $("#" + dkey + "_error").text(dvalue[0]);
                                $("#" + dkey + "_error").removeClass('text-muted');
                                $("#" + dkey + "_error").css({'display': 'block', 'color': 'red'});
                            });
                            $('#editAdminUserSubmitBtnId').attr('disabled', false);
                            $('.errorMessage').delay(6000).fadeOut();
                        }

                    });
                }
            });
        }
    });
});