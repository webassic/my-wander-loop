$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#users-list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[0, "asc"]],
            ajax: {
                url: '/api/users/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",    
                    "next": "Next "
                }
             },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'firstname', name: 'firstname'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'user_type', name: 'user_type', render: function (data, type, row) {
                        if (data != null && data == 'Administration') {
                            return '<span class="font-weight-bold text-danger">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'User') {
                            return '<span class="font-weight-bold text-success">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && data == 'active') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'inactive') {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'created_at', name: 'created_at',
                    render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<a href="/users/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a>\n\
                       <a href="javascript:void(0);" data-toggle="modal" data-target="#activateDeactivateUserModal" onclick="activateDeactivate(' + data.id + ',' + "'" + data.status + "'" + ')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Activate/Deactivate"><i class="flaticon-settings-1"></i></a>';
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                name = aData.firstname != '' ? aData.firstname + " " + aData.lastname : '';
                $('td:eq(0)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            }
        });
        
         //Redirect to view page
        $('#users-list').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 6) {
                var data = otable.row(this).data();
                document.location.href = '/users/view/' + data.id;
            }
        });
        
        $('#listing_status').delegate('#status', 'change', function () {
            otable.columns(5).search(this.value).draw();
        });
        $('#listing_user_type').delegate('#user_type', 'change', function () {
            otable.columns(4).search(this.value).draw();
        });
        $('#users-list_filter input').prop('title', 'In case you are trying to search based on a Date, then use YYYY-MM-DD format.');
    });

    $("form[name='activate_deactivate_user']").validate({
        submitHandler: function (form) {
            var id = $("#userId").val();
            var status = $("#currentStatus").val();

            if(status == 'active'){
              var updateStatus = 'inactive';
            }
            $.ajax({
                type: "PUT",
                url: '/api/usersleads/' + id  + '/' + updateStatus,
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    $("#success-alert").css('display', 'block');
                    $(".error").css('display', 'none');
                    $("#success-msg").text(response.message);
                    $("#activateDeactivateUserFormId").get(0).reset();
                    $('#activateDeactivateUserModal').modal('toggle');
                    $('#users-list').DataTable().ajax.reload();
                    setInterval(function () {
                        $("#success-alert").fadeOut('slow');
                    }, 3000);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
});
function activateDeactivate(id, status) {
    $("#userId").val(id);
    $("#currentStatus").val(status);
    if (status == 'active') {
        $('#activateDeactivateUserModalLabel').text('Deactivate User');
        $('#activateDeactivateContent').text('If you marked customer as an inactive then all its band users will be marked as an inactive. This action cannot be undone. Do you want to continue?');
    } 
}