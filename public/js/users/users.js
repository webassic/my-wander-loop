$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });

    var otable;
    $(document).ready(function () {
        $('#opacitylow').css({'opacity': 0.5});
        $('.loader').css({'opacity': 2.0});
        overlay = $('<div></div>').prependTo('body').attr('id', 'overlay');
        otable = $('#admin-users-list').DataTable({
            responsive: true,
            paging: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: true,
            "pageLength": 10,
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "aaSorting": [[0, "asc"]],
            ajax: {
                url: '/api/admin/users/listings',
                data: function (d) {
                }
            },
            "language": {
                "paginate": {
                    "first": "First page",
                    "last": "Last page",
                    "previous": "Previous ",
                    "next": "Next "
                }
            },
            oLanguage: {
                sProcessing: '<div class="kt-spinner kt-spinner--md kt-spinner--warning"></div>'
            },
            columns: [
                {data: 'firstname', name: 'firstname'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'user_type', name: 'user_type', render: function (data, type, row) {
                        if (data != null && data == 'Administration') {
                            return '<span class="font-weight-bold text-danger">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'User') {
                            return '<span class="font-weight-bold text-success">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'status', name: 'status', render: function (data, type, row) {
                        if (data != null && data == 'active') {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else if (data != null && data == 'inactive') {
                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + data.charAt(0).toUpperCase() + data.slice(1) + '</span>';
                        } else {
                            return 'NA';
                        }
                    }},
                {data: 'created_at', name: 'created_at',
                    render: function (d) {
                        return moment(d).format("DD-MM-YYYY") != 'Invalid date' ? moment(d).format("DD-MMM-YYYY") : '';
                    }},
                {data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        if (data.user_type == 'User') {
                            return '<a href="/users/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a>';
                        } else {
                            return '<a href="javascript:void(0);" data-toggle="modal" data-target="#changePasswordUsersModal" onclick="changePasswordAdminUser(' + data.id + ')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Change Password"><i class="fa fa-key" aria-hidden="true"></i></a><a href="/admin-users/profile/view/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon2-information"></i></a><a href="/admin-users/edit/' + data.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit/Update"><i class="flaticon-edit"></i></a><a href="javascript:void(0);" data-toggle="modal" data-target="#deleteAdminUsersModal" onclick="deleteAdminUser(' + data.id + ')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="flaticon-delete"></i></a>';
                        }
                    }}
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                name = aData.firstname != '' ? aData.firstname + " " + aData.lastname : '';
                $('td:eq(0)', nRow).html(name);
            },
            fnInitComplete: function (data, res) {
                if (res.hasOwnProperty("errors")) {
                    $('#authenticate-error-message').html('<div class="alert alert-danger">' + res.errors + '</div>');
                    setTimeout(function () {
                        $('#authenticate-error-message').html('');
                    }, 5000);
                }
                $('#opacitylow').css({'opacity': 2.0});
                $('.loader').css({'opacity': 0.5});
                overlay.remove();
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).css('cursor', 'pointer');
            }
        });

        //Redirect to view page
        $('#admin-users-list').on('click', 'tbody tr', function (evt) {
            var cell = $(evt.target).closest('td');
            if (cell.index() != 6) {
                var data = otable.row(this).data();
                document.location.href = '/admin-users/profile/view/' + data.id;
            }
        });

        $('#listing_status').delegate('#status', 'change', function () {
            otable.columns(5).search(this.value).draw();
        });
        $('#listing_user_type').delegate('#user_type', 'change', function () {
            otable.columns(4).search(this.value).draw();
        });
        $('#admin-users-list_filter input').prop('title', 'In case you are trying to search based on a Date, then use YYYY-MM-DD format.');
    });

    $("form[name='delete_adminusers']").validate({
        submitHandler: function (form) {
            var url = $("#deleteAdminUserFormId").attr('action');
            var id = $("#adminUserId").val();
            $.ajax({
                type: "DELETE",
                url: url + '/' + id,
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                    if (response > 1) {
                        $("#danger-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#error-msg").text('Admin User could not delete.');
                        $("#deleteAdminUserFormId").get(0).reset();
                        $('#deleteAdminUsersModal').modal('toggle');
                        $('#admin-users-list').DataTable().ajax.reload();
                        setInterval(function () {
                            $("#danger-alert").fadeOut('slow');
                        }, 3000);
                    } else {
                        $("#success-alert").css('display', 'block');
                        $(".error").css('display', 'none');
                        $("#success-msg").text('Admin User has been deleted successfully.');
                        $("#deleteAdminUserFormId").get(0).reset();
                        $('#deleteAdminUsersModal').modal('toggle');
                        if (id != $("#loggedInId").val()) {
                            $('#admin-users-list').DataTable().ajax.reload();
                        } else {
                            window.location.href = "/login";
                        }
                        setInterval(function () {
                            $("#success-alert").fadeOut('slow');

                        }, 3000);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });


    $(document).on('change', '#oldPasswordInputId', function (event) {
        if ($("#oldPasswordInputId").val() == "") {
            $('#oldPasswordInputError').css("display", "block");
            $('#oldPasswordInputError').html('This field is required.');
            event.preventDefault();
            return false;
        } else {
            $('#oldPasswordInputError').css("display", "none");
            $('#oldPasswordInputError').html('');
        }
    });
    //single new password input validations..        
    $(document).on('change', '#newPasswordInputId', function (event) {
        if ($("#newPasswordInputId").val() == "") {
            $('#newasswordInputError').css("display", "block");
            $('#newasswordInputError').html('This field is required.');
            event.preventDefault();
            return false;
        } else {
            var filter = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
            if (!filter.test($("#newPasswordInputId").val())) {
                $('#newasswordInputError').css("display", "block");
                $("#newasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
                return false;
            } else {
                if (($("#newPasswordInputId").val().length < 8)) {
                    $('#newasswordInputError').css("display", "block");
                    $("#newasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
                    return false;
                } else {
                    $('#newasswordInputError').css("display", "none");
                    $('#newasswordInputError').html('');
                }
            }
        }
    });

    //single new repeat password input validations..        
    $(document).on('keyup', '#repeatNewPasswordInputId', function (event) {
        if ($("#repeatNewPasswordInputId").val() == "") {
            $('#newRepeatPasswordInputError').css("display", "block");
            $('#newRepeatPasswordInputError').html('This field is required.');
            event.preventDefault();
            return false;
        } else {
            var filter = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
            if (!filter.test($("#repeatNewPasswordInputId").val())) {
                $('#newRepeatPasswordInputError').css("display", "block");
                $("#newRepeatPasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
                return false;
            } else {
                if (($("#newPasswordInputId").val().length < 8)) {
                    $('#newRepeatPasswordInputError').css("display", "block");
                    $("#newRepeatPasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
                    return false;
                } else {
                    if ($("#newPasswordInputId").val() !== $("#repeatNewPasswordInputId").val()) {
                        $('#newRepeatPasswordInputError').css("display", "block");
                        $('#newRepeatPasswordInputError').html('The new passwords did not match, please try again.');
                    } else {
                        $('#newRepeatPasswordInputError').css("display", "none");
                        $('#newRepeatPasswordInputError').html('');
                    }
                }
            }
        }
    });
});

function deleteAdminUser(id) {
    $("#adminUserId").val(id);
}
function changePasswordAdminUser(id) {
    $("#hiddenAdminUserId").val(id);
}

/**
 * 
 * @param {type} formID
 * @returns {Boolean}
 */
function updateAdminUserPassword(formID) {
    var oldPsw = $("#oldPasswordInputId").val();
    var newPsw = $("#newPasswordInputId").val();
    var repeatNewPsw = $("#repeatNewPasswordInputId").val();
    if (oldPsw == "") {
        $('#oldPasswordInputError').css("display", "block");
        $("#oldPasswordInputError").html('This field is required.');

    }
    if (newPsw == "") {
        $('#newasswordInputError').css("display", "block");
        $('#newRepeatPasswordInputError').css("display", "block");

    }
    if (repeatNewPsw == "") {
        $("#newasswordInputError").html('This field is required.');
        $("#newRepeatPasswordInputError").html('This field is required.');

    } else {
        var filter = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
        if (!filter.test(newPsw)) {
            $('#oldPasswordInputError').css("display", "none");
            $('#newasswordInputError').css("display", "block");
            $("#newasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
            return false;
        } else if (!filter.test(repeatNewPsw)) {
            $('#oldPasswordInputError').css("display", "none");
            $('#newasswordInputError').css("display", "none");
            $('#newRepeatPasswordInputError').css("display", "block");
            $("#newRepeatPasswordInputError").html('Please enter 8 characters including only Alphabets and Numbers.');
            return false;
        } else {
            $('#oldPasswordInputError').css("display", "none");
            $('#newasswordInputError').css("display", "none");
            $('#newRepeatPasswordInputError').css("display", "none");
            if (newPsw === repeatNewPsw) {
                $('#passwordBtn').attr("disabled", false);
                $('#newRepeatPasswordInputError').css("display", "none");
                $('#newRepeatPasswordInputError').html('');
                $.ajax({
                    url: '/api/admin-users/update_password',
                    type: "POST",
                    data: $('#' + formID).serialize(),
                    dataType: "json",
                    success: function (returnData)
                    {
                        console.log(returnData);
                        if (returnData == 200) {
                            $('#successPasswordMsg').css("display", "block");
                            $('#passwordBtn').attr("disabled", true);
                            $('#oldPasswordInputError').css("display", "none");
                            $('#newasswordInputError').css("display", "none");
                            $('#newRepeatPasswordInputError').css("display", "none");
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        } else if (returnData == 201) {
                            $('#oldPasswordInputError').css("display", "block");
                            $('#oldPasswordInputError').html('New password and repeate new password does not match.');
                        } else if (returnData == 202) {
                            $('#oldPasswordInputError').css("display", "block");
                            $('#oldPasswordInputError').html('The old password is incorrect, please try again.');
                        }
                    }
                });
            } else {
                $('#successPasswordMsg').css("display", "none");
                $('#oldPasswordInputError').css("display", "none");
                $('#newasswordInputError').css("display", "none");
                $('#newRepeatPasswordInputError').css("display", "block");
                $('#newRepeatPasswordInputError').html('The new passwords did not match, please try again.');
            }
        }
    }
}