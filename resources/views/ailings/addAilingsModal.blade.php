<div class="modal fade " id="addAilingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addAilingModalLabel">Add Common illness</h5>
                <button type="button" class="close" data-formname="add_ailings" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post','url'=>'/api/ailings','name'=>'add_ailings' ,'id' => 'addAilingFormId']) !!}
            <div class="modal-body">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-6">
                            {{ Form::label("Common illness Name: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            {{Form::text("name", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter Ailing name",
                                    "id" => "name", 
                                    "required" => "required",
                                 ])}}     

                            <span class="form-text text-muted errorMessage" id="name_error">Please enter Common illness name</span>
                        </div>
                        <div class="form-group col-6">
                            {{ Form::label("Status: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            {!!Form::select('status',['' => 'Please Select','active' => 'Active','inactive' => 'Inactive'], 
                            'active',['class' => 'form-control', 'required'=>'required','id' => 'status']) !!}   

                            <span class="form-text text-muted errorMessage" id="status_error">Please Select Status</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="add_ailings" data-dismiss="modal" >Cancel</button>
                <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='addAilingSubmitBtnId'>Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
