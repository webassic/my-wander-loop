<div class="modal fade " id="editAilingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['method' => 'put','url'=>'/api/ailings','name'=>'edit_ailings' ,'id' => 'editAilingFormId']) !!}
            <div class="modal-header">
                <h5 class="modal-title" id="editAilingModalLabel">Edit Common illness</h5>
                <button type="button" class="close" data-formname="edit_ailings" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-6">
                            {{ Form::label("Common illness Name: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            {{Form::text("name", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter Ailing name",
                                    "id" => "e_name", 
                                    "required" => "required",
                                 ])}}     

                            <span class="form-text text-muted errorMessage" id="e_name_error">Please enter Common illness name</span>
                        </div>
                        <div class="form-group col-6">
                            {{ Form::label("Status: ", null, ['class' => 'control-label']) }}

                            <?php $status = ['active' => 'Active', 'inactive' => 'Inactive']; ?>
                            {!! Form::select('status',$status, null,['class' => 'form-control','id' =>'estatus']) !!}
                            <span class="form-text text-muted">Please Select Status</span>
                        </div>
                    </div>
                </div>
                {{Form::hidden('id',null,['id'=>'eid'])}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="edit_ailings" data-dismiss="modal" >Cancel</button>
                <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='editAilingSubmitBtnId'>Update</button>

            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
