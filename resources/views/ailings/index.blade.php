@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/ailings')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/ailings')}}" class="kt-subheader__breadcrumbs-link">
                    Common illness</a>

            </div>
        </div>
    </div>
</div>

<div class="row" id="successMsg"> 
    @if(Session::has('success'))
    <div class="col-sm-12 alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Common illness List
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <button type="button" class="btn btn-default yellow_bg_color black_text_color btn btn-primary"
                            data-toggle="modal" data-target="#addAilingsModal">
                        <i class="la la-plus black_text_color"></i>Add Common illness</button>
                </div>	
            </div>		
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row" > 
            <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                <div class="alert-text" id="success-msg"></div>
            </div> 
            <div class="alert alert-custom alert-danger fade show col-12" role="alert" id="danger-alert" style='display: none;'>                                               
                <div class="alert-text" id="error-msg"></div>
            </div> 
        </div> 
        <div class="row">
            <div class="col-sm-3" id='listing_status'>
                <div class="form-group">
                    {!! Form::label('Status') !!}
                    <?php $status = ['active' => 'Active', 'inactive' => 'Inactive']; ?>
                    {!! Form::select('status',['' => 'Status'] +$status, null,['class' => 'form-control','id' =>'status']) !!}
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable" id="ailings-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Created On</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- end:: Content -->
@include('ailings.addAilingsModal')
@include('ailings.editAilingsModal')

@endsection
@push('scripts')
<script src="{{ asset('js/ailings/ailings.js') }}" defer></script>
@endpush
