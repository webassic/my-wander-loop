<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePasswordModalLabel">Change Password</h5>
                <button type="button" class="close" data-formname="change_password" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12" id='change_password_status'></div>
                </div>
                {!! Form::open(['method' => 'POST','id'=>'change_password','name'=>'change_password']) !!}
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="current_password" required="required" autocomplete="current-password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
                        <div class="col-md-6">
                            <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Confirm New Password</label>
                        <div class="col-md-6">
                            <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password" required="required">
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="button" class="btn btn-secondary model-close" data-formname="change_password" data-dismiss="modal">Close</button>
                            <button type="Submit" class="btn btn-primary mr-2" id='change_password_submit'>Update</button>
                            <img src="{{url('\images\loader\light_blue_material_design_loading.gif')}}" id="changePasswordFormIcon" alt="alt" style="width:7%;margin-right: 12px;float: right;display: none; margin-top: -15px;" />
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>    
        </div>
    </div>
</div>