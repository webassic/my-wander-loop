<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
    <head>
        <base href="../../../">
        <meta charset="utf-8" />
        <title>My Wander Loop | Login</title>
        <meta name="description" content="Login page example">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

        <!--end::Fonts -->
        <link href="{{asset('css/login-6.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link rel="shortcut icon" href="{{url('/images/favicon/myWanderLoop_Icon.png')}}" style="flex: 0 1 auto;height: 50px;width: 50px;border-radius: 50%;"/>
        <style>
            *{
                color:#000;
            }
            @media (min-width:360px) and (max-width:767.9px)
            {
                html, body {
                    font-size: 15px;
                }
            }

        </style> 
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root" id="app">
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                    <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                        <div class="kt-login__wrapper">
                            <div class="kt-login__container">
                                <div class="kt-login__body">
                                    <div class="kt-login__logo  text-center">
                                        <a href="https://mywanderloop.com">
                                            <img src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"> <br>
                                            <div style="font-style: oblique; font-size: 12px;">securing your freedom</div>
                                        </a>
                                    </div>
                                    <div class="kt-login__signin">
                                        <div class="m-login__head text-center">
                                            <h3 class="m-login__title">
                                                Welcome Back!
                                            </h3>
                                        </div>
                                        <div class="panel-heading text-center"> Login to manage your MyWanderLoop account</div>
                                        @if ($errors->any())
                                        <div class="alert alert-danger" id="errorMessage">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif  

                                        @if(session()->has('message'))
                                        <div class="alert alert-success" id="successMessage">
                                            {{ session()->get('message') }}
                                        </div>
                                        @endif
                                        @if(session()->has('error'))
                                        <div class="alert alert-danger" id="errorMessage">
                                            {{ session()->get('error') }}
                                        </div>
                                        @endif
                                        <div class="kt-login__form">
                                            <!--begin::Form-->
                                            <form method="POST" class="kt-form" action="{{ route('login') }}" id="kt_login_form" novalidate="novalidate">
                                                @csrf
                                                <div class="form-group">
                                                    <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="kt-login__extra">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="remember"> Remember me
                                                        <span></span>
                                                    </label>
                                                    @if (Route::has('password.request'))
                                                    <a class="kt_login_forgot" href="{{ route('password.request') }}"s>
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                    @endif

                                                </div>
                                                <!--begin::Action-->
                                                <div class="kt-login__actions">
                                                    <button id="btn btn-default" type="submit" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648; color: #74788d; font-weight: bold">Sign In</button>
                                                </div>
                                                <div class="kt-login__account">
                                                    <span class="kt-login__account-msg">
                                                        Don't have an account yet ?
                                                    </span>&nbsp;&nbsp;
                                                    <a href="{{route('register')}}" id="kt_login_signup" class="kt-login__account-link" style=" font-weight: bold;">Sign Up!</a>
                                                </div>
                                                <!--end::Action-->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-color: #F6C648;">
                        <div class="kt-login__section ">
                            <div class="kt-login__block text-center">
                                <img src="/images/Easy_to_wear.png" alt="Easy to wear" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Page 

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#errorMessage').delay(6000).fadeOut();
                $('#successMessage').delay(6000).fadeOut();
            });
        </script>
        <!--end::Page Scripts -->
    </body>

    <!-- end::Body -->
</html>