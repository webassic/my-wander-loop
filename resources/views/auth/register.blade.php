<!DOCTYPE html>
<html lang="en">

    <!-- begin::Head -->
    <head>
        <base href="../../../">
        <meta charset="utf-8" />
        <title>My Wander Loop | Login</title>
        <meta name="description" content="Login page example">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

        <!--end::Fonts -->


        <link href="{{asset('css/login-6.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link rel="shortcut icon" href="{{url('/images/favicon/myWanderLoop_Icon.png')}}" style="flex: 0 1 auto;height: 50px;width: 50px;border-radius: 50%;"/>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <style>
            *{
                color:#000;
            }
            .marginTop20 {
                margin-top: 20px !important;
            }
            #hiddenRecaptcha-error {
                margin-top: 0.5rem !important;
            }
            .loadingImageLoader{
                display: none;
                position:fixed;
                top:0px;
                right:0px;
                width:100%;
                height:100%;
                background-color:#666;    
                background-repeat:no-repeat;
                background-position:center;
                z-index:10000000; 
                opacity: 0.4;
            }
            @media (min-width:360px) and (max-width:767.9px)
            {
                html, body {
                    font-size: 15px;
                }
                .kt-login.kt-login--v6 .kt-login__content .kt-login__section{
                    margin-top:0px !important;
                    align-self:center !important;
                }
            }
            .kt-login.kt-login--v6 .kt-login__content .kt-login__section{
                margin-top:10vh;
                align-items: unset !important;
            }

            @media (max-width:767.9px)
            {
                .g-recaptcha {
                    transform:scale(0.77);
                    -webkit-transform:scale(0.77);
                    transform-origin:0 0;
                    -webkit-transform-origin:0 0;
                } 
            }

        </style> 
    </head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root" id="app">
             <div class="loadingImageLoader" style="background-image:url({{('/images/loading.gif')}});"></div>
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                    <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                        <div class="kt-login__wrapper">
                            <div class="kt-login__container">
                                <div class="kt-login__body">
                                    <div class="kt-login__logo">
                                        <a href="https://mywanderloop.com">
                                            <img src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"> <br>
                                            <div style="font-style: oblique; font-size: 12px;">securing your freedom</div>
                                        </a>
                                    </div>
                                    <div class="kt-login__signup_new">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title" style="font-weight: bold;">Sign Up</h3>
                                            <div class="kt-login__desc">Enter your details to create your account:</div>
                                        </div>
                                        <div class="kt-login__form">
                                            <div class="row">
                                                <div class="col-sm-12" id='success_status'></div>
                                            </div>
                                            <div class="alert alert-custom alert-success" role="alert" id="success-alert" style='display: none;'>
                                                <div class="alert-text" id="success-msg"></div>
                                                <!--                                                <div class="alert-close">
                                                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-top: -15px;">
                                                                                                        <span aria-hidden="true"><i class="ki ki-close">X</i></span>
                                                                                                    </button>
                                                                                                </div>-->
                                            </div>
                                            <input type="hidden" id="baseUrl" value="{{url('/')}}">
                                            <form class="kt-form" id="registerFormId" action="{{url('/api/postregister')}}">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="First Name" name="firstname" id="firstname" autofocus="autofocus">
                                                    <span id="firstnameerror" class="servererror" style="display:none"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Last Name" name="lastname" id="lastname">
                                                    <span id="lastnameerror" class="servererror " style="display:none"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="email" placeholder="Email" name="email" autocomplete="off" id="email">
                                                    <span id="emailerror" class="servererror" style="display:none"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="tel" placeholder="Mobile" name="mobile" autocomplete="off" id="mobile" maxlength="15">
                                                    <span id="mobileerror" class="servererror" style="display:none"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="password" placeholder="Password" name="password" id="password">
                                                    <span id="passworderror" class="servererror" style="display:none"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control " type="password" placeholder="Confirm Password" name="confirmed_password" id="confirmed_password">
                                                    <span id="confirmed_passworderror" class="servererror" style="display:none"></span>
                                                </div>
                                                <div class="form-group m-form__group marginTop20">
                                                    <div id="recaptcha">
                                                        <div class="wrap-validation">
                                                            <div class="check-val">
                                                                <div class="g-recaptcha" data-sitekey="6LfazrwZAAAAACpStbLrRKufhHD17wqL3OjHzrPy" style="transform:scale(1.12);-webkit-transform:scale(1.12);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <br>
                                                    <span class="servererror col-md-8 col-md-offset-2" id="g-recaptcha-responseerror" style="display:none"></span>

                                                </div>
                                                <div class="kt-login__extra">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="agree" id="agree"> I Agree the <a target="_blank" href="{{ url('/terms-and-conditions')}}"><p><u style="color: blue;">terms and conditions</u></p></a>.
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="kt-login__actions">
                                                    <button id="registerFormSubmitId" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648; color: #74788d; font-weight: bold;">Sign Up</button>
                                                    <!--<a  href="{{ route('login')}}" class="btn btn-outline-brand btn-pill">Cancel</a>-->
                                                </div>
                                                <div class="kt-login__account">
                                                    <span class="kt-login__account-msg">
                                                        Already have an account ?
                                                    </span>&nbsp;&nbsp;
                                                    <a href="{{ route('login')}}" id="btn btn-link" class="kt-login__account-link" style="font-weight: bold;">Sign In!</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-color: #F6C648;">
                        <div class="kt-login__section ">
                            <div class="kt-login__block text-center">
                                <img src="/images/Easy_to_wear.png" alt="Easy to wear" style="width: 100%;">
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#5d78ff",
            "dark": "#282a3c",
            "light": "#ffffff",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995"
        },
        "base": {
            "label": [
                "#c5cbe3",
                "#a1a8c3",
                "#3d4465",
                "#3e4466"
            ],
            "shape": [
                "#f0f3ff",
                "#d9dffa",
                "#afb4d4",
                "#646c9a"
            ]
        }
    }
};
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script>
jQuery(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
        }
    });
});
        </script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/login/login-general.js') }}" type="text/javascript"></script>


        <!--end::Page Scripts -->
    </body>

    <!-- end::Body -->
</html>