
<div class="modal fade " id="deleteBandTypesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <form class="form" action="{{url('/api/bandtypes')}}" id="deleteBandTypeFormId">
            <div class="modal-header">
                <h5 class="modal-title" id="addGroupModalLabel">Delete Band Type</h5>
                <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                    {{Form::hidden('bandTypeId','',['id'=>'bandTypeId'])}}                
                    <div class="col-12">Are you sure you want to delete this Band Type Details?</div>
          
            </div>
           
           <div class="modal-footer">
                <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="deleteBandType" data-dismiss="modal" >No</button>
                <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='deleteBandTypeSubmitBtnId'>Yes</button>
                
            </div>
           </form>
           
        </div>
    </div>
</div>
