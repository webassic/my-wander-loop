
<div class="modal fade " id="viewBandTypesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" action="{{url('/api/bandtypes')}}" id="viewBandTypeFormId">
            <div class="modal-header">
                <h5 class="modal-title" id="viewBandTypeModalLabel">View Band Type</h5>
                <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-6">
                                {{ Form::label("Band Name: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                                {{Form::text("band_name", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter band name",
                                    "id" => "vband_name", 
                                    "disabled" => "disabled",
                                 ])}}     
        
                                <span class="form-text text-muted errorMessage" id="eband_name_error">Please enter band name</span>
                            </div>
                            <div class="form-group col-6">
                                    {{ Form::label("Color: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                                    {{Form::text("color", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter band color",
                                    "id" => "vcolor", 
                                    "disabled" => "disabled",
                                 ])}}    
                                    <span class="form-text text-muted">Please enter color</span>
                            </div>
                        </div>
                        <div class="row">
                             <div class="form-group col-6">
                                    {{ Form::label("Description: ", null, ['class' => 'control-label']) }}
                                    {{Form::textarea("description", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter description",
                                    "id" => "vdescription", 
                                    "rows"=>"2",
                                    "style"=>"resize:none;",
                                    "disabled"=>"disabled"
                                 ])}}    
                                    <span class="form-text text-muted">Write something about band</span>
                            </div>
                             <div class="form-group col-6">
                                    {{ Form::label("Status: ", null, ['class' => 'control-label']) }}
                                    
                                    <?php $status = ['active' => 'Active', 'inactive' => 'Inactive']; ?>
                                    {!! Form::select('status',$status, null,['class' => 'form-control','id' =>'vstatus','disabled'=>'disabled']) !!}
                                    <span class="form-text text-muted">Please select status</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                               {{ Form::label("Glow in dark: ", null, ['class' => 'control-label']) }}
                                
                                <div class="kt-radio-inline ">
                                    <label class="kt-radio kt-radio--warning">
                                             {{ Form::radio('glow_dark', 'yes','0',['disabled'=>'disabled']) }} Yes
                                            <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--danger">
                                        {{ Form::radio('glow_dark', 'no','1',['disabled'=>'disabled']) }} No
                                        <span></span>
                                    </label>
                                </div>
                                <span class="form-text text-muted">Do you want band to be glow in drak?</span>
                            </div>
                            <div class="form-group col-6">
                                {{ Form::label("Fristname Printed On Band: ", null, ['class' => 'control-label']) }}

                                 <div class="kt-radio-inline">
                                     <label class="kt-radio kt-radio--warning">
                                              {{ Form::radio('firstname_printed', 'yes','0',['disabled'=>'disabled']) }} Yes
                                             <span></span>
                                     </label>
                                     <label class="kt-radio kt-radio--danger">
                                         {{ Form::radio('firstname_printed', 'no','1',['disabled'=>'disabled']) }} No
                                         <span></span>
                                     </label>
                                 </div>
                                 <span class="form-text text-muted">Do you want first name to be print on band?</span>
                             </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                {{ Form::label("Mobile Number Printed On Band: ", null, ['class' => 'control-label']) }}

                                 <div class="kt-radio-inline">
                                     <label class="kt-radio kt-radio--warning">
                                              {{ Form::radio('mobile_printed', 'yes','0',['disabled'=>'disabled']) }} Yes
                                             <span></span>
                                     </label>
                                     <label class="kt-radio kt-radio--danger">
                                         {{ Form::radio('mobile_printed', 'no','1',['disabled'=>'disabled']) }} No
                                         <span></span>
                                     </label>
                                 </div>
                                 <span class="form-text text-muted">Do you want mobile number to be print on band?</span>
                             </div>
                            <div class="form-group col-6">
                                {{ Form::label("Scanner Phone Number: ", null, ['class' => 'control-label']) }}

                                 <div class="kt-radio-inline ">
                                     <label class="kt-radio kt-radio--warning">
                                              {{ Form::radio('scanner_phone_number', 'yes','0',['disabled'=>'disabled']) }} Yes
                                             <span></span>
                                     </label>
                                     <label class="kt-radio kt-radio--danger">
                                         {{ Form::radio('scanner_phone_number', 'no','1',['disabled'=>'disabled']) }} No
                                         <span></span>
                                     </label>
                                 </div>
                                 <span class="form-text text-muted">Do you want scanner phone number to be print on band?</span>
                             </div>
                        </div>   
                    </div>
                   {{Form::hidden('id',null,['id'=>'vid'])}}
                
          
            </div>
           
           <div class="modal-footer">
                <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="view_band_types" data-dismiss="modal" >Cancel</button>
                <button type="button" class="btn btn-default yellow_bg_color black_text_color" id='ViewBandTypeSubmitBtnId' onclick="openEditMOdal()">Click here to Update</button>
                
            </div>
           </form>
           
        </div>
    </div>
</div>
