@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/admin-transactions/index')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/admin-transactions/index')}}" class="kt-subheader__breadcrumbs-link">
                   Display Subscriptions Transactions </a>
            </div>
        </div>
    </div>
</div> 
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Display Subscriptions Transactions
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <a  href="{{ URL::to('admin-transactions/export_banduser_transactions') }}" class="add-margin btn btn-default btn-icon-sm yellow_bg_color black_text_color" >
                                <i class="la la-download"></i> Export to Excel
                            </a>                             
                        </div>                         
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row" > 
                @if(session()->has('message'))
                <div class="alert alert-success col-md-12" id="successMessage">
                    {{ session()->get('message') }}
                </div>
                @endif
            </div> 
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="admin_banduser_transactions">
                <thead>
                    <tr>
                        <th>Guardian Name</th>
                        <th>Band User Name</th>
                        <th>Payment Date</th>
                        <th>Payment ID</th>
                        <th>Subscription Start Date</th>
                        <th>Subscription End Date</th>
                        <th>Invoice Number</th>
                        <th>Status</th>
                        <th>Package Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<!-- end:: Content -->

@endsection
@push('scripts')
<script src="{{ asset('js/bandusers/banduser_transactions.js') }}" defer></script>
@endpush
