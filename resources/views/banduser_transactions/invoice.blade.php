<style>
    body{
        font-family: Poppins,Helvetica,sans-serif !important;
    }
    .paragraph
    {
        margin:0px;
    }
    .border-bot
    {
        border-bottom: 2px solid lightgray;
    }
    table {
        border-collapse: collapse;
        width: 100%;
        font-size: 13px;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

</style> 

<div style="width:100%;height:330px;">
    <div style="width:40%;float:left;">
        <img src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"> <br>
        <div style="font-style: oblique; font-size: 14px; margin-left: 25px;">securing your freedom</div>       
        <div style="margin-top:10px;">
            <p class="paragraph" style="margin-top:20px;font-size:14px;">
                <span> Bill To :</span> - {{$invoice['guardianName']}}    
            </p>

            <p class="paragraph" style="font-size:14px;">
                <span> Address :</span> -  {{$invoice['guardianAddress']}}
            </p> 
        </div>        
    </div>
    <div style="width:60%;float:right;">
        <div>
            <p class="paragraph" style="font-size:14px;">
                Sare Jahan Se Acha Socio Eco Pol Foundation (P) Ltd   
            </p>
            <p class="paragraph" style="font-size:14px;">
                (A section 8 Not for Profit Company)
            </p> 
            <p class="paragraph" style="font-size:14px;">
                Regd Office: E-102 Kalpataru Regency Phase II, 
            </p> 
            <p class="paragraph" style="font-size:14px;">
                Lane 10, Kalyani Nagar, Pune 411 006, India
            </p> 
            <p class="paragraph" style="font-size:14px;">
                Email - service@mywanderloop.com
            </p> 
            <p class="paragraph" style="font-size:14px;">
                CIN - U85100PN2013NPL149608
            </p> 
            <p class="paragraph" style="font-size:14px;">
                GST - 27AAUCS0138A1ZH
            </p> 
        </div>        
    </div>
    <div style="width:60%;float:left;margin-top:150px;">
        <span style="width:100%;">
            MyWanderLoop Subscription Statement
        </span>
        <p class="paragraph border-bot" style="margin-top:10px;font-size:14px;">
            Email or talk to us about your account or bill, visit https://mywanderloop.com/contactus
        </p>
        <p class="paragraph border-bot" style="margin:5px 0px;width:100%;">
            Invoice Summary
        </p>
        <div style="width:100%;">
            <p class="paragraph " style="width:50%;float:left;font-size:14px;">
                Invoice Number : 
            </p>
            <p class="paragraph " style="width:50%;float:right;text-align:right;font-size:14px;">
                {{$invoice['invoiceNumber']}}    
            </p>
        </div><br>
        <div style="width:100%;">
            <p class="paragraph border-bot" style="width:50%;float:left;font-size:14px;">
                Invoice Date : 
            </p>
            <p class="paragraph border-bot" style="width:50%;float:right;text-align:right;font-size:14px;">
                {{$invoice['invoiceDate']}}    
            </p>
        </div> <br><br>
        <div style="width:100%;">
            <span class="paragraph border-bot" style="width:50%;float:left;">
                TOTAL AMOUNT DUE : 
            </span>
            <span class="paragraph border-bot" style="width:50%;float:right;text-align:right;">
                {{$invoice['currency']}} {{$invoice['totalAmount']}} .00
            </span>
        </div> 
    </div>    
</div><br>
<div style="width:100%;height:65px;">
    <p style="font-size:18px;">This Account Summary is for the billing period <i>{{$invoice['paymentStartDate']}} - {{$invoice['paymentEndDate']}} </i><p>
    <p style="font-size:14px;margin-top:-3px;">Greetings from MyWanderLoop. We're writing to provide you with an account summary of your use of services. Additional information about your bill, individual service charge details, and your account history are available on the Transactions Page.</p>
</div><br><br>
<table>
    <tr><th colspan="4" style="background-color:lightblue;">Summary</th></tr>
    <tr style="background-color:#dddddd;">
        <th>Description</th>
        <th>Quantity</th>
        <th>Rate</th>
        <th>Amount</th>
    </tr>     
    <tr>
        <td>myWanderLoop Yearly Subscription for Silicone Band and Service: {{$invoice['packageName']}} Package<br/>
            <?php
            foreach ($invoice['bandUserList'] as $bandUser) {
                echo $bandUser['name'] . "<br/>";
            }
            ?>
        </td>
        <td>{{$invoice['quantity']}}</td>
        <td>{{$invoice['currency']}} {{$invoice['orignalCost']}}.00</td>
        <td>{{$invoice['currency']}} {{$invoice['quantity'] * $invoice['orignalCost']}}.00</td>
    </tr>
    <?php if ($invoice['guardianState'] != 'maharashtra') { ?>
        <tr>
            <td>IGST ({{$invoice['gstRate']}}%) </td>
            <td>{{$invoice['quantity']}}</td>
            <td>{{$invoice['currency']}} {{$invoice['gstAmount']}}.00</td>
            <td>{{$invoice['currency']}} {{$invoice['quantity'] * $invoice['gstAmount']}}.00</td>
        </tr>
        <?php
    } else {
        $divideGSTRate = ($invoice['gstRate'] / 2);
        $divideGSTAmount = ($invoice['gstAmount'] / 2);
        ?>
        <tr>
            <td>CGST ({{$divideGSTRate}}%) </td>
            <td>{{$invoice['quantity']}}</td>
            <td>{{$invoice['currency']}} {{$divideGSTAmount}}.00</td>
            <td>{{$invoice['currency']}} {{$invoice['quantity'] * $divideGSTAmount}}.00</td>
        </tr>
        <tr>
            <td>SGST ({{$divideGSTRate}}%) </td>
            <td>{{$invoice['quantity']}}</td>
            <td>{{$invoice['currency']}} {{$divideGSTAmount}}.00</td>
            <td>{{$invoice['currency']}} {{$invoice['quantity'] * $divideGSTAmount}}.00</td>
        </tr>
    <?php } ?>

    <tr style="background-color:#dddddd;">
        <td colspan="3"> Total for this statement </td>
        <td> {{$invoice['currency']}} {{$invoice['totalAmount']}}.00</td>
    </tr>
</table> 

<div style="width:100%;position:absolute;">
    <p style="padding:5px 10px;font-size: 12px;width:50%;float:left;color:gray;text-align:justify;">Please note there are no usage charges as of now. This is the total charge for the subscription for one year.  The invoice is raised on the person paying for the Band and Services.  The product will be dispatched to the Band User's address mentioned while filling up the form.  All charges and prices are in Indian Rupees. myWanderLoop products and services are sold by Sare Jahan Se Acha Socio Eco Pol Foundation (P) Ltd.</p>
    <p style="padding:5px 10px;font-size: 12px;width:45%;float:left;text-align: left;color:black;">Payment details: Invoice is due on the date of receipt. Payments are automatically charged to your associated payment method. </p>

</div><br>
<div style="width:100%;position:absolute;top:950px;"> 
    <p style="text-align:center;border-top:1px solid lightgray;font-size: 12px;">This is a computer generated invoice and need not be signed. Any queries may please be addressed to service@mywanderloop.com quoting invoice number.</p>
</div>

