<div class="modal fade" id="aadhaarVerificationStepsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Aadhaar Card Verification Steps</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>

            <div class="modal-body">
                <div class="row">                     
                    <div class="col-sm-12">
                        <ol>
                            <li>Click on the button "UIDAI Verification" - this will open UIDAI.GOV.IN web page in a new browser tab.</li>
                            <li>Please navigate to the new browser tab of "Aadhaar Paperless Offline e-KYC".</li>
                            <li>Enter your 12 Digit Aadhaar Card number and security code at the Aadhaar card verification form in new window.</li>
                            <li>Receive and verify OTP on your registered Mobile Number.</li>
                            <li>Provide a 4 digit Share Code and memorize it.</li>
                            <li>Download the Zip file.</li>
                            <li>Upload the Zip file, provide last 4 digit of Aadhaar card and 4 digit Share code below and complete the form.</li>
                            <li>After downloading the zip file from the UIDAI website, kindly return to 
mywanderloop website to upload the zip file.</li>
                        </ol>
                    </div>

                </div>	                

            </div>
            <div class="modal-footer">
                {{Form::button('Ok',['data-dismiss'=>"modal","class" => "btn btn-default yellow_bg_color black_text_color mr-2"])}}
            </div>
        </div>
    </div>
</div>
