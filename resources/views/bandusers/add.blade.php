@extends('layouts.app')
<style>     
    table  
    {
        border: 1px solid #ccc;
        border-collapse: collapse;
    }
    table th
    {
        background-color: #F7F7F7;
        font-size: 1rem;
        font-weight: 600;
        color: #646c9a;
    }
    table th, table td
    {
        padding: 5px;
        border-color: #ccc;
        font-size: 12px;
    }
    .ailingOtherOption{
        padding-top: 5px;        
    }
    .displayNone{
        display: none !important;
    }
    .horizontalLine{
        border-bottom: 1px dashed #ecf0f3;
        margin-bottom: 15px;
    }

    input[type="file"] {
        display: block;
    }
    .imageThumb {
        height: 150px;
        max-height: 150px;
        width: 150px;
        border: 2px solid;
        padding: 1px;
        cursor: pointer;
    }
    .pip {
        display: inline-block;
        margin: 10px 10px 0 0;
    }
    .remove {
        display: block; 
        /*color: white;*/
        text-align: center;
        cursor: pointer;
    }
    .remove i{
        float: right !important;
        padding-bottom: 5px;
    }
    .remove:hover {
        background: white;
        color: black;
    }
    .kt-checkbox>span {
        border: 1px solid #3699ff !important;
        border-width: 2px !important;
    }
    .kt-checkbox>input:checked~span {
        border: 1px solid #3699ff !important;
        border-width: 2px !important;
    }
    .kt-checkbox>span:after {
        border: solid #3699ff !important;
    }
    .table-bordered {
        border: 1px solid #FFF !important;
    }
    .table-responsive {
        display:inline-table !important;
    }

    @media (min-width:360px) and (max-width:767.9px)
    {
        .table-responsive {
            display:inline !important;
        }
    }
    #spnFilePath{
        border: 3px solid #fd397a;
        padding: 5px;     
        margin-top: -15px;
    }    
</style>

@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-link">
                    Band Users </a>
            </div>
        </div>
    </div>
</div>
{!! Form::open(array('url' => '/api/bandusers','id'=>'bandUserFormId',"enctype"=>"multipart/form-data")) !!}
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-user"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                <?php
                if ($bandUserCnt > 1) {
                    echo 'Add Details of Band User Number <span id="bandUserCount">1</span> of <span id="finalBandUserCount">' . $bandUserCnt . '</span>';
                    $submitText = 'Submit and Add Next Band User';
                } else {
                    echo 'Add Details of Band User';
                    $submitText = 'Submit';
                }
                ?> 
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" id='add_banduser_status'></div>
        <div class="col-sm-12" id='error_banduser_status'></div>
    </div>
    <div class="card-body" id="showHideBandUserDetail">
        <div class="form-group row">
            <div class="col-lg-6">
                <h5> <span class="text-danger"> Band User Contact Information </span>                    
                </h5>
            </div>
        </div>   
        <div class="row">
            <div class="col-lg-4">
                {{ Form::label("Title:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::select("title",$titles, '',["class" => "form-control","placeholder" => "Select Title", "id" => "title",'tabindex' => '1','autocomplete'=>'off'])}}                
                </div>                        
            </div>
            <div class="col-lg-4">
                {{ Form::label("First Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::text("firstname", '', ["class" => "form-control","placeholder" => "Enter First Name", "id" => "firstname","required" => "required",'tabindex' => '2','autocomplete'=>'off'])}}                              
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Last Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::text("lastname", '', ["class" => "form-control","placeholder" => "Enter Last Name", "id" => "lastname",'tabindex' => '3','autocomplete'=>'off'])}}                                      
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-lg-4">
                {{ Form::label("Date of Birth: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                <div class="input-group date" >
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="la la-calendar"></i>
                        </span>
                    </div>
                    {{Form::select("year",$datearray['year'],'',
                                [
                                   "class" => "form-control  kt-selectpicker dobvalidate",
                                   "id" => "year",    
                                   "title" => "Year",
                                   'tabindex' => '4'
                                 ])
                    }}
                    &nbsp;
                    {{Form::select("month",$datearray['month'],'',
                                [
                                   "class" => "form-control  kt-selectpicker dobvalidate",
                                   "id" => "month",    
                                   "title" => "Month",
                                   'tabindex' => '5'
                                 ])
                    }}  &nbsp;
                    {{Form::select("date",$datearray['date'],'',
                                [
                                   "class" => "form-control  kt-selectpicker",
                                   "id" => "date",    
                                   "title" => "Date",
                                   'tabindex' => '6'
                                 ])
                    }}
                </div>
                <div id="validatedob" style="color:#EF4F7A;font-size:11px;"></div>
                <div id="doberror" class="error" ></div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Blood Group", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-tint"></i></span></div>
                    {{Form::select("blood_group",$bloodGroupArray, '',
                                [
                                   "class" => "form-control",
                                   "placeholder" => "Select Blood Group",
                                   "id" => "blood_group",
                                   'tabindex' => '7','autocomplete'=>'off'
                                ])
                    }}                             
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Guardian's Relationship with Band User:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::select("relationship_with_applicant",$relationShipWithGuardian, '',
                                [
                                   "class" => "form-control",
                                   "placeholder" => "Select Guardian's Relationship with Band User",
                                   "id" => "relationship_with_applicant",'tabindex' => '8','autocomplete'=>'off'
                                ])
                    }} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                {{ Form::label("Address:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("address", '', ["class" => "form-control","placeholder" => "Enter Address", "id" => "address",'tabindex' => '9','autocomplete'=>'off'])}}                              
                </div>
            </div> 
            <div class="col-lg-4">
                {{ Form::label("Locality:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("locality", '', ["class" => "form-control","placeholder" => "Enter Locality", "id" => "locality",'tabindex' => '10','autocomplete'=>'off'])}}                                      
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("City:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("city", '', ["class" => "form-control","placeholder" => "Enter City", "id" => "city",'tabindex' => '11','autocomplete'=>'off'])}}                                      
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-lg-4">
                {{ Form::label("State:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("state", '', ["class" => "form-control","placeholder" => "Enter State", "id" => "state",'tabindex' => '12','autocomplete'=>'off'])}}                              
                </div>
            </div> 
            <div class="col-lg-4">
                {{ Form::label("Pincode:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                    {{Form::text("pin", '', ["class" => "form-control","placeholder" => "Enter Pincode", "id" => "pin",'tabindex' => '13',"maxlength" => "6",'autocomplete'=>'off'])}}                                      
                </div>
            </div> 
            <div class="col-lg-4">
                {{ Form::label("Languages known:", null, ['class' => 'control-label']) }}
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-language" aria-hidden="true"></i></span></div>
                    {{Form::text("languages_known", '', ["class" => "form-control","placeholder" => "Enter Languages known",'tabindex' => '14', "id" => "languages_known","maxlength" => "200",'autocomplete'=>'off'])}} 
                </div>
            </div>

        </div> 
        <div class="form-group row">
            <div class="col-lg-4">
                {{ Form::label("Upload Image: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                <div class="input-group field" id="imageDiv">
                    <input type="file"  required="" id="files" name="files[]" accept="image/*" tabindex="15"/>
                </div> 
                <div class="help_text"> For best results please upload 150 X 150 pixel image.</div>
            </div>
            <div class="col-md-8">
                <div id="addImagesDiv">
                </div>  
                <div id="fileError" class="error" style="display: none;">User can upload only one photo.</div>
            </div>
        </div>
        <div class="horizontalLine"></div>
        <!-- Start Contact Information -->
        <div class="row">
            <div class="col-lg-6">
                <h5 > <span class="text-danger">During Emergency Contact Information</span>                    
                </h5>
            </div>
        </div>                  
        <div class="kt-portlet__body kt-portlet__body--fit-top">
            <div class="form-group row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="mobileLable">
                        
                    {{ Form::label("", null, ['class' => 'control-label']) }}Primary Mobile number in India <br/>to be printed on the Band:<span class="text-danger">*</span>       
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-mobile"></i></span></div>
                        {{Form::text("mobile", '', ["class" => "form-control","placeholder" => "Enter Mobile", "id" => "mobile","minlength" => "10","maxlength" => "10",'tabindex' => '16','autocomplete'=>'off'])}} 
                    </div>
                    <div class="help_text"> An OTP will be sent to this number.</div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="alternateMobileLable">
                        {{ Form::label("", null, ['class' => 'control-label']) }}Secondary Mobile number in India <br/>to be contacted:    
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-mobile"></i></span></div>
                        {{Form::text("alternate_mobile", '', ["class" => "form-control","placeholder" => "Enter Mobile", "id" => "alternate_mobile","minlength" => "10","maxlength" => "10",'tabindex' => '17','autocomplete'=>'off'])}} 
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="landlineLable">
                        
                    {{ Form::label("Landline (Country code/Area code/Number):", null, ['class' => 'control-label']) }}                
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                        {{Form::text("landline", '', ["class" => "form-control","placeholder" => "Enter Landline", "id" => "landline",'tabindex' => '18',"maxlength" => "15",'autocomplete'=>'off'])}}                              
                    </div>
                </div>                
            </div>

            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("", null, ['class' => 'control-label']) }}Primary Email contact:<span class="text-danger">*</span>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                        {{Form::text("email", '', ["class" => "form-control","placeholder" => "Enter Email", "id" => "email",'tabindex' => '19','autocomplete'=>'off'])}}                                      
                        <div class="text-muted errorMessage" id="email_error"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    {{ Form::label("", null, ['class' => 'control-label']) }}Secondary Email contact:  
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                        {{Form::text("alternate_email", '', ["class" => "form-control","placeholder" => "Enter Email", "id" => "alternate_email",'tabindex' => '20','autocomplete'=>'off'])}}                                      
                    </div>
                </div>                                
            </div>                      
        </div>
        <input type="hidden" value="{{$packageName}}" name="package_name" id="package_name" />       
        <div class="horizontalLine"></div>
        <!-- End Contact Information -->
        <?php if($packageName != env('RAZORPAY_BASIC_PACKAGE')) {?>
        <!-- Start Aadhaar Card Verification -->
        <div class="row">
            <div class="col-lg-6">
                <h5 > <span class="text-danger"> Aadhaar Card Verification </span>                    
                </h5>
            </div>
            <div class="col-lg-12">
                <p>Please click on the link to know safe verification process of your <a href="#" data-toggle="modal" data-target="#aadhaarVerificationStepsModal" style="text-decoration: underline;">Aadhaar card</a></p>
                <a href="https://resident.uidai.gov.in/offline-kyc" target="_blank" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2">
                    <i class="la la-plus"></i>
                    UIDAI Verification
                </a> 
            </div>  
        </div> 
        <br/>
        <div class="kt-portlet__body kt-portlet__body--fit-top">
            <div class="row">  
                <div class="col-lg-3">
                    {{ Form::label("Upload Zip file: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>                    
                    <div class="form-group input-group field" id="aadhharCardDiv">
                        <input type="button" id="btnFileUpload" value="Choose File" />                        
                        <input style="display: none;" type="file" id="aadhaar_card_file" name="aadhaar_card_file" accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed" tabindex="21"/>
                    </div>
                    <div id="uploadFileError" class="error" style="display: none;">Allowed format: zip. Max file size: 2 MB.</div>
                </div>
<!--                <div class="col-lg-4">
                    {{ Form::label("Last 4 digit Aadhaar Card Number :", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                    <div class="form-group input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                        {{Form::text("aadhaar_card_number", '', ["class" => "form-control","placeholder" => "Enter Last 4 digit Aadhaar Card Number", "id" => "aadhaar_card_number","maxlength" => "4",'tabindex' => '22','autocomplete'=>'off'])}}                                      
                    </div>
                </div>-->
                <div class="col-lg-5">
                    {{ Form::label("Enter Share code (as entered in UIDAI website):", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                        {{Form::text("share_code", '', ["class" => "form-control","placeholder" => "Enter Share code (as entered in UIDAI website)", "id" => "share_code","maxlength" => "4",'tabindex' => '23','autocomplete'=>'off'])}}                              
                    </div>
                </div> 
            </div>
            <div class="row">
                    <div class="col-lg-4">
                        <span id="spnFilePath" style="display: none"></span>
                    </div>
                </div>
        </div>
        <!-- End Aadhaar Card Verification -->
        <div class="horizontalLine"></div>
        <!-- Start Medical History -->
        <div class="form-group row">
            <div class="col-lg-12">
                <h5 > <span class="text-danger"> Medical History </span>                    
                </h5>               
            </div>
            <div class="col-sm-12" id='errorMedicalHistory'></div>
            <div class="col-lg-12">
                <p>This section helps keep record of patient's medical history. It is important information and can come handy at the time of wander off events. Please provide details. </p>
                <a href="#" data-toggle="modal" data-target="#addMedicalHistoryModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2" id="addMedicalHistoryLink">
                    <i class="la la-plus"></i>
                    Add Medical History
                </a> 
            </div>            
        </div>
        <label class="kt-checkbox">
            <input type="checkbox" name="medicalHistoryCheckbox" id="medicalHistoryCheckbox">Not Applicable
            <span></span>
        </label>

        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="medicalHistoryDataTable">
                <thead>
                    <tr>
                        <th>Current Illness From</th>
                        <th>Since When</th>
                        <th>Primary Physician Name</th>
                        <th>Secondary Physician Name</th>
                        <th>Last admitted Hospital Name</th>                    
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="noRecordsMedicalHistory"><td colspan="6">No records!!</td></tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        <div class="horizontalLine"></div>
        <!-- Start Wander off Events History -->         
        <div class="form-group row">
            <div class="col-lg-12">
                <h5 > <span class="text-danger">Wander off Events History</span>                    
                </h5>
            </div> 
            <div class="col-sm-12" id='errorWanderOffHistory'></div>
            <div class="col-lg-12">
                <p>This section helps keep record of patient's past wander of events. It is important information and can come handy at the time of wander off events to locate the patient. Please provide details.</p>
                <a href="#" data-toggle="modal" data-target="#addWanderOffEventsHistoryModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2"  id="addwanderOffEventHistoryLink">
                    <i class="la la-plus"></i>
                    Add Wander off Events History
                </a>              
            </div>
        </div>        
        <label class="kt-checkbox">
            <input type="checkbox" name="wanderOffHistoryCheckbox" id="wanderOffHistoryCheckbox">Not Applicable
            <span></span>
        </label>

        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="wanderOffEventHistoryDataTable">
                <thead>
                    <tr>
                        <th>Lost City</th>
                        <th>Lost Location</th>
                        <th>Lost Date</th>
                        <th>Found City</th>
                        <th>Found Location</th>                    
                        <th>Found Date</th>                    
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="noRecordsWandeOffHistory"><td colspan="7">No records!!</td></tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
    <?php } ?>

<!--    <div class="card-body" id="showConfirmBandUserBtn" style="display: none;">
        <h5>Band user's details added successfully. Please click on "Confirm Band Users" button to confirm band user. </h5>
    </div>-->
</div>
<div class="row">
    <div class="col-sm-12" id='below_error_banduser_status'></div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-8">            
            {{Form::submit($submitText,["class" => "btn btn-default yellow_bg_color black_text_color mr-2", "id" => "bandUsersSubmitId", 'tabindex' => '24'])}}
            <a href="#" data-toggle="modal" data-target="#confirmBandUsersModal" class="btn btn-default yellow_bg_color black_text_color mr-2 displayNone"  id="confirmBandUsersBtn">
                Confirm Band Users
            </a>   
            <a href="{{ url('/bandusers') }}" class="btn btn-default black_bg_color yellow_text_color mr-2" tabindex="25">Cancel</a>
        </div>
    </div>
</div>

{{ Form::close() }}

@include('bandusers.addMedicalHistoryModal')
@include('bandusers.aadhaarVerificationStepsModal')
@include('bandusers.showMedicalHistoryModal')
@include('bandusers.editMedicalHistoryModal')
@include('bandusers.addWanderOffEventsHistoryModal')
@include('bandusers.showWanderOffEventHistoryModal')
@include('bandusers.editWanderOffEventHistoryModal')
@include('bandusers.confirmBandUsersModal')
@include('bandusers.bandUserVerifyOtpModal')
@include('bandusers.deleteMedicalWanderOffHistoryModal')
@endsection
@push('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{ asset('js/bandusers/add_banduser.js?'.time()) }}" defer></script>
<script>
var userDetails = '<?= json_encode(\Auth::user(), JSON_HEX_APOS) ?>';
var planAmount = "<?= @$planAmount ?>";
var rzKey = "<?= @$credentials['key_id'] ?>";
var rzimage = "<?= url('/images/favicon/myWanderLoop_Icon.png'); ?>";
$(document).ready(function ()
{
    $('#ailing_id').on('change', function () {
        if ($("#ailing_id option:selected").text() == "Others") { // Need to change dynamic
            $('#otherOptionDiv').removeClass("displayNone");
        } else {
            $('#otherOptionDiv').addClass("displayNone");
        }
    });
    adjustHeight();
    function adjustHeight(){
        var landline = $("#landlineLable").height();
        var alternateMobile = $("#alternateMobileLable").height();
        if(alternateMobile > landline){
            $("#landlineLable").height(alternateMobile);
            $("#mobileLable").height(alternateMobile);
        }else if (landline > alternateMobile){
            $("#alternateMobileLable").height(landline);
            $("#mobileLable").height(landline);
        }
    }
});
</script> 
@endpush
