<div class="modal fade" id="addMedicalHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMedicalHistoryModalLabel">Add Medical History</h5>
                <button type="button" class="close" data-formname="addMedicalHistoryForm" data-dismiss="modal" onclick="resetForm();">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post','id'=>'addMedicalHistoryForm','name'=>'addMedicalHistoryForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Patient's Hospital Preference:", null, ['class' => 'control-label']) }}
                            {{Form::text("hospital_preference", '', ["class" => "form-control","placeholder" => "Enter Patient's Hospital preference", "id" => "hospital_preference",'tabindex' => '-1','autocomplete'=>'off','autofocus' => 'autofocus'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Hospital Registration No.:", null, ['class' => 'control-label']) }}
                            {{Form::text("hospital_registration_no", '', ["class" => "form-control","placeholder" => "Hospital Registration No", "id" => "hospital_registration_no",'autocomplete'=>'off'])}}
                        </div>
                    </div>                                                           
                </div>	                                	
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("City:", null, ['class' => 'control-label']) }} 
                            {{Form::text("hospital_city", '', ["class" => "form-control","placeholder" => "Enter City", "id" => "hospital_city",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Emergency Medication:", null, ['class' => 'control-label']) }}
                            {{Form::text("recent_illness", '', ["class" => "form-control","placeholder" => "Enter Emergency Medication", "id" => "recent_illness",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Current Illness:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                            {!! Form::select('ailing_id',['' => 'Current Illness'] +$getAllAilingsList, null,['class' => 'form-control','id' =>'ailing_id','autocomplete'=>'off']) !!}
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Illness Since:", null, ['class' => 'control-label']) }} 
                            <div class="input-group date" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                                {{Form::text("since_when", '' , $attributes = ["class" => "form-control", "placeholder" => "Select Illness Since","id" => "kt_datepicker_since_when","data-date-end-date"=>"0d", 'autocomplete'=>'off'])}}
                            </div>                           
                        </div>
                    </div>
                </div>
                <div class="row displayNone" id="otherOptionDiv">                     
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Other:", null, ['class' => 'control-label']) }} 
                            {{Form::text("ailing_other", '', ["class" => "form-control","placeholder" => "Other", "id" => "ailing_other",'autocomplete'=>'off'])}}
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Primary Physician Name:", null, ['class' => 'control-label']) }} 
                            {{Form::text("primary_physician", '', ["class" => "form-control","placeholder" => "Enter Primary Physician Name", "id" => "primary_physician",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Secondary Physician Name:", null, ['class' => 'control-label']) }} 
                            {{Form::text("secondary_physician", '', ["class" => "form-control","placeholder" => "Enter Secondary Physician Name", "id" => "secondary_physician",'autocomplete'=>'off'])}}
                        </div>
                    </div>                   
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Last admitted Hospital Name:", null, ['class' => 'control-label']) }} 
                            {{Form::text("admitted_hospital", '', ["class" => "form-control","placeholder" => "Enter Last admitted Hospital Name", "id" => "admitted_hospital",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                </div>  
            </div>
            <div class="modal-footer">
                {{Form::hidden("checkWanderOffButtonFlag", '' , $attributes = ["class" => "form-control", "id" => "checkWanderOffButtonFlag"])}}
                {{Form::submit('Save',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "saveMedicalHistoryBtn"])}}
                {{Form::submit('Save And Add More',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "saveAndMoreMedialHistoryBtn"])}}
                {{Form::button('Cancel',['data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn", "onclick" => "resetForm();"])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
