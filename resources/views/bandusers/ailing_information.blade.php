<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead class="thead-light">
            <tr>
                <th class="kt-font-bolder" scope="col">Patient’s Hospital Preference</th>
                <th class="kt-font-bolder" scope="col">Hospital Registration Number</th>
                <th class="kt-font-bolder" scope="col">City</th>
                <th class="kt-font-bolder" scope="col">Emergency Medication</th>
                <th class="kt-font-bolder" scope="col">Current Illness</th>
                <th class="kt-font-bolder" scope="col">Illness Since</th>
                <th class="kt-font-bolder" scope="col">Primary Physician</th>
                <th class="kt-font-bolder" scope="col">Secondary Physician</th>
                <th class="kt-font-bolder" scope="col">Last admitted Hospital Name</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($data['ailings_details']) && !empty($data['ailings_details']))
            @foreach($data['ailings_details'] as $akey => $avalue)
            <tr>

                <td>{{isset($avalue['hospital_preference']) && !empty($avalue['hospital_preference']) ? $avalue['hospital_preference']:'NA'}}</td>
                <td>{{isset($avalue['hospital_registration_no']) && !empty($avalue['hospital_registration_no']) ? $avalue['hospital_registration_no']:'NA'}}</td>
                <td>{{isset($avalue['hospital_city']) && !empty($avalue['hospital_city']) ? $avalue['hospital_city']:'NA'}}</td>
                <td>{{isset($avalue['recent_illness']) && !empty($avalue['recent_illness']) ? $avalue['recent_illness']:'NA'}}</td>
                <td>{{isset($avalue['ailing_name']) && !empty($avalue['ailing_name']) ? $avalue['ailing_name']:'NA'}}</td>
                <td>{{isset($avalue['since_when']) && !empty($avalue['since_when']) ? date('d/m/Y', strtotime($avalue['since_when'])):'NA'}}</td>
                <td>{{isset($avalue['primary_physician']) && !empty($avalue['primary_physician']) ? $avalue['primary_physician']:'NA'}}</td>
                <td>{{isset($avalue['secondary_physician']) && !empty($avalue['secondary_physician']) ? $avalue['secondary_physician']:'NA'}}</td>
                <td>{{isset($avalue['admitted_hospital']) && !empty($avalue['admitted_hospital']) ? $avalue['admitted_hospital']:'NA'}}</td>                
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9" class="text-center">No data available</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
