<div class="modal fade" id="bandUserVerifyOtpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bandUserVerifyOtpModal">Verify Your Mobile Number</h5>
                <button type="button" class="close" data-formname="bandUserVerifyOtpModal" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(array('url' => '/api/bandusers','id'=>'bandUserVerifyOtpFormId')) !!}
            <div class="modal-body">
                <div class="row">                                   
                    <div class="col-sm-12" id='bandUserOTPMsg'></div>
                    <div class="col-sm-12">
                        <span class="help-text" id="addMobileNumber"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Enter OTP:</span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            {!!Form::number('otp',null,array('name' => 'otp','class'=>'form-control','placeholder'=>'Enter OTP', 'tabindex'=>'1','id' => 'banduser_otp','min' => 1,"maxlength" => "6","required" => "required"))!!}
                            {!!Form::hidden('hidden_banduser_otp',null,array('name' => 'hidden_banduser_otp','id' => 'hidden_banduser_otp'))!!}                            
                        </div> 
                    </div>                        
                </div>     
            </div>           
            <div class="modal-footer">
                {{Form::submit('Verify OTP',["class" => "btn btn-default yellow_bg_color black_text_color mr-2", 'id' => 'bandUserVerifyOTP'])}}
                {{Form::button('Cancel',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn"])}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
