@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/admin/band-users')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/admin/band-users')}}" class="kt-subheader__breadcrumbs-link">
                    Band Users For Approval </a>

            </div>
        </div>
    </div>
</div>

<div class="row" id="successMsg"> 
    @if(Session::has('success'))
    <div class="col-sm-12 alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
               Band Users For Approval List
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row" > 
            <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                <div class="alert-text" id="success-msg"></div>
            </div> 
            <div class="alert alert-custom alert-danger fade show col-12" role="alert" id="danger-alert" style='display: none;'>                                               
                <div class="alert-text" id="error-msg"></div>
            </div> 
        </div> 
        <table class="table table-striped table-bordered table-hover table-checkable" id="all_band_users_list">
            <thead>
                <tr>
                    <th>Guardian Name</th>
                    <th>Band User Name</th>
                    <th>Unique Id</th>
                    <th>Package Type</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade " id="approvedBandUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" id="pendingApproveBandUserFormId" name="pending_approve_band_user" method="PUT">
                <div class="modal-header">
                    <h5 class="modal-title" id="pendingApproveBandUserModalLabel"></h5>
                    <button type="button" class="close" data-formname="pending_approve_band_user" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    {{Form::hidden('userId','',['id'=>'userId'])}}                
                    {{Form::hidden('currentStatus','',['id'=>'currentStatus'])}}   
                    <div class="col-12" id='pendingApproveBandUserContent'></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="activate_deactivate_user" data-dismiss="modal" >No</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='pendingApproveBandUserSubmitBtnId'>Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@push('scripts')
<script src="{{ asset('js/bandusers/admin_banduserlist.js') }}" defer></script>
<!--<script>
   
</script>-->

@endpush
