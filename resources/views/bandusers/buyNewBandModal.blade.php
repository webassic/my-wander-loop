<div class="modal fade" id="buyNewBandModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="buyNewBandModalLabel">Buy New Band</h5>
                <button type="button" class="close" data-formname="buyNewBandModalForm" data-dismiss="modal" id="buyNewBandModalCloseBtnId">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(array('id'=>'buyNewBandsId','name'=>'buyNewBandModalForm')) !!}
            <div class="modal-body">
                {!!Form::hidden('bands_number',1,array('name' => 'bands_number','class'=>'form-control','id' => 'bands_number'))!!}
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Did you loose your band?</span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40" style="margin-top: 10px;">
                            <div class="kt-radio-inline ">
                                <label class="kt-radio kt-radio--warning">
                                    {{ Form::radio('band_lost', 'Yes') }} Yes
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--danger">
                                    {{ Form::radio('band_lost', 'No','1') }} No
                                    <span></span>
                                </label>
                            </div>
                        </div> 

                    </div>                        
                </div> 
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Price per band (in Rupees):</span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            {!!Form::text('price_per_band',$credentials['banduser_cost'],array('class'=>'form-control','placeholder'=>'Enter price per band','id' => 'price_per_band','min' => 1,"required" => "required","disabled"=>"disabled"))!!}
                        </div> 

                    </div>                        
                </div>  
<!--                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Total price (in Rupees) :</span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            {!!Form::text('total_amount',$credentials['banduser_cost'],array('class'=>'form-control','placeholder'=>'Total Price','id' => 'total_amount',"required" => "required","readonly"=>"true"))!!}
                        </div> 

                    </div>                        
                </div>   
-->
                {!!Form::hidden('total_amount',$credentials['banduser_cost'],array('id' => 'total_amount'))!!} 
                {!!Form::hidden('banduser_id',null,array('id' => 'banduser_id'))!!} 
            </div>           
            <div class="modal-footer">
                {{Form::submit('Submit',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "buyNewBandsBtnId"])}}
                {{Form::button('Cancel',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn " ,"id" => "cancelBuyNewBandsBtn"])}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
