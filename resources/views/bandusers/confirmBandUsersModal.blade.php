<div class="modal fade" id="confirmBandUsersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMedicalHistoryModalLabel">Confirm Band Users</h5>
                <button type="button" class="close" data-formname="confirmBandUsersForm" data-dismiss="modal" onclick="cancelButton();">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
            </div>
            {!! Form::open(['method' => 'post','id'=>'','name'=>'']) !!}
            <div class="modal-body">
                <div class="kt-portlet__body">
                    <div class="row"> 
                        <div class="alert alert-custom alert-success fade show col-12" role="alert" id="successMsgAlert" style='display: none;'>                                               
                            <div class="alert-text" id="successMsg"></div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <input type="hidden" name="package_name" id="selected_package_name" />
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_confirm_bandusers">
                        <thead>
                            <tr>
                                <th><label class="kt-checkbox" style="margin-bottom: 10px !important;"><input type="checkbox" class="multiple_checkall" ><span></span></label></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th> 
                                <th>Subscription Amount</th> 
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right">Total Amount:</th>
                                <th rowspan="1" colspan="2">₹ <span id="totalAmount"></span></th>
                            </tr>
<!--                            <tr>
                                <th colspan="3" style="text-align:right">Apply Promo Code:</th>
                                <th rowspan="1">
                                    <input type="text" id="valid_promo_code"/><br/>
                                    <span id="errorPromoCode" class="error" style="display: none;">This is required.</span>
                                    <span id="successPromoCode" class="successMessage" style="display: none;"></span>
                                </th>
                                <th><input type="button" value="Apply" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2" onclick="checkPromoCode();"/></th>
                            </tr>
                            <tr>
                                <th colspan="3" style="text-align:right">Grand Total Amount:</th>
                                <th rowspan="1" colspan="2" id="grandTotalAmt"></th>
                            </tr>-->
                        </tfoot>
                    </table>                    
                    <!--end: Datatable -->
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-default black_bg_color yellow_text_color mr-2" onclick="cancelButton();">Cancel</a>
                {{Form::button('Confirm and go to payment',["class" => "btn btn-default yellow_bg_color black_text_color mr-2", "id" => "confirmAndGoToPaymentBtn"])}}               
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
