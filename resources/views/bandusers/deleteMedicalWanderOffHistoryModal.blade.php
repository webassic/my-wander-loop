<div class="modal fade " id="deleteMedicalWanderOffHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="#">
                <div class="modal-header">
                    <h5 class="modal-title">Delete <span id="spanHeadingName"></span></h5>
                    <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div> 
                <div class="modal-body">
                    {{Form::hidden('hiddenMedicalWanderOffId','',['id'=>'hiddenMedicalWanderOffId'])}}                
                    {{Form::hidden('hiddenMedicalWanderOffKey','',['id'=>'hiddenMedicalWanderOffKey'])}}                
                    <div class="col-12">Are you sure you want to delete this <span id="spanDeleteMsg"></span> Details?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="deleteBandUser" data-dismiss="modal" >No</button>
                    <button type="button" class="btn btn-default yellow_bg_color black_text_color" id="deleteMedicalWanderOffHistoryBtn" onclick="confirmDeleteMedicalHistory();">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
