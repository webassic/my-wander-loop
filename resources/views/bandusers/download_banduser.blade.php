<style>
    @font-face {
        font-family: 'Poppins';
        font-style: normal;
        font-weight: normal;
        src: url({{ storage_path('fonts/Poppins-Regular.ttf') }}) format("truetype");
    }
    @font-face {
        font-family: 'Poppins';
        font-style: normal;
        font-weight: bold;
        src: url({{ storage_path('fonts/Poppins-Bold.ttf') }}) format("truetype");
    }    
</style>

<div style="text-align: center;">
    <img src="{{ public_path("assets/media/company-logos/myWanderLoop.png") }}" alt="" style="width: 150px;">
    <br/>
    <div style="font-style: oblique; font-size: 12px;">securing your freedom</div>
</div>
<div style="padding: 20px;">
    <div class="row" style="text-align: center; border: 1px solid #000;padding: 20px;">
        <div class="col-md-4">
            <label class="control-label"> Emergency Contact Number :</label>
            {{$bandUserDetails['mobile']}}           
        </div>
        <br> 
        <div class="col-md-4">
            <label class="control-label">Unique ID :</label>
            {{$bandUserDetails['uniqueid']}}
        </div>
        <?php if($bandUserDetails['package_name'] != env('RAZORPAY_BASIC_PACKAGE')){ ?>
        <br>  
        <div class="col-md-4">
            <label class="control-label">QR Code:</label>
            <?php
            if (isset($bandUserDetails['bandusers_qrcode']) && !empty($bandUserDetails['bandusers_qrcode'])) {
                foreach ($bandUserDetails['bandusers_qrcode'] as $profileImg) {
                    if ($profileImg['file_type'] == 'qr-code') {
                        ?>
                        <div class="symbol symbol-circle mr-3">
                            <img src="{{ public_path($profileImg['file_path']) }}" alt="" style="width: 150px;">
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
        <?php } ?>
    </div>                               
</div>                               
