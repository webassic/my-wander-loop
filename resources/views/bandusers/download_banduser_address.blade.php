<style>
    @font-face {
        font-family: 'Poppins';
        font-style: normal;
        font-weight: normal;
        src: url({{ storage_path('fonts/Poppins-Regular.ttf') }}) format("truetype");
    }
    @font-face {
        font-family: 'Poppins';
        font-style: normal;
        font-weight: bold;
        src: url({{ storage_path('fonts/Poppins-Bold.ttf') }}) format("truetype");
    }    
    .marginLeft30{
        margin-left: 30%; 
    }
</style>

<div style="text-align: center;">
    <img src="{{ public_path("assets/media/company-logos/myWanderLoop.png") }}" alt="" style="width: 150px;">
    <br/>
    <div style="font-style: oblique; font-size: 12px;">securing your freedom</div>
</div>
<div style="padding: 20px;">
    <div class="row" style="border: 1px solid #000;padding: 20px;">
        <div class="col-md-6 marginLeft30">
            <label class="control-label">Name :</label>
            {{ucwords($bandUserDetails['title'] ." ". $bandUserDetails['firstname'] . " ".$bandUserDetails['lastname'])}}
            <br> &nbsp;
        </div>
        <div class="col-md-6 marginLeft30">
            <label class="control-label">Address :</label>
            {{ucfirst($bandUserDetails['address'])}}
        </div> &nbsp;
        <div class="col-md-6 marginLeft30">
            <label class="control-label">Locality :</label>
            {{ucfirst($bandUserDetails['locality'])}}
        </div>  &nbsp;       
        <div class="col-md-6 marginLeft30">
            <label class="control-label">City :</label>
            {{ucfirst($bandUserDetails['city'])}}
        </div>  &nbsp;       
        <div class="col-md-6 marginLeft30">
            <label class="control-label">State :</label>
            {{ucfirst($bandUserDetails['state'])}}
        </div>  &nbsp;       
        <div class="col-md-6 marginLeft30">
            <label class="control-label">Pincode :</label>
            {{ucfirst($bandUserDetails['pin'])}}
        </div>  &nbsp;       
        <div class="col-md-6 marginLeft30">
            <label class="control-label">Phone No :</label>
            {{ucfirst($bandUserDetails['guardian_mobile'])}}
        </div>      
    </div>                               
</div>                               
