@extends('layouts.app')
<style>     
    table  
    {
        border: 1px solid #ccc;
        border-collapse: collapse;
    }
    table th
    {
        background-color: #F7F7F7;
        font-size: 1rem;
        font-weight: 600;
        color: #646c9a;
    }
    table th, table td
    {
        padding: 5px;
        border-color: #ccc;
        font-size: 12px;
    }
    .ailingOtherOption{
        padding-top: 5px;        
    }
    .displayNone{
        display: none !important;
    }
    .horizontalLine{
        border-bottom: 1px dashed #ecf0f3;
        margin-bottom: 15px;
    }

    input[type="file"] {
        display: block;
    }
    .imageThumb {
        height: 150px;
        max-height: 150px;
        width: 150px;
        border: 2px solid;
        padding: 1px;
        cursor: pointer;
    }
    .pip {
        display: inline-block;
        margin: 10px 10px 0 0;
    }
    .remove {
        display: block; 
        text-align: center;
        cursor: pointer;
    }
    .remove i{
        float: right !important;
        padding-bottom: 5px;
    }
    .remove:hover {
        background: white;
        color: black;
    }
    .kt-checkbox>span {
        border: 1px solid #3699ff !important;
        border-width: 2px !important;
    }
    .kt-checkbox>input:checked~span {
        border: 1px solid #3699ff !important;
        border-width: 2px !important;
    }
    .kt-checkbox>span:after {
        border: solid #3699ff !important;
    }
    a[disabled="disabled"] {
        pointer-events: none;
    }
    .table-bordered {
        border: 1px solid #FFF !important;
    }
    .table-responsive {
        display:inline-table !important;
    }
    @media (min-width:360px) and (max-width:767.9px)
    {
        .table-responsive {
            display:inline !important;
        }
    }
    #spnFilePath{
        border: 3px solid #fd397a;
        padding: 5px;     
        margin-top: -15px;
    } 
</style>

@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-link">
                    Band Users </a>
            </div>
        </div>
    </div>
</div>
{!! Form::open(array('url' => '/api/bandusers/update','id'=>'editBandUserFormId',"enctype"=>"multipart/form-data")) !!}
<?php
if ($bandUserDetails['status'] == "active" || $bandUserDetails['status'] == "pending") {
    $disabled = 'disabled';
} else {
    $disabled = '';
}
?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-user"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Edit Details of Band User 
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" id='add_banduser_status'></div>
        <div class="col-sm-12" id='error_banduser_status'></div>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <div class="col-lg-6">
                <h5 > <span class="text-danger"> Band User Contact Information </span>                    
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                {{ Form::label("Title:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::hidden("id", $bandUserDetails['id'])}}  
                    {{Form::hidden("status", $bandUserDetails['status'])}}
                    {{Form::select("title",$titles, $bandUserDetails['title'],["class" => "form-control","placeholder" => "Select Title", "id" => "title",'tabindex' => '1', $disabled])}}                
                </div>                        
            </div>
            <div class="col-lg-4">
                {{ Form::label("First Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::text("firstname", $bandUserDetails['firstname'], ["class" => "form-control","placeholder" => "Enter First Name", "id" => "firstname","required" => "required",'tabindex' => '2',$disabled])}}                              
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Last Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::text("lastname", $bandUserDetails['lastname'], ["class" => "form-control","placeholder" => "Enter Last Name", "id" => "lastname",'tabindex' => '3',$disabled])}}                                      
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-lg-4">
                {{ Form::label("Date of Birth: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                <div class="input-group date" >
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="la la-calendar"></i>
                        </span>
                    </div>
                    {{Form::select("year",$datearray['year'],isset($bandUserDetails['dob']) && !empty($bandUserDetails['dob'])? date('Y',strtotime($bandUserDetails['dob'])):'',
                                [
                                   "class" => "form-control  kt-selectpicker dobvalidate",
                                   "id" => "year",    
                                   "title" => "Year",'tabindex' => '4',$disabled
                                 ])
                    }}
                    &nbsp;
                    {{Form::select("month",$datearray['month'], isset($bandUserDetails['dob']) && !empty($bandUserDetails['dob'])? date('M',strtotime($bandUserDetails['dob'])):'',
                                [
                                   "class" => "form-control  kt-selectpicker dobvalidate",
                                   "id" => "month",    
                                   "title" => "Month",'tabindex' => '5',$disabled
                                 ])
                    }}  &nbsp;
                    {{Form::select("date",$datearray['date'], isset($bandUserDetails['dob']) && !empty($bandUserDetails['dob'])? date('d',strtotime($bandUserDetails['dob'])):'',
                                [
                                   "class" => "form-control  kt-selectpicker",
                                   "id" => "date",    
                                   "title" => "Date" ,'tabindex' => '6', $disabled
                                 ])
                    }}
                </div>
                <div id="validatedob" style="color:#EF4F7A;font-size:11px;"></div>
                <div id="doberror" class="error" ></div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Blood Group", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-tint"></i></span></div>
                    {{Form::select("blood_group",$bloodGroupArray, $bandUserDetails['blood_group'],
                                [
                                   "class" => "form-control",
                                   "placeholder" => "Select Blood Group",
                                   "id" => "blood_group",'tabindex' => '7', $disabled
                                ])
                    }}                             
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Guardian's Relationship With Band User:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                    {{Form::select("relationship_with_applicant",$relationShipWithGuardian, $bandUserDetails['relationship_with_applicant'],
                                [
                                   "class" => "form-control",
                                   "placeholder" => "Select Relationship with Band User",
                                   "id" => "relationship_with_applicant" ,'tabindex' => '8',$disabled
                                ])
                    }} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                {{ Form::label("Address:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("address", $bandUserDetails['address'], ["class" => "form-control","placeholder" => "Enter Address", "id" => "address",'tabindex' => '9'])}}                              
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Locality:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("locality", $bandUserDetails['locality'], ["class" => "form-control","placeholder" => "Enter Locality", "id" => "locality",'tabindex' => '10'])}}                                      
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("City:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("city", $bandUserDetails['city'], ["class" => "form-control","placeholder" => "Enter City", "id" => "city",'tabindex' => '11'])}}                                      
                </div>
            </div>                                              
        </div>
        <div class="form-group row">
            <div class="col-lg-4">
                {{ Form::label("State:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                    {{Form::text("state", $bandUserDetails['state'], ["class" => "form-control","placeholder" => "Enter State", "id" => "state",'tabindex' => '12'])}}                              
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Pincode:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                    {{Form::text("pin", $bandUserDetails['pin'], ["class" => "form-control","placeholder" => "Enter Pincode","maxlength" => "6", "id" => "pin",'tabindex' => '13','autocomplete'=>'off'])}}                                      
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::label("Languages known:", null, ['class' => 'control-label']) }}
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-language" aria-hidden="true"></i></span></div>
                    {{Form::text("languages_known", $bandUserDetails['languages_known'], ["class" => "form-control","placeholder" => "Enter Languages known", "id" => "languages_known","maxlength" => "200",'autocomplete'=>'off','tabindex' => '14'])}} 
                </div>
            </div>            
        </div>   
        <div class="form-group row">
            <div class="col-lg-4">
                {{ Form::label("Upload Image: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                <div class="input-group field" id="imageDiv">
                    <input type="file" id="files" name="files[]" accept="image/*" tabindex="15" multiple <?php echo $disabled; ?> />                    
                </div>
                <div class="help_text"> For best results please upload 150 X 150 pixel image.</div>
            </div>
            <div class="col-md-8">
                <div id="addImagesDiv">
                    <?php
                    if (isset($bandUserDetails['bandusers_images']) && !empty($bandUserDetails['bandusers_images'])) {
                        foreach ($bandUserDetails['bandusers_images'] as $bandUserImage) {
                            $imagePath = ($bandUserImage['file_path']);
                            ?>
                            <span class="pip">
                                <?php if ($bandUserDetails['status'] != "active" && $bandUserDetails['status'] != "pending") { ?>                                    
                                    <span class="remove"><i class="icon-2x text-dark-50 flaticon-close"></i></span>
                                <?php } ?>
                                <input type="hidden" name="latestPhoto[]" value="{{$imagePath}}" title="{{$bandUserImage['file_name']}}" name="{{$bandUserImage['file_name']}}">
                                <input type="hidden" name="banduserImageId[]" value="{{$bandUserImage['id']}}" name="banduserImageId">
                                <img class="imageThumb" src="/{{$imagePath}}" alt="{{$bandUserImage['file_name']}}">
                            </span>
                            <?php
                        }
                    }
                    ?>
                </div>  
                <div id="fileError" class="error" style="display: none;">User can upload only one photo.</div>
            </div>
        </div>
        <div class="horizontalLine"></div>
        <!-- Start Contact Information -->
        <div class="row">
            <div class="col-lg-6">
                <h5 > <span class="text-danger">During Emergency Contact Information</span>                    
                </h5>
            </div>
        </div>                  
        <div class="kt-portlet__body kt-portlet__body--fit-top">
            <div class="form-group row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="mobileLable">
                        
                    {{ Form::label("", null, ['class' => 'control-label']) }}Primary Mobile number in India <br/>to be printed on the Band:<span class="text-danger">*</span>                 
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-mobile"></i></span></div>
                        {{Form::text("mobile", $bandUserDetails['mobile'], ["class" => "form-control","placeholder" => "Enter Mobile", "id" => "mobile","minlength" => "10","maxlength" => "10",'tabindex' => '16'])}} 
                    </div>
                    <div class="help_text"> An OTP will be sent to this number.</div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="alternateMobileLable">
                        
                    {{ Form::label("", null, ['class' => 'control-label']) }}Secondary Mobile number in India <br/>to be contacted:              
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-mobile"></i></span></div>
                        {{Form::text("alternate_mobile", $bandUserDetails['alternate_mobile'], ["class" => "form-control","placeholder" => "Enter Mobile", "id" => "alternate_mobile","minlength" => "10","maxlength" => "10",'tabindex' => '17'])}} 
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div id="landlineLable">
                        
                    {{ Form::label("Landline (Country code/Area code/Number):", null, ['class' => 'control-label']) }}                  
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                        {{Form::text("landline", $bandUserDetails['landline'], ["class" => "form-control","placeholder" => "Enter Landline", "id" => "landline","maxlength" => "15",'tabindex' => '18'])}}                              
                    </div>
                </div>                
            </div>

            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("", null, ['class' => 'control-label']) }}Primary Email contact:<span class="text-danger">*</span>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                        {{Form::text("email", $bandUserDetails['email'], ["class" => "form-control","placeholder" => "Enter Email", "id" => "email",'tabindex' => '19'])}}                                      
                        <div class="text-muted errorMessage" id="email_error"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    {{ Form::label("", null, ['class' => 'control-label']) }}Secondary Email contact:    
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                        {{Form::text("alternate_email", $bandUserDetails['alternate_email'], ["class" => "form-control","placeholder" => "Enter Email", "id" => "alternate_email",'tabindex' => '20'])}}                                      
                    </div>
                </div>
            </div> 
        </div>
        <!-- End Contact Information -->
        <input type="hidden" value="{{$bandUserDetails['package_name']}}" name="package_name" />
        <?php 
        if($bandUserDetails['package_name'] != env('RAZORPAY_BASIC_PACKAGE')) { ?>
        <!-- Start Aadhaar Card Verification -->
        <div class="row">
            <div class="col-lg-6">
                <h5 > <span class="text-danger"> Aadhaar Card Verification </span>                    
                </h5>
            </div>
            <div class="col-lg-12">
                <p>Please click on the link to know safe verification process of your <a href="#" data-toggle="modal" data-target="#aadhaarVerificationStepsModal" style="text-decoration: underline;">Aadhaar card</a></p>
                <a  <?php echo $disabled; ?> href="https://resident.uidai.gov.in/offline-kyc" target="_blank" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2">
                    <i class="la la-plus"></i>
                    UIDAI Verification
                </a> 
            </div>  
        </div> 
        <br/>
        <div class="kt-portlet__body kt-portlet__body--fit-top">
            <div class="row">  
                <div class="col-lg-3">
                    {{ Form::label("Upload Zip file: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                    <div class="form-group input-group field" id="aadhharCardDiv">
                        <!--<input type="file" id="aadhaar_card_file" name="aadhaar_card_file" accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed" <?php // echo $disabled; ?> tabindex="21"/>-->
                        <input type="button" id="btnFileUpload" value="Choose File" />                        
                        <input style="display: none;" type="file" id="aadhaar_card_file" name="aadhaar_card_file" accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed" <?php echo $disabled; ?> tabindex="21"/>
                    </div>      
                    <div id="uploadFileError" class="error" style="display: none;">Allowed format: zip. Max file size: 2 MB.</div>
                </div>
<!--                <div class="col-lg-4">
                    {{ Form::label("Last 4 digit Aadhaar Card Number :", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                    <div class="form-group input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                        {{Form::text("aadhaar_card_number", $bandUserDetails['aadhaar_card_number'], ["class" => "form-control","placeholder" => "Enter Last 4 digit Aadhaar Card Number", "id" => "aadhaar_card_number","maxlength" => "4",'tabindex' => '22', $disabled])}}                                      
                    </div>
                </div>-->
                <div class="col-lg-5">
                    {{ Form::label("Enter Share code (as entered in UIDAI website):", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-bookmark-o"></i></span></div>
                        {{Form::text("share_code", $bandUserDetails['share_code'], ["class" => "form-control","placeholder" => "Enter Share code (as entered in UIDAI website)", "id" => "share_code","maxlength" => "4",'tabindex' => '23', $disabled])}}                              
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span id="spnFilePath" style="display: none"></span>
                </div>
            </div>
        </div>
        <!-- End Aadhaar Card Verification -->
        <div class="horizontalLine"></div>
        <!-- Start Medical History -->
        <div class="form-group row">
            <div class="col-lg-12">
                <h5 > <span class="text-danger"> Medical History </span>                    
                </h5>               
            </div>
            <div class="col-sm-12" id='errorMedicalHistory'></div>
            <div class="col-lg-12">
                <p>This section helps keep record of patient's medical history. It is important information and can come handy at the time of wander off events. Please provide details. </p>
                <a href="#" data-toggle="modal" data-target="#addMedicalHistoryModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2" id="addMedicalHistoryLink">
                    <i class="la la-plus"></i>
                    Add Medical History
                </a> 
            </div>            
        </div>
        <label class="kt-checkbox">
            <?php
            $medicalHistoryChecked = '';
            if ($bandUserDetails['medical_history_not_applicable'] == 'on') {
                $medicalHistoryChecked = 'checked';
            }
            ?>
            <input type="checkbox" name="medicalHistoryCheckbox" id="medicalHistoryCheckbox" value="{{$bandUserDetails['medical_history_not_applicable']}}" <?php echo $medicalHistoryChecked; ?>>Not Applicable
            <span></span>
        </label>

        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="medicalHistoryDataTable">
                <thead>
                    <tr>
                        <th>Current Illness From</th>
                        <th>Since When</th>
                        <th>Primary Physician Name</th>
                        <th>Secondary Physician Name</th>
                        <th>Last admitted Hospital Name</th>                    
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $ailingsDetailsArr = [];
                    if (isset($bandUserDetails['ailings_details']) && !empty($bandUserDetails['ailings_details'])) {
                        $ailingsDetailsArr = $bandUserDetails['ailings_details'];
                        foreach ($bandUserDetails['ailings_details'] as $key => $ailings_detail) {
                            ?>
                            <tr id="medical_history_row_{{$key}}">
                                <td>{{$ailings_detail['ailing_name']}}</td>
                                <td><?php
                                    if (isset($ailings_detail['since_when']) && !empty($ailings_detail['since_when'])) {
                                        echo date('d/m/Y', strtotime($ailings_detail['since_when']));
                                    } else {
                                        echo 'NA';
                                    }
                                    ?> 
                                </td>
                                <td>{{$ailings_detail['primary_physician']}}</td>
                                <td>{{$ailings_detail['secondary_physician']}}</td>
                                <td>{{$ailings_detail['admitted_hospital']}}</td>                                   
                                <td>
                                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showMedicalHistory({{$ailings_detail['id']}})"><i class="flaticon2-information"></i></a>
                                    <?php if ($bandUserDetails['status'] != "active") { ?>                                    
                                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editMedicalHistory({{$ailings_detail['id']}})" title="Edit/Update"><i class="flaticon-edit"></i></a>
                                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteMedicalHistory(this)" data-value="{{$ailings_detail['id']}}" data-key="{{$key}}"><i class="flaticon-delete"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    <?php } else { ?>
                        <tr id="noRecordsMedicalHistory"><td colspan="6">No records!!</td></tr>
                    <?php } ?>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        <div class="horizontalLine"></div>
        <!-- Start Wander off Events History -->         
        <div class="form-group row">
            <div class="col-lg-12">
                <h5 > <span class="text-danger">Wander off Events History</span>                    
                </h5>
            </div> 
            <div class="col-sm-12" id='errorWanderOffHistory'></div>
            <div class="col-lg-12">
                <p>This section helps keep record of patient's past wander of events. It is important information and can come handy at the time of wander off events to locate the patient. Please provide details.</p>
                <a href="#" data-toggle="modal" data-target="#addWanderOffEventsHistoryModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2"  id="addwanderOffEventHistoryLink">
                    <i class="la la-plus"></i>
                    Add Wander off Events History
                </a>              
            </div>
        </div>        
        <label class="kt-checkbox">
            <?php
            $wanderOffHistoryChecked = '';
            if ($bandUserDetails['wanderoff_history_not_applicable'] == 'on') {
                $wanderOffHistoryChecked = 'checked';
            }
            ?>
            <input type="checkbox" name="wanderOffHistoryCheckbox" id="wanderOffHistoryCheckbox" value="{{$bandUserDetails['wanderoff_history_not_applicable']}}" <?php echo $wanderOffHistoryChecked; ?>>Not Applicable
            <span></span>
        </label>

        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable table-responsive" id="wanderOffEventHistoryDataTable">
                <thead>
                    <tr>
                        <th>Lost City</th>
                        <th>Lost Location</th>
                        <th>Lost Date</th>
                        <th>Found City</th>
                        <th>Found Location</th>                    
                        <th>Found Date</th>                    
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $wanderOffHistoryDetailsArr = [];
                    if (isset($bandUserDetails['wanderoff_history']) && !empty($bandUserDetails['wanderoff_history'])) {
                        $wanderOffHistoryDetailsArr = $bandUserDetails['wanderoff_history'];
                        foreach ($bandUserDetails['wanderoff_history'] as $key => $wanderoffHistory) {
                            ?>
                            <tr id="wander_off_event_history_row_{{$key}}">
                                <td>{{$wanderoffHistory['lost_city']}}</td>
                                <td>{{$wanderoffHistory['lost_address']}}</td>
                                <td>{{date('d/m/Y', strtotime($wanderoffHistory['lost_date']))}}</td>
                                <td>{{$wanderoffHistory['found_city']}}</td>
                                <td>{{$wanderoffHistory['found_address']}}</td>
                                <td>{{date('d/m/Y', strtotime($wanderoffHistory['found_date']))}}</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View" onclick="showWanderOffEventHistory({{$wanderoffHistory['id']}})"><i class="flaticon2-information"></i></a>
                                    <?php if ($bandUserDetails['status'] != "active") { ?>    
                                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="editWanderOffEventHistory({{$wanderoffHistory['id']}})" title="Edit/Update"><i class="flaticon-edit"></i></a>
                                        <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete" onclick="deleteWanderOffEventHistory(this)" data-value="{{$wanderoffHistory['id']}}" data-key="{{$key}}"><i class="flaticon-delete"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    <?php } else { ?>
                        <tr id="noRecordsWandeOffHistory"><td colspan="7">No records!!</td></tr>
                    <?php } ?>                    
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>                 
    </div>
    <?php } ?>
</div>
<div class="row">
    <div class="col-sm-12" id='below_error_banduser_status'></div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-8">            
            {{Form::submit('Update',["class" => "btn btn-default yellow_bg_color black_text_color mr-2", "id" => "editBandUsersSubmitId",'tabindex' => '24'])}}              
            <a href="{{ url('/bandusers')}}" class="btn btn-default black_bg_color yellow_text_color mr-2" tabindex="25">Cancel</a>
        </div>
    </div>
</div>

{{ Form::close() }}

@include('bandusers.addMedicalHistoryModal')
@include('bandusers.aadhaarVerificationStepsModal')
@include('bandusers.showMedicalHistoryModal')
@include('bandusers.editMedicalHistoryModal')
@include('bandusers.addWanderOffEventsHistoryModal')
@include('bandusers.showWanderOffEventHistoryModal')
@include('bandusers.editWanderOffEventHistoryModal')
@include('bandusers.confirmBandUsersModal')
@include('bandusers.bandUserVerifyOtpModal')
@include('bandusers.deleteMedicalWanderOffHistoryModal')
@endsection
@push('scripts')
<script src="{{ asset('js/bandusers/edit_banduser.js?'.time()) }}" defer></script>
<script>
                                var updatedMedicalHistoryDataArr = '<?php echo json_encode(@$ailingsDetailsArr, JSON_HEX_APOS); ?>';
                                var updatedWanderOffHistoryDataArr = '<?php echo json_encode(@$wanderOffHistoryDetailsArr, JSON_HEX_APOS); ?>';
                                var statusDisabled = '<?php echo ($disabled); ?>';
                                $(document).ready(function ()
                                {
                                $('#ailing_id').on('change', function () {
                                if ($("#ailing_id option:selected").text() == "Others") { // Need to change dynamic
                                $('#otherOptionDiv').removeClass("displayNone");
                                } else {
                                $('#otherOptionDiv').addClass("displayNone");
                                }
                                });
                                });
    adjustHeight();
    $( window ).resize(function() {
        adjustHeight();
    });
    function adjustHeight(){
        var landline = $("#landlineLable").height();
        var alternateMobile = $("#alternateMobileLable").height();
        if(alternateMobile > landline){
            $("#landlineLable").height(alternateMobile);
             $("#mobileLable").height(alternateMobile);
        }else if (landline > alternateMobile){
            $("#alternateMobileLable").height(landline);
            $("#mobileLable").height(landline);
        }
    }
</script>
@endpush
