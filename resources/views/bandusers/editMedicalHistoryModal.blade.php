<div class="modal fade" id="editMedicalHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editMedicalHistoryModalLabel">Update Medical History</h5>
                <button type="button" class="close" data-formname="editMedicalHistoryForm" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post','id'=>'editMedicalHistoryForm','name'=>'editMedicalHistoryForm']) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Patient's Hospital Preference:", null, ['class' => 'control-label']) }}
                            {{Form::text("hospital_preference", '', ["class" => "form-control","placeholder" => "Enter Patient's Hospital preference", "id" => "edit_hospital_preference",'tabindex' => '-1','autocomplete'=>'off','autofocus' => 'autofocus'])}}
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Hospital Registration No.", null, ['class' => 'control-label']) }} 
                            {{Form::text("hospital_registration_no", '', ["class" => "form-control","placeholder" => "Hospital Registration No", "id" => "edit_hospital_registration_no",'autocomplete'=>'off'])}}
                        </div>
                    </div>                                        
                </div>	                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("City:", null, ['class' => 'control-label']) }} 
                            {{Form::text("hospital_city", '', ["class" => "form-control","placeholder" => "Enter City", "id" => "edit_hospital_city",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Emergency Medication:", null, ['class' => 'control-label']) }} 
                            {{Form::text("recent_illness", '', ["class" => "form-control","placeholder" => "Enter Emergency Medication", "id" => "edit_recent_illness",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{Form::hidden("id", '' , $attributes = ["class" => "form-control", "id" => "edit_id"])}}
                            {{ Form::label("Current Illness:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                            {!! Form::select('ailing_id',['' => 'Current ailing'] +$getAllAilingsList, null,['class' => 'form-control','id' =>'edit_ailing_id','autocomplete'=>'off']) !!}
                            {{Form::hidden("ailing_name", '' , $attributes = ["class" => "form-control", "id" => "edit_ailing_name"])}}
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Illness Since:", null, ['class' => 'control-label']) }} 
                            <div class="input-group date" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                                {{Form::text("since_when", '' , $attributes = ["class" => "form-control", "placeholder" => "Select Illness Since","id" => "edit_kt_datepicker_since_when","data-date-end-date"=>"0d",'autocomplete'=>'off'])}}
                            </div>                           
                        </div>
                    </div>                                    
                </div>
                <div class="row displayNone" id="editOtherOptionDiv">                     
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Other:", null, ['class' => 'control-label']) }} 
                            {{Form::text("ailing_other", '', ["class" => "form-control","placeholder" => "Other", "id" => "edit_ailing_other",'autocomplete'=>'off'])}}
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Primary Physician Name:", null, ['class' => 'control-label']) }}
                            {{Form::text("primary_physician", '', ["class" => "form-control","placeholder" => "Enter Primary Physician Name", "id" => "edit_primary_physician",'autocomplete'=>'off'])}}
                        </div>
                    </div>    
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Secondary Physician Name:", null, ['class' => 'control-label']) }}
                            {{Form::text("secondary_physician", '', ["class" => "form-control","placeholder" => "Enter Secondary Physician Name", "id" => "edit_secondary_physician",'autocomplete'=>'off'])}}
                        </div>
                    </div>                                     
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {{ Form::label("Last admitted Hospital Name:", null, ['class' => 'control-label']) }} 
                            {{Form::text("admitted_hospital", '', ["class" => "form-control","placeholder" => "Enter Last admitted Hospital Name", "id" => "edit_admitted_hospital",'autocomplete'=>'off'])}}
                        </div>
                    </div>                       
                </div>                
            </div>
            <div class="modal-footer">
                {{Form::submit('Update',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "updateMedicalHistoryBtn"])}}
                {{Form::button('Cancel',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn"])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
