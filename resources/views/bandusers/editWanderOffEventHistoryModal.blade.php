<div class="modal fade" id="editWanderOffEventHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Wander off Events History</h5>
                <button type="button" class="close" data-formname="editWanderOffEventHistoryForm" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post','id'=>'editWanderOffEventHistoryForm','name'=>'editWanderOffEventHistoryForm']) !!}
            <div class="modal-body"> 
                <div class="row">                     
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{Form::hidden("id", '' , $attributes = ["class" => "form-control", "id" => "edit_wander_off_id"])}}
                            {{ Form::label("Lost City:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
                            {{Form::text("lost_city", '', ["class" => "form-control","placeholder" => "Enter Lost City", "id" => "edit_lost_city",'tabindex' => '-1','autocomplete'=>'off','autofocus' => 'autofocus'])}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Lost Location:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>  
                            {{Form::text("lost_address", '', ["class" => "form-control","placeholder" => "Enter Lost Location", "id" => "edit_lost_address",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Lost Date:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            <div class="input-group date" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                                {{Form::text("lost_date", '',$attributes = ["class" => "form-control","placeholder" => "Select Lost Date", "id" => "edit_kt_datepicker_lost_date","data-date-end-date"=>"0d",'autocomplete'=>'off'])}}
                            </div>
                        </div>
                    </div>
                </div>	                                 	
                <div class="row">                     
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Found City:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            {{Form::text("found_city", '', ["class" => "form-control","placeholder" => "Enter Found City", "id" => "edit_found_city",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Found Location:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>  
                            {{Form::text("found_address", '', ["class" => "form-control","placeholder" => "Enter Found Location", "id" => "edit_found_address",'autocomplete'=>'off'])}}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Found Date:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                            <div class="input-group date" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                                {{Form::text("found_date", '',$attributes = ["class" => "form-control","placeholder" => "Select Found Date","id" => "edit_kt_datepicker_found_date","data-date-end-date"=>"0d",'autocomplete'=>'off'])}}
                            </div>
                        </div>
                    </div>
                </div>	  
                <div class="row">                     
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label("Additional Comments:", null, ['class' => 'control-label']) }} 
                            {{Form::text("additional_comments", '', ["class" => "form-control","placeholder" => "Enter Additional Comments", "id" => "edit_additional_comments",'autocomplete'=>'off'])}}
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                {{Form::submit('Update',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "updateWanderOffHistoryBtn"])}}
                {{Form::button('Cancel',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn" ])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
