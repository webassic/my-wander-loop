<div class="modal fade" id="enterNumberOfBandUsersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addGroupModalLabel">Select Package and Number of Band Users</h5>
                <button type="button" class="close" data-formname="addNumberOfBandsId" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(array('url' => '/api/bandusers','id'=>'addNumberOfBandsId')) !!}
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Select Package: <i class="fa fa-info-circle" data-toggle="tooltip" title="Select a Pricing Plan for the band user(s). You will see the details of each plan upon selection."></i></span>  
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                             {{Form::select("package_name",$packageList, '',["class" => "form-control","placeholder" => "Select Package", "id" => "package_name",'tabindex' => '1','autocomplete'=>'off'])}}                
                        </div> 
                        <div id="number_of_bandserror" class="error" ></div>
                    </div>                        
                </div>     
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Enter number of band users:</span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            {!!Form::number('number_of_bands',null,array('name' => 'number_of_bands','class'=>'form-control','placeholder'=>'Enter number of band users', 'tabindex'=>'2','id' => 'number_of_bands','min' => 1,"required" => "required"))!!}
                        </div> 
                        <div id="number_of_bandserror" class="error" ></div>
                    </div>                        
                </div>
                <div class="form-group hidden" id="packageInformation">
                    <div class="row">                        
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Selected Package Information:</span>                            
                        </div> 
                    </div>           
                    <div class="row">                        
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <span class="col-form-label" id="packageDescription"></span>                            
                        </div> 
                    </div> 
                    <br/>
                    <div class="row">                        
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <span class="col-form-label kt-font-bolder">Package Name: </span>                            
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <span class="col-form-label minusMarginLeft40" id="packageName"></span>                            
                        </div> 
                    </div> 
                    <br/>
                    <div class="row">     
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <span class="col-form-label kt-font-bolder">Package Amount: </span>                            
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <span class="col-form-label minusMarginLeft40" id="packageAmount"></span>                            
                        </div> 
                    </div>           
                 </div>           
            </div>           
            <div class="modal-footer">
                {{Form::submit('Add',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "addBandUsersBtn"])}}
                {{Form::button('Cancel',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn" ,"id" => "cancelBandUsersBtn"])}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
