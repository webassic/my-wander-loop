<div class="row" style="margin-top: -50px;">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--head-noborder">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title  kt-font-danger">
                        <i class="flaticon2-user"></i> Band User Information
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit-top">
                <div class="kt-section kt-section--space-sm">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Blood Group:</label>
                            {{isset($data['blood_group']) && !empty($data['blood_group']) ? $data['blood_group'] : 'NA'}}
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Unique Id:</label>
                            {{isset($data['uniqueid']) && !empty($data['uniqueid']) ? $data['uniqueid'] : 'NA'}}
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Date Of Birth:</label>
                            {{isset($data['dob']) && !empty($data['dob']) ? date('d/m/Y', strtotime($data['dob'])) : ''}}
                        </div>                                             
                    </div>
                    &nbsp;
                    <div class="row">                       
                        <div class="col-md-5">
                            <label class="control-label">Guardian's Relationship With Band User:</label>
                            {{isset($data['relationship_with_applicant']) && !empty($data['relationship_with_applicant']) ? $data['relationship_with_applicant'] : ''}}
                        </div>                        
                    </div>
                    &nbsp;
                    <div class="row">                       
                        <div class="col-md-12">
                            <label class="control-label">Band User Address:</label>
                            {{!empty($data['address']) && isset($data['address']) ? ucfirst($data['address']) : 'NA'}}
                        </div>                        
                    </div>
                    &nbsp;
                    <div class="row">                       
                        <div class="col-md-3">
                            <label class="control-label">Locality:</label>
                            {{!empty($data['locality']) && isset($data['locality']) ? ucfirst($data['locality']) : 'NA'}}
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">City:</label>
                            {{!empty($data['city']) && isset($data['city']) ? ucfirst($data['city']) : 'NA'}}
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">State:</label>
                            {{!empty($data['state']) && isset($data['state']) ? ucfirst($data['state']) : 'NA'}}
                        </div>                             
                        <div class="col-md-2">
                            <label class="control-label">Pin Code:</label>
                            {{!empty($data['pin']) && isset($data['pin']) ? $data['pin'] : 'NA'}}
                        </div>   
                    </div>
                    &nbsp;
                    <div class="row">                       
                        <div class="col-md-12">
                            <label class="control-label">Languages Known:</label>
                            {{isset($data['languages_known']) && !empty($data['languages_known']) ? $data['languages_known']:'NA'}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
