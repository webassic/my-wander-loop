@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/bandusers')}}" class="kt-subheader__breadcrumbs-link">
                    Band Users </a>

            </div>
        </div>
    </div>
</div>
<div class="row"  id='error_banduser_status' style="display: none;margin-left: 15px;margin-right: 15px;">
    <div class="col-sm-12 col-md-12"><div class="alert alert-danger">Please add guardian information.</div></div>
</div>
<div class="row"  id='error_confirm_payment_banduser' style="display: none;margin-left: 15px;margin-right: 15px;">
    <div class="col-sm-12 col-md-12"><div class="alert alert-danger">Their is no bandusers available for make payment.</div></div>
</div>
@if($error['rzerror'])
<div class="row">
    <div class="col-sm-12 col-md-12"><div class="alert alert-danger">
            {{@$error['rzerror']}}
        </div>
    </div>
</div>
@endif
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Band Users
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <a href="{{ URL::to('users/export_bandusers') }}" class="add-margin btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color" >
                                <i class="la la-download"></i> Export to Excel
                            </a>                             
                            &nbsp;
<!--                            <a href="#" data-toggle="modal" data-target="#selectPackageModal" class="add-margin btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color displayNone">
                                <i class="la la-money"></i>                            
                                Make Payment
                            </a> -->
                            <a href="#" id="checkPackageTypeCnt" class="add-margin btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color displayNone">
                                <i class="la la-money"></i>                            
                                Make Payment
                            </a> 
                            &nbsp;
                            <a href="#" onclick="checkGuardianInfo();" class="add-margin btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color">
                                <i class="la la-plus"></i>
                                Add Band Users
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row" > 
                <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                    <div class="alert-text" id="success-msg"></div>
                </div> 
                <div class="alert alert-custom alert-danger fade show col-12" role="alert" id="danger-alert" style='display: none;'>                                               
                    <div class="alert-text" id="error-msg"></div>
                </div> 
            </div> 
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_banduser">
                <thead>
                    <tr>
                        <th>Unique ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Package Type</th>
                        <th>Package Expiry Date</th>
                        <th>Status</th>
                        <th>Edit, View, Delete,<br/> Buy New Band</th>
                    </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>

<!-- end:: Content -->
@include('bandusers.enterNumberOfBandUsersModal')
@include('bandusers.selectPackageModal')
@include('bandusers.confirmBandUsersModal')
@include('bandusers.deleteBandUserModal')
@include('bandusers.buyNewBandModal')
@endsection
@push('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{ asset('js/bandusers/add_banduser.js?'.time()) }}" defer></script>
<script>
                                var guardianId = "<?= $guardianId ?>";
                                var userDetails = '<?= json_encode(\Auth::user(), JSON_HEX_APOS) ?>';
                                var planAmount = "<?= @$planAmount ?>";
                                var rzKey = "<?= @$credentials['key_id'] ?>";
                                var rzimage = "<?= url('/images/favicon/myWanderLoop_Icon.png'); ?>";

                                function checkGuardianInfo() {
                                    if (guardianId >= 1) {
                                        $("#enterNumberOfBandUsersModal").modal('show');
                                        $('#error_banduser_status').css('display', 'none');
                                    } else {
                                        $("#enterNumberOfBandUsersModal").modal('hide');
                                        $('#error_banduser_status').css('display', 'block');
                                        setInterval(function () {
                                            $("#error_banduser_status").fadeOut('slow');
                                        }, 6000);
                                    }
                                }
                                $(document).ready(function () {
                                    $('[data-toggle="tooltip"]').tooltip();   
                                    
                                    $("#addBandUsersBtn").click(function () {
                                        $("#addNumberOfBandsId").validate({
                                            rules: {
                                                // simple rule, converted to {required:true}
                                                package_name: {
                                                    required: true
                                                },
                                                number_of_bands: {
                                                    required: true
                                                }
                                            },
                                            submitHandler: function (form) {
                                                if (guardianId >= 1) {
                                                    var bandUserCnt = $("#number_of_bands").val();
                                                    var package =  $('#package_name').find("option:selected").text().toLowerCase();
                                                    location.href = '/bandusers/add/' + btoa(bandUserCnt) + '/?p=' + btoa(package);
                                                 } else {
                                                    resetformdata('addNumberOfBandsId');
                                                    $("#enterNumberOfBandUsersModal").modal('hide');
                                                    $('#error_banduser_status').css('display', 'block');
                                                    setInterval(function () {
                                                        $("#error_banduser_status").fadeOut('slow');
                                                    }, 6000);
                                                }
                                            }
                                        });                                                                               
                                    });
                                    function resetformdata(form_name) {
                                        $('#' + form_name).each(function () {
                                            this.reset();
                                        });
                                    }
                                    $('#cancelBandUsersBtn').click(function (e) {
                                        resetformdata('addNumberOfBandsId');
                                    });                                                                       
                                });                                                               
</script>
<script src="{{ asset('js/bandusers/banduser.js') }}" defer></script>
@endpush
