<div class="tab-pane active" id="profile_images_view" role="tabpanel">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    <div class="symbol-list d-flex flex-wrap">
                        <?php
                        if (isset($data['bandusers_images']) && !empty($data['bandusers_images'])) {
                            foreach ($data['bandusers_images'] as $profileImg) {
                                if($profileImg['file_type']=='image'){
                                ?>
                                <div class="symbol symbol-circle mr-3">
                                    <img alt="Pic" src="/{{$profileImg['file_path']}}" width="150px;" height="150px;"/>
                                </div>
                                <?php }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
