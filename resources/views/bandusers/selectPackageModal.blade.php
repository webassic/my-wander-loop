<div class="modal fade" id="selectPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select Package for which you want to make payment for</h5>
                <button type="button" class="close" data-formname="" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['id'=>'selectPackageModalID']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">                                             
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <span class="col-form-label kt-font-bolder displayMiddle">Select Package: <i class="fa fa-info-circle" data-toggle="tooltip" title="At a time you can pay for all band users under single package. Please select a package for which you want to make payment."></i></span>  
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                             {{Form::select("select_package_name",$packageList, '',["class" => "form-control","placeholder" => "Select Package", "id" => "select_package_name",'tabindex' => '1','autocomplete'=>'off'])}}                
                        </div>                         
                    </div>                        
                    </div>                                
                 </div>                         
            <div class="modal-footer">
                 <!--<a href="#" data-toggle="modal" data-target="#confirmBandUsersModal" class="btn btn-default yellow_bg_color black_text_color mr-2"  id="confirmBandUsersBtn">Make Payment</a>--> 
                {{Form::submit('Make Payment',["class" => "btn btn-default yellow_bg_color black_text_color mr-2","id" => "confirmMakePaymentBandUserBtn"])}}
                {{Form::button('Cancel',['data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn" ,"onclick" => "cancelButton();"])}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
