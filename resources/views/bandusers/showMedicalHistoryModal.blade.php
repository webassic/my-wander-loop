<div class="modal fade" id="showMedicalHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Show Medical History</h5>
                <button type="button" class="close" data-formname="show_medical_history" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post']) !!}
            <div class="modal-body">
                <div class="row">  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Patient's Hospital Preference:</span>
                            <span id="txtHospitalPreference"></span>
                        </div>
                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Hospital Registration No:</span>
                            <span id="txtHospitalRrgistrationNo"></span>
                        </div>
                    </div>                                       
                </div>	                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">City:</span>
                            <span id="txtCity"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Emergency Medication:</span>
                            <span id="txtAnyMajorRecentIllness"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Current Illness:</span>
                            <span id="txtCurrentlyAilingFrom"></span>
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Illness Since:</span>
                            <span id="txtSinceWhen"></span>
                        </div>
                    </div>                                      
                </div>
                <div class="row displayNone" id="otherOptionDiv">                     
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Other:</span>
                            <span id="txtOther"></span>
                        </div>
                    </div>                     
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Primary Physician Name:</span>
                            <span id="txtPrimaryPhysicianName"></span>
                        </div>
                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Secondary Physician Name:</span>
                            <span id="txtSecondaryPhysicianName"></span>
                        </div>
                    </div>                                       
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Last admitted Hospital Name:</span>
                            <span id="txtLastadmittedHospitalName"></span>
                        </div>
                    </div>                                                             
                </div>    
            </div>
            <div class="modal-footer">
                {{Form::button('Close',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn"])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
