<div class="modal fade" id="showWanderOffEventHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Show Wander Off Event History</h5>
                <button type="button" class="close" data-formname="showWanderOffEventHistory" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            {!! Form::open(['method' => 'post']) !!}
            <div class="modal-body">
                <div class="row">                     
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Lost City:</span>
                            <span id="txtLostCity"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Lost Location:</span>
                            <span id="txtLostLocation"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Lost Date:</span>
                            <span id="txtLostDate"></span>
                        </div>
                    </div>
                </div>	                                                 
                <div class="row">                     
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Found City:</span>
                            <span id="txtFoundCity"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Found Location:</span>
                            <span id="txtFoundLocation"></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Found Date:</span>
                            <span id="txtFoundDate"></span>
                        </div>
                    </div>
                </div>                
                <div class="row">                     
                    <div class="col-sm-12">
                        <div class="form-group">
                            <span class="col-form-label kt-font-bolder">Additional Comments:</span>
                            <span id="txtAdditionalComments"></span>
                        </div>
                    </div>                     
                </div>                
            </div>
            <div class="modal-footer">
                {{Form::button('Close',[' data-dismiss'=>"modal", "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn"])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
