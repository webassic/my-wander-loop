@extends('layouts.app') 
@section('content') 
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <?php
            if (auth()->user()->user_type == "Administration") {
                $url = '/admin/band-users/view/' . $data['id'];
            } else {
                $url = '/bandusers/view/' . $data['id'];
            }
            ?>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url($url)}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url($url)}}" class="kt-subheader__breadcrumbs-link">
                    Band Users </a>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content Head -->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--Begin:: Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row" > 
                <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                    <div class="alert-text" id="success-msg"></div>
                </div>                 
            </div>
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div>
                        <?php
                        if (isset($data['bandusers_images']) && !empty($data['bandusers_images'])) {
                            foreach ($data['bandusers_images'] as $key => $profileImg) {
                                if ($profileImg['file_type'] == 'image') {
                                    if ($key == 0) {
                                        $set = "DefaultOneImg";
                                    } else {
                                        $set = "";
                                    }
                                    if ($set == "DefaultOneImg") {
                                        ?>
                                        <img alt="Pic" src="/{{$profileImg['file_path']}}" width="150" height="150"  data-toggle="modal" data-target=".bs-example-modal-sm"  class="pointer"/>   
                                    <?php }
                                    ?>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <?php
                                            if (isset($data['bandusers_images']) && !empty($data['bandusers_images'])) {
                                                foreach ($data['bandusers_images'] as $key => $profileImg) {
                                                    if ($profileImg['file_type'] == 'image') {
                                                        if ($key == 0) {
                                                            $active = 'active';
                                                        } else {
                                                            $active = '';
                                                        }
                                                        ?>
                                                        <div class="item {{$active}}">
                                                            <img alt="Pic" src="/{{$profileImg['file_path']}}" width="300" height="300"/>                                                             
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   

                    <div class="kt-widget__content">
                        <div class="kt-widget__head">                            
                            <div class="kt-widget__user">                                 
                                <a href="#" class="kt-widget__username">
                                    {{!empty($data['firstname']) && isset($data['firstname']) ? $data['title'].'. '.$data['firstname'] .' '.$data['lastname'] : ''}}
                                </a>
                                @if(isset($data['status']) && !empty($data['status']) && $data['status'] == 'active')
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @else 
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-danger">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @endif
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline  kt-badge--unified-danger">
                                    {{ucfirst($data['package_name'])}} Package
                                </span>
                            </div>
                            <div class="kt-widget__action"> 
                                <?php
                                if (auth()->user()->user_type == "Administration" && $data['status'] == 'pending') {
                                    if ($data['package_name'] == env('RAZORPAY_BASIC_PACKAGE')) {
                                        ?>
                                        <a href="#" data-toggle="modal" data-target="#approvedBandUserModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color" title="Mark As Approve" onclick="changeStatus({{$data['id']}}, 'pending')"><i class="la la-check"></i>Mark As Approve</a>
                                        <?php
                                    } else {
                                        if (isset($data['bandusers_aadhaar_info']) && !empty($data['bandusers_aadhaar_info'])) {
                                            if ($data['bandusers_aadhaar_info'][0]['status'] == 'success') {
                                                ?>
                                                <a href="#" data-toggle="modal" data-target="#approvedBandUserModal" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color" title="Mark As Approve" onclick="changeStatus({{$data['id']}}, 'pending')"><i class="la la-check"></i>Mark As Approve</a>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="kt-widget__subhead">
                            <div class="row">
                                <?php if (isset($data['institution_name']) && !empty($data['institution_name'])) { ?>
                                    <div class="col-md-6">
                                        <span class="col-form-label kt-font-bolder">Institution Name:</span>
                                        <span>{{isset($data['institution_name']) && !empty($data['institution_name']) ? $data['institution_name'] : ''}}</span>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4">
                                    <?php
                                    if (isset($data['bandusers_qrcode']) && !empty($data['bandusers_qrcode']) && auth()->user()->user_type != 'User') {
                                        foreach ($data['bandusers_qrcode'] as $profileImg) {
                                            if ($profileImg['file_type'] == 'qr-code') {
                                                ?>
                                                <div class="symbol symbol-circle mr-3">
                                                    <img alt="Pic" src="{{url('getimage/qr_codes/'.$profileImg['bandusers_id'].'/'.$profileImg['file_name'])}}" width="100px;" height="100px;"/>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">           
                <div class="card">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="kt-portlet kt-portlet--head-noborder">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title  kt-font-danger">
                                            <i class="flaticon2-user"></i> During Emergency Contact Information
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body kt-portlet__body--fit-top">
                                    <div class="kt-section kt-section--space-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label">Primary Mobile number in India to be printed on the Band:</label>
                                                {{isset($data['mobile']) && !empty($data['mobile']) ? $data['mobile'] : 'NA'}}
                                            </div>                                            
                                        </div>
                                        &nbsp;
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label">Secondary Mobile number in India to be contacted:</label>
                                                {{isset($data['alternate_mobile']) && !empty($data['alternate_mobile']) ? $data['alternate_mobile'] : 'NA'}}
                                            </div>                                             
                                        </div>
                                        &nbsp;
                                        <div class="row">                                             
                                            <div class="col-md-12">
                                                <label class="control-label">Landline (Country code/Area code/Number):</label>
                                                {{isset($data['landline']) && !empty($data['landline']) ? $data['landline'] : 'NA'}}
                                            </div>
                                        </div>
                                        &nbsp;
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Primary Email contact:</label>
                                                {{isset($data['email']) && !empty($data['email']) ? $data['email'] : 'NA'}}
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Secondary Email contact:</label>
                                                {{isset($data['alternate_email']) && !empty($data['alternate_email']) ? $data['alternate_email'] : 'NA'}}
                                            </div>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    @include('bandusers.general_information') 
                </div>
                <?php if ($data['package_name'] != env('RAZORPAY_BASIC_PACKAGE')) { ?>
                    <div class="card">                     
                        <div class="row" style="margin-top: -50px;">
                            <div class="col-xl-12">
                                <div class="kt-portlet kt-portlet--head-noborder">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title  kt-font-danger">
                                                <i class="fa fa-id-card"></i>  Aadhaar Card Verification
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body kt-portlet__body--fit-top">
                                        <div class="kt-section kt-section--space-sm">
                                            <div class="row">
                                                <?php if ($data['status'] != 'active') { ?>
                                                    <div class="col-md-5">
                                                        <label class="control-label">Download Offline Aadhar verification Zip file:</label>
                                                        <a href="{{$data['aadhaar_card_file_path']}}" download="{{$data['aadhaar_card_file_name']}}" style="text-decoration: underline;"/>Click Here</a>
                                                    </div>
                                                <?php } ?>
                                                <?php // if ($data['aadhaar_card_number'] != "") {  ?>
                                                <!--                                                <div class="col-md-5">
                                                                                                    <label class="control-label">Last 4 Digit Aadhaar Card Number :</label>
                                                                                                    {{$data['aadhaar_card_number']}}
                                                                                                    &nbsp;&nbsp; &nbsp;
                                                <?php // if (auth()->user()->user_type == "Administration" && $data['status'] == 'active') { ?>
                                                                                                        <a href="#" data-toggle="modal" data-target="#deletAadhaarCardModal" style="color: red; text-decoration: underline;" title="Approved" onclick="deleteAadhharCard({{$data['id']}})">Delete</a>
                                                <?php // } ?>
                                                                                                </div>-->
                                                <?php // }  ?>
                                                <div class="col-md-2">
                                                    <label class="control-label">Share Code :</label>
                                                    {{$data['share_code']}}
                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="row">
                                                <?php
                                                if (auth()->user()->user_type == "Administration" && $data['status'] == 'pending') {
                                                    if (empty($data['bandusers_aadhaar_info']) || $data['bandusers_aadhaar_info'][0]['status'] == 'error') {
                                                        ?>
                                                        <div class="col-md-3">
                                                            <input id="verifyAadhaarBtn" type="button" value="Verify Aadhaar" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color btn-sm mr-2">
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                                @if(isset($data['bandusers_aadhaar_info']) && !empty($data['bandusers_aadhaar_info']))
                                                @if($data['bandusers_aadhaar_info'][0]['status'] == 'error')
                                                <?php
                                                $json = json_decode($data['bandusers_aadhaar_info'][0]['response'], true);
                                                ?>
                                                <h5 class="error">Error : {{$json['message']}}</h5> 
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                             
                    </div>
                    <?php if (auth()->user()->user_type == "Administration") { ?>
                        @if(isset($data['bandusers_aadhaar_info']) && !empty($data['bandusers_aadhaar_info']))
                        @if($data['bandusers_aadhaar_info'][0]['status'] == 'success')
                        <div class="card">                     
                            <div class="row" style="margin-top: -50px;">
                                <div class="col-xl-12">
                                    <div class="kt-portlet kt-portlet--head-noborder">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title  kt-font-danger">
                                                    <i class="flaticon2-check-mark"></i> Verified Information:
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body kt-portlet__body--fit-top">
                                            <div class="kt-section kt-section--space-sm">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th class="kt-font-bolder" scope="col">Photo</th>
                                                                <th class="kt-font-bolder" scope="col">Full Name</th>
                                                                <th class="kt-font-bolder" scope="col">DoB</th>
                                                                <th class="kt-font-bolder" scope="col">Gender</th>
                                                                <th class="kt-font-bolder" scope="col">Address</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['bandusers_aadhaar_info'] as $akey => $avalue)

                                                            <tr>
                                                                <td>
                                                                    <img alt="Pic" src="/{{$avalue['image']}}" width="100" height="100"/>
                                                                </td>
                                                                <td>{{isset($avalue['username']) && !empty($avalue['username']) ? $avalue['username']:'NA'}}</td>
                                                                <td>{{isset($avalue['dob']) && !empty($avalue['dob']) ? date('d/m/Y', strtotime($avalue['dob'])):'NA'}}</td>
                                                                <td>{{isset($avalue['gender']) && !empty($avalue['gender']) ? $avalue['gender']:'NA'}}</td>
                                                                <td>{{isset($avalue['address']) && !empty($avalue['address']) ? $avalue['address']:'NA'}}</td>                                                       
                                                            </tr>

                                                            @endforeach                                                        
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                             
                            </div>
                        </div>                   
                        @endif
                        @endif
                    <?php } ?>  
                    <div class="card">                     
                        <div class="row" style="margin-top: -50px;">
                            <div class="col-xl-12">
                                <div class="kt-portlet kt-portlet--head-noborder">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title  kt-font-danger">
                                                <i class="flaticon2-notification"></i> Medical History
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body kt-portlet__body--fit-top">
                                        <div class="kt-section kt-section--space-sm">
                                            @include('bandusers.ailing_information')
                                        </div>
                                    </div>
                                </div>
                            </div>                                             
                        </div>
                    </div>
                    <div class="card">                     
                        <div class="row" style="margin-top: -70px;">
                            <div class="col-xl-12">
                                <div class="kt-portlet kt-portlet--head-noborder">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title  kt-font-danger">
                                                <i class="flaticon2-chart"></i> Wandered Off Events History
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body kt-portlet__body--fit-top">
                                        <div class="kt-section kt-section--space-sm">
                                            @include('bandusers.wandered_off_information')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                             
                    </div> 
                <?php } ?>
            </div>
            <?php if (auth()->user()->user_type != "Administration") { ?>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">     
                            <a href="{{ url('/bandusers/edit/'.$data['id'])}}" class="btn btn-default yellow_bg_color black_text_color mr-2">Edit</a>
                            <a href="{{ url('/bandusers')}}" class="btn btn-default black_bg_color yellow_text_color mr-2">Cancel</a>
                        </div>
                    </div>
                </div>
            <?php } ?>            
        </div>
    </div>
</div>
<?php if (auth()->user()->user_type == "Administration") { ?>
    <div class="modal fade " id="approvedBandUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" id="pendingApproveBandUserFormId" name="pending_approve_band_user" method="PUT">
                    <div class="modal-header">
                        <h5 class="modal-title" id="pendingApproveBandUserModalLabel"></h5>
                        <button type="button" class="close" data-formname="pending_approve_band_user" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        {{Form::hidden('userId','',['id'=>'userId'])}}                
                        {{Form::hidden('currentStatus','',['id'=>'currentStatus'])}}   
                        <div class="col-12" id='pendingApproveBandUserContent'></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="activate_deactivate_user" data-dismiss="modal" >No</button>
                        <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='pendingApproveBandUserSubmitBtnId'>Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
    <div class="modal fade " id="deletAadhaarCardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" action="{{url('/api/bandusers')}}" id="deleteAadhaarCardFormId">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete Aadhaar Card Number</h5>
                        <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        {{Form::hidden('bandUserId','',['id'=>'bandUserId'])}}                
                        <div class="col-12">Are you sure you want to delete 4 digit Aadhaar card number?</div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="deleteBandUser" data-dismiss="modal" >No</button>
                        <button type="button" class="btn btn-default yellow_bg_color black_text_color" id="deleteAdhaarCardNumber">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>
@endsection
@push('scripts') 
<script>
    $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("form[name='pending_approve_band_user']").validate({
    submitHandler: function (form) {
    $('.loadingImageLoader').css('display', 'block');
    var id = $("#userId").val();
    var status = $("#currentStatus").val();
    if (status == 'pending') {
    var updateStatus = 'active';
    }
    $.ajax({
    type: "PUT",
            url: '/api/changeBandUserStatus/' + id,
            dataType: "json",
            data: {
            status: updateStatus
            },
            ContentType: 'application/json',
            success: function (response) {
            $('.loadingImageLoader').css('display', 'none');
            $("#success-alert").css('display', 'block');
            $("#success-msg").text(response.message);
            $("#pendingApproveBandUserFormId").get(0).reset();
            $('#approvedBandUserModal').modal('hide');
            setInterval(function () {
            $("#success-alert").fadeOut('slow');
            window.location.reload();
            }, 3000);
            },
            error: function (error) {
            console.log(error);
            }
    });
    }
    });
    $("#deleteAdhaarCardNumber").click(function() {
    var id = $("#bandUserId").val();
    $.ajax({
    type: "PUT",
            url: '/api/deleteAadhaarCardNumber/' + id,
            dataType: "json",
            ContentType: 'application/json',
            success: function (response) {
            window.location.reload();
            $('.loadingImageLoader').css('display', 'none');
            $("#success-alert").css('display', 'block');
            $("#success-msg").text(response.message);
            $('#deletAadhaarCardModal').modal('hide');
            setInterval(function () {
            $("#success-alert").fadeOut('slow');
            }, 3000);
            },
            error: function (error) {
            console.log(error);
            }
    });
    });
    //
    $("#verifyAadhaarBtn").click(function() {
    var id = '<?= $data['id']; ?>';
    $('.loadingImageLoader').css('display', 'block');
    $.ajax({
    type: "PUT",
            url: '/api/verifyAadhharDetails/' + id,
            dataType: "json",
            ContentType: 'application/json',
            success: function (response) {
            $('.loadingImageLoader').css('display', 'none');
            window.location.reload();
            },
            error: function (error) {
            console.log(error);
            }
    });
    });
    });
    function changeStatus(id, status) {
    if (status == 'pending') {
    $("#userId").val(id);
    $("#currentStatus").val(status);
    $("#approvedBandUserModal").modal('show');
    $("#pendingApproveBandUserContent").text('Are you sure you want to Approve this Band User ?');
    $('#pendingApproveBandUserModalLabel').text('Approve Band User');
    }
    }

    function deleteAadhharCard(id) {
    $("#bandUserId").val(id);
    }


</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush