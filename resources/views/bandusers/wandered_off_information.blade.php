 
<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead class="thead-light">
            <tr>
                <th class="kt-font-bolder" scope="col">Lost City</th>
                <th class="kt-font-bolder" scope="col">Lost Location</th>
                <th class="kt-font-bolder" scope="col">Lost Date</th>
                <th class="kt-font-bolder" scope="col">Found City</th>
                <th class="kt-font-bolder" scope="col">Found Location</th>
                <th class="kt-font-bolder" scope="col">Found Date</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($data['wanderoff_history']) && !empty($data['wanderoff_history']))
            @foreach($data['wanderoff_history'] as $wkey => $wvalue)
            <tr>
                <td>{{$wvalue['lost_city']}}</td>
                <td>{{$wvalue['lost_address']}}</td>
                <td>{{date('d/m/Y', strtotime($wvalue['lost_date']))}}</td>
                <td>{{$wvalue['found_city']}}</td>
                <td>{{$wvalue['found_address']}}</td>
                <td>{{date('d/m/Y', strtotime($wvalue['found_date']))}}</td>

                @endforeach
            </tr>
            @else
            <tr>
                <td colspan="6" class="text-center">No data avaiable</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
