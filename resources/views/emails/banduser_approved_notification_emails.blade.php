Hi {{$maildata['guardian_firstname'].' '.$maildata['guardian_lastname']}}, <br/><br/>
Greetings from MyWanderLoop!
<br/><br/>
Your Band User {{$maildata['firstname'].' '.$maildata['lastname'] }} has been approved and Unique ID is {{$maildata['uniqueid']}}. 
<br/><br/>
Thank you! <br>
myWanderLoop Team