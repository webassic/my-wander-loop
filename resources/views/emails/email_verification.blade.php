
Hi {{$maildata['firstname'].' '.$maildata['lastname']}}, <br/><br/>
Welcome to MyWanderLoop!<br/><br/>

Please click on the button below to verify your email Id : <br/><br/><br/>
<?php 
    $url = 'https://app.mywanderloop.com/verify/'.$maildata['email_verification_code'];
?>
<a style="color: #fff;background-color: #0095d6;border-color:#0095d6;padding: 15px;text-decoration: none;border-radius: 5px;font-size: 18px;font-family: 'Open Sans', sans-serif;" href="{{$url}}"> Verify me</a><br/><br/><br/>

<br/><br/>
Thank you! <br>
myWanderLoop Team

