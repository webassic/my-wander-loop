<style>
    span{
        font-weight: bold;
    }
</style>
Dear {{ !empty($maildata['guardianName']) ? ucwords($maildata['guardianName']): '' }}<br /><br />
We have received your payment for a new Band order. The Band will be dispatched to the Band User address within 7 working days.
<br>
<br>
You can get more information from our website <a href="www.mywanderloop.com">www.mywanderloop.com.</a> We are also working with various other service providers to delight you with various value added services through this.
<br>
<br> 
Your invoice is attached herewith.
<br>
<br>
Best regards, 
<br>
MyWanderLoop Team