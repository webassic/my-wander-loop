<style>
    span{
        font-weight: bold;
    }
</style>
Dear {{ !empty($maildata['guardianName']) ? ucwords($maildata['guardianName']): '' }}<br /><br />
Welcome to myWanderLoop and thank you for subscribing to its services. myWanderLoop is an unique and earnest 
attempt to keep Senior Citizens/Patients safe outside their home. The Silicon Band has certain key personal details 
which can be read through the QR code printed on it based on the form filled up by you. In case of an emergency, 
any passerby can read the QR code or simply call the emergency number printed on it. The Band will be dispatched 
to the address you have mentioned for the Band user.   
<br>
<br>
While we have received your payment, we need a few days to verify the information provided for its correctness and 
approve the same. You will hear from us once approved. We hope to complete this process and despatch your order 
within 7 working days. 
<br>
<br>
You can get more information from our website <a href="www.mywanderloop.com">www.mywanderloop.com.</a> We are also working with various other service  
providers to delight you with various value added services through this.
<br>
<br>
Your invoice is attached herewith.
<br>
<br>
Best regards, 
<br>
MyWanderLoop Team