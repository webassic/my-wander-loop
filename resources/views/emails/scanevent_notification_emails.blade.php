<style>
    span{
        font-weight: bold;
    }
</style>
Dear {{ !empty($maildata['firstname'])? $maildata['firstname'].' '.$maildata['lastname']: '' }}<br /><br />
There has been a scan event for {{ !empty($maildata['banduser_name'])? $maildata['banduser_name']: '' }} with unique id - {{ !empty($maildata['banduser_uniqueid'])? $maildata['banduser_uniqueid']: '' }}. The scan event information as follows : <br>
<table style="width:100%;  border-collapse: collapse;">
    <tr>
        <td><span>Band Scanned On : </span> &nbsp; {{!empty($maildata['scanned_on'])? $maildata['scanned_on']: ''}}</td>       
    </tr>
    <tr>
        <td><span> Scanner's Mobile Number : </span> &nbsp; {{!empty($maildata['scanner_mobile'])? $maildata['scanner_mobile']: ''}}</td>
       
    </tr>
    <tr>
        <td><span> Scanner's Mobile Number Verify ? : </span> &nbsp; {{ !empty($maildata['mobile_verify'])? ucwords($maildata['mobile_verify']): ''}}</td>
        
    </tr>
    <tr>
        <td><span> Scanner's IP Address : </span> &nbsp; {{!empty($maildata['ip_address'])? $maildata['ip_address']: ''}}</td>        
    </tr>
    <tr>
        <td><span> Location : </span> &nbsp; {{!empty($maildata['location'])? $maildata['location']: 'Not Captured'}}</td>        
    </tr>
</table>
<br>
Thank you! <br>
myWanderLoop Team

  