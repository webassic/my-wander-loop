<style>
    span{
        font-weight: bold;
    }
</style>
Dear {{ !empty($maildata['firstname'])? ucwords($maildata['firstname'].' '.$maildata['lastname']): '' }}<br /><br />
There has been a comment on  support ticket id {{$maildata['support_ticket_id']}} as follows:
<br>
{{ucfirst($maildata['adminMessage'])}}
<br>
<br>
Thank you! <br>
myWanderLoop Team
