@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
            <div class="kt-grid__item kt-grid__item--middle">
                <h3 class="kt-login__title">{{isset($authenticationErrorMessage) && !empty($authenticationErrorMessage) ? $authenticationErrorMessage : 'You are not authorized to access this resource.'}} </h3>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
</div>
@endsection
