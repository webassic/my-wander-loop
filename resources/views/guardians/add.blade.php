@extends('layouts.app')
@section('content')
<?php
//dd($exists);
?>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-link">
                    Guardian </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-user"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Guardian Details 
            </h3>
            
        </div>
    </div>
    
    @if(empty($exists) || $exists[0]['guardian_type']==null)
    
    <div class="card-body">
    <div class="form-group row">
                <div class="col-lg-6">
                   {{ Form::label("Select Guardian type:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
                    
                    <div class="radio-inline">
                        <label class="radio radio-solid">
                            {{Form::radio('guardian_type', 'Individual','1') }} Individual &nbsp; &nbsp;
                        </label>
                        <label class="radio radio-solid">
                            {{Form::radio('guardian_type', 'Institution') }} Institution &nbsp; &nbsp;
                        </label>
                    </div>
                    <br>
                    {{Form::hidden('hiddenconfirmurl','/api/guardians/guardian_type/'.auth()->user()->id,[
                        "id" => "confirmTypeUrl",
                      
                    ])}}
                    {{Form::button('Confirm',[
                        "class" => "btn btn-default yellow_bg_color black_text_color mr-2",
                        "id" => "confirmSelection",
                      
                    ])}}
                </div>
            </div>
            </div>
    @endif
    @if(!empty($exists) && $exists[0]['guardian_type']=='Individual')
        @include('guardians.individual')
    @elseif(!empty($exists) &&  $exists[0]['guardian_type']=='Institution')
        @include('guardians.institution')
    @endif
</div>
@include('guardians.confirmTypeModal')
@endsection
@push('scripts')
<script>
    jQuery(document).ready(function () {
         var baseUrl = $("#baseUrl").val();
        $(".cancelBtn").click(function () {
            location.href=baseUrl + '/guardians';
        });
        $("#confirmSelection").click(function () {
            $("#confirmTypeModal").modal();
            var radioValue = $("input[name='guardian_type']:checked").val();
            var html = "<p>Do you want to select Guardian Type as " + radioValue + "? <br>";
            html += "This selection cannot be changed once submitted. Please confirm.</p>";
            $("#customMessageConfirmType").html(html);
        });
        $("#confirmTypeOkay").click(function () {
            var radioValue = $("input[name='guardian_type']:checked").val();
            var url = $("#confirmTypeUrl").val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    "guardian_type": radioValue
                },
                dataType: "json",
                ContentType: 'application/json',
                success: function (response) {
                     location.reload();
//                    $("#confirmTypeModal").modal("hide");
                },
                error: function (error) {
                    // location.reload();
                    console.log('Error ' + error);
                },

            });
        });
    });
</script>
<script src="{{ asset('js/guardians/guardian.js') }}" defer></script>
@endpush
