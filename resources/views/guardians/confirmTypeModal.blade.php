<div class="modal fade" id="confirmTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addGroupModalLabel">Confirm Guardian Type</h5>
                <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div id="customMessageConfirmType" class="black_text_color">Are you sure about your selection?</div>
          
            </div>
           
           <div class="modal-footer">
                <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="confirm_type" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-default yellow_bg_color black_text_color" id='confirmTypeOkay'>Yes</button>
                
            </div>
           
           
        </div>
    </div>
</div>
