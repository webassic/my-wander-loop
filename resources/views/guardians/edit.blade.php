@extends('layouts.app')
@section('content')
<?php
//dd($exists);
?>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-link">
                    Guardian </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-user"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Guardian Details 
            </h3>
            
        </div>
    </div>
    @if(!empty($exists) && $exists[0]['guardian_type']=='Individual')
   
        @include('guardians.edit_individual')
    @elseif(!empty($exists) &&  $exists[0]['guardian_type']=='Institution')
    
        @include('guardians.edit_institution')
    @endif
</div>

@endsection
@push('scripts')
<script>
    jQuery(document).ready(function () {
         var baseUrl = $("#baseUrl").val();
        $(".cancelBtn").click(function () {
            location.href=baseUrl + '/guardians';
        });
    });
</script>
<script src="{{ asset('js/guardians/guardian.js') }}" defer></script>
@endpush
