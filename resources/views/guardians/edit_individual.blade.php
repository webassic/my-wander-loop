<!--Individual Form-->
{!! Form::open(array('url' => '/api/guardians/edit_individual','id'=>'editIndividualFormId')) !!}
<div class="card-body">
    <div class="alert alert-custom alert-success fade show" role="alert" id="success-alert" style='display: none;'>                                               
        <div class="alert-text" id="success-msg"></div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-top: -15px;">
                <span aria-hidden="true"><i class="ki ki-close">X</i></span>
            </button>
        </div>
    </div>            
    <div class="form-group row">
        <div class="col-lg-6">
            <h5 > <span class="text-danger"> Guardian Type : </span> 
                @if(isset($exists[0]['guardian_type']))
                {{$exists[0]['guardian_type']}}
                @endif
            </h5>


        </div>

    </div>
    <div class="form-group row">
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Title:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
           
            {{Form::text("title",(isset($exists[0]['title']))? $exists[0]['title'] :'',
                                [
                                   "class" => "form-control ",
                                   "id" => "title",    
                                   "disabled" =>"disabled",
                                ])
            }}
            <div id="titleerror" class="error" ></div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("First Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>                    
            <div class="form-group input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                {{Form::text("firstname", auth()->user()->firstname
                                   ,
                                    [
                                       "class" => "form-control",
                                       "placeholder" => "Enter First Name",
                                       "id" => "firstname",
                                       "disabled" =>"disabled",
                                    ])
                }} 
            </div>
            <div id="firstnameerror" class="error" ></div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Last Name:", null, ['class' => 'control-label']) }} <span class="text-danger">*</span>
            <div class="form-group input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                {{Form::text("lastname", auth()->user()->lastname
                                   ,
                                    [
                                       "class" => "form-control",
                                       "placeholder" => "Enter Last Name",
                                       "id" => "lastname",
                                       "disabled" =>"disabled",
                                    ])
                }}
            </div>
            <div id="lastnameerror" class="error" ></div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Email:", null, ['class' => 'control-label']) }}  <br> &nbsp;
            <div class="input-group">
                
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                {{ Form::email("email", $value = auth()->user()->email, $attributes = [
                                "class" => "form-control",
                                "placeholder"=>"Email",
                                "disabled" =>"disabled",
                                 "id"=>"email" ,
                               ])
                }}
            </div>
             <div id="emailerror" class="error" ></div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Mobile Number (Country code/Area code/Number):", null, ['class' => 'control-label']) }} 
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-mobile"></i>
                    </span>
                </div>
                {{ Form::tel("mobile", $value = auth()->user()->mobile, $attributes = [
                                "class" => "form-control",
                                "placeholder"=>"Mobile",
                                "id"=>"mobile" ,
                                "maxlength" =>"15"
                            ])
                }}
                
            </div>
                <div id="mobileerror" class="error" ></div>

        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Landline (Country code/Area code/Number):", null, ['class' => 'control-label']) }} <br> &nbsp;
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-mobile"></i>
                    </span>
                </div>
                {{ Form::tel("landline", (isset($exists[0]['landline']))? $exists[0]['landline'] : '', $attributes = [
                                "class" => "form-control",
                                "placeholder"=>"Enter Landline",
                                 "autocomplete"=>"off" ,
                                 "id"=>"landline",
                                 "maxlength" =>"15",
                                ])
                }}
            </div> 
               
            <div id="landlineerror" class="error" ></div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Date of Birth: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
            <div class="input-group date" >
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
                {{Form::text("dob",date('d',strtotime($exists[0]['dob'])).'/'.date('m',strtotime($exists[0]['dob'])).'/'.date('Y',strtotime($exists[0]['dob']))
                                   ,
                                    [
                                       "class" => "form-control",
                                       "placeholder" => "Date Of Birth",
                                       "id" => "dob",
                                       "disabled" =>"disabled",
                                    ])
                }}
               
                
                    
                <!--<div id="kt_datepicker_guardian-error" class="error invalid-feedback" style="display:none">This field is required.</div>-->
            </div>
           
            <div id="validatedob" style="color:#EF4F7A;font-size:11px;"></div>
            <div id="doberror" class="error" ></div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Address: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>
            <div class="form-group input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                {{Form::textarea("address", (isset($exists[0]['address']))? $exists[0]['address'] : ''
                                   ,
                                   $attributes = [
                                       "class" => "form-control",
                                       "placeholder" => "Enter Address",
                                       "id" => "address",
                                       "rows"=>"2",
                                       "style"=>"resize:none;"
                                    ])
                }}        <br>            
            </div>

        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Locality: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>                    
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-map-marker"></i>
                    </span>
                </div>
                {{Form::text("locality", (isset($exists[0]['locality']))? $exists[0]['locality'] : ''
                                   ,
                                   $attributes = [
                                       "class" => "form-control",
                                       "placeholder" => "Enter Address",
                                       "id" => "locality",
                                       "required" => "required",
                                    ])
                }}                        
            </div>
            <div id="localityerror" class="error" ></div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("City: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
            <div class="form-group input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-map-marker"></i></span></div>
                {{Form::text("city", (isset($exists[0]['city']))? $exists[0]['city'] : ''
                                   ,
                                   $attributes = [
                                       "class" => "form-control",
                                       "placeholder" => "Enter City",
                                       "id" => "city", 
                                       "required" => "required",
                                    ])
                }}                          
            </div>
            <!--                    <div id="cityerror" class="error" ></div>-->
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("State: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-map-marker"></i>
                    </span>
                </div>
                {{Form::text("state", (isset($exists[0]['state']))? $exists[0]['state'] : ''
                                   ,
                                   $attributes = [
                                       "class" => "form-control",
                                       "placeholder" => "Enter State",
                                       "id" => "state", 
                                       "required" => "required",
                                    ])
                }}                           
            </div>
            <!--<div id="stateerror" class="error" ></div>-->
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            {{ Form::label("Pincode: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="la la-bookmark-o"></i>
                    </span>
                </div>
                {{Form::text("pin", (isset($exists[0]['pin']))? $exists[0]['pin'] : ''
                                   ,
                                   $attributes = [
                                       "class" => "form-control",
                                       "placeholder" => "Enter Pincode",
                                       "id" => "pin", 
                                       "required" => "required",
                                       "pattern" => "^[^\s][^_][A-Za-z0-9- ]+$",
                                       "title" => "Only alphanumeric, hyphen and spaces allowed. No space allows at start.",
                                       "maxlength" => "10"
                                    ])
                }}                        
            </div>
            <!--<div id="pinerror" class="error" ></div>-->
        </div>
    </div>

</div>

<div class="card-footer">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-8">
            {{Form::submit('Save',[
                        "class" => "btn btn-default yellow_bg_color black_text_color mr-2",
                        "id" => "editIndividualSubmitId",
                      
                    ])}}
            {{Form::button('Cancel',[
                        "class" => "btn btn-default black_bg_color yellow_text_color mr-2 cancelBtn",   
                      
                    ])}}
        </div>
    </div>
</div>
<input type="hidden" id="userId" value="<?= auth()->user()->id ?>">
<input type="hidden" id="gid" value="{{(isset($exists[0]['id']))? $exists[0]['id'] : ''}}">
<input type="hidden" id="is_exist" name="is_exist"  value="1">

{{ Form::close() }}
