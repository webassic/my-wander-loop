@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-link">
                    Guardian  {{ session()->get('message') }}</a>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<!--    Begin:: Portlet -->
<!--    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                     <h3 class="kt-portlet__head-title  kt-font-danger">
                            Guardian Information
                        </h3>
                </div>
            </div>
        </div>
    </div>-->
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">                
               @if(!empty($data) && isset($data['guardian_type']) && $data['guardian_type']=='Individual')
                <div class="kt-portlet__body kt-portlet__body--fit-top" id="individual" >
                    <div class="kt-section kt-section--space-sm">
                         &nbsp;
                         <div class="form-group form-group-xs row">
                            <h3 class="kt-portlet__head-title  kt-font-danger">Guardian Information</h3>
                            <div class="alert alert-primary col-12" id="successMessageAjaxCall" style="display:none"></div>
                        </div>
                         <hr>
                         &nbsp;
                        <div class="form-group form-group-xs row">
                            <h4 class="kt-portlet__head-title  kt-font-danger">Personal Details</h4>
                        </div>
                        <div class="form-group form-group-xs row">
                           <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder">Guardian Type:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="name">
                               <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary"> {{!empty($data['guardian_type']) && isset($data['guardian_type']) ? $data['guardian_type'] : ''}}</span>
                            </div>
                        </div>
                         &nbsp;
                        <div class="form-group form-group-xs row">                            
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder">Name:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="name">
                               {{!empty($data['title']) && isset($data['title']) && $data['title']!='Others' ? ucfirst($data['title']) : ''}} {{!empty($data['firstname']) && isset($data['firstname']) ? ucfirst($data['firstname']) : ''}}  {{!empty($data['lastname']) && isset($data['lastname']) ? ucfirst($data['lastname']) : ''}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Date Of Birth:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="dob">
                                {{!empty($data['dob']) && isset($data['dob']) ? $data['dob'] : 'NA'}}
                            </div>
                        </div>

                        &nbsp;
                        <div class="form-group form-group-xs row">
                            
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Email:</span>
                            </div>
                           <div class="col-lg-3 col-md-6 col-sm-12" id="email">
                              {{!empty($data['email']) && isset($data['email']) ? $data['email'] : 'NA'}}
                            </div>
                             <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Mobile Number (Country code/Area code/Number):</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="mobile">
                                 {{!empty($data['mobile']) && isset($data['mobile']) ? $data['mobile'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                           
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Landline (Country code/Area code/Number):</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="landline">
                               {{!empty($data['landline']) && isset($data['landline']) ? $data['landline'] : 'NA'}}
                            </div>
                            
                        </div>
                        &nbsp;
                        <hr>
                       <div class="form-group form-group-xs row">
                            <h4 class="kt-portlet__head-title  kt-font-danger">Address Details</h4>
                        </div>
                        <div class="form-group form-group-xs row">
                            
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Address:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="address">
                                {{!empty($data['address']) && isset($data['address']) ? ucfirst($data['address']) : 'NA'}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Locality:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="locality">
                                {{!empty($data['locality']) && isset($data['locality']) ? $data['locality'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> City:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="city">
                               {{!empty($data['city']) && isset($data['city']) ? $data['city'] : 'NA'}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> State:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="state">
                                {{!empty($data['state']) && isset($data['state']) ? $data['state'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Pincode:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="pin">
                               {{!empty($data['pin']) && isset($data['pin']) ? $data['pin'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                    </div>
                    <div class="row mb-2">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        {{Form::button('Click Here To Update',[
                            "class" => "btn btn-default yellow_bg_color black_text_color mr-2",
                            "id" => "updateDetails",

                        ])}}
                        {{Form::button('Back',[
                            "class" => "btn btn-default black_bg_color yellow_text_color mr-2",  
                             "id" => "back",

                        ])}}
                    </div>
                </div>
                </div>
               @elseif(!empty($data) && isset($data['guardian_type']) && $data['guardian_type']=='Institution')
                <div class="kt-portlet__body kt-portlet__body--fit-top" id="institution" >
                    <div class="kt-section kt-section--space-sm">
                         &nbsp;
                        <div class="form-group form-group-xs row">
                            <h3 class="kt-portlet__head-title  kt-font-danger">Institution Details</h3>
                            <div class="alert alert-primary col-12" id="successMessageAjaxCall" style="display:none"></div>
                        </div>
                         <hr>
                         <div class="form-group form-group-xs row">
                           <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder">Guardian Type:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="name">
                               <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary">{{!empty($data['guardian_type']) && isset($data['guardian_type']) ? $data['guardian_type'] : ''}}</span>
                            </div>
                        </div>
                         &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder">Institution Name:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="mobile">
                               {{!empty($data['institution_name']) && isset($data['institution_name']) ? $data['institution_name'] : 'NA'}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Mobile Number (Country code/Area code/Number):</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="mobile">
                                 {{!empty($data['mobile']) && isset($data['mobile']) ? $data['mobile'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder">Landline (Country code/Area code/Number):</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="instituteLandline">
                                 {{!empty($data['landline']) && isset($data['landline']) ? $data['landline'] : 'NA'}}
     
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Address:</span>
                            </div>
                           <div class="col-lg-3 col-md-6 col-sm-12" id="instituteAddress">
                              {{!empty($data['address']) && isset($data['address']) ? $data['address'] : 'NA'}}
                            </div>
                            
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Locality:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="instituteLocality">
                                {{!empty($data['locality']) && isset($data['locality']) ? $data['locality'] : 'NA'}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> City:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="instituteCity">
                                {{!empty($data['city']) && isset($data['city']) ? $data['city'] : 'NA'}}
                            </div>
                            
                        </div>
                        &nbsp;
                       
                        <div class="form-group form-group-xs row">
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> State:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="instituteState">
                                {{!empty($data['state']) && isset($data['state']) ? $data['state'] : 'NA'}}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <span class="col-form-label kt-font-bolder"> Pincode:</span>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12" id="institutePincode">
                                 {{!empty($data['pin']) && isset($data['pin']) ? $data['pin'] : 'NA'}}
                            </div>
                        </div>
                        &nbsp;
                    </div>
                    <div class="row mb-2">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        {{Form::button('Click Here To Update',[
                            "class" => "btn btn-default yellow_bg_color black_text_color mr-2",
                            "id" => "updateDetails",

                        ])}}
                        {{Form::button('Back',[
                            "class" => "btn btn-default black_bg_color yellow_text_color mr-2",  
                             "id" => "back",

                        ])}}
                    </div>
                </div>
                </div>
               @endif
                
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    jQuery(document).ready(function () {
        var baseUrl = $("#baseUrl").val();
           $("#updateDetails").click(function(){
               location.href=baseUrl + '/guardians/add';
           });
           $("#back").click(function(){
               location.href=baseUrl + '/dashboard';
           });
    });
</script>
@endpush
