@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-link">
                    Guardians </a>

            </div>
        </div>
    </div>
</div>
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                View Profile
            </h3>
            <!--<span class="kt-subheader__separator kt-subheader__separator--v"></span>-->
            <!--            <div class="kt-subheader__group" id="kt_subheader_search">
                            <span class="kt-subheader__desc" id="kt_subheader_total">
                                Sandra Stone </span>
                        </div>-->
        </div>
        <!--        <div class="kt-subheader__toolbar">
                    <a href="#" class="btn btn-default btn-bold">
                        Back </a>
                </div>-->
    </div>
</div>

<!-- end:: Content Head -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--Begin:: Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-user"></i>
                        </span>
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <div class="kt-widget__user">
                                <a href="#" class="kt-widget__username">
                                    {{!empty($data['firstname']) && isset($data['firstname']) ? $data['title'].'. '.$data['firstname'] .' '.$data['lastname'] : ''}}
                                </a>
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success">{{isset($data['user_type']) && !empty($data['user_type']) ? $data['user_type'] : ''}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__subhead">
                            <a href="javascript:;"><i class="flaticon2-calendar"></i>{{isset($data['dob']) && !empty($data['dob']) ? date('d-M-Y', strtotime($data['dob'])) : ''}}</a>
                            <a href="javascript:;"><i class="flaticon2-new-email"></i>{{isset($data['email']) && !empty($data['email']) ? $data['email'] : ''}}</a>
                            <a href="javascript:;"><i class="flaticon-support"></i>{{isset($data['mobile']) && !empty($data['mobile']) ? $data['mobile'] : ''}} </a>
                            <a href="javascript:;"><i class="flaticon2-calendar-3"></i>{{isset($data['landline']) && !empty($data['landline']) ? $data['landline'] : ''}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title  kt-font-danger">
                            Address Details
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Address:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['address']) && isset($data['address']) ? $data['address'] : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Locality:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['locality']) && isset($data['locality']) ? $data['locality'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">City:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['city']) && isset($data['city']) ? $data['city'] : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">State:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['state']) && isset($data['state']) ? $data['state'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Pin Code:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['pin']) && isset($data['pin']) ? $data['pin'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title  kt-font-danger">
                            Other Details
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Identity Proof:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['identity_type']) && isset($data['identity_type']) ? $data['identity_type'] : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Renew Date:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['renew_date']) && isset($data['renew_date']) ? date('d-M-Y', strtotime($data['renew_date'])) : ''}}
                            </div>
                        </div>
                        &nbsp;
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection