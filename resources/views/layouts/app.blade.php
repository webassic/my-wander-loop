<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- begin::Head -->
    <head>
        <!-- include head section -->
        @include('layouts.head')
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
        <input type='hidden' id='baseUrl' value='{{url('/')}}'>
        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <a href="index.html">
                    <img alt="Logo" src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="150px"/>

                </a>
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
                <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>
        <!-- end:: Header Mobile -->

        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                @if(isset(auth::user()->user_type) && !empty(auth::user()->user_type) && auth::user()->user_type == 'Administration')
                @include('layouts.admin_sidebar')                    
                @elseif(isset(auth::user()->user_type) && !empty(auth::user()->user_type) && auth::user()->user_type == 'User')
                @include('layouts.guardian_sidebar')   
                @else
                @include('layouts.sidebar')   
                @endif
                <!-- begin:: Aside -->
                <!-- end:: Aside -->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
                        @include('layouts.navbar')                    
                    </div>

                    <!-- end:: Header -->
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="app">
                            <!-- begin:: Content Head -->
                            <div class="loadingImageLoader" style="background-image:url({{('/images/loading.gif')}});"></div>
                            @yield('content')
                        </div>
                        <!-- end:: Content Head -->
                    </div>
                    @include('auth.changePassword')
                    @include('layouts.footer')

                </div>
            </div>
        </div>

        <!--end::Page Scripts -->
        @include('layouts.footer_script_files')
        <script>

            //activate leftside menu and submenu..
            $(document).ready(function () {
                var pathName = window.location.pathname.split("/");
                $('.kt-menu__item a').each(function (index) {
                    var a_hrefPath = $(this).attr('href').split('/');
                    if (pathName[1] == 'dashboard') {
                        $(this).closest('.dashboardmenuclass').addClass('kt-menu__item--active');
                    } else if ((a_hrefPath[3] == pathName[1]) && (pathName[1] !== 'dashboard')) {
                        $('.kt-menu__item .dashboardmenuclass').removeClass('kt-menu__item--active');
                        $(this).closest('li').addClass('kt-menu__item--active');
                        $(this).closest('.kt-menu__item .kt-menu__item--submenu').addClass('kt-menu__item--active kt-menu__item--open');
                    }
                });
            });


            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>

        @stack('scripts')
    </body>
</html>