<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            <span><a href="https://webassic.com/" target="_blank"><img alt="Logo" src="{{url('\images\favicon\webassic_favicon.png')}}"/>
                    Web Application Designed and Developed  
                </a>
                By -<i> Webassic IT Solutions</i></span>
        </div>
    </div>
</div>
