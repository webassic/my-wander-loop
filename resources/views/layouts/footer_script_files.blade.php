<script src="{{ asset('js/datatables.bundle.js') }}" defer></script>
<script src="{{ asset('js/column-rendering.js') }}" defer></script>
<script src="{{ asset('js/changePassword/changePassword.js') }}" defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
jQuery(document).ready(function () {
//    var pathName = window.location.pathname.split("/");
//    $('ul.kt-menu__nav li a').each(function (index) {
//        var a_hrefPath = $(this).attr('href').split('/');
//        if (pathName[1] == 'dashboard') {
//            $(this).closest('.kt-menu__item').addClass('kt-menu__item--active');
//        } else if ((a_hrefPath[3] == pathName[1]) && (pathName[1] !== 'dashboard')) {
//            $('.kt-menu__nav .kt-menu__item--submenu').removeClass('kt-menu__item--active');
////                                                $(this).closest('li').addClass('kt-menu__item--active');
//            $(this).closest('.kt-menu__nav .kt-menu__item--submenu').addClass('kt-menu__item--open kt-menu__item--expanded');
//        }
//    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + "<?= session('access_token') ?>"
        }
    });
    // Retrieve
    if(localStorage.getItem("successMessage")!==''){
    if($("#successMessageAjaxCall" ).length !== 0) {
            document.getElementById("successMessageAjaxCall").innerHTML = localStorage.getItem("successMessage");
    }
        
        $("#successMessageAjaxCall").css('display','block');
        setTimeout(function(){
            $('#successMessageAjaxCall').fadeOut();
           localStorage.setItem("successMessage", '');
        },6000);
    }
    
    
    // Event log
$(function(){
    var baseUrl = $("#baseUrl").val();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: baseUrl + '/api/eventlog' ,
        dataType: "json",
        success: function (response) {
            if (response == 0) {
                 $("#eventLog").css('display', 'block');  
            }else{
               $("#eventLog").css('display', 'none');  
            }
        },
        error: function (error) {
            console.log(error);


        }
    });
});
});
</script>