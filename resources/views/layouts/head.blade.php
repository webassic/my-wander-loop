<head>
    <meta charset="utf-8">
    <title>My Wander Loop</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'My Wander Loop') }}</title>

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom_style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{url('/images/favicon/myWanderLoop_Icon.png')}}" style="flex: 0 1 auto;height: 50px;width: 50px;border-radius: 50%;"/>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <style>
        .kt-aside-menu, #kt_aside {
            background-color: #F6C648;
        }
        .kt-aside-menu .kt-menu__nav > .kt-menu__item > .kt-menu__link .kt-menu__link-text {
            color: #000;
        }
        .kt-font-brand {
            color: #F6C648!important;
        }
        .yellow_text_color{
            color:#F6C648!important;
        }
        .yellow_bg_color{
            background-color: #F6C648!important;
        }
        .black_text_color{
            color:#000!important;
        }
        .kt-header-mobile .kt-header-mobile__toolbar .kt-header-mobile__toggler {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgb(246,198,75)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E")!important;
          }
          #kt_header_mobile_topbar_toggler i{
              color:rgb(246,198,75);
          }
        .black_bg_color{
            background-color: #000!important;

        }
        .kt-aside-menu .kt-menu__nav >  .kt-menu__heading .kt-menu__link-text, .kt-aside-menu .kt-menu__nav > .kt-menu__link .kt-menu__link-text {
            color: #000;
        }
        .kt-menu__item.kt-menu__item--active {
            color: #FFF;
        }
        .help_text{
            font-size:12px;
        }
    </style>
</head>
