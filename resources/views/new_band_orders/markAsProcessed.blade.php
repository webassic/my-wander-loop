<div class="modal fade" id="markAsProcessedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{url('/api/new_band_orders/updateStatus')}}" id="markAsProcessedFromId" name="mark_as_processed">
                <div class="modal-header">
                    <h5 class="modal-title">Mark As Processed</h5>
                    <button type="button" class="close" data-formname="mark_as_processed" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                                 
                    <div class="col-12">Do you want to mark this order as PROCESSED?</div>
                    <input type="hidden" id="markAsProcessedInput">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="delete_promocode" data-dismiss="modal" >No</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='markAsProcessedSubmitBtnId'>Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>