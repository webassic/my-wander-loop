<div class="modal fade" id="markAsShippedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{url('/api/new_band_orders/updateStatus')}}" id="markAsShippedFromId" name="mark_as_shipped">
                <div class="modal-header">
                    <h5 class="modal-title">Mark As Shipped</h5>
                    <button type="button" class="close" data-formname="mark_as_shipped" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                                 
                    <div class="col-12">Do you want to mark this order as SHIPPED?</div>
                    <input type="hidden" id="markAsShippedInput">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="delete_promocode" data-dismiss="modal" >No</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='markAsShippedSubmitBtnId'>Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>