@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/users_new_band_orders')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/users_new_band_orders')}}" class="kt-subheader__breadcrumbs-link">
                    Recent Lost/Damage Reorders </a>
            </div>
        </div>
    </div>
</div>

<div class="row" id="successMsg"> 
    @if(Session::has('success'))
    <div class="col-sm-12 alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Recent Lost/Damage Reorders List
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="dropdown dropdown-inline">
                    <a href="{{ url('/export_user_bandorders')}}" class="add-margin btn btn-default btn-icon-sm yellow_bg_color black_text_color" >
                        <i class="la la-download black_text_color"></i> Export to Excel
                    </a>                             
                </div>

            </div>		
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row" > 
            <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                <div class="alert-text" id="success-msg"></div>
            </div> 
            <div class="alert alert-custom alert-danger fade show col-12" role="alert" id="danger-alert" style='display: none;'>                                               
                <div class="alert-text" id="error-msg"></div>
            </div> 
        </div> 

        <table class="table table-striped table-bordered table-hover table-checkable" id="user-new-band-orders-list">
            <thead>
                <tr>
                    <th>Band User Name</th>
                    <th>No. Of Bands</th>
                    <th>Band Lost</th>
                    <th>Price Per Band</th>
                    <th>Total Amount</th>
                    <th>Paid Amount</th>
                    <th>Payment ID</th>
                    <th>Order Status</th>
                    <th>Order Date</th>
                    <th>Id</th>
                    <th>Package Type</th>
                    <th>Download Invoice</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- end:: Content -->


@endsection
@push('scripts')
<script src="{{ asset('js/new_band_orders/new_band_orders_user.js') }}" defer></script>
@endpush
