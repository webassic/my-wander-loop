@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/promo-codes')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/promo-codes')}}" class="kt-subheader__breadcrumbs-link">
                    Promo Codes </a>
            </div>
        </div>
    </div>
</div>
<div class="row" id="successMsg"> 
    @if(Session::has('success'))
    <div class="col-sm-12 alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Edit Promo Code
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-sm-12" id='edit_promocode_status'></div>
            <div class="col-sm-12" id='edit_error_promocode_status'></div>
        </div>
        {!! Form::open(array('method' => 'POST','url' => '/api/promocodes/update','id'=>'editPromoCodeFormId','name' => 'edit_promo_codes')) !!}
        <input type="hidden" name="id" value="{{$promoCodeDetails['id']}}" />
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Promo Code Name", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('name',isset($promoCodeDetails['name']) ? $promoCodeDetails['name'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Promo Code name', 'tabindex'=>'1','id' => 'name','style' => 'text-transform:uppercase','maxlength' => 10))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Valid Till Date", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    <div class="input-group date"  data-z-index="1100">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="la la-calendar-check-o glyphicon-th"></i></span>
                        </div>
                        <input type="text" class="form-control" id="valid_till"  name="valid_till" placeholder="Select Date" required="required" autocomplete="off" value="{{isset($promoCodeDetails['valid_till']) ? $promoCodeDetails['valid_till'] : ''}}"/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Applicable to", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">
                    {!!Form::select('users_id',
                    isset($data['usersList']) ? $data['usersList'] : [],
                    isset($promoCodeDetails['user_ids']) && !empty($promoCodeDetails['user_ids']) ? $promoCodeDetails['user_ids'] : null,['class' => 'form-control select2', 
                    'required'=>'required','id' => 'users_id', 'multiple' => 'multiple']) !!}
                </div>
                <div class="col-lg-3">
                    @if(isset($promoCodeDetails['user_ids']) && !empty($promoCodeDetails['user_ids']))
                    <?php
                    $countForUserIds = 0;
                    $countForUserIds = substr_count($promoCodeDetails['user_ids'], ",") + 1;
                    ?>
                    @if($countForUserIds == count($data['usersList']))
                    <input id="selectAllId" type="checkbox" checked="checked"> Select All
                    @else
                    <input id="selectAllId" type="checkbox"> Select All
                    @endif
                    @else
                    <input id="selectAllId" type="checkbox"> Select All
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {!! Form::label('Discount Offered Type') !!}
                </div>
                <div class="col-lg-6">
                    <?php $discount_offered_type = ['%' => '%', 'Rs' => 'Rs']; ?>
                    {!!Form::select('discount_offered_type',$discount_offered_type,isset($promoCodeDetails['discount_offered_type']) && $promoCodeDetails['discount_offered_type'] != '' ? $promoCodeDetails['discount_offered_type'] : null,['class' => 'form-control','id'=> 'discount_offered_type', 'required'=>'required',"title" => "Select Discount Offered Type"]) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Discount Offered", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">
                    {!!Form::number('discount_offered',isset($promoCodeDetails['discount_offered']) ? $promoCodeDetails['discount_offered'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Discount Offered','id' => 'discount_offered','min' => 0,'max' => '100','tabindex'=>'2'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {!! Form::label('Status') !!}
                </div>
                <div class="col-lg-6">
                    <?php $status = ['active' => 'Active', 'inactive' => 'Inactive']; ?>
                    {!!Form::select('is_active',$status,isset($promoCodeDetails['is_active']) && $promoCodeDetails['is_active'] != '' ? $promoCodeDetails['is_active'] : null,['class' => 'form-control','id'=> 'is_active', 'required'=>'required','tabindex'=>'10',"title" => "Select Status"]) !!}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="reset" class="btn btn-default model-close black_bg_color yellow_text_color"
                            data-formname="add_promocodes" data-dismiss="modal" value="Reset">Cancel</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='editPromoCodesSubmitBtnId'>Update</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
<?php
$user_ids = isset($promoCodeDetails['user_ids']) ? $promoCodeDetails['user_ids'] : [];
?>
@endsection
@push('scripts')
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}" defer></script>
<script>
var KTSelect2 = function () {
    // Private functions
    var demos = function () {
        // multi select
        $('#users_id').select2({
            placeholder: "Please Select"
        });
        $('.kt-selectpicker').selectpicker();
    }

    // Public functions
    return {
        init: function () {
            demos();
        }
    };
}();
// Initialization
jQuery(document).ready(function () {
    KTSelect2.init();
});

</script>
<script>
    $(document).ready(function () {
        var userID = '<?php echo $promoCodeDetails['user_ids']; ?>'
        usersArray = userID.split(',');
        $('#users_id').val(usersArray).change();

        $('#users_id').select2();
        $("#selectAllId").click(function () {
            if ($("#selectAllId").is(':checked')) {
                $("#users_id > option").prop("selected", "selected");
                $("#users_id").trigger("change");
            } else {
                $("#users_id > option").prop("selected", false);
                $("#users_id").trigger("change");
            }
        });

    });
</script>
<script src="{{ asset('js/promocodes/promocodes.js') }}" defer></script>
@endpush