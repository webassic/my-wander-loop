@extends('layouts.app')
<style>
    .horizontalLine{
        border-bottom: 1px dashed #ecf0f3;
         margin:-30px 0 20px 0;
    }
</style>
@section('content')
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/razorpay_credentials/'.$data['id']) }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/razorpay_credentials/'.$data['id']) }}" class="kt-subheader__breadcrumbs-link">
                    Settings </a>
            </div>
        </div>
    </div>
</div> 
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Settings
            </h3>
        </div>
    </div>
    {!! Form::open(array('method' => 'POST','url' => '/api/razorpay_credentials/update','id'=>'editRazorPayFormId','name' => 'edit_razorpay')) !!}
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-sm-12" id='success-alert'></div>
        </div>
        <div class="kt-portlet__head-label">
            <h5 class="kt-portlet__head-title  kt-font-danger">
                {{!empty($data['test_mode']) && isset($data['test_mode']) ? $data['test_mode'] : ''}}
            </h5>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::hidden('eid',$data['id'],array('id'=>'eid'))!!}
                    {!!Form::text('test_key_id',isset($data['test_key_id']) ? $data['test_key_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Key ID', 'tabindex'=>'1','id' => 'test_key_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key Secret:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>  
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_key_secret',isset($data['test_key_secret']) ? $data['test_key_secret'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Key Secret', 'tabindex'=>'2','id' => 'test_key_secret'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Basic Plan ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_basic_plan_id',isset($data['test_basic_plan_id']) ? $data['test_basic_plan_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Basic Plan ID', 'tabindex'=>'3','id' => 'test_basic_plan_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Standard Plan ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_plan_id',isset($data['test_plan_id']) ? $data['test_plan_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Standard Plan ID', 'tabindex'=>'4','id' => 'test_plan_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Cost of 1 Band in INR including GST:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_banduser_cost',isset($data['test_banduser_cost']) ? $data['test_banduser_cost'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Cost', 'tabindex'=>'5','id' => 'test_banduser_cost'))!!}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice Number Prefix:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_invoice_number_prefix',isset($data['test_invoice_number_prefix']) ? $data['test_invoice_number_prefix'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Invoice Number Prefix', 'tabindex'=>'6','id' => 'test_invoice_number_prefix', 'minlength' => 4, 'maxlength' => 4))!!}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice number Start From:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_invoice_number_start',isset($data['test_invoice_number_start']) ? $data['test_invoice_number_start'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Invoice number Start From', 'tabindex'=>'7','id' => 'test_invoice_number_start', 'minlength' => 6, 'maxlength' => 10))!!}
                </div>
            </div>  
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("GST Rate:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('test_gst_rate',isset($data['test_gst_rate']) ? $data['test_gst_rate'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter GST Rate', 'tabindex'=>'8','id' => 'test_gst_rate'))!!}
                </div>
            </div>
        </div>
        <div class="horizontalLine"></div>
        <div class="kt-portlet__head-label">
            <h5 class="kt-portlet__head-title  kt-font-danger">
                {{!empty($data['live_mode']) && isset($data['live_mode']) ? $data['live_mode'] : ''}}
            </h5>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_key_id',isset($data['live_key_id']) ? $data['live_key_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Key ID', 'tabindex'=>'9','id' => 'live_key_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key Secret:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>  
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_key_secret',isset($data['live_key_secret']) ? $data['live_key_secret'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Key Secret', 'tabindex'=>'10','id' => 'live_key_secret'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Basic Plan ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_basic_plan_id',isset($data['live_basic_plan_id']) ? $data['live_basic_plan_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Basic Plan ID', 'tabindex'=>'11','id' => 'live_basic_plan_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Standard Plan ID:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_plan_id',isset($data['live_plan_id']) ? $data['live_plan_id'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Plan ID', 'tabindex'=>'12','id' => 'live_plan_id'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Cost of 1 Band in INR including GST:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_banduser_cost',isset($data['live_banduser_cost']) ? $data['live_banduser_cost'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Cost', 'tabindex'=>'13','id' => 'live_banduser_cost'))!!}
                </div>
            </div>
             <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice Number Prefix:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_invoice_number_prefix',isset($data['live_invoice_number_prefix']) ? $data['live_invoice_number_prefix'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Invoice Number Prefix', 'tabindex'=>'14','id' => 'live_invoice_number_prefix', 'minlength' => 4, 'maxlength' => 4))!!}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice number Start From:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_invoice_number_start',isset($data['live_invoice_number_start']) ? $data['live_invoice_number_start'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Invoice number Start From', 'tabindex'=>'15','id' => 'live_invoice_number_start', 'minlength' => 6, 'maxlength' => 10))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("GST Rate:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>   
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('live_gst_rate',isset($data['live_gst_rate']) ? $data['live_gst_rate'] : '',array('required'=>'required','class'=>'form-control','placeholder'=>'Enter GST Rate', 'tabindex'=>'16','id' => 'live_gst_rate'))!!}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-6">
                    <a href="{{ url('/razorpay_credentials/'.$data['id']) }}" class="btn btn-default black_bg_color yellow_text_color mr-2" >Cancel</a>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='editRazorPaySubmitBtnId'>Update</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/razorpay/razorpay_credentials.js') }}" defer></script>
@endpush