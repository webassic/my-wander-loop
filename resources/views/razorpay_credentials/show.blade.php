@extends('layouts.app')
<style>
    .horizontalLine{
        border-bottom: 1px dashed #ecf0f3;
        margin:-30px 0 20px 0;
    }
</style>
@section('content')
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/razorpay_credentials/'.$data['id']) }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/razorpay_credentials/'.$data['id']) }}" class="kt-subheader__breadcrumbs-link">
                    Settings </a>
            </div>
        </div>
    </div>
</div> 
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Settings
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-portlet__head-label">
            <h5 class="kt-portlet__head-title  kt-font-danger">
                {{!empty($data['test_mode']) && isset($data['test_mode']) ? $data['test_mode'] : ''}}
            </h5>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_key_id']) && isset($data['test_key_id']) ? $data['test_key_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key Secret", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_key_secret']) && isset($data['test_key_secret']) ? $data['test_key_secret'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Basic Plan ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_basic_plan_id']) && isset($data['test_basic_plan_id']) ? $data['test_basic_plan_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Standard Plan ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_plan_id']) && isset($data['test_plan_id']) ? $data['test_plan_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Cost of 1 Band in INR including GST", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_banduser_cost']) && isset($data['test_banduser_cost']) ? $data['test_banduser_cost'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice Number Prefix", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_invoice_number_prefix']) && isset($data['test_invoice_number_prefix']) ? $data['test_invoice_number_prefix'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice number Start From", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_invoice_number_start']) && isset($data['test_invoice_number_start']) ? $data['test_invoice_number_start'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Last Invoice Number", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_last_invoice_number']) && isset($data['test_last_invoice_number']) ? $data['test_last_invoice_number'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("GST Rate", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['test_gst_rate']) && isset($data['test_gst_rate']) ? $data['test_gst_rate'] : ''}}
                </div>
            </div>           
        </div>
        <div class="horizontalLine"></div>
        <div class="kt-portlet__head-label">
            <h5 class="kt-portlet__head-title  kt-font-danger">
                {{!empty($data['live_mode']) && isset($data['live_mode']) ? $data['live_mode'] : ''}}
            </h5>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_key_id']) && isset($data['live_key_id']) ? $data['live_key_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Razorpay Key Secret", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_key_secret']) && isset($data['live_key_secret']) ? $data['live_key_secret'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Basic Plan ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_basic_plan_id']) && isset($data['live_basic_plan_id']) ? $data['live_basic_plan_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Standard Plan ID", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_plan_id']) && isset($data['live_plan_id']) ? $data['live_plan_id'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Cost of 1 Band in INR including GST", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_banduser_cost']) && isset($data['live_banduser_cost']) ? $data['live_banduser_cost'] : ''}}
                </div>
            </div>  
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice Number Prefix", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_invoice_number_prefix']) && isset($data['live_invoice_number_prefix']) ? $data['live_invoice_number_prefix'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Invoice number Start From", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_invoice_number_start']) && isset($data['live_invoice_number_start']) ? $data['live_invoice_number_start'] : ''}}
                </div>
            </div>           
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("Last Invoice Number", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_last_invoice_number']) && isset($data['live_last_invoice_number']) ? $data['live_last_invoice_number'] : ''}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    {{ Form::label("GST Rate", null, ['class' => 'control-label']) }} 
                </div>
                <div class="col-lg-6"> 
                    {{!empty($data['live_gst_rate']) && isset($data['live_gst_rate']) ? $data['live_gst_rate'] : ''}}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-6">
                    <a href="{{ url('/dashboard') }}" class="btn btn-default black_bg_color yellow_text_color mr-2" >Cancel</a>
                    <a href="{{ url('/razorpay_credentials/edit/'.$data['id']) }}" class="btn btn-default yellow_bg_color black_text_color">
                        Edit
                    </a>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts') 
@endpush