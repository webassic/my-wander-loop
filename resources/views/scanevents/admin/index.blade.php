@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/scanevents/index')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/scanevents/index')}}" class="kt-subheader__breadcrumbs-link">
                    Scan Events </a>

            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
   
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Scan Events
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                      
                        <div class="dropdown dropdown-inline">
                            <a href="{{ URL::to('export_adminscanevent') }}" class="add-margin btn btn-warning btn-icon-sm" >
                                <i class="la la-download"></i> Export to Excel
                            </a>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="admin_scanevent_list">
                <thead>
                    <tr>
                        <th>Guardian Name</th>
                        <th>Band User Unique Id</th>
                        <th>Band User Name</th>
                        <th>Scanner Mobile</th>
                        <th>Scanned On</th>
                        <th>Scanner Mobile Verified</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
@include('scanevents.admin.view_scanevent')
@endsection
@push('scripts')
<script src="{{ asset('js/scanevents/scaneventslisting.js') }}" defer></script>
@endpush
        