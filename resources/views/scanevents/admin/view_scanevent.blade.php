
<div class="modal fade " id="adminViewScanEventsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
           
            <div class="modal-header">
                <h5 class="modal-title" id="viewScanEventsModalLabel">Scan Events</h5>
                <button type="button" class="close" data-formname="confirm_type" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="kt-portlet kt-portlet--head-noborder">   
                                    <div class="kt-portlet__body kt-portlet__body--fit-top" >
                                        <div class="kt-section kt-section--space-sm">
                                            <div class="form-group form-group-xs row">
                                                <h5 class="kt-portlet__head-title  kt-font-danger">Band Users Details</h5>
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">

                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder">Band User Uniqueid:</span>
                                                </div>
                                                <div class="col-3" id="banduser_uniqueid">

                                                </div>
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Band User Name:</span>
                                                </div>
                                                <div class="col-3" id="banduser_name">

                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">

                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder">Guardian Name :</span>
                                                </div>
                                                <div class="col-3" id="guardian_name">

                                                </div>
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Guardian's Email Id:</span>
                                                </div>
                                                <div class="col-3" id="guardian_email">

                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">

                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder">Guardian's Mobile number :</span>
                                                </div>
                                                <div class="col-3" id="guardian_mobile">

                                                </div>
                                                
                                            </div>

                                            &nbsp;
                                            <hr>
                                            <div class="form-group form-group-xs row">
                                                <h5 class="kt-portlet__head-title  kt-font-danger">Scanner's Details :</h5>
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Mobile Number:</span>
                                                </div>
                                                <div class="col-3" id="scanner_mobile">

                                                </div>
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Mobile verified :</span>
                                                </div>
                                                <div class="col-3" id="mobile_verify">

                                                </div>
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Scanned On :</span>
                                                </div>
                                                <div class="col-3" id="scanned_on">
                                                </div>
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Scanner's Location:</span>
                                                </div>
                                                <div class="col-3" id="location">  
                                                    ---
                                                </div> 
                                            </div>
                                            &nbsp;
                                            <div class="form-group form-group-xs row">
                                                
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Location Not Captured Reason :</span>
                                                </div>
                                                <div class="col-3" id="latlog_error">
                                                </div>
                                                <div class="col-3">
                                                    <span class="col-form-label kt-font-bolder"> Band User Information Displayed ? :</span>
                                                </div>
                                                <div class="col-3" id="banduser_info">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
           
           
        </div>
    </div>
</div>


