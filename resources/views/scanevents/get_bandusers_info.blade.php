<div id="banduserDetailsContainer" class="col-md-12" style="display:none">
    <div id="detailsContainer">
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-4 col-4">
                <img src="{{url('/images/users/default.jpg')}}" width="100px" id='profile_img'>
            </div>
            <div class="col-md-8 col-8">
                <div class="form-group form-group-xs row">
                    <div class="col-md-6 text-justify">
                        <span class="col-form-label kt-font-bolder">Blood Group:</span>
                    </div>
                    <div class="col-md-6" id="bloodgroup">
                    </div>
                </div>
                &nbsp;
                <div class="form-group form-group-xs row">
                    <div class="col-md-12 text-justify">
                        <span class="col-form-label kt-font-bolder">Emergency Contact No:</span>
                    </div>
                    <div class="col-md-12" id="primary_mobile">

                    </div>
                </div> 
            </div>
        </div>
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Name:</span>
            </div>
            <div class="col-md-6" id="banduser_name">

            </div>
        </div>
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">DOB:</span>
            </div>
            <div class="col-md-6" id="date_of_birth">

            </div>
        </div>
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Address:</span>
            </div>
            <div class="col-md-6" id="fulladdress">

            </div>
        </div>
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Medical History</span>
            </div>
        </div>
        &nbsp;
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Condition 1 :</span>
            </div>
            <div class="col-md-6" id="condition1">

            </div>
        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Condition 2 :</span>
            </div>
            <div class="col-md-6" id="condition2">

            </div>
        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Emergency Medication :</span>
            </div>
            <div class="col-md-6" id="emergencyMedication">
            </div>

        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Preferred Doctor :</span>
            </div>
            <div class="col-md-6" id="preferredDoctor">
            </div>

        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6 text-justify">
                <span class="col-form-label kt-font-bolder">Preferred Hospital :</span>
            </div>
            <div class="col-md-6" id="hospitalPreference">
            </div>
        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6">
                <span class="col-form-label kt-font-bolder">Hospital Registration:</span>
            </div>
            <div class="col-md-6" id="hospitalRegistration">
            </div>
        </div>
        &nbsp; 
        <div class="form-group form-group-xs row">
            <div class="col-md-6">
                <span class="col-form-label kt-font-bolder">Preferred Languages:</span>
            </div>
            <div class="col-md-6" id="preferredLanguages">
            </div>
        </div>
        &nbsp; 
    </div>
    <div id="detailsErrorContainer" style="display: none;">
        <div class="form-group form-group-xs row">
            <div class="col-md-12 mt-5 mb-5" id="detailsErrorMessageDiv">

            </div>
        </div>
    </div>
</div>