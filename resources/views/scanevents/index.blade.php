<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <base href="../../../">
        <meta charset="utf-8" />
        <title>My Wander Loop | Login</title>
        <meta name="description" content="Login page example">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
        <!--end::Fonts -->
        <link href="{{asset('css/login-6.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="{{url('/images/favicon/myWanderLoop_Icon.png')}}" style="flex: 0 1 auto;height: 50px;width: 50px;border-radius: 50%;"/>
        <style>
            *{
                color:#000;
            }
        </style> 
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root" id="app">
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                    <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                        <div class="kt-login__wrapper">
                            <div class="kt-login__container">
                                <div class="kt-login__body">
                                    <div class="kt-login__logo  text-center">
                                        <a href="/">
                                            <img src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"> <br>
                                        </a>
                                    </div>
                                    <div class="kt-login__signin">
                                        <div class="m-login__head text-center">
                                            <h3 class="m-login__title" id="mainHeading">
                                                Get Band User's Information
                                            </h3>
                                        </div>
                                        <div class="kt-login__form">
                                            <!--success message--> 
                                            <div class="alert alert-success" id="successMessage" style="display:none"></div>

                                            <!--error message--> 
                                            <div class="alert alert-danger" id="errorMessage" style="display:none"></div>
                                            <input type="hidden" id="storeMobileStore">
                                            <input type="hidden" id="generatedOtp" name="generatedOtp">
                                            <input type="hidden" id="hiddenUid">
                                            <!--begin::Form-->
                                            <form  class="kt-form"  id="scanEventsFormId" >
                                                @csrf
                                                <div class="form-group">
                                                    <input id="uniqueid" type="text" placeholder="Enter Band User's Unique Id" class="form-control " name="uniqueid" value="{{isset($_GET['uid']) ? $_GET['uid'] :''}}" required  autofocus>
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <input id="mobile" type="tel" placeholder="Enter Your Mobile Number" class="form-control " name="mobile" value="" required  autofocus>
                                                   
                                                </div>
                                               
                                                <!--begin::Action-->
                                                <div class="kt-login__actions">
                                                    <button id="verifyMobile" type="submit" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648;">
                                                        {{ __('Verify Your Mobile Number') }}
                                                    </button>
                                                </div>
                                                <!--end::Action-->
                                            </form>
                                            <!--begin::Form-->
                                            <form  class="kt-form"  id="mobileVerificationFormId" style="display:none;">
                                                @csrf
                                                <div class="form-group">
                                                    <input id="otp" type="text" placeholder="Enter OTP" class="form-control " name="uniqueid" value="{{isset($_GET['uid']) ? $_GET['uid'] :''}}" required  autofocus>
                                                   
                                                </div>
                                                
                                               
                                                <!--begin::Action-->
                                                <div class="kt-login__actions">
                                                    <button id="mobileVerificationBtnId" type="submit" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648;">
                                                        {{ __('Verify OTP') }}
                                                    </button>
                                                </div>
                                                <!--end::Action-->
                                            </form>
                                            @include('scanevents.get_bandusers_info')
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-login__account">
                                <span class="kt-login__account-msg">
                                    Don't have an account yet ?
                                </span>&nbsp;&nbsp;
                                <a href="{{route('register')}}" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a>
                                @if (Route::has('password.request'))
                                <a class="kt-login__account-link" href="{{ route('login') }}">
                                    {{ __('Login') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-color: #F6C648;">
                        <div class="kt-login__section ">
                            <div class="kt-login__block text-center">
                                <h3 class="m-login__welcome text-left" style="margin-left:15%;margin-right:15%;">
                                    <div class="check_icon">
                                        <i class="fa fa-check"></i> &nbsp; Help people get started on becoming dementia-friendly
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="check_icon">
                                        <i class="fa fa-check"></i> &nbsp; Identify Dementia patients in case they wander off
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="check_icon">
                                        <i class="fa fa-check"></i> &nbsp; Get Silicon wrist-band printed with QR code
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="check_icon">
                                        <i class="fa fa-check"></i> &nbsp; The most cost effective solution to Alzheimer patients
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="check_icon">
                                        <i class="fa fa-check"></i> &nbsp;Highest level of privacy and security. <a href="{{ route('register') }}"  style="color:#0995D6">Sign up</a> Now!
                                    </div>

                                </h3>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- end:: Page 
        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/scanevents/getbandusers_info.js') }}" defer></script>
        <!--end::Page Scripts -->
    </body>
    <!-- end::Body -->
</html>