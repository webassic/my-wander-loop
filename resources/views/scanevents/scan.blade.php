<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <base href="../../../">
        <meta charset="utf-8" />
        <title>My Wander Loop | Scan Event</title>
        <meta name="description" content="Login page example">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--begin::Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
        <!--end::Fonts -->
        <link href="{{asset('css/login-6.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="{{url('/images/favicon/myWanderLoop_Icon.png')}}" style="flex: 0 1 auto;height: 50px;width: 50px;border-radius: 50%;"/>
        <style>
            *{
                color:#000;
            }
            .kt-grid__item{
                width:100%!important;
            }
            .custom_kt-login__body{
                border:2px solid yellow;
                border-radius:5px;
                padding:20px 20px;
            } 
            .help_text{
                font-size: 12px;
                color: #646c9a;
                padding-top: 12px;
            }
            .loadingImageLoader{
                display: none;
                position:fixed;
                top:0px;
                right:0px;
                width:100%;
                height:100%;
                background-color:#666;    
                background-repeat:no-repeat;
                background-position:center;
                z-index:10000000; 
                opacity: 0.4;
            }
            .kt-login.kt-login--v6 .kt-login__aside .kt-login__wrapper .kt-login__container .kt-login__logo {
                margin: 1rem auto 1rem auto !important;
            }
            .kt-login.kt-login--v6 .kt-login__aside .kt-login__wrapper .kt-login__container .kt-login__form {
                margin-top: 1rem !important;
            }
            @media (min-width:360px) and (max-width:450px){
                #profile_img{
                    width:80px;
                }
            }
            @media (max-width:359.9px){
                #profile_img{
                    width:60px;
                }
            }
        </style> 
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
        <input type='hidden' id='baseUrl' value='{{url('/')}}'>
        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root" id="app">
            <div class="loadingImageLoader" style="background-image:url({{('/images/loading.gif')}});"></div>
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                    <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                        <div class="kt-login__wrapper" >
                            <div class="kt-login__container">
                                <div class="kt-login__body custom_kt-login__body" style="min-height: 450px;">
                                    <div class="kt-login__logo  text-center">
                                        <a href="/">
                                            <img src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"> <br>
                                        </a>
                                        <h5 class="mt-2"> Securing your freedom</h5>
                                    </div>
                                    <div class="kt-login__signin"> 
                                        <div class="kt-login__form" >
                                            <h3 class="m-login__title text-center" id="mainHeading">
                                                Senior Citizen Details
                                            </h3>
                                            &nbsp;
                                            <!--success message--> 
                                            <div class="alert alert-success" id="successMessage" style="display:none"></div>

                                            <!--error message--> 
                                            <div class="alert alert-danger" id="errorMessage" style="display:none"></div>
                                            <input type="hidden" id="storeMobileStore">
                                            <input type="hidden" id="generatedOtp" name="generatedOtp">
                                            <input type="hidden" id="hiddenUid">
                                            <input type="hidden" id="scaneventId">
                                            <input type="hidden" id="lat_long_error">
                                            <input type="hidden" id="latitude">
                                            <input type="hidden" id="logitude">

                                            <!--begin::Form-->
                                            <form  class="kt-form"  id="scanEventsFormId" style="display: none;">
                                                @csrf
                                                <div class="form-group">
                                                    <input id="uniqueid" type="text" placeholder="Enter Band User's Unique Id" class="form-control " name="uniqueid" value="{{isset($_GET['x']) ? $_GET['x'] :''}}" required readonly>

                                                </div>
                                                <div class="form-group">
                                                    <input id="mobile" type="tel" placeholder="Enter Your Mobile Number" class="form-control " name="mobile" value="" required  autofocus>

                                                </div>
                                                &nbsp;                                                
                                                <!--begin::Action-->
                                                <div class="kt-login__actions">
                                                    <button id="verifyMobile" type="submit" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648;">
                                                        {{ __('Submit') }}
                                                    </button>
                                                </div>
                                                <!--end::Action-->
                                            </form>
                                            <div id="invalidUniqueErrorMessage" style="display: none;padding-top: 50px;color: red;text-align: center;">
                                                Sorry unique ID is inactive. Please ensure you have the right unique ID and try again.
                                            </div>
                                            <!--begin::Form-->
                                            <form  class="kt-form"  id="mobileVerificationFormId" style="display:none;">
                                                @csrf
                                                <div class="form-group">
                                                    <input id="otp" type="text" placeholder="Enter OTP" class="form-control " name="uniqueid" value="{{isset($_GET['uid']) ? $_GET['uid'] :''}}" required  autofocus>
                                                </div>
                                                <div class="help_text">Depending upon the telephone operator, it may take a few minutes to hours to receive the OTP. If you do not receive it, retry by reloading this page.</div>

                                                <!--begin::Action-->
                                                <div class="kt-login__actions">
                                                    <button id="mobileVerificationBtnId" type="submit" class="btn btn-brand btn-pill btn-elevate" style="background-color: #F6C648;">
                                                        {{ __('Verify OTP') }}
                                                    </button>
                                                </div>
                                                <!--end::Action-->
                                            </form>
                                            @include('scanevents.get_bandusers_info')


                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="kt-login__account">
                                <span class="kt-login__account-msg">
                                    For any emergency please contact on <a href="tel:+91-945-5550-650" class="text-danger">+91-945-5550-650 </a>
                                </span>&nbsp;&nbsp;
                            </div>-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <!-- end:: Page 
        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/scanevents/getbandusers_info.js') }}" defer></script>
   
   
        <!--end::Page Scripts -->
    </body>
    <!-- end::Body -->
</html>