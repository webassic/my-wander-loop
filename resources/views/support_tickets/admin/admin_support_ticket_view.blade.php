@extends('layouts.app')
@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/support_tickets/admin/index')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/support_tickets/admin/index')}}" class="kt-subheader__breadcrumbs-link">
                    Help Desk </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="row">
    <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
        <div class="alert-text" id="success-msg"></div>
    </div> 
    <div class="alert alert-custom alert-danger fade show col-12" role="alert" id="danger-alert" style='display: none;'>                                               
        <div class="alert-text" id="error-msg"></div>
    </div> 
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"> 
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    View Support Ticket 
                    <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline" id="showStatus"></span>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper" id="checkSupportTicketStatus" style="display: none;">
                    <div class="kt-portlet__head-actions">                     
                        <a href="#" data-toggle="modal" data-target="#markAsResolvedSupportTicketModal" onclick="resolvedSupportTicket({{$supportid}}, 'resolved')" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color">
                            <i class="la la-check"></i>
                            Mark As Resolved
                        </a>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-xl-12">
                    <div class="kt-portlet kt-portlet--head-noborder">   
                        <div class="kt-portlet__body kt-portlet__body--fit-top" >
                            <div class="kt-section kt-section--space-sm">
                                <div class="form-group form-group-xs row">
                                    <h5 class="kt-portlet__head-title  kt-font-danger"><span id="unique_ticket_id"></span> : <span id="subject"></span></h5>
                                </div>
                                &nbsp;
                                <div class="form-group form-group-xs row">
                                    <div class="col-3">
                                        <span class="col-form-label kt-font-bolder"> Created By :</span>
                                    </div>
                                    <div class="col-3" id="gardian_name">

                                    </div>

                                </div>
                                &nbsp;
                                <div class="form-group form-group-xs row">
                                    <div class="col-3">
                                        <span class="col-form-label kt-font-bolder"> Type:</span>
                                    </div>
                                    <div class="col-3" id="related_type">  
                                        ---
                                    </div> 
                                </div> 
                                &nbsp;
                                <div class="form-group form-group-xs row">
                                    <div class="col-3">
                                        <span class="col-form-label kt-font-bolder"> Message:</span>
                                    </div>
                                    <div class="col-6" id="message">  
                                        ---
                                    </div>  
                                </div>
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content">
                    <div class="kt-chat">
                        <div class="kt-portlet kt-portlet--head-lg- kt-portlet--last">                        
                            <div class="kt-portlet__body">
                                <div class="form-group form-group-xs row">
                                    <h5 class="kt-portlet__head-title  kt-font-danger">Support Ticket Messages</h5>
                                </div>
                                <div class="kt-scroll kt-scroll--pull" data-mobile-height="300" id="loadSupportTicketCommentsDiv">

                                </div>
                            </div>
                            <div id="checkStatusForShowForm">
                                {!! Form::open(array('url' => '/api/admin_support_ticket_comments','id'=>'addSupportTicketCommentFormId')) !!}
                                <div class="kt-portlet__foot">
                                    <div class="kt-chat__input">
                                        <div class="kt-chat__editor">
                                            {{Form::textarea("message", null,$attributes = ["style" => "height:50px;","placeholder" => "Type here...","id" => "s_t_message"])}}  
                                            {{Form::hidden("support_ticket_id", $supportid,$attributes = ["id" => "hidden_support_ticket_id"])}}  
                                        </div>
                                        <div class="kt-chat__toolbar">
                                            <div class="kt_chat__tools">                                        
                                            </div>
                                            <div class="kt_chat__actions">
                                                <button type="submit" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">reply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('support_tickets.user.markAsResolvedSupportTicketModal')
@endsection
@push('scripts')
<script>
    var supportTicketId = "<?= $supportid ?>";</script>
<script src="{{ asset('js/support_ticktes/admin_support_tickets_show.js') }}" defer></script>
@endpush