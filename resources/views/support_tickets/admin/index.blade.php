@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/support_tickets/admin/index')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/support_tickets/admin/index')}}" class="kt-subheader__breadcrumbs-link">
                    Help Desk </a>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Help Desk Management
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="dropdown dropdown-inline">
                        <a href="{{ URL::to('export_adminhelpdesk') }}" class="add-margin btn btn-default btn-icon-sm yellow_bg_color black_text_color" >
                            <i class="la la-download black_text_color"></i> Export to Excel
                        </a>                             
                    </div>
                    &nbsp;
                    <div class="kt-portlet__head-actions">                      
                        <a href="#" id="markAsResolvedSTBtn" onclick="markAsResolvedSupportTickets()" class="btn btn-brand btn-elevate btn-icon-sm yellow_bg_color black_text_color">
                            <i class="la la-check"></i>
                            Mark As Resolved
                        </a>   
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="alert alert-custom alert-success fade show col-12" role="alert" id="success-alert" style='display: none;'>                                               
                <div class="alert-text" id="success-msg"></div>
            </div> 
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="support_ticket_management_listing">
                <thead>
                    <tr>
                        <th><label class="kt-checkbox" style="margin-bottom: 10px !important;"><input type="checkbox" class="multiple_checkall" ><span></span></label></th>
                        <th>Ticket Unique ID</th>
                        <th>Created By</th>
                        <th>Subject</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
@include('support_tickets.user.markAsResolvedSupportTicketModal')
@endsection
@push('scripts')
<script src="{{ asset('js/support_ticktes/support_tickets_listing.js') }}" defer></script>
@endpush