@extends('layouts.app')
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/support_tickets')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/support_tickets')}}" class="kt-subheader__breadcrumbs-link">
                    Help Desk </a>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Add Support Ticket
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-sm-12" id='add_support_ticket_status'></div>
            <div class="col-sm-12" id='error_support_ticket_status'></div>
        </div>
        {!! Form::open(array('url' => '/api/support_tickets','id'=>'addSupportTicketsFormId')) !!}
        <div class="card-body">         
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("subject: ", null, ['class' => 'control-label', 'name' => 'subject']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">               
                    {{Form::text("subject", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter Subject",
                                    "id" => "subject", 
                                    "required" => "required",
                                    "tabindex" => "1"
                                 ])}}     

                </div> 
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Type: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">
                    <div class="kt-radio-inline">
                        <label class="kt-radio kt-radio--warning">
                            {{ Form::radio('related_to', 'banduser_name','1') }} Band User Name
                            <span></span>
                        </label>
                        <label class="kt-radio kt-radio--warning">
                            {{ Form::radio('related_to', 'general_account') }} General
                            <span></span>
                        </label>
                    </div>
                </div>

            </div> 

            <div class="form-group row" id="relatedToDiv">
                <div class="col-lg-3">
                    {{ Form::label("Related To:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">
                    {!!Form::select('users_id',
                    isset($data['usersList']) ? $data['usersList'] : [],
                    null,['class' => 'form-control select2', 
                    'id' => 'relatedTo', 'multiple' => 'multiple',"tabindex" => "2"]) !!}
                    <div id="bandUsersListError" class="errorMessageText" style="display: none;">This field is required.</div> 
                </div>
                <div class="col-lg-3">
                    <label class="kt-checkbox">
                        <input type="checkbox"  id="selectAllId">Select All
                        <span></span>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Message: ", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6">                    
                    {{Form::textarea("message", null
                                ,
                                $attributes = [
                                    "class" => "form-control",
                                    "placeholder" => "Enter Message",
                                    "id" => "message", 
                                    "rows"=>"2",
                                    "style"=>"resize:none;",
                                    "tabindex" => "3"
                                 ])}}    
                </div>                            
            </div>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">    
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='addSupportTicketSubmitBtnId' tabindex="4">Submit</button>
                    <a href="{{ url('/support_tickets') }}" class="btn btn-default model-close black_bg_color yellow_text_color" tabindex="5">Cancel</a>
                </div>
            </div>
        </div>

        {{ Form::close() }}
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/support_ticktes/support_tickets_listing.js?'.time()) }}" defer></script>
<script>
$(document).ready(function () {

    var KTSelect2 = function () {
        // Private functions
        var demos = function () {
            // multi select
            $('#relatedTo').select2({
                placeholder: "Please Select",
            });

            $('.kt-selectpicker').selectpicker();
        }

        // Public functions
        return {
            init: function () {
                demos();
            }
        };
    }();
// Initialization
    jQuery(document).ready(function () {
        KTSelect2.init();
    });

    $(document).ready(function () {
        $('#relatedTo').select2();
        $("#selectAllId").click(function () {
            if ($("#selectAllId").is(':checked')) {
                $("#relatedTo > option").prop("selected", "selected");
                $("#relatedTo").trigger("change");
            } else {
                $("#relatedTo > option").prop("selected", false);
                $("#relatedTo").trigger("change");
            }
        });
    });


    bandUserList = '<?php echo count($data); ?>';
    typeVal = $('#addSupportTicketsFormId input[type=radio]').val();

    if (bandUserList == 0 && typeVal == "banduser_name") {
        $("#addSupportTicketSubmitBtnId").attr('disabled', true);
    } else {
        $("#addSupportTicketSubmitBtnId").attr('disabled', false);
    }

    $('#addSupportTicketsFormId input[type=radio]').change(function () {
        if (this.value == "general_account") {
            $("#relatedToDiv").hide();
            $("#addSupportTicketSubmitBtnId").attr('disabled', false);
        } else {
            $("#relatedToDiv").show();
            $("#addSupportTicketSubmitBtnId").attr('disabled', true);
        }
    });
});
</script>
@endpush
