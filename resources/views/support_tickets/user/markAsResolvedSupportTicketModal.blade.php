<div class="modal fade " id="markAsResolvedSupportTicketModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{url('/api/support_tickets/update')}}" id="resolvedSupportTicketFormId" name="resolved_support_ticket">
                <div class="modal-header">
                    <h5 class="modal-title"><span id="statusTitle"></span> Support Ticket</h5>
                    <button type="button" class="close" data-formname="resolved_support_ticket" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    {{Form::hidden('supportTicketId','',['id'=>'supportTicketId'])}}                
                    {{Form::hidden('supportTicketStatus','',['id'=>'supportTicketStatus'])}}                
                    <div class="col-12">Are you sure you want to mark this Support Ticket as <span id="statusMessage"></span>?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="resolved_support_ticket" data-dismiss="modal" >No</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='resolvedSupportTicketSubmitBtnId'>Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
