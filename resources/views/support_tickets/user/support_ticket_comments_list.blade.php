<div class="kt-chat__messages">
    <?php
    if (isset($supportTicketCommentsListArray) && !empty($supportTicketCommentsListArray)) {
        foreach ($supportTicketCommentsListArray as $supportTicketCommentsList) {
            $loggedInID = auth()->user()->id;
            if ($supportTicketCommentsList['message_by'] != $loggedInID) {
                ?>
                <div class="kt-chat__message">
                    <div class="kt-chat__user">
                        <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">
                            {{ substr($supportTicketCommentsList['firstname'], 0, 1) }}
                        </span>
                        <a href="#" class="kt-chat__username">{{$supportTicketCommentsList['firstname'] . " ". $supportTicketCommentsList['lastname']}}</span></a>
                        <span class="kt-chat__datetime">{{date('d-m-Y H:i:s', strtotime($supportTicketCommentsList['created_at']))}}</span>
                    </div>
                    <div class="kt-chat__text kt-bg-light-success">
                        {{$supportTicketCommentsList['message']}}
                    </div>
                </div>
            <?php } else { ?>
                <div class="kt-chat__message kt-chat__message--right">
                    <div class="kt-chat__user">
                        <span class="kt-chat__datetime">{{date('d-m-Y H:i:s', strtotime($supportTicketCommentsList['created_at']))}}</span>
                        <a href="#" class="kt-chat__username">{{$supportTicketCommentsList['firstname'] . " ". $supportTicketCommentsList['lastname']}}</span></a>
                        <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">
                            {{ substr($supportTicketCommentsList['firstname'], 0, 1) }}
                        </span>
                    </div>
                    <div class="kt-chat__text kt-bg-light-brand">
                        {{$supportTicketCommentsList['message']}}
                    </div>
                </div>
                <?php
            }
        }
    }else{ ?>
    <h5>Messages are not found.</h5>
       
   <?php }
    ?>                                        
</div>