@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/support_tickets')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/support_tickets')}}" class="kt-subheader__breadcrumbs-link">
                    Help Desk </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                View Support Ticket 
                @if($data['status'] == 'pending')
                <?php $class = 'kt-badge--unified-danger'; ?>
                @else
                <?php $class = 'kt-badge--unified-success'; ?>             
                @endif
                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline {{$class}}">{{ucfirst($data['status'])}}</span>
            </h3>
        </div>        
    </div>

    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-xl-12">
                <div class="kt-portlet kt-portlet--head-noborder">   
                    <div class="kt-portlet__body kt-portlet__body--fit-top" >
                        <div class="kt-section kt-section--space-sm">
                            <div class="form-group form-group-xs row">
                                <h5 class="kt-portlet__head-title  kt-font-danger"><span>#{{$data['unique_ticket_id']}}</span> : <span>{{ucfirst($data['subject'])}}</span></h5>
                            </div>
                            &nbsp;                                 
                            <div class="form-group form-group-xs row">
                                <div class="col-3">
                                    <span class="col-form-label kt-font-bolder"> Type:</span>
                                </div>
                                <div class="col-3">  
                                    <?php
                                    if ($data['related_to'] == "select_all" || $data['related_to'] == "banduser_name") {
                                        echo 'Band Users';
                                    } else {
                                        echo 'General';
                                    }
                                    ?>
                                </div> 
                            </div>
                            &nbsp;                                 
                            <div class="form-group form-group-xs row">
                                <div class="col-3">
                                    <span class="col-form-label kt-font-bolder"> Related To:</span>
                                </div>
                                <div class="col-6">  
                                    <?php
                                    if ($data['related_to'] == "select_all" || $data['related_to'] == "banduser_name") {
                                        $myArray = array();
                                        foreach ($data['support_ticket_bandusers'] as $key => $val) {
                                            $myArray[] = '<span>' . $val['bandusers']['title'] . " " . $val['bandusers']['firstname'] . " " . $val['bandusers']['lastname'] . '</span>';
                                        }
                                        echo implode(', ', $myArray);
                                    } else {
                                        echo 'General';
                                    }
                                    ?>
                                </div> 
                            </div>
                            &nbsp;
                            <div class="form-group form-group-xs row">
                                <div class="col-3">
                                    <span class="col-form-label kt-font-bolder"> Message:</span>
                                </div>
                                <div class="col-6">  
                                    {{ucfirst($data['message'])}}
                                </div>  
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content">
                <div class="kt-chat">
                    <div class="kt-portlet kt-portlet--head-lg- kt-portlet--last">                        
                        <div class="kt-portlet__body">
                            <div class="form-group form-group-xs row">
                                <h5 class="kt-portlet__head-title  kt-font-danger">Support Ticket Messages</h5>
                            </div>
                            <div class="kt-scroll kt-scroll--pull" data-mobile-height="300" id="loadSupportTicketCommentsDiv">

                            </div>
                        </div>
                        @if($data['status'] == 'pending')
                        {!! Form::open(array('url' => '/api/support_ticket_comments','id'=>'addSupportTicketCommentFormId')) !!}
                        <div class="kt-portlet__foot">
                            <div class="kt-chat__input">
                                <div class="kt-chat__editor">
                                    {{Form::textarea("message", null,$attributes = ["style" => "height:50px;","placeholder" => "Type here...","id" => "s_t_message"])}}  
                                    {{Form::hidden("support_ticket_id", $data['id'],$attributes = ["id" => "hidden_support_ticket_id"])}}  
                                </div>
                                <div class="kt-chat__toolbar">
                                    <div class="kt_chat__tools">                                        
                                    </div>
                                    <div class="kt_chat__actions">
                                        <button type="submit" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">reply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/support_ticktes/add_support_ticket_comments.js?'.time()) }}" defer></script>
<script>
var supportTicketId = "<?= $data['id']; ?>";
$(document).ready(function () {
    $('#addSupportTicketsFormId input[type=radio]').change(function () {
        if (this.value == "general_account") {
            $("#relatedToDiv").hide();
        } else {
            $("#relatedToDiv").show();
        }
    });
});
</script>
@endpush
