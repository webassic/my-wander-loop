@extends('layouts.app')
@section('content')
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/admin-users')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/admin-users')}}" class="kt-subheader__breadcrumbs-link">
                    Admin Users </a>
            </div>
        </div>
    </div>
</div>
<div class="row" id="successMsg"> 
    @if(Session::has('success'))
    <div class="col-sm-12 alert alert-success">
        {{Session::get('success')}}
    </div>
    @endif
</div>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Add Admin User
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-sm-12" id='add_adminuser_status'></div>
        </div>
        {!! Form::open(array('url' => '/api/admin-users','id'=>'adminUserFormId','name' => 'admin_users')) !!}
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("First Name", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('firstname',null,array('required'=>'required','class'=>'form-control','placeholder'=>'Enter First Name', 'tabindex'=>'1','id' => 'firstname'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Last Name", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('lastname',null,array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Last Name', 'tabindex'=>'2','id' => 'lastname'))!!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Email", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('email',null,array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Email', 'tabindex'=>'3','id' => 'email'))!!}
                    <span class="form-text text-muted errorMessage" id="email_error"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Mobile", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    {!!Form::text('mobile',null,array('required'=>'required','class'=>'form-control','placeholder'=>'Enter Mobile', 'tabindex'=>'4','id' => 'mobile',"minlength" => "6","maxlength" => "15"))!!}
                    <span class="form-text text-muted errorMessage" id="mobile_error"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Password", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    <input class="form-control" type="password" placeholder="Password" name="password" id="password">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-3">
                    {{ Form::label("Confirm Password", null, ['class' => 'control-label']) }}<span class="text-danger">*</span> 
                </div>
                <div class="col-lg-6"> 
                    <input class="form-control" type="password" placeholder="Confirm Password" name="confirmed_password" id="confirmed_password">
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
         
                     <a href="{{ url('/admin-users') }}" class="btn btn-default black_bg_color yellow_text_color mr-2" >Cancel</a>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='addAdminUserSubmitBtnId'>Submit</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
@push('scripts') 
<script src="{{ asset('js/users/admin_users.js') }}" defer></script>
<script>
$("#adminUsersCancelledBtnId").click(function(){
    
});
</script>
@endpush