@extends('layouts.app')
@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/users')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                @if(auth()->user()->user_type == 'User')
                <a href="{{ url('/users/view/'.$data['user_id'])}}" class="kt-subheader__breadcrumbs-link">
                    Admin Details </a>
                @else
                <a href="{{ url('/users/view/'.$data['user_id'])}}" class="kt-subheader__breadcrumbs-link">
                    Guardian Details </a>
                @endif
            </div>
            <span class="kt-subheader__separator kt-hidden"></span>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--Begin:: Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-user"></i>
                        </span>
                    </div>
                    <div class="kt-widget__content">
                        <input type="hidden" name="guardiansId" id='guardiansId' value="{{$data['id']}}">
                        <div class="kt-widget__head">
                            <div class="kt-widget__user">
                                <a href="#" class="kt-widget__username">
                                    @if(isset($data['guardian_type']) && !empty($data['guardian_type']) && $data['guardian_type'] == 'Institution')
                                    {{!empty($data['firstname']) && isset($data['firstname']) ? $data['firstname'] .' '.$data['lastname'] : ''}}
                                    @else
                                        @if($data['title']=='null' || $data['title']=='Default' || $data['title']=='Others')
                                        
                                            {{!empty($data['firstname']) && isset($data['firstname']) ? $data['firstname'] .' '.$data['lastname'] : ''}}
                                        @else
                                         {{!empty($data['title']) && isset($data['title']) ? $data['title'].' . ':''}}   {{!empty($data['firstname']) && isset($data['firstname']) ? $data['firstname'] .' '.$data['lastname'] : ''}}
                                        @endif
                                    
                                    @endif
                                </a>
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success">{{isset($data['user_type']) && !empty($data['user_type']) ? $data['user_type'] : ''}}</span>
                            </div>
                            <div class="kt-widget__action">
                                @if(isset($data['status']) && !empty($data['status']) && $data['status'] == 'active')
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @else 
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-danger">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="kt-widget__subhead">
                            <a href="javascript:;"><i class="flaticon2-new-email"></i>{{isset($data['email']) && !empty($data['email']) ? $data['email'] : ''}}</a>
                            <a href="javascript:;"><i class="flaticon-support"></i>{{isset($data['mobile']) && !empty($data['mobile']) ? $data['mobile'] : ''}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End:: Portlet-->
    <div class="row">
        <div class="col-xl-12">
            <!--Begin:: Portlet-->
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#guardians_tab_view" role="tab">
                                    <i class="flaticon2-note"></i> General Information
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#band_users_tab_view" role="tab">
                                    <i class="flaticon2-user-outline-symbol"></i> Band Users
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content kt-margin-t-20">
                        @include('users.guardians_tab_view')

                        @include('users.bandusers_tab_view')

                    </div>
                </div>
            </div>
            <!--End:: Portlet-->
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('js/bandusers/banduser.js') }}" defer></script>
@endpush
