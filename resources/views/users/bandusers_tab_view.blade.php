<div class="tab-pane" id="band_users_tab_view" role="tabpanel">
 <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <a href="{{ URL::to('admin/allbanduser/export').'/'.$data['id'] }}" class="add-margin btn btn-warning btn-icon-sm" >
                                <i class="la la-download"></i> Export to Excel
                            </a>                             
                        </div>
                    </div>
                </div>
            </div>
<br> &nbsp;
    <table class="table table-striped- table-bordered table-hover table-checkable" id="band_users-list">
        <thead>
            <tr>
                <th>Unique ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Relationship With Applicant</th>
                <th>Package Type</th>
                <th>Status</th>
                <th>Created On</th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>
</div>