<div class="modal fade" id="changePasswordUsersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-formname="changepassword_adminuser" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <form class="form" action="{{url('/api/admin-users/update_password')}}" id="changePasswordAdminUserFormId" name="change_password_adminusers">
                <div class="modal-body">
                    <label id="successPasswordMsg" class="successMsg hidden">Password updated successfully</label>
                    <div class="row">             
                        {{ csrf_field()}}
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <span class="col-form-label kt-font-bolder displayMiddle">
                                {{Form::hidden('id','',['id'=>'hiddenAdminUserId'])}}                    
                                {{ Form::label("Old Password:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>                                   
                            </span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            <input id="oldPasswordInputId" name="password" type="password" class="form-control" placeholder="Old Password" minlength="8" maxlength="25">
                            <label id="oldPasswordInputError" class="error errorLabel hidden">This field is required.</label>
                        </div>                         
                    </div>  
                    <div class="row">                                             
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <span class="col-form-label kt-font-bolder displayMiddle">
                                {{ Form::label("New Password:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>                                   
                            </span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            <input id="newPasswordInputId" name="new_password" type="password" class="form-control"  placeholder="New Password" minlength="8" maxlength="25">
                            <label id="newasswordInputError" class="error errorLabel hidden">This field is required.</label>
                        </div> 
                    </div>  
                    <div class="row">                                             
                        <div class="col-lg-5 col-md-5 col-sm-5">
                            <span class="col-form-label kt-font-bolder displayMiddle">
                                {{ Form::label("Repeat New Password:", null, ['class' => 'control-label']) }}<span class="text-danger">*</span>                                   
                            </span>                            
                        </div>                                                          
                        <div class="col-lg-6 col-md-6 col-sm-6 minusMarginLeft40">
                            <input id="repeatNewPasswordInputId" name="repeat_new_password" type="password" class="form-control"  placeholder="Repeat New Password" minlength="8" maxlength="25">
                            <label id="newRepeatPasswordInputError" class="error errorLabel hidden">This field is required.</label>
                        </div> 
                    </div>                                          	
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="delete_promocode" data-dismiss="modal" >No</button>
                    <button type="button" class="btn btn-default yellow_bg_color black_text_color" id='changeAdminUserPasswordSubmitBtnId'  onclick="updateAdminUserPassword('changePasswordAdminUserFormId');">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
