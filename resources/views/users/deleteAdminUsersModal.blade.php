<div class="modal fade" id="deleteAdminUsersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" action="{{url('/api/admin-users')}}" id="deleteAdminUserFormId" name="delete_adminusers">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Admin User</h5>
                    <button type="button" class="close" data-formname="delete_adminuser" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    {{Form::hidden('adminUserId','',['id'=>'adminUserId'])}}                
                    {{Form::hidden('loggedInId',\Auth::user()->id,['id'=>'loggedInId'])}}                
                    <div class="col-12">Are you sure you want to delete this Admin User Details?</div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default model-close black_bg_color yellow_text_color" data-formname="delete_promocode" data-dismiss="modal" >No</button>
                    <button type="submit" class="btn btn-default yellow_bg_color black_text_color" id='deleteAdminUserSubmitBtnId'>Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
