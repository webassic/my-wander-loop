
<div class="tab-pane active" id="guardians_tab_view" role="tabpanel">
    <!--<div class="kt-notes kt-scroll kt-scroll--pull" data-scroll="true">-->
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title  kt-font-danger">
                            Personal Details
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    @if(isset($data['guardian_type']) && !empty($data['guardian_type']) && $data['guardian_type'] == 'Individual')
                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Registration Number:</span>
                            </div>
                            <div class="col-3">
                               {{isset($data['reg_no']) && !empty($data['reg_no']) ? $data['reg_no'] : '---'}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Guardian Type:</span>
                            </div>
                            <div class="col-3">
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-danger">{{isset($data['guardian_type']) && !empty($data['guardian_type']) ? ucfirst($data['guardian_type']) : ''}}</span>
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Date Of Birth:</span>
                            </div>
                            <div class="col-3">
                                {{isset($data['dob']) && !empty($data['dob']) ? date('d-M-Y', strtotime($data['dob'])) : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Landline Number:</span>
                            </div>
                            <div class="col-3">
                                {{isset($data['landline']) && !empty($data['landline']) ? $data['landline'] : ''}}
                            </div>

                        </div>
                        
                        &nbsp;
                        
                    </div>
                    @else

                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Registration Number:</span>
                            </div>
                            <div class="col-3">
                                {{isset($data['reg_no']) && !empty($data['reg_no']) ? $data['reg_no'] : '---'}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Guardian Type:</span>
                            </div>
                            <div class="col-3">
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-danger">{{isset($data['guardian_type']) && !empty($data['guardian_type']) ? ucfirst($data['guardian_type']) : ''}}</span>
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Landline Number:</span>
                            </div>
                            <div class="col-3">
                                {{isset($data['landline']) && !empty($data['landline']) ? $data['landline'] : ''}}
                            </div>

                        </div>
                        &nbsp;
                        
                    </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title  kt-font-danger">
                            Address Details
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Address:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['address']) && isset($data['address']) ? $data['address'] : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Locality:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['locality']) && isset($data['locality']) ? $data['locality'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">City:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['city']) && isset($data['city']) ? $data['city'] : ''}}
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">State:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['state']) && isset($data['state']) ? $data['state'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Pin Code:</span>
                            </div>
                            <div class="col-3">
                                {{!empty($data['pin']) && isset($data['pin']) ? $data['pin'] : ''}}
                            </div>
                        </div>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <!--</div>-->
    </div>
</div>