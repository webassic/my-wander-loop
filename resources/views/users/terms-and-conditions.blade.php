<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- begin::Head -->
    <head>
        <!-- include head section -->
        @include('layouts.head')
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
        <input type='hidden' id='baseUrl' value='{{url('/')}}'>
        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <a href="index.html">
                    <img alt="Logo" src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="150px"/>

                </a>
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
                <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>
        <!-- end:: Header Mobile -->

        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                    <!-- begin:: Aside -->
                    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                        <div class="kt-aside__brand-logo">
                            <a href="{{ url('/home') }}" >
                                <img alt="Logo" src="{{url('/assets/media/company-logos/myWanderLoop.png')}}" width="200px"/>
                                <!--My Wander Loop-->
                            </a>
                        </div>                         
                    </div>
                </div>
                <!-- begin:: Aside -->
                <!-- end:: Aside -->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">                    
                    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                        <div class="kt-container  kt-container--fluid ">
                            <div class="kt-subheader__main">
                                <a href="{{ url('/login')}}" class="kt-subheader__breadcrumbs-link">
                                    <h3 class="kt-subheader__title">Home </h3>
                                </a>
                                <span class="kt-subheader__separator kt-hidden"></span>
                                <div class="kt-subheader__breadcrumbs">
                                    <a href="{{ url('/register')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="{{ url('/register')}}" class="kt-subheader__breadcrumbs-link">
                                        Sign Up </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end:: Header -->
                    <!-- begin:: Content Head -->
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">                                        
                                <h3 class="kt-portlet__head-title">
                                    Welcome to our Terms and Conditions
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    Your privacy is critically important to us and our terms and conditions respect your privacy.
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    MyWanderLoop is an initiative by Sare Jahan Se Acha Socia Eco Pol Foundation Pvt. Ltd. 
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    If you've interacted with us by submitting any of the contact us / Sign up forms and have supplied your email address / Phone, MyWanderLoop may occasionally send you an email, facsimile, phone or text messages to tell you about new features, solicit your feedback, or just keep you up to date with what's going on with MyWanderLoop.
                                    If you have signed up for our services, then we may send you Transactional SMS too. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. MyWanderLoop takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('layouts.footer')
                </div>
            </div>
        </div>

        <!--end::Page Scripts -->
        @include('layouts.footer_script_files')
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>

        @stack('scripts')
    </body>
</html>