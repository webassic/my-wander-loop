@extends('layouts.app')
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <a href="{{ url('/home')}}" class="kt-subheader__breadcrumbs-link">
                <h3 class="kt-subheader__title">
                    Home </h3>
            </a>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ url('/guardians')}}" class="kt-subheader__breadcrumbs-link">
                    Profile </a>
            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--Begin:: Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-user"></i>
                        </span>
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <div class="kt-widget__user">
                                <a href="#" class="kt-widget__username" id="firstLastname">
                                     
                                </a>
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-success" id="user_type"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet kt-portlet--head-noborder">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title  kt-font-danger">
                            General Information
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-top">
                    <div class="kt-section kt-section--space-sm">
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Email:</span>
                            </div>
                            <div class="col-3" id="email">
                               email value
                            </div>
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Mobile:</span>
                            </div>
                            <div class="col-3" id="mobile">
                                mobile value
                            </div>
                        </div>
                        &nbsp;
                        <div class="form-group form-group-xs row">
                            <div class="col-3">
                                <span class="col-form-label kt-font-bolder">Status:</span>
                            </div>
                            <div class="col-3">
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline " id="status"></span>
<!--                                @if(isset($data['status']) && !empty($data['status']) && $data['status'] == 'active')
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @else 
                                <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-danger">{{isset($data['status']) && !empty($data['status']) ? ucfirst($data['status']) : ''}}</span>
                                @endif-->
                            </div>
                        </div>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <!--</div>-->
    </div>
</div>
@endsection
@push('scripts')
<script>
    jQuery(document).ready(function () {
    var userId = "<?=auth()->user()->id?>";
     $.ajax({
            type: "GET",
            url: '/api/user/profile/view/'+ userId,
            dataType: "json",
            ContentType :'application/json',
            success: function (response) {
               console.log(response);
              $.each(response, function( key, value ) {
                    if(key=='data'){
                        $("#firstLastname").text(value.firstname +' '+value.lastname);
                        $("#email").text(value.email);
                        $("#mobile").text(value.mobile);
                        $("#user_type").text(value.user_type);
                        if(value.status == 'active'){
                            $("#status").text('Active').addClass('kt-badge--unified-primary');
                        }else if(value.status == 'inactive'){
                            $("#status").text('Inactive').addClass('kt-badge--unified-danger');
                        }
                        
                    }

                });
            },
            error: function (error) {               
                console.log(error);
            }
        });

});
  
</script>
@endpush