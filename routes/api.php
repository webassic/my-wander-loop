<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('postregister', 'UserController@register');
/**
 * Scan event Route (without authentication)
 */
Route::get('/banduser_exist/{uniqueid}', 'API\BanduserController@banduserExist');
Route::get('/banduser_data/{uniqueid}', 'API\BanduserController@banduserDetails');
Route::get('/generateOtp', 'UserController@getOtp');
Route::get('/scanevent_getotp/{mobile}', 'API\ScanEventController@scanEventGetOtp');
Route::post('/capture_user_info', 'API\ScanEventController@store');
Route::put('/update_scan_event/{id}', 'API\ScanEventController@update');

// Common Routes - Password Update 
Route::group(['middleware' => ['auth:api']], function() {
    // Password Update 
    Route::post('/password/update', 'API\ChangePasswordController@update');

    Route::get('test', 'UserController@test');
    //Event Logs
    Route::get('/eventlog', 'API\ScanEventController@storeEventLog');
    Route::get('/support-eventlog', 'API\SupportTicketsController@getEventLog');
});

// Guardian/Customer Module 
Route::group(['middleware' => ['auth:api', 'user']], function() {
    // Guardian 
    Route::apiresource('guardians', 'API\GuardiansController');
    Route::post('/guardians/guardian_type/{guardian_id}', 'API\GuardiansController@storeGuardianType');
    Route::put('/guardians/institution/{guardian_id}', 'API\GuardiansController@storeInstitution');
    Route::get('/unqiue_mobile_check/{mobile_no}', 'UserController@unqiueMobileCheck');
    Route::get('/dobvaliidate/{month}/{year}', 'API\GuardiansController@getdays');
    Route::put('/guardians/edit_individual/{id}', 'API\GuardiansController@updateIndividualData');
    Route::put('/guardians/edit_institution/{id}', 'API\GuardiansController@updateInstitutionData');

    //bandusers
    Route::apiResource('bandusers', 'API\BanduserController');
    Route::get('bandusers_datatable', 'API\BanduserController@datatableListings');
    Route::post('/bandusers', 'API\BanduserController@store');
    Route::get('confirm_bandusers_datatable', 'API\BanduserController@confirmBandUsersDatatableListings');
    Route::get('/banduser_getotp/{mobile}/{id}', 'API\BanduserController@bandUserGetOtp');
    Route::delete('/bandusers/{id}', 'API\BanduserController@destroy');
    Route::post('/bandusers/update', 'API\BanduserController@update');

    Route::apiResource('institutions', 'API\InstitutionController');
    //user profile details
    Route::get('/user/profile/view/{id}', 'UserController@userProfileDetails');
    Route::get('/locations', 'API\GuardiansController@getLocation');
    Route::get('/scanevents/listings', 'API\ScanEventController@listings');
    Route::get('/scanevents/{id}', 'API\ScanEventController@show');

    //User Support Tickets Module 
    Route::get('/support_tickets/listings', 'API\SupportTicketsController@listings');
    Route::post('/support_tickets', 'API\SupportTicketsController@store');

    // User Support Ticket Comments Module 
    Route::post('/support_ticket_comments', 'API\SupportTicketCommentsController@store');
    Route::get('/get_support_ticket_comments_list/{id}', 'API\SupportTicketCommentsController@getSupportTicketCommentsList');

//    Route::post('/check_valid_promo_code', 'PromoCodesController@checkValidPromoCode');
    //Razorpay routes
    Route::post('/create_subscription', 'API\BanduserController@createSubscription');
    Route::post('/save_transaction_details', 'API\BanduserController@saveTransactionDetails');
    
    //New Band orders routes
    Route::post('/buy_new_band', 'API\NewBandOrdersController@storeNewBandOrder');
    Route::put('/band_orders/razorpay_callback', 'API\NewBandOrdersController@updateTransactionDetails');
    Route::get('/user-new-band-orders/listings', 'API\NewBandOrdersController@userNewBandOrdersListings');
    Route::get('/user-delivered-orders/listings', 'API\NewBandOrdersController@userDeliveredListings');
    
    // Transaction
    Route::get('transaction_datatable', 'API\TransactionsController@bandUserTransactionListing');    
    
    Route::post('/get_package_details', 'API\BanduserController@getPackageDetails');
    Route::get('/get_package_type_detail_count', 'API\BanduserController@getPackageTypeDetailCount');
});

// Admin Module 
Route::group(['middleware' => ['auth:api', 'administration']], function() {
    // Admin - Ailings
    Route::get('/ailings/listings', 'API\AilingController@listings');
    Route::get('/ailings/{id}', 'API\AilingController@show');
    Route::post('/ailings', 'API\AilingController@store');
    Route::put('/ailings/{id}', 'API\AilingController@update');
    Route::delete('/ailings/{id}', 'API\AilingController@destroy');

    // Admin - Band Types
    Route::get('/bandtypes/listings', 'API\BandTypeController@listings');
    Route::delete('/bandtypes/{id}', 'API\BandTypeController@destroy');
    Route::put('/bandtypes/{id}', 'API\BandTypeController@update');
    Route::get('/bandtypes/{id}', 'API\BandTypeController@show');
    Route::post('/bandtypes', 'API\BandTypeController@store');

    // Admin - Users Module 
    Route::get('/admin/users/listings', 'UserController@listings');
    //leads
    Route::get('/users/listings', 'UserController@payingCustomerListings');
    //paying customers --> active
    Route::get('/customers/listings', 'UserController@customerListings');
    //paying customers --> active
    Route::get('/inactive_customers/listings', 'UserController@inactiveCustomerListings');

    Route::apiResource('users', 'UserController');
    Route::put('users/{id}/{action}', 'UserController@updateStatus');
    Route::put('usersleads/{id}/{action}', 'UserController@updateStatusForLeads');
    Route::post('/admin-users', 'UserController@store');
    Route::post('/admin-users/edit/{id}', 'UserController@update');
    Route::delete('/admin-users/{id}', 'UserController@destroy');
    Route::post('/admin-users/update_password', 'UserController@updatePassword');

    // Admin - Promo Codes Module 
    Route::get('/promocodes/listings', 'PromoCodesController@listings');
    Route::get('/promocodes/{id}', 'PromoCodesController@show');
    Route::post('/promocodes', 'PromoCodesController@store');
    Route::post('/promocodes/update', 'PromoCodesController@update');
    Route::delete('/promocodes/{id}', 'PromoCodesController@destroy');

    // Admin - Band Users Listings 
    Route::get('/bandusers/listings/{guardiansId}', 'API\BanduserController@bandUsersListingsBasedOnGuardiansId');
    Route::get('/all/bandusers/listings', 'API\BanduserController@bandUsersListingsForApproval');
    Route::put('/changeBandUserStatus/{id}', 'API\BanduserController@approveBandUserByAdmin');
    Route::put('/inactiveBandUserStatus/{id}', 'API\BanduserController@inactiveBandUserByAdmin');
    Route::put('/deleteAadhaarCardNumber/{id}', 'API\BanduserController@deleteAadhaarCardNumber');
    Route::get('/active/bandusers/listings', 'API\BanduserController@activeBandUsersListings');
    Route::get('/inactive/bandusers/listings', 'API\BanduserController@inactiveBandUsersListings');
    Route::put('/verifyAadhharDetails/{id}', 'API\BanduserController@verifyAadhharDetailsByAdmin');

    //Admin Scan Events
    Route::get('/admin-scanevents', 'API\ScanEventController@adminList');
    Route::get('/admin-scanevents/{id}', 'API\ScanEventController@show');

    //Admin Support Ticket
    Route::get('/admin-supporttickets', 'API\SupportTicketsController@adminList');
    Route::get('/admin-supporttickets/{id}', 'API\SupportTicketsController@adminshow');
    Route::post('/support_tickets/update/{id}', 'API\SupportTicketsController@updateStatus');

    // Admin Support Ticket Comments Module 
    Route::post('/admin_support_ticket_comments', 'API\SupportTicketCommentsController@store');
    Route::get('/get_admin_support_ticket_comments_list/{id}', 'API\SupportTicketCommentsController@getSupportTicketCommentsList');

    // Admin - Razorpay Credentials 
    Route::post('/razorpay_credentials/update/{id}', 'API\RazorpayCredentialsController@update');
    
    //Admin New Band Orders Module
    Route::get('/admin-new-band-orders/listings', 'API\NewBandOrdersController@adminNewBandOrdersListings');
    Route::get('/admin-delivered-orders/listings', 'API\NewBandOrdersController@adminDeliveredListings');
    Route::put('/new_band_orders/updateStatus', 'API\NewBandOrdersController@updateStatus');
   
    // Transaction
    Route::get('admin_transaction_datatable', 'API\TransactionsController@bandUserTransactionListing');    
});
