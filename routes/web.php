<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//Route::get('/dashboard', function () { //  After dashboard done please remove redirection
//    return redirect('/guardians');
//});

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
}); 

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/', function () {
    return redirect('/dashboard');
});
Route::get('/verify/{verification_code}', 'UserController@activateUserAccount');
Route::get('/scan', function () {
    return view('scanevents.scan');
});
Route::get('/terms-and-conditions', function () {
    return view('users.terms-and-conditions');
});

Auth::routes();

    Route::get('/facebook_login', function () {
        return view('facebookLogin');
    });
    Route::get('auth/facebook', 'Auth\FaceBookController@redirectToFacebook');
    Route::get('auth/facebook/callback', 'Auth\FaceBookController@handleFacebookCallback');
    
Route::group(['middleware' => 'auth'], function() {
    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::get('/home', function() {
        return redirect('/dashboard');
    });
    //protect images from non logged in users
    Route::get('getimage/{name}/{banduserid}/{file}', 'API\BanduserController@serveImage');   
});

/* Authorization Error pages */
Route::get('/authorization-error', 'UserController@authorizationErrorIndex');

// Guardian Customer Module 
Route::group(['middleware' => ['auth', 'user']], function() {
    // Guardians/Users Module
    Route::get('/guardians', 'API\GuardiansController@index');

    // This is sample guardian add view 
    Route::get('/guardians/add', 'API\GuardiansController@add');

    // My profile view 
    Route::get('/user/profile', 'UserController@userProfile');

    Route::get('/institution', 'API\InstitutionController@index');
    Route::get('/bandusers', 'API\BanduserController@index');
    Route::get('/users/export_bandusers', 'API\BanduserController@exportUserBanduser');
    Route::get('/bandusers/add/{bandUserCnt}', 'API\BanduserController@add');
    Route::get('/bandusers/edit/{id}', 'API\BanduserController@edit');
    Route::get('/bandusers/view/{id}', 'API\BanduserController@show');
    // Scan Events Module 
    Route::get('/scan-events', 'API\ScanEventController@index');

    //User Support Tickets Module 
    Route::get('/support_tickets', 'API\SupportTicketsController@index');
    Route::get('/support_tickets/add', 'API\SupportTicketsController@add');
    Route::get('/support_tickets/{id}', 'API\SupportTicketsController@show');
    
    // Transaction
    Route::get('/transactions/index', 'API\TransactionsController@bandUserTransactions');
    Route::get('/transactions/cancel_subscription/{id}', 'API\TransactionsController@cancelSubscription');
    Route::get('/transactions/download_invoice/{id}/{payment_id}', 'API\TransactionsController@downloadInvoice');
    Route::get('/transactions/export_banduser_transactions', 'API\TransactionsController@exportBanduserTransactions');

    //User New Band Orders Module
    Route::get('/users_new_band_orders', 'API\NewBandOrdersController@userIndex');
    Route::get('/users_delivered_orders', 'API\NewBandOrdersController@userDeliveredIndex');
    Route::get('/export_user_bandorders', 'API\NewBandOrdersController@exportUserBandOrders');
    Route::get('/export_user_delivered_orders', 'API\NewBandOrdersController@exportUserDeliveredOrders');
    Route::get('/user_bandorders/download_invoice/{id}', 'API\NewBandOrdersController@downloadInvoice');
});

// Admin Module 
Route::group(['middleware' => ['auth', 'administration']], function() {
    // My profile view 
    Route::get('/admin-users/profile/view/{id}', 'UserController@adminProfileDetails');

    // Users Module
    Route::get('/admin-users', 'UserController@index');
    Route::get('/export_adminusers', 'UserController@adminusersExport');
    Route::get('/admin-users/add', 'UserController@add');
    Route::get('/admin-users/edit/{id}', 'UserController@edit');
    //want to change url users to leads
    Route::get('/users', function(){
        return redirect('/leads');
    });
    Route::get('/leads', 'UserController@payingCustomerIndex');
    Route::get('/exportleads', 'UserController@exportLeads');
    Route::get('/exportcustomers', 'UserController@exportActiveCustomer');
    Route::get('/export_inactivecustomers', 'UserController@exportInactiveCustomer');
    Route::get('/users/view/{id}', 'UserController@show');
    Route::get('/customers','UserController@customerIndex');
    Route::get('/inactive-customers','UserController@inactiveCustomerIndex');

    // Admin - Promo Codes Module 
    Route::get('/promo-codes', 'PromoCodesController@index');
    Route::get('/promo-codes/add', 'PromoCodesController@add');
    Route::get('/promo-codes/edit/{id}', 'PromoCodesController@edit');

    // Band Users View 
    Route::get('/admin/band-users/view/{id}', 'API\BanduserController@show');
    Route::get('/admin/band-users', 'API\BanduserController@bandUsersApproval');
    Route::get('/active/admin-bandusers/', 'API\BanduserController@activeBandUsers');
    Route::get('/active/admin-bandusers/export', 'API\BanduserController@activeBandUsersExport');
    Route::get('/inactive/admin-bandusers', 'API\BanduserController@inactiveBandUsers');
    Route::get('/inactive/admin-bandusers/export', 'API\BanduserController@inactiveBandUsersExport');
    Route::get('/admin/download-band-users/{id}', 'API\BanduserController@downloadBandUserDetails');
    Route::get('/admin/allbanduser/export/{id}', 'API\BanduserController@bandusersExport');
    Route::get('/admin/download-band-users-address/{id}', 'API\BanduserController@downloadBandUserAddressDetails');

    // Admin - Band Type Module 
    Route::get('/band-types', 'API\BandTypeController@index');

    // Admin - Ailings Module 
    Route::get('/ailings', 'API\AilingController@index');

    //Admin Scan Events Module 
    Route::get('/scanevents/index', 'API\ScanEventController@adminIndex');
    Route::get('/export_adminscanevent', 'API\ScanEventController@exportAdminScanEvent');

    //Admin Support Tickets Module 
    Route::get('/support_tickets/admin/index', 'API\SupportTicketsController@adminIndex');
    Route::get('/support_tickets/show/{id}', 'API\SupportTicketsController@adminViewPage');
    Route::get('/export_adminhelpdesk', 'API\SupportTicketsController@exportAdminHelpDesk');
    
    // Admin - RazorpayCredentials Module 
    Route::get('/razorpay_credentials/{id}', 'API\RazorpayCredentialsController@show');
    Route::get('/razorpay_credentials/edit/{id}', 'API\RazorpayCredentialsController@edit');
    
    //Admin New Band Orders Module
    Route::get('/new_band_orders', 'API\NewBandOrdersController@adminIndex');
    Route::get('/delivered_orders', 'API\NewBandOrdersController@adminDeliveredIndex');
    Route::get('/export_admin_bandorders', 'API\NewBandOrdersController@exportAdminBandOrders');
    Route::get('/export_admin_deliveredorders', 'API\NewBandOrdersController@exportAdminDeliveredOrders');    
    Route::get('/bandorders/download_invoice/{id}', 'API\NewBandOrdersController@downloadInvoice');
    
    // Transaction
    Route::get('/admin-transactions/index', 'API\TransactionsController@adminBandUserTransactions');
    Route::get('/admin-transactions/download_invoice/{id}/{payment_id}', 'API\TransactionsController@downloadInvoice');
    Route::get('/admin-transactions/export_banduser_transactions', 'API\TransactionsController@exportAdminBanduserTransactions');


});
